# ! /usr/bin/env python
#coding=utf-8
# -*- coding: utf-8 -*-
#date 14-5-13
import cv

def main():
    img = cv.LoadImage('code.jpeg')
    canny = cv.CreateImage(cv.GetSize(img), cv.IPL_DEPTH_8U, 1)
    re = cv.Canny(img, canny, 50, 150, 3)
    re = cv.SaveImage('code_canny.jpeg', canny)
    print re


if __name__ == '__main__':
    main()
