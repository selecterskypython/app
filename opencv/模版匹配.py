# ! /usr/bin/env python
#coding=utf-8
# -*- coding: utf-8 -*-
#date 14-5-14
import cv

def main():
    img = cv.LoadImage('code2.jpeg')
    tpl = cv.LoadImage('r.png')
    w = img.width - tpl.width + 1
    h = img.height - tpl.height + 1
    res = cv.CreateImage((w, h), cv.IPL_DEPTH_32F, 1)
    cv.MatchTemplate(img, tpl, res, cv.CV_TM_CCOEFF_NORMED)
    #minval=maxval=minloc=maxloc=0
    minval,maxval,minloc,maxval=cv.MinMaxLoc(res)
    cv.Rectangle(img, (minloc[0], minloc[1]), (minloc[0] + tpl.width, minloc[1] + tpl.height), (0, 0, 255), 1, 0, 0)
    cv.SaveImage('code_match.jpeg', img)
    print 'x:%d,y:%d' % (minloc[0], minloc[1])

if __name__ == '__main__':
    main()
