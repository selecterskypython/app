# ! /usr/bin/env python
#coding=utf-8
# -*- coding: utf-8 -*-
#date 14-5-13
import cv2

def main():
    img = cv2.imread('code.jpeg')
    edge = cv2.Canny(img, 100, 200)
    cv2.imwrite('code_.jpeg',edge)
    print edge


if __name__ == '__main__':
    main()
