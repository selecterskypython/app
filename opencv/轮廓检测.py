# ! /usr/bin/env python
#coding=utf-8
# -*- coding: utf-8 -*-
#date 14-5-13
import cv

def main():
    storage = cv.CreateMemStorage(0)
    img = cv.LoadImage('code.jpeg', 0)
    contour = cv.FindContours(img, storage, cv.CV_RETR_CCOMP, cv.CV_CHAIN_APPROX_SIMPLE)
    pContourImg = cv.CreateImage(cv.GetSize(img),cv.IPL_DEPTH_8U,3)
    cv.CvtColor(img, pContourImg, cv.CV_GRAY2BGR)

    cv.DrawContours(pContourImg, contour, cv.CV_RGB(255, 0, 0), cv.CV_RGB(0, 0, 255), 2, 2, 8)
    cv.SaveImage('code_.jpeg', pContourImg)


if __name__ == '__main__':
    main()
