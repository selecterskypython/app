# ! /usr/bin/env python
#coding=utf-8
# -*- coding: utf-8 -*-
#date 14-5-14
import cv

def main():
    img = cv.LoadImage('e.png',0)
    tpl = cv.LoadImage('n.png',0)


    basehsv = cv.CreateHist([0], cv.CV_HIST_ARRAY, [0,256],1)
    test1hsv = cv.CreateHist([0], cv.CV_HIST_ARRAY, [0,256],1)

    cv.CalcHist([img],[0],None,[256],[0,255])
    cv.CalcHist([img], test1hsv)

    cv.NormalizeHist(basehsv, 1)
    cv.NormalizeHist(test1hsv, 1)

    v = cv.CompareHist(basehsv,test1hsv,cv.CV_COMP_CORREL)
    print v


if __name__ == '__main__':
    main()
