#! /usr/bin/env python

#a=os.popen(cmd).read()
#commands.getoutput(cmd)
#commands.getstatus(file) 

import sys,os
premissions = {
'gps':[
	'ACCESS_COARSE_LOCATION',
	'ACCESS_LOCATION_EXTRA_COMMANDS',
	'ACCESS_MOCK_LOCATION',
	'INSTALL_LOCATION_PROVIDER'
	],
'sms':[
	'BROADCAST_SMS',
	'CALL_PHONE',
	'CALL_PRIVILEGED',
	'PROCESS_OUTGOING_CALLS',
	'READ_CALL_LOG',
	'REBOOT',
	'RECEIVE_BOOT_COMPLETED',
	'RECEIVE_MMS',
	'RECEIVE_SMS',
	'RECEIVE_WAP_PUSH',
	'ADD_VOICEMAIL',
	'SEND_SMS',
	'WAKE_LOCK', 
	'WRITE_SMS'
	],
'media':[
	'CAMERA',
	'CAPTURE_AUDIO_OUTPUT',
	'CAPTURE_SECURE_VIDEO_OUTPUT',
	'CAPTURE_VIDEO_OUTPUT',
	'RECORD_AUDIO'
	],
'network':[
	'ACCESS_NETWORK_STATE',
	'ACCESS_WIFI_STATE',
	'BLUETOOTH',
	'BLUETOOTH_ADMIN',
	'BLUETOOTH_PRIVILEGED',
	'BROADCAST_WAP_PUSH',
	'CHANGE_NETWORK_STATE',
	'CHANGE_WIFI_MULTICAST_STATE',
	'CHANGE_WIFI_STATE',
	'INTERNET',
	'USE_SIP'
	]
}
prefix = "android.permission."

def execFliter(argvs,filename):
	if not os.path.exists(filename):
		print "filename:",filename," not exists"
		return;
	try:
		fp = open(filename,"rw")
	except ValueError:
		print "filename:",filename," not writeable"
		fp.close()
		return;
	body = fp.read() 
	print "body:",body
	isOper = False
	for k in argvs:
		if premissions.has_key(k):
			value = premissions[k]
			for vv in value:
				str = prefix + vv
				body.replace(str, "")
				isOper = True
	if isOper:
		fp.write(body)
	fp.close()
	print "body:",body

def main():
	argv = sys.argv
	if len(argv) < 2:
		print "pls enter file name,eg:__FILE__ abc.apk"
		exit(1)
	filename = argv[1]
	if len(argv) > 2:		
		execFliter(argv[1:],filename)

main()