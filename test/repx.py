#! /usr/bin/env python
#coding=utf-8
# -*- coding: utf-8 -*-
#date 14-3-13
import re

def main():
    str ='    <li><a href="http://www.jb51.net"><span>首页</span></a></li> \
<li><a href="http://www.jb51.net/web/" title="网页制作"><span>网页制作</span></a></li> \
<li><a href="http://www.jb51.net/list/index_96.htm" title="脚本专栏"><span>脚本专栏</span></a></li> \
<li><a href="http://www.jb51.net/list/index_1.htm" title="网络编程"><span>网络编程</span></a></li> \
<li><a href="http://www.jb51.net/list/index_104.htm" title="数据库"><span>数据库</span></a></li> \
<li><a href="http://www.jb51.net/jiaoben/" title="脚本下载"><span>脚本下载</span></a></li> \
<li><a href="http://www.jb51.net/cms/" title="cms使用教程"><span>CMS教程</span></a></li> \
<li><a href="http://www.jb51.net/books/" title="电子书籍"><span>电子书籍</span></a></li> \
<li><a href="http://www.jb51.net/pingmian/" title="平面设计"><span>平面设计</span></a></li> \
<li><a href="http://www.jb51.net/media/" title="媒体动画"><span>媒体动画</span></a></li> \
<li><a href="http://www.jb51.net/os/" title="操作系统"><span>操作系统</span></a></li> \
<li><a href="http://www.jb51.net/yunying/" title="网站运营"><span>网站运营</span></a></li> \
<li><a href="http://www.jb51.net/hack/" title="网络安全"><span>网络安全</span></a></li>'
    #只匹配内容用(?)表示 ?表示贪蓝模式
    rex = re.compile("<li><a href=\"(.*?)\"")
    result = rex.findall(str)
    if result != None:
        print result

    #匹配所有，会显示正则的内容
    tit = re.compile("<span>.*?</span>")
    result = tit.findall(str)
    for x in result:
        print x

    #匹配一个内容
    one = re.compile("title=\"(.*?)\"")
    result = one.search(str)
    if result!=None:
        print("result",result.group())


if __name__ == '__main__':
    main()
