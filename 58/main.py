#! /usr/bin/env python
#coding=utf-8
# -*- coding: utf-8 -*-

"""
功能：使用当前目录下的db.xls文件记录数据，自动登录www.yiwugou.com网站后台，发布产品数据

执行完成后生成db_result.xls文件，记录操作结果

"""

import string,thread,time
import sys

import json
import os
from Cjx.http import httpCient
from optparse import OptionParser
import cookielib
__author__ = 'appie'

isExit = False
http = None


def main():

    global http,option    
    http = httpCient(True)

    domain = '58.com'

    cookie = cookielib.Cookie(0, 'isfirst', 'true', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)

    cookie = cookielib.Cookie(0, 'cookieuid1', '05dvUVOk3HAxiF/EDZ3FAg==', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)

    cookie = cookielib.Cookie(0, 'showcountdown', 'true', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)


    cookie = cookielib.Cookie(0, '_TCN', 'C4E61ED0AFABBBB66D915DE5AD94645E', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)

    cookie = cookielib.Cookie(0, 'operate', 'nofilter', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)

    cookie = cookielib.Cookie(0, 'id58', '"05dz8VOwoTdNBXV8C0QNAg=="', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)

    cookie = cookielib.Cookie(0, 'www58com', '"AutoLogin=false&UserID=26118556&UserName=selectersky&CityID=0&Email=&AllMsgTotal=0&CommentReadTotal=0&CommentUnReadTotal=0&MsgReadTotal=0&MsgUnReadTotal=0&RequireFriendReadTotal=0&RequireFriendUnReadTotal=0&SystemReadTotal=0&SystemUnReadTotal=0&UserCredit=0&UserScore=0&PurviewID=&IsAgency=false&Agencys=null&SiteKey=2A31A22D9706340595FC6E8F6DD0DB15052A0657EA27860B0&Phone=&WltUrl=&UserLoginVer=4E2D7FDC8FC545758763CE1E8F414D1CE&LT=1404084541614"', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)

    cookie = cookielib.Cookie(0, '58cooper', '"userid=26118556&username=selectersky&cooperkey=3f35337885089d3c6e0479298454d8dd"', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)

    cookie = cookielib.Cookie(0, '58passport', '"14040845411548924313622024&7A8AA9BF524FE0547A4BAEE92E17DCD64&f37e36"', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)

    cookie = cookielib.Cookie(0, 'm', '"98%3Aff%3Ad0%3A89%3A00%3A7a"', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)


    cookie = cookielib.Cookie(0, 'r', '"540_960"', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)


    cookie = cookielib.Cookie(0, 'brand', '"Lenovo"', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)

    cookie = cookielib.Cookie(0, 'owner', '"baidu"', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)


    cookie = cookielib.Cookie(0, 'uid', '26118556', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)


    cookie = cookielib.Cookie(0, 'platform', 'android', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)

    cookie = cookielib.Cookie(0, 'os', 'android', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)

    cookie = cookielib.Cookie(0, 'lon', '25.228549', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)


    cookie = cookielib.Cookie(0, 'locationstate', '0', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)

    cookie = cookielib.Cookie(0, 'location', '79,85,2999', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)


    cookie = cookielib.Cookie(0, 'osv', '4.1.2', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)


    cookie = cookielib.Cookie(0, 'maptype', '2', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)

    cookie = cookielib.Cookie(0, 'cid', '79', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)


    cookie = cookielib.Cookie(0, 'PPU', 'UID=26118556&PPK=499276b2&PPT=b490328c&SK=4E2D7FDC8FC5457587B7CCE3941808D10943BAB0D8A7328E4&LT=1404084541624&UN=selectersky&LV=e5d60885', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)

    cookie = cookielib.Cookie(0, 'channelid', '446', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)

    cookie = cookielib.Cookie(0, 'productorid', '1', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)


    cookie = cookielib.Cookie(0, 'ua', 'Lenovo+A820t', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)


    cookie = cookielib.Cookie(0, 'X-Wap-Proxy-Cookie', 'none', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)

    cookie = cookielib.Cookie(0, 'uuid', '8b9aa8a8-e00e-4746-ad02-60137b6dc88e', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)

    cookie = cookielib.Cookie(0, 'apn', 'WIFI', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)

    cookie = cookielib.Cookie(0, 'lat', '36.186296', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)

    cookie = cookielib.Cookie(0, 'cimei', '862326025244100', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)


    cookie = cookielib.Cookie(0, 'cversion', '5.5.0.1', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)


    cookie = cookielib.Cookie(0, 'source', '"ordinarycate"', None,False, domain, False, False, '/', True, False, None, True, None, None, {}, rfc2109=False)
    http.setCookie(cookie)
    last = 1404104388+60*50
    i = 0
    while(True):

        if time.time()>last or i>10:
            i = 0
            content = http.urlcontent('http://jiang.webapp.58.com/placeorder/?jptype=2')
            print content
        else:
            i += 1
            time.sleep(1)

if __name__ == '__main__':
    main()

