#! /usr/bin/env python
#coding=utf-8
# -*- coding: utf-8 -*-

"""
功能：自动查号 自动预约 输入参数 -h 查看帮助

完成时间：2014-07-11

2014-07-14 增加科室过滤功能和显示费用 支持windows和linux

2014-09-16 增加设置代理参数-x 增加自动循环查询参数-a 

"""

import string,thread,time,random
import sys

import json
import os
import Image,base64,StringIO
import platform
from Cjx.http import httpCient
from optparse import OptionParser
__author__ = 'appie'

isExit = False
http = None

option = None

token = None

URL = "http://211.140.7.44:9901/YYGH_12580_SERVICE/service.htm"

def isWin():
    if platform.platform().find('Win')>-1:
        return True
    return False

def sendRequest(method, args, service = "patientService", type="mt"):
    argstr = json.dumps(args)
    data = "{\"version\":\"2.0\",\"httpCode\":\"QjfjcNUhCSM=\",\"type\":\"%s\",\"token\":\"%s\",\"service\":\"%s\",\"method\":\"%s\",\"args\":%s}" % (type, token, service, method, argstr)
    #print data
    param = {"key":data}

    content = http.urlcontent(URL, param)
    return content
def decodeJson(content):
    '''
    解析json数据
    '''
    try:
        body = json.loads(content)
        return body
    except Exception, e:        
        return False

def chkResult(content):    
    '''
    检查操作结果
    '''
    json = decodeJson(content)
    if not json:
        return False
    if json['code'] != u'0':
        #print body['code']
        return False
    return json['data']

def login(username, password):
    '''
    登录 登录成功返回token等相关信息 
    '''
    method = "Login"
    args = {"patientCard":username, "password":password}
    print u"正在登录 %s(%s) ..." % (username, password)
    content = sendRequest(method, args)
    return decodeJson(content)
    #print content

def QueryNumResourceListForPat(schemeId, resDate, resTimeSign):
    '''
    执行queryRemainByName后，查询到某个医生有号，这个方法返回具体的号信息，然后根据这个信息调用此方法查询这个号的具体信息。返回格式：
    "numList": [{
            "numId": 50654050,
            "resNumber": 18,
            "resState": "0",
            "resTime": "0954",
            "schemeTime": ""
        }]

    这个方法只有在登录后才能用
    '''
    method = "QueryNumResourceListForPat"
    service = 'numResourceService'
    args = {'schemeId':schemeId,'resDate':resDate,'resTimeSign':resTimeSign}
    content = sendRequest(method, args, service, 'pc')
    #print content
    return chkResult(content)

def queryImageCode(hosId, numId):
    '''
    获取验证码 numId在调用QueryNumResourceListForPat时得到 如果操作成功，返回图片Image 操作失败返回False

    这个方法只有在登录后才能用
    '''

    method = "QueryOrderCode"
    service = 'getPltCodeService'
    args = {'hosId':hosId,'numId':numId}
    content = sendRequest(method, args, service, 'pc')
    #print content
    re = chkResult(content)
    if re != False:
        stream = base64.b64decode(re['orderCode'])
        image = Image.open(StringIO.StringIO(stream))
        return image
    return re

def submitOrder(numResourceId, docId, docName, hosName, hosId, deptId, schemeId, orderTime, orderTimeSign, orderNumber, deptName, resDate, orderCode):
    '''
    提交订单 通过QueryNumResourceListForPat获取numResourceId(numId),orderNumber(resNumber)
    通过queryImageCode显示验证码，等待用户输入

    docid,docname,hosName,hosId,deptId,schemeId,orderTime,orderTimeSign,deptName,resDate可以通过queryRemainByName获取

    这个方法只有在登录后才能用
    '''
    method = "RegisterOrder"
    service = 'orderService'
    args = {'numResourceId':numResourceId,'docId':docId,'docName':docName,'hosName':hosName, 'hosId':hosId, 'deptId':deptId,
    'schemeId':schemeId, 'orderTime':orderTime,'orderTimeSign':orderTimeSign, 'orderNumber':orderNumber, 'deptName':deptName, 'resDate':resDate, 'orderCode':orderCode}

    content = sendRequest(method, args, service, 'pc')

    #print content
    re = decodeJson(content)
    return re

def queryByName(name):
    '''
    根据医生名称查询医生的信息
    '''
    #转换为unicode编码
    method = "SearchDoc"
    service = "searchService"
    args = {"key":name, "page":"1"}
    content = sendRequest(method, args, service)
    #print content
    return chkResult(content)
    #print content

def queryRemainByName(name, type=3, deptId = 0):
    '''
    根据名字查询是否有号 {code:0,data:{},msg:''} code为0表示查询成功 data为查询的数据 否则为失败 msg返回相应错误信息
    '''
    #没有指定科室，则去自动查询，如果有多条结果，则只会选择每一条
    re = {'code':-1,'msg':u'没有找到任何内容','data':'','hosId':0,'hosName':'','deptId':0}
    if deptId == 0:
        data = queryByName(name)
        #print data
        if data == False:
            re['code'] = 1
            re['msg'] = u'没有找到该人'            
            return re
        people = data[u'searchList'][u'result'][0]
        docName = people['docName']
        deptId = people['deptId']

    method = "QuerySchemeList"
    service = "schemeService"
    #type 1专家 2普通 3全部
    args = {"docName":name, "type":type, "deptId":deptId}
    #while(True):


    content = sendRequest(method, args, service)    
    result = chkResult(content)    
    if not result:
        re['code'] = 2
        re['msg'] = u'没有排班信息'         
        return re
    l = result[u'schemeList']
    #保存hostid,hosName 请求验证码和提交订单时使用
    re['hosId'] = result['hosId']
    re['hosName'] = result['hosName']
    re['deptId'] = deptId
    data = [];    
    for row in l:
        if row['pmResRemaining']>0 or row['amResRemaining']>0:
            data.append(row)            
    if len(data)>0:
        re['code'] = 0
        re['data'] = data
        re['msg']=''
    else:                
        re['code'] = 3
        re['msg'] = u'共找到记录 %d 条，全部预约满' % len(l)    
    return re 

    #if content == False:
    #    time.sleep(1.5)
        #continue
    #data = content["schemeList"]
    
    #print result['code']
    # l = result["schemeList"]
    # for row in l:
    #     #print row
    #     if row[u'pmResRemaining']>0:
    #         print row;
    #         break;
    # time.sleep(1.5)
def queryHosIdByName(name, output=True):
    '''    
    根据医院名称查询医院列表ID 需要医院ID才能查询科室ID 需要科室ID才能预约

    查询方法，只作查询用
    '''
    method = "SearchHos"
    service = "searchService"
    args = {"key":name,"page":1}
    content = sendRequest(method, args, service)    
    result = chkResult(content)
    if not output:
        return result

    if not result:
        print u'没有找到任何结果'         
        return
    l = result['searchList']
    ll = l['result']
    print u'\n%10s %s'%(u'编号', u'名称')
    print u'-----------------------------------------'
    for row in ll:
        print u'%10d|%s' % (int(row['hosCode']), row['hosName'])
    print u'-----------------------------------------'

def queryDepIdByDoctor(name):
    '''    
    根据医生名称查询医生的相关信息

    查询方法，只作查询用 
    ''' 
    result = queryByName(name)
    if not result:
        print u'没有找到任何结果'         
        return
    l = result['searchList']
    ll = l['result']
    print u'\n%5s%5s%10s%5s %s'%(u'全名',u'科室ID', u'科室名称', u'医院ID', u'医院名称')
    print u'-------------------------------------------------------'
    for row in ll:
        print u'%5s%5s%10s%5s %s' % (row['docName'], row['deptId'], row['deptName'], row['hosId'], row['hosName'])
    print u'-------------------------------------------------------'

def queryDepIdByHos(hosid, output=True):
    '''    
    根据医院ID查询所有的科室ID 如果指定了option.deptname则自动过滤存在的记录

    查询方法，只作查询用
    ''' 
    method = "QueryClassDepartList"
    service = "departService"
    args = {'hosId':hosid}
    content = sendRequest(method, args, service)    
    result = chkResult(content)    
    if not result:    
        if output:    
            print u'没有找到任何结果，医院ID不对？'         
        return False
    data = []
    l = result['departList']
    for row in l:
        ll = row['depart']
        for row2 in ll:
            #如果指定了科室名，则只只显示满足条件的
            if option.deptname:
                if row2['deptName'].find(option.deptname)>-1:
                    data.append(row2)
            else:
                data.append(row2)

    #不输入结果，则直接返回
    if len(data)<1:
        if output:
            print u'没有找到任何结果，科室关键字:%s'%param.deptname
        return False

    print u'\n%10s %s'%(u'编号', u'名称')
    print u'-----------------------------------------'
    for row in data:        
        print u'%10d|%s' % (row['deptId'],row['deptName'])
    print u'-----------------------------------------'

#def queryOrSubmitOrder(name, type, deptId, autoSubmit=False):


def main():

    global http,option,token
    usage = u"根据医生下单:%prog -s -n 沈瑞林 -u 432524198412219014 -p Jokenchen\n查询科室下可用号:%prog -i 浙江大学医学院附属第二医院 -j 内科"
    version = u"1.0"
    parser = OptionParser(usage)
    parser.add_option("-u","--username",dest="username",help=u"登录用户名 只有实际预约时有用",default='')
    parser.add_option("-p","--password",dest="password",help=u"登录密码 只有实际预约时有用",default='')
    parser.add_option("-n","--name",dest="name",help=u"名字，如果为空，则必须指定depid，这时为查询该科室下所有医生空名额",default='')
    parser.add_option("-t","--type",dest = "type", help=u'类型 1为专家 2为普通 3为所有,默认为所有',default=3)
    parser.add_option("-d","--depid",dest="deptId",help=u'科室ID，必须为大于等于0的数字，如果没有指定或为0，则根据name自动查询，建议只有在医生重名时使用',default=0)
    parser.add_option("-s", "--submit",action="store_true", dest="submit", default=False,help=u"自动提交订单") 
    parser.add_option('-a','--again',action='store_true', dest="again", default=False,help=u'自动重复查询')



    #查询类参数 一次只能使用一次查询
    parser.add_option('-k',"--doctor",dest="doctor",help=u'根据医生名字查询医生的信息')


    parser.add_option("-i","--hosname",dest="hosname",help=u'根据医院名称查询医院列表ID')
    parser.add_option('-j','--deptname',dest="deptname",help=u'科室名称，必须和hosname或hosid配合使用')
    parser.add_option('-l',"--hosid",dest="hosId",help=u'根据医院ID查询所有的科室ID')
    parser.add_option('-x','--proxy',dest='proxy',help=u'代理地址 格式如127.0.0.1:8087')
    

    (option,args) = parser.parse_args()
    #输入参数编码转换 转换到Unicode下使用 windows环境命令行下使用了gb2312编码，而linux环境下是utf8编码
    charset = 'utf8'
    if isWin():
        charset = 'gb2312'
    if option.name:
        option.name = option.name.decode(charset)
    if option.doctor:
        option.doctor = option.doctor.decode(charset)
    if option.hosname:
        option.hosname = option.hosname.decode(charset)
    if option.deptname:
        option.deptname = option.deptname.decode(charset)

    if option.proxy:
        http = httpCient(False,None,option.proxy)
    else:
        http = httpCient()

    if option.hosId:
        queryDepIdByHos(option.hosId)
        return

    if option.hosname and not option.deptname:
        queryHosIdByName(option.hosname)
        return
    if option.doctor:
        queryDepIdByDoctor(option.doctor)
        return   

    # if not option.name and not option.deptId:
    #     print u'--name参数和--depid参数必须指定一个'
    #     return

    #只指定了科室名，则根据医院名称或医院ID自动查询该科室下的所有可用号
    if option.deptname and not option.deptId and not option.name:
        #没有指定医院ID 则去自动查询，然后取第一条记录
        if not option.hosId and not option.hosname:
            print u'指定了deptName，则hosId或hosName任指定一个'
            return
        #没有指定hosId，则根据医院名称自动查询 如果有多个医院匹配，只会选择第一个医院
        hosName = None
        if not option.hosId:
            data = queryHosIdByName(option.hosname, False)
            if not data:
                print u'没有找到医院 %s 任何信息，请确认输入正确' % option.hosname
                return
            data = data['searchList']
            row = data['result'][0]
            option.hosId = row['hosCode']
            hosName = row['hosName']
            print u'匹配医院:%s,共有 %d 个结果' % (hosName, len(data['result']))
        #根据hosId和deptName匹配科室
        data = queryDepIdByHos(option.hosId, False)
        if not data:
            print u'医院 %s 没有找到任何科室信息' % hosName
            return
        for row in data:            
            print u'匹配医院:%s,匹配科室:%s,科号:%s' % (hosName, row['deptName'], row['deptId'])            
             #break
        #选择第一条记录
        option.deptId = data[0]['deptId']

    if option.again:
        while True:
            data = queryRemainByName(option.name, option.type, option.deptId)
            if data['code'] == 0:
                break;
            else:
                print data['msg']
            sleep_time = random.random()*2
            time.sleep(sleep_time)
    else:
        data = queryRemainByName(option.name, option.type, option.deptId)
    if data['code'] != 0:
        print data['msg']
    else:
        #只是查询
        if not option.submit:
            print u'\n%10s %10s %10s %10s %10s'%(u'科号', u'名称', u'类型', u'时间', u'费用')
            print u'---------------------------------------------------------------------'
            for row in data['data']:                
                print u'%10s %10s %10s %10s %10s' % (option.deptId,row['docName'],row['docTitle'],row['date'],row['regFee'])
            print u'---------------------------------------------------------------------'
            return
        hosId = data['hosId']
        hosName = data['hosName'];
        deptId = data['deptId']
        #使用第一个号来预约
        data = data['data'][0];

        #登录 获取token
        re = login(option.username,option.password)
        if re['code'] == u'0':
            token = re['data']['token']
        else:
            print u'登录失败，原因：%s' % re['message']
            return

        #查询具体号的信息
        re = QueryNumResourceListForPat(data['schemeId'], data['resDate'], data['resTimeSign'])
        if not re:
            print u'查询号具体信息失败，请确认网络是否已经连接'
            return
        #多个号信息，只选第一个
        list = re['numList'];
        pat = list[0]

        numId = pat['numId']
        #查询验证码，显示验证码
        image = queryImageCode(hosId, numId)
        if not image:
            print u'获取验证码失败，请确认网络是否已经连接'
            return
        image.show()
        #等待用户输入看到的验证码
        ch = raw_input("enter image code:")    
        #提交订单    
        #numResourceId, docId, docName, hosName, hosId, deptId, schemeId, orderTime, orderTimeSign, orderNumber, deptName, resDate, orderCode
        re = submitOrder(numId, data['docId'], data['docName'], hosName, hosId, deptId, data['schemeId'],
            pat['resTime'], data['resTimeSign'], pat['resNumber'], data['deptName'], data['resDate'], ch)

        if re['code'] != u'0':
            print u'操作失败，原因：%s' % re['message']
        else:
            print u'恭喜，操作成功，取号密码：%s，稍候有短信发到你的手机' % re['data']['takeCode']


if __name__ == '__main__':
    main()

