#! /usr/bin/env python
#coding=utf-8
# -*- coding: utf-8 -*-
#date 14-3-19

"""
功能:

读取可用代理 并且测试速度 然后保存到文件中 如果出现卡顿现象，请适当调整超时时间（越小越快，不过相对准确率也较低）和最大线程数（尽量在100以内）。
或者在控制台按一下回车，有时也能使其得到响应

urls 格式
[
    {
        'url':'http://proxy.ipcn.org/proxylist.html',   #要远程获取代理列表的网址 必须参数
        'listRepx':'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:\d+',        #列表正则匹配 必须参数
        'detailRepx':{                  #指定内容ip和端口正则匹配 可选参数 如果不设置，则直接使用列表作为ip和端口进行连接
            'ip':'140px;\">(.*?)</span>', #列表中ip地址的匹配正则
            'port':'50px;\">(.*?)</span>' #列表中端口的匹配正则
        },
        'pageFormat':'http://info.hustonline.net/proxy/prolist.aspx?show=3&indexPage=%d'          #分页格式 只能有一个 %d 参数，可选参数 如果不设置，则不选取分页
        'start':1                       #开始分页  可选参数 当设置了pageFormat的时候，这个参数必须设置并且必须是大于0的数字
        'end':99                        #结束分页 可选参数 当设置了pageFormat的时候，这个参数必须设置并且必须是大于start的数字
    }
]
Get proxies from urls, and test their speed"""
import urllib, urllib2, re, time, threading

urls = [
    {
        "url":"http://proxy.ipcn.org/proxylist.html",
        "listRepx":"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:\d+"
    },
    # {
    #     "url":"http://www.proxy360.cn/Region/China",
    #     "listRepx":"list_proxy_ip(.*?)</div>",
    #     "detailRepx":{
    #         "ip":"140px;\">(.*?)</span>",
    #         "port":"50px;\">(.*?)</span>"
    #         }
    # },
    {
        "url":"http://www.xici.net.co/nn/",
        "listRepx":"images/flag/(.*?)</tr>",
        "detailRepx":{
            "ip":"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}",
            "port":"<td>([0-9]+?)</td>"
        },
        "pageFormat":"http://www.xici.net.co/nn/%d",
        "start":2,
        "end":5
    },
    {
        "url":"http://www.xici.net.co/nt/",
        "listRepx":"images/flag/(.*?)</tr>",
        "detailRepx":{
            "ip":"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}",
            "port":"<td>([0-9]+?)</td>"
        },
        "pageFormat":"http://www.xici.net.co/nt/%d",
        "start":2,
        "end":5
    },
    {
        "url":"http://www.xici.net.co/wn/",
        "listRepx":"images/flag/(.*?)</tr>",
        "detailRepx":{
            "ip":"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}",
            "port":"<td>([0-9]+?)</td>"
        },
        "pageFormat":"http://www.xici.net.co/wn/%d",
        "start":2,
        "end":5
    },
    {
        "url":"http://www.xici.net.co/wt/",
        "listRepx":"images/flag/(.*?)</tr>",
        "detailRepx":{
            "ip":"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}",
            "port":"<td>([0-9]+?)</td>"
        },
        "pageFormat":"http://www.xici.net.co/wt/%d",
        "start":2,
        "end":5
    },
       # "http://info.hustonline.net/index/proxyshow.aspx"
]		#where to get proxies

#测试代理的网址
test_url = "http://www.163.com"
#打开测试网址成功匹配的关键字
test_pattern = re.compile(r"""googlebot""")
#超时
time_out = 10.0
#输出的文件
output_file = "Proxies.txt"
#最大运行的线程数 不能设置太大，否则可能会卡死 例如超时是10，最大线程数为50，则平均是每秒5个测试
MAX_THREAD_NUM = 50

class TestTime(threading.Thread):
    """test a proxy's speed in new thread by recording its connect time"""
    def __init__(self, proxy):
        threading.Thread.__init__(self)
        self.proxy = proxy
        self.time = None
        self.stat = proxy + " time out!"
    def run(self):
        start = time.time()
        try:
            f = urllib.urlopen(test_url, proxies = {"http":"http://"+self.proxy})
        except:
            self.stat = self.proxy+" fails!"
        else:
            data = f.read()
            f.close()
            end = time.time()
            if test_pattern.search(data): #if data is matched
                self.time = end-start
                self.stat = self.proxy+" time: "+str(self.time)
            else:
                self.stat = self.proxy+" not matched!"

def totest(proxy, result):
    """test a proxy's speed in time_out seconds"""
    test = TestTime(proxy)
    test.setDaemon(True)
    print "testing "+proxy
    test.start()
    test.join(time_out) #wait time_out seconds for testing
    print test.stat
    if test.time:
        result.append((test.time, proxy))


def allspacetrim(str):
    """
    去掉所有的空白字符 包括中间的
    """
    content = re.sub("(\s|&nbsp;)+", "", str)
    return content

def analyseIpByUrl(url, item):
    """
    分析指地址的ip地址
    url 要分析的网址
    item urls设置的一项

    如果获取成功，返回ip列表数组，如果获取失败，则返回none
    """
    header = {
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.152 Safari/537.36"
    }
    print "getting proxy from "+url
    try:
        request = urllib2.Request(url, None, header)
        f = urllib2.urlopen(request)
    except:
        print url+" can not open!\n"
        return None

    data = f.read()
    f.close()
    listRepx = item['listRepx']

    repx = re.compile(listRepx, re.I | re.S)
    result = repx.findall(data)
    if len(result) <= 0:
        print 'url ', url, ' get ip list failed!'
        return None
    #如果没有定义获取ip和端口的正则，则直接返回
    if not item.has_key('detailRepx'):
        print 'url ', url, ' get ip list count:', len(result)
        return result
    detailRepx = item['detailRepx']

    iplist = []

    ipRepx = re.compile(detailRepx['ip'], re.I | re.S)
    portRepx = None
    #定义了端口的正则
    if detailRepx['port']:
        portRepx = re.compile(detailRepx['port'], re.I | re.S)

    #读取ip和端口
    for i in result:
        tmp = ipRepx.findall(i)
        ip = None
        #成功获取了IP
        if len(tmp) > 0:
            ip = allspacetrim(tmp[0])
        else:
            print 'url ',url,' get ip failed!item:', i
        #有定义了端口的正则
        if portRepx:
            tmp = portRepx.findall(i)
            #成功获取了端口，直接附加到ip的后面，以:分隔
            if len(tmp)>0:
                ip = ip + ":" + allspacetrim(tmp[0])
            else:
                print 'url ', url, 'get port failed!item:', i
        #找到了ip地址
        if ip:
            iplist.append(ip)
    print 'url ', url, ' get ip list count:', len(iplist)
    return iplist


if __name__ == "__main__":
    #get old proxies in output_file
    try:
        f = open(output_file)
    except:
        allproxies = set()
    else:
        allproxies = set([x[:-1] for x in f.readlines()])
        f.close()

    #get else proxies from urls
    for item in urls:
        url = item['url']
        result = analyseIpByUrl(url, item)
        if result:
            allproxies.update(result)
        if not item.has_key('pageFormat') or not item.has_key('start') or not item.has_key('end'):
            continue
        #设置了分页信息
        pageFormat = item['pageFormat']

        if item['start'] > 0 and item['end'] > item['start']:
            #循环获取所有的分页
            for i in range(item['start'], item['end']):
                url = pageFormat % i
                result = analyseIpByUrl(url, item)
                if result:
                    allproxies.update(result)



    #test all proxies' speed
    result = []
    count = len(allproxies)
    print 'total find proxy ip:', count

    i = 1
    for proxy in allproxies:
        #超过了最大线程，等待线程结束
        if i % MAX_THREAD_NUM == 0:
            print 'wait other thread end,finished ', i, ' of ', count
            time.sleep(time_out)
        #new thread to test every proxy
        t = threading.Thread(target=totest, args=(proxy, result))
        t.setDaemon(True)
        t.start()
        i += 1


    #show all proxies' speed
    time.sleep(time_out+5.0)
    result.sort()
    for i in xrange(len(result)):
        print str(i+1)+"\t"+result[i][1]+"   \t:\t"+str(result[i][0])

    #output needed proxies
    num = len(result)
    try:
        f = open(output_file, "w")
    except:
        print "Can not open output file!"
    else:
        f.writelines([x[1]+"\n" for x in result[:num]])
        f.close()
        print str(num)+" proxies are output."