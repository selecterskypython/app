#! /usr/bin/env python
#coding=utf-8
# -*- coding: utf-8 -*-
#date 14-3-19
from Cjx.http import httpCient

def main():
    http = httpCient()

    #去掉左边空格测试
    txt = "&nbsp;&nbsp;      s &nbsp;f&nbsp;&nbsp;   "
    l = http.lspacetrim(txt)
    r = http.rspacetrim(txt)
    a = http.spacetrim(txt)
    all = http.allspacetrim(txt)
    print "l=",l,",len=",len(l),"\nr=",r,",len=",len(r), "\na=", a, ",len=",len(a), "\nall=",all,",len=",len(all)

    txt = "&nbsp;<a>sfsf</b>sfsf     ll  sd<b>&nbsp; &lt;sfsfsf&gt; ss"
    h = http.alltrim(txt)
    print "allhtmltrim:",h,",len=",len(h)
if __name__ == '__main__':
    main()
