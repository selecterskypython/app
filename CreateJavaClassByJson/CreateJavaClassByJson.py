#! /usr/bin/env python
#coding=utf-8
# -*- coding: utf-8 -*-
__author__ = 'cjx'

#功能：根据json文件自动生成一个java类

import sys
import json
import os
import types
import time

def main():
    argv = sys.argv;
    if(len(argv)!=4):
        print("format:",__file__," <json file> <java file> <package>\n ag:",__file__," 1.json XXX.java com.test")
        return
    jsonfilename =argv[1]
    filename = argv[2]
    package = argv[3]
    print 'start create,json file:',jsonfilename,',create file:',filename,',package:',package,'\n'

    if not os.path.exists(jsonfilename):
        print("json file：",jsonfilename," not exists")
        return
    fp = None
    try:
        fp = open(jsonfilename,"r")
    except ValueError:
        print("read json file:",jsonfilename,"content failure")
        return
    jsonbody = fp.read()
    fp.close()
    try:
        fp = open(filename,"w+")
    except ValueError:
        print("open write file ",filename," failure")
        return
    jsonobj = json.loads(jsonbody)
    property = getJavaProperty(jsonobj)
    method = getJavaMethod(jsonobj)
    javabody = getJavaBody(package,filename)
    #print javabody
    javabody = javabody.replace('$property$',property)
    javabody = javabody.replace('$method$',method)
    fp.write(javabody)
    fp.close()
    print 'finished!'

#获取类模版
def getJavaBody(packagename,filename):
    return '''package '''+packagename+''';

/**
 * Created by cjx on '''+time.strftime('%Y-%m-%d')+'''.
 */
public class '''+filename+''',
{
$property$

$method$
}
    '''

#生成JAVA属性
def getJavaProperty(jsonobj):
    str = ''
    for k in jsonobj:
        tmp = '    private '+getJavaTypeName(jsonobj[k])+' '+k+';\n'
        str = str+tmp
    return str;

#生成方法
def getJavaMethod(jsonobj):
    str = ''
    for k in jsonobj:
        v = jsonobj[k]
        key_upper = k[0].upper() + k[1:]
        javaType=getJavaTypeName(v)
        tmp = '    public '+javaType+' get'+key_upper+'(){return this.'+k+';}\n'
        str = str+tmp
        tmp = '    public void set'+key_upper+'('+javaType+' value){this.'+k+'=value;}\n'
        str = str+tmp
    return str

#获取当前变量的数据类型 该方法暂时有问题，所有的返回值为String
def getJavaTypeName(v):
    str = 'String'
    t = type(v)
    if t is types.BooleanType:
        str = 'boolean'
    elif t is types.FloatType:
        str = 'float'
    elif t is types.LongType:
        str = 'long'
    elif t is types.IntType:
        str = 'int'
    return str

#执行主函数
main()