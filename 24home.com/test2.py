#! /usr/bin/env python
#coding=utf-8
# -*- coding: utf-8 -*-
import string
from Cjx.http import httpCient
__author__ = 'appie'

http = httpCient(True,None,"202.106.16.36:3128")
#第条产品信息正则
repxStr = "j_normal(.*?)j_shadow"
#总共分页数正则
pageCountRepxStr = "共([0-9]*?)页"
#要获取的产品信息正则
detailDict = {
    'price':'<span>(.*?)</span>',
    'url':'href=\"(.*?)\"',
    'title':'title=\"(.*?)\"',
    'img':'src=\"(.*?)\"'
}

alllist = []
url = "http://www.24home.com/productList/33-0-0-0-3-0-49-1-1.htm"
content = http.urlcontent(url)
list = http.itemlist(content,repxStr,0,pageCountRepxStr)
#处理每个产品数据
pro_data = list['list']
print len(pro_data)
if len(pro_data) > 0:
    for item in pro_data:
        #获取产品详细信息
        detail = http.itemDetail(item,detailDict)
        #转换价格
        try:
            detail['price'] = string.atof(detail['price'])
        except:
            print 'covert price failed,price:', detail['price'], ',name:', detail['title'], ',url:', detail['url']
        #压入数组
        alllist.append(detail)
print alllist

print http.urlcontent("http://iframe.ip138.com/ic.asp")