#! /usr/bin/env python
#coding=utf-8
# -*- coding: utf-8 -*-

"""
功能：使用当前目录下的db.xls文件记录数据，自动登录www.yiwugou.com网站后台，然后修改密码

执行完成后生成db_result.xls文件，记录操作结果

"""
import xlrd
import xlwt
import sys

from xlutils.copy import copy
import os
from Cjx.http import httpCient

__author__ = 'appie'
isExit = False
http = None
#用于显示状态的列的位置 一般是最后一列
cellStatusNum = 4

xmlWrite = None
xmlWriteTable = None
file = u"db.xls"
result_file = u"db_result.xls"
charset = "utf8"


def isNum(value):
    try:
        x = value + 1
    except TypeError:
        return False
    else:
        return True


def login(username,password):
    loginurl = "http://work.yiwugou.com/loginprocess.htm"
    print u"正在登录 %s(%s) ..." % (username, password)
    param = {
        "done":"http://www.yiwugou.com/",
        "userName":username,
        "password":password,
        "isRememberMe":1
    }
    #登录
    content = http.urlcontent(loginurl, param)

    if not content or content.find("登录_义乌购") > 0:
        msg = u"用户 %s(%s)登录失败" % (username, password)
        print msg
        return [False, msg]
    return [True, ""]

def post(oldpassword,newpassword):
    print u"正在修改密码 %s->%s" % (oldpassword, newpassword)
    url = "http://work.yiwugou.com/product/subuser/process.htm"
    param = {
        'oldpasd':oldpassword,
        'newpasd':newpassword,
        'newpasd2':newpassword
    }
    content = http.urlcontent(url, param)

    if not content or content.find("密码修改完成") == -1:
        #print content
        msg = u"修改密码 %s->%s 失败"%(oldpassword,newpassword)
        print msg
        return [False, msg]
    return [True, ""]

def showResult(row, errmsg=None):
    try:
        if not errmsg:
            xmlWriteTable.write(row, cellStatusNum, u"操作成功")
        else:
            xmlWriteTable.write(row, cellStatusNum, u"操作失败")
            xmlWriteTable.write(row, cellStatusNum+1, errmsg)
    except:
        print u"结果写数据表失败，行:%d"%row

def showmsg(msg):
    msg = msg+u"(按回车键退出)"
    print msg
    raw_input()

def stringByCell(table,row,col,fieldName):
    try:
        r = table.row_values(row)
        tmp = r[col]
        if isNum(tmp):
            value = unicode(int(tmp))
        else:
            if len(tmp)<1:
                showResult(row, fieldName+u"不能为空")
                return False
            else:
                value = unicode(tmp)
        return value
    except:
        showResult(row, fieldName+u"操作失败")
        return False

def main():

    global http
    #http = httpCient(False,None,"192.168.22.120:8888")
    http = httpCient()
    global charset
    charset = sys.getfilesystemencoding()

    if not os.path.isfile(file):
        showmsg(u"数据库文件 %s 在当前目录不存在，请确认" % file)
        return
    try:
        xls = xlrd.open_workbook(file)
        table = xls.sheets()[0]
    except:
        showmsg(u"打开数据库文件 %s 失败，请确认有操作权限" % file)
        return

    global xmlWrite, xmlWriteTable
    try:
        xmlWrite = copy(xls)
        xmlWriteTable = xmlWrite.get_sheet(0)
    except:
        showmsg(u"创建数据库读写对象失败，请确认数据表格式")
        return
    nrows = table.nrows
    if nrows<3:
        showmsg(u"没有找到可操作的记录，自动退出")
        return
    succ = 0
    for i in range(nrows ):
        #row = table.row_values(i)
        # status = row[cellStatusNum]
        # if len(status)>0:
        #     continue
        #跳过第一行和第二行
        if i==0 or i==1:continue
        #0为序号，直接跳过
        username = stringByCell(table,i,1,u"用户名")
        if not username:continue

        oldpassword = stringByCell(table,i,2,u"旧密码")
        if not oldpassword:continue

        newpassword = stringByCell(table,i,3,u"新密码")
        if not newpassword:continue

        result = login(username, oldpassword)
        if not result[0]:
            showResult(i, result[1])
            continue

        result = post(oldpassword, newpassword)
        if result[0] == False:
            showResult(i, result[1])
            continue
        showResult(i)
        succ = succ + 1
    #table.flush_row_data()
    #xls.save(file)
    try:
        if os.path.isfile(result_file):
            os.remove(result_file)

        xmlWrite.save(result_file)
    except:
        showmsg(u"写入结果数据失败")
    showmsg(u"所有操作已经完成，共有记录 %d 条，成功操作 %d 条，结果保存在 %s 文件中"% (nrows-2,succ,result_file))


if __name__ == '__main__':
    main()

