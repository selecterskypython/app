# ! /usr/bin/env python
#coding=utf-8
# -*- coding: utf-8 -*-
#date 14-5-5
import xlrd
import xlwt
import os

from xlutils.copy import copy

def main():
    lenstr = """产品名字：个性蝴蝶结镶嵌心形锆石耳钉
产品颜色: 白色
产品材质：铜
处理工艺：AAA锆石+镀18K白金
产品描述：无铅无镍，防过敏，简单的搭配，晶莹剔透的锆石，彰显女性的高贵优雅与柔美。
锆石的作用：使面料能吸湿干燥，孔隙吸附的微生物能分解有"""
    print "len:%d,%s"%(len(lenstr),lenstr[0:120])
    file = "db_test.xls"
    savefile = "db_test_save.xls"
    xls = xlrd.open_workbook(file)
    xmlWrite = copy(xls)
    xmlWriteTable = xmlWrite.get_sheet(0)
    t = "舭桂林"
    xmlWriteTable.write(1,9,u"测试")
    xmlWriteTable.write(1,10,t.decode("utf8"))

    if os.path.isfile(savefile):
        os.remove(savefile)
    xmlWrite.save(savefile)


if __name__ == '__main__':
    main()
