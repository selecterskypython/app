passport._define("login_tangram.js", 
function() {
    var e,
    d,
    c = e = function(i, j) {
        return c.dom ? c.dom(i, j) : null
    };
    c.version = "2.0.0.1";
    c.guid = "$BAIDU$";
    c.key = "tangram_guid";
    window[c.guid] = window[c.guid] || {};
    c.check = c.check || 
    function() {};
    c.lang = c.lang || {};
    c.forEach = function(j, o, m) {
        var l,
        p,
        k;
        if (typeof o == "function" && j) {
            if (typeof j.length == "number") {
                for (l = 0, p = j.length; l < p; l++) {
                    k = j[l] || (j.charAt && j.charAt(l));
                    o.call(m || null, k, l, j)
                }
            } else {
                if (typeof j == "number") {
                    for (l = 0; l < j; l++) {
                        o.call(m || null, l, l, l)
                    }
                } else {
                    if (typeof j == "object") {
                        for (l in j) {
                            if (j.hasOwnProperty(l)) {
                                o.call(m || null, j[l], l, j)
                            }
                        }
                    }
                }
            }
        }
        return j
    };
    c.type = (function() {
        var j = {},
        i = [, "HTMLElement", "Attribute", "Text", , , , , "Comment", "Document", , "DocumentFragment", ],
        l = "Array Boolean Date Error Function Number RegExp String",
        k = j.toString;
        c.forEach(l.split(" "), 
        function(m) {
            j["[object " + m + "]"] = m.toLowerCase();
            c["is" + m] = function(n) {
                return c.type(n) == m.toLowerCase()
            }
        });
        return function(n) {
            var m = typeof n;
            return m != "object" ? m: n == null ? "null": n._type_ || j[k.call(n)] || i[n.nodeType] || (n == n.window ? "Window": "") || "object"
        }
    })();
    c.isDate = function(i) {
        return c.type(i) == "date" && i.toString() != "Invalid Date" && !isNaN(i)
    };
    c.isElement = function(i) {
        return c.type(i) == "HTMLElement"
    };
    c.isEnumerable = function(i) {
        return i != null && typeof i == "object" && (typeof i.length == "number" || typeof i[0] != "undefined")
    };
    c.isNumber = function(i) {
        return c.type(i) == "number" && isFinite(i)
    };
    c.isPlainObject = function(k) {
        var i,
        j = Object.prototype.hasOwnProperty;
        if (c.type(k) != "object") {
            return false
        }
        if (k.constructor && !j.call(k, "constructor") && !j.call(k.constructor.prototype, "isPrototypeOf")) {
            return false
        }
        for (i in k) {}
        return i === undefined || j.call(k, i)
    };
    c.isObject = function(i) {
        return typeof i === "function" || (typeof i === "object" && i != null)
    };
    c.extend = function(l, p) {
        var m,
        x,
        u,
        j,
        k,
        r = 1,
        o = arguments.length,
        w = l || {},
        s,
        t;
        c.isBoolean(l) && (r = 2) && (w = p || {}); ! c.isObject(w) && (w = {});
        for (; r < o; r++) {
            x = arguments[r];
            if (c.isObject(x)) {
                for (u in x) {
                    j = w[u];
                    k = x[u];
                    if (j === k) {
                        continue
                    }
                    if (c.isBoolean(l) && l && k && (c.isPlainObject(k) || (s = c.isArray(k)))) {
                        if (s) {
                            s = false;
                            t = j && c.isArray(j) ? j: []
                        } else {
                            t = j && c.isPlainObject(j) ? j: {}
                        }
                        w[u] = c.extend(l, t, k)
                    } else {
                        if (k !== undefined) {
                            w[u] = k
                        }
                    }
                }
            }
        }
        return w
    };
    c.createChain = function(n, l, k) {
        var j = n == "dom" ? "$DOM": "$" + n.charAt(0).toUpperCase() + n.substr(1);
        var m = Array.prototype.slice;
        var i = c[n] = c[n] || l || 
        function(o) {
            return c.extend(o, c[n].fn)
        };
        i.extend = function(o) {
            var p;
            for (p in o) {
                i[p] = function() {
                    var t = arguments[0];
                    n == "dom" && c.type(t) == "string" && (t = "#" + t);
                    var s = i(t);
                    var r = s[p].apply(s, m.call(arguments, 1));
                    return c.type(r) == "$DOM" ? r.get(0) : r
                }
            }
            return c.extend(c[n].fn, o)
        };
        c[n][j] = c[n][j] || k || 
        function() {};
        i.fn = c[n][j].prototype;
        return i
    };
    c.overwrite = function(j, m, l) {
        for (var k = m.length - 1; k > -1; k--) {
            j.prototype[m[k]] = l(m[k])
        }
        return j
    };
    c.object = c.object || {};
    c.object.isPlain = c.isPlainObject;
    c.createChain("string", 
    function(i) {
        var j = c.type(i),
        l = new String(~"string|number".indexOf(j) ? i: j),
        k = String.prototype;
        c.forEach(c.string.$String.prototype, 
        function(n, m) {
            k[m] || (l[m] = n)
        });
        return l
    });
    c.string.extend({
        trim: function() {
            var i = new RegExp("(^[\\s\\t\\xa0\\u3000]+)|([\\u3000\\xa0\\s\\t]+\x24)", "g");
            return function() {
                return this.replace(i, "")
            }
        } ()
    });
    c.createChain("array", 
    function(l) {
        var k = c.array.$Array.prototype,
        j = Array.prototype,
        i;
        c.type(l) != "array" && (l = []);
        for (i in k) {
            j[i] || (l[i] = k[i])
        }
        return l
    });
    c.overwrite(c.array.$Array, "concat slice".split(" "), 
    function(i) {
        return function() {
            return c.array(Array.prototype[i].apply(this, arguments))
        }
    });
    c.array.extend({
        indexOf: function(j, k) {
            c.check(".+(,number)?", "baidu.array.indexOf");
            var i = this.length; (k = k | 0) < 0 && (k = Math.max(0, i + k));
            for (; k < i; k++) {
                if (k in this && this[k] === j) {
                    return k
                }
            }
            return - 1
        }
    });
    c.createChain("Callbacks", 
    function(w) {
        var u = {};
        function s(y) {
            var x = u[y] = {};
            c.forEach(y.split(/\s+/), 
            function(z, A) {
                x[z] = true
            });
            return x
        }
        w = typeof w === "string" ? (u[w] || s(w)) : c.extend({},
        w);
        var l,
        i,
        m,
        k,
        n,
        p,
        o = [],
        r = !w.once && [],
        j = function(x) {
            l = w.memory && x;
            i = true;
            p = k || 0;
            k = 0;
            n = o.length;
            m = true;
            for (; o && p < n; p++) {
                if (o[p].apply(x[0], x[1]) === false && w.stopOnFalse) {
                    l = false;
                    break
                }
            }
            m = false;
            if (o) {
                if (r) {
                    if (r.length) {
                        j(r.shift())
                    }
                } else {
                    if (l) {
                        o = []
                    } else {
                        t.disable()
                    }
                }
            }
        },
        t = {
            add: function() {
                if (o) {
                    var y = o.length; (function x(z) {
                        c.forEach(z, 
                        function(A, B) {
                            if ((typeof A === "function") && (!w.unique || !t.has(A))) {
                                o.push(A)
                            } else {
                                if (A && A.length) {
                                    x(A)
                                }
                            }
                        })
                    })(arguments);
                    if (m) {
                        n = o.length
                    } else {
                        if (l) {
                            k = y;
                            j(l)
                        }
                    }
                }
                return this
            },
            remove: function() {
                if (o) {
                    c.forEach(arguments, 
                    function(x, z) {
                        var y;
                        while ((y = c.array(o).indexOf(x, y)) > -1) {
                            o.splice(y, 1);
                            if (m) {
                                if (y <= n) {
                                    n--
                                }
                                if (y <= p) {
                                    p--
                                }
                            }
                        }
                    })
                }
                return this
            },
            has: function(x) {
                return c.array(o).indexOf(x) > -1
            },
            empty: function() {
                o = [];
                return this
            },
            disable: function() {
                o = r = l = undefined;
                return this
            },
            disabled: function() {
                return ! o
            },
            lock: function() {
                r = undefined;
                if (!l) {
                    t.disable()
                }
                return this
            },
            locked: function() {
                return ! r
            },
            fireWith: function(y, x) {
                x = x || [];
                x = [y, x.slice ? x.slice() : x];
                if (o && (!i || r)) {
                    if (m) {
                        r.push(x)
                    } else {
                        j(x)
                    }
                }
                return this
            },
            fire: function() {
                t.fireWith(this, arguments);
                return this
            },
            fired: function() {
                return !! i
            }
        };
        return t
    },
    function() {});
    c.createChain("Deferred", 
    function(k) {
        var n = Array.prototype.slice;
        var j = [["resolve", "done", c.Callbacks("once memory"), "resolved"], ["reject", "fail", c.Callbacks("once memory"), "rejected"], ["notify", "progress", c.Callbacks("memory")]],
        l = "pending",
        m = {
            state: function() {
                return l
            },
            always: function() {
                i.done(arguments).fail(arguments);
                return this
            },
            then: function() {
                var o = arguments;
                return c.Deferred(function(p) {
                    c.forEach(j, 
                    function(r, s) {
                        var u = r[0],
                        t = o[s];
                        i[r[1]]((typeof t === "function") ? 
                        function() {
                            var w = t.apply(this, arguments);
                            if (w && (typeof w.promise === "function")) {
                                w.promise().done(p.resolve).fail(p.reject).progress(p.notify)
                            } else {
                                p[u + "With"](this === i ? p: this, [w])
                            }
                        }: p[u])
                    });
                    o = null
                }).promise()
            },
            promise: function(o) {
                return typeof o === "object" ? c.extend(o, m) : m
            }
        },
        i = {};
        m.pipe = m.then;
        c.forEach(j, 
        function(o, p) {
            var s = o[2],
            r = o[3];
            m[o[1]] = s.add;
            if (r) {
                s.add(function() {
                    l = r
                },
                j[p ^ 1][2].disable, j[2][2].lock)
            }
            i[o[0]] = s.fire;
            i[o[0] + "With"] = s.fireWith
        });
        m.promise(i);
        if (k) {
            k.call(i, i)
        }
        c.extend(c, {
            when: function(t) {
                var r = 0,
                w = n.call(arguments),
                o = w.length,
                p = o !== 1 || (t && (typeof t.promise === "function")) ? o: 0,
                z = p === 1 ? t: c.Deferred(),
                s = function(B, C, A) {
                    return function(D) {
                        C[B] = this;
                        A[B] = arguments.length > 1 ? n.call(arguments) : D;
                        if (A === y) {
                            z.notifyWith(C, A)
                        } else {
                            if (! (--p)) {
                                z.resolveWith(C, A)
                            }
                        }
                    }
                },
                y,
                u,
                x;
                if (o > 1) {
                    y = new Array(o);
                    u = new Array(o);
                    x = new Array(o);
                    for (; r < o; r++) {
                        if (w[r] && (typeof w[r].promise === "function")) {
                            w[r].promise().done(s(r, x, w)).fail(z.reject).progress(s(r, u, y))
                        } else {--p
                        }
                    }
                }
                if (!p) {
                    z.resolveWith(x, w)
                }
                return z.promise()
            }
        });
        return i
    },
    function() {});
    c._util_ = c._util_ || {};
    c.global = c.global || (function() {
        var i = c._util_.$global = window[c.guid];
        return function(k, l, j) {
            if (typeof l != "undefined") {
                l = j && typeof i[k] != "undefined" ? i[k] : l;
                i[k] = l
            } else {
                if (k && typeof i[k] == "undefined") {
                    i[k] = {}
                }
            }
            return i[k]
        }
    })();
    c.browser = c.browser || 
    function() {
        var j = navigator.userAgent;
        var i = {
            isStrict: document.compatMode == "CSS1Compat",
            isGecko: /gecko/i.test(j) && !/like gecko/i.test(j),
            isWebkit: /webkit/i.test(j)
        };
        try { / (\d + \.\d + ) / .test(external.max_version) && (i.maxthon = +RegExp["\x241"])
        } catch(k) {}
        switch (true) {
        case / msie(\d + \.\d + ) / i.test(j) : i.ie = document.documentMode || +RegExp["\x241"];
            break;
        case / chrome\ / (\d + \.\d + ) / i.test(j) : i.chrome = +RegExp["\x241"];
            break;
        case / (\d + \.\d) ? ( ? :\.\d) ? \s + safari\ / ?(\d + \.\d + ) ? /i.test(j)&&!/chrome / i.test(j) : i.safari = +(RegExp["\x241"] || RegExp["\x242"]);
            break;
        case / firefox\ / (\d + \.\d + ) / i.test(j) : i.firefox = +RegExp["\x241"];
            break;
        case / opera(\ / |)(\d + (\.\d + ) ? )(. + ?(version\ / (\d + (\.\d + ) ? ))) ? /i.test(j):i.opera=+(RegExp["\x246"]||RegExp["\x242"]);break}c.extend(c,i);return i}();c.id=function(){var j=c.global("_maps_id"),i=c.key;c.global("_counter",1,true);return function(l,o){var n,m=c.isString(l),k=c.isObject(l),p=k?l[i]:m?l:"";if(c.isString(o)){switch(o){case"get":return k?p:j[p];break;case"remove":case"delete":if(n=j[p]){if(c.browser.ie&&c.isElement(n)){n.removeAttribute(i)}else{delete n[i]}delete j[p]}return p;break;case"decontrol":!(n=j[p])&&k&&(l[i]=p=c.id());p&&delete j[p];return p;break;default:if(m){(n=j[p])&&delete j[p];n&&(j[n[i]=o]=n)}else{if(k){p&&delete j[p];j[l[i]=o]=l}}return o}}if(k){!p&&(j[l[i]=p=c.id()]=l);return p}else{if(m){return j[l]}}return"TANGRAM__PSP_"+c._util_.$global._counter++}}();c.id.key="tangram_guid";c.merge=function(p,n){var o=p.length,m=0;if(typeof n.length==="number"){for(var k=n.length;m<k;m++){p[o++]=n[m]}}else{while(n[m]!==undefined){p[o++]=n[m++]}}p.length=o;return p};c.array.extend({unique:function(n){var k=this.length,j=this.slice(0),m,l;if("function"!=typeof n){n=function(o,i){return o===i}}while(--k>0){l=j[k];m=k;while(m--){if(n(l,j[m])){j.splice(k,1);break}}}k=this.length=j.length;for(m=0;m<k;m++){this[m]=j[m]}return this}});c.query=c.query||(function(){var l=/ ^ (\w * )# ([\w\ - \$] + ) $ / ,
            i = /^#([\w\-\$]+)$/;
            rTag = /^\w+$/,
            rClass = /^(\w*)\.([\w\-\$]+)$/,
            rComboClass = /^(\.[\w\-\$]+)+$/;
            rDivider = /\s*,\s*/,
            rSpace = /\s+/g,
            slice = Array.prototype.slice;
            function k(p, n) {
                var A,
                z,
                m,
                r,
                o,
                y,
                s,
                w,
                u = [];
                if (l.test(p)) {
                    m = RegExp.$2;
                    o = RegExp.$1 || "*";
                    c.forEach(n.getElementsByTagName(o), 
                    function(t) {
                        t.id == m && u.push(t)
                    })
                } else {
                    if (rTag.test(p) || p == "*") {
                        c.merge(u, n.getElementsByTagName(p))
                    } else {
                        if (rClass.test(p)) {
                            s = [];
                            o = RegExp.$1;
                            y = RegExp.$2;
                            A = " " + y + " ";
                            if (n.getElementsByClassName) {
                                s = n.getElementsByClassName(y)
                            } else {
                                c.forEach(n.getElementsByTagName("*"), 
                                function(t) {
                                    t.className && (" " + t.className + " ").indexOf(A) > -1 && (s.push(t))
                                })
                            }
                            if (o && (o = o.toUpperCase())) {
                                c.forEach(s, 
                                function(t) {
                                    t.tagName.toUpperCase() === o && u.push(t)
                                })
                            } else {
                                c.merge(u, s)
                            }
                        } else {
                            if (rComboClass.test(p)) {
                                w = p.substr(1).split(".");
                                c.forEach(n.getElementsByTagName("*"), 
                                function(t) {
                                    if (t.className) {
                                        A = " " + t.className + " ";
                                        z = true;
                                        c.forEach(w, 
                                        function(x) {
                                            A.indexOf(" " + x + " ") == -1 && (z = false)
                                        });
                                        z && u.push(t)
                                    }
                                })
                            }
                        }
                    }
                }
                return u
            }
            function j(m, o) {
                var n,
                p = m,
                t = "__tangram__",
                r = [];
                if (!o && i.test(p) && (n = document.getElementById(p.substr(1)))) {
                    return [n]
                }
                o = o || document;
                if (o.querySelectorAll) {
                    if (o.nodeType == 1 && !o.id) {
                        o.id = t;
                        n = o.querySelectorAll("#" + t + " " + p);
                        o.id = ""
                    } else {
                        n = o.querySelectorAll(p)
                    }
                    return n
                } else {
                    if (p.indexOf(" ") == -1) {
                        return k(p, o)
                    }
                    c.forEach(k(p.substr(0, p.indexOf(" ")), o), 
                    function(s) {
                        c.merge(r, j(p.substr(p.indexOf(" ") + 1), s))
                    })
                }
                return r
            }
            return function(n, p, o) {
                if (!n || typeof n != "string") {
                    return o || []
                }
                var m = [];
                n = n.replace(rSpace, " ");
                o && c.merge(m, o) && (o.length = 0);
                c.forEach(n.indexOf(",") > 0 ? n.split(rDivider) : [n], 
                function(r) {
                    c.merge(m, j(r, p))
                });
                return c.merge(o || [], c.array(m).unique())
            }
        })();
        c.createChain("dom", 
        function(i, j) {
            var l,
            k = new c.dom.$DOM(j);
            if (!i) {
                return k
            }
            if (i._type_ == "$DOM") {
                return i
            } else {
                if (i.nodeType || i == i.window) {
                    k[0] = i;
                    k.length = 1;
                    return k
                } else {
                    if (i.length && k.toString.call(i) != "[object String]") {
                        return c.merge(k, i)
                    } else {
                        if (typeof i == "string") {
                            if (i.charAt(0) == "<" && i.charAt(i.length - 1) == ">" && i.length > 2) {
                                if (c.dom.createElements) {
                                    c.merge(k, c.dom.createElements(i))
                                }
                            } else {
                                c.query(i, j, k)
                            }
                        } else {
                            if (typeof i == "function") {
                                return k.ready ? k.ready(i) : k
                            }
                        }
                    }
                }
            }
            return k
        },
        function(i) {
            this.length = 0;
            this._type_ = "$DOM";
            this.context = i || document
        }).extend({
            size: function() {
                return this.length
            },
            splice: function() {},
            get: function(i) {
                if (typeof i == "number") {
                    return i < 0 ? this[this.length + i] : this[i]
                }
                return Array.prototype.slice.call(this, 0)
            },
            toArray: function() {
                return this.get()
            }
        });
        c.dom.extend({
            ready: function() {
                var n = false,
                m = [],
                j;
                if (document.addEventListener) {
                    j = function() {
                        document.removeEventListener("DOMContentLoaded", j, false);
                        k()
                    }
                } else {
                    if (document.attachEvent) {
                        j = function() {
                            if (document.readyState === "complete") {
                                document.detachEvent("onreadystatechange", j);
                                k()
                            }
                        }
                    }
                }
                function k() {
                    if (!k.isReady) {
                        k.isReady = true;
                        for (var p = 0, o = m.length; p < o; p++) {
                            m[p]()
                        }
                    }
                }
                function i() {
                    try {
                        document.documentElement.doScroll("left")
                    } catch(o) {
                        setTimeout(i, 1);
                        return
                    }
                    k()
                }
                function l() {
                    if (n) {
                        return
                    }
                    n = true;
                    if (document.readyState === "complete") {
                        k.isReady = true
                    } else {
                        if (document.addEventListener) {
                            document.addEventListener("DOMContentLoaded", j, false);
                            window.addEventListener("load", k, false)
                        } else {
                            if (document.attachEvent) {
                                document.attachEvent("onreadystatechange", j);
                                window.attachEvent("onload", k);
                                var o = false;
                                try {
                                    o = window.frameElement == null
                                } catch(p) {}
                                if (document.documentElement.doScroll && o) {
                                    i()
                                }
                            }
                        }
                    }
                }
                l();
                return function(o) {
                    if (o) {
                        k.isReady ? o() : m.push(o)
                    }
                    return this
                }
            } ()
        });
        c.dom.ready = c.dom.fn.ready;
        c.support = c.support || 
        function() {
            var l = document.createElement("div"),
            k,
            i,
            j;
            l.innerHTML = '<a href="/a" style="top:1px; float: left; opacity: .55">Tangram</a><input type="checkbox">';
            i = l.getElementsByTagName("A")[0];
            j = l.getElementsByTagName("input")[0];
            j.checked = true;
            k = {
                opacity: i.style.opacity === "0.55",
                cssFloat: !!i.style.cssFloat,
                noCloneChecked: j.cloneNode(true).checked,
                noCloneEvent: true
            };
            if (!l.addEventListener && l.attachEvent && l.fireEvent) {
                l.attachEvent("onclick", 
                function() {
                    k.noCloneEvent = false
                });
                l.cloneNode(true).fireEvent("onclick")
            }
            c(function() {
                var p = document.getElementsByTagName("body")[0],
                n = document.createElement("div"),
                m = document.createElement("div"),
                x = "padding: 0; margin: 0; border: ",
                w = "left: 0; top: 0; width: 0px; height: 0px; ",
                s = w + x + "0; visibility: hidden;",
                y = w + x + "5px solid #000; position: absolute;",
                A,
                B,
                u,
                o,
                z;
                n.style.cssText = "position: static;" + s;
                p.insertBefore(n, p.firstChild);
                n.appendChild(m);
                m.style.cssText = "position: absolute;" + s;
                m.innerHTML = '<div style="' + y + 'display: bloack;"><div style="' + x + '0; display: block; overflow: hidden;"></div></div><table style="' + y + '" cellpadding="0" cellspacing="0"><tr><td></td></tr></table>';
                A = m.firstChild;
                B = A.firstChild;
                z = A.nextSibling;
                k.hasBorderWidth = B.offsetTop >= 5;
                k.hasTableCellBorderWidth = z.rows[0].cells[0].offsetTop >= 5;
                B.style.position = "fixed";
                B.style.top = "20px";
                k.fixedPosition = B.offsetTop === 20 || B.offsetTop === 15;
                k.deleteExpando = true;
                try {
                    delete m.test
                } catch(r) {
                    k.deleteExpando = false
                }
                u = document.createElement("select");
                o = u.appendChild(document.createElement("option"));
                u.disabled = true;
                k.optDisabled = !o.disabled;
                k.optSelected = o.selected;
                m.setAttribute("className", "t");
                m.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>";
                var t = m.getElementsByTagName("input")[0];
                k.checkOn = (t.value === "on");
                k.htmlSerialize = !!m.getElementsByTagName("link").length;
                k.leadingWhitespace = (m.firstChild.nodeType === 3);
                k.getSetAttribute = m.className !== "t";
                k.pixelMargin = true;
                m.innerHTML = "";
                m.style.cssText = "box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding:1px;border:1px;display:block;width:4px;margin-top:1%;position:absolute;top:1%;";
                if (window.getComputedStyle) {
                    k.pixelMargin = (window.getComputedStyle(m, null) || {}).marginTop !== "1%"
                }
                var t = document.createElement("input");
                t.value = "t";
                t.setAttribute("type", "radio");
                k.radioValue = t.value === "t";
                k.hrefNormalized = (i.getAttribute("href") === "/a");
                k.style = /top/.test(i.getAttribute("style"));
                k.enctype = !!document.createElement("form").enctype;
                p.removeChild(n);
                n = m = A = B = z = null
            });
            return k
        } ();
        c.createChain("event", 
        function() {
            var i = {};
            return function(k, j) {
                switch (c.type(k)) {
                case "object":
                    return i.originalEvent === k ? i: (i = new c.event.$Event(k));
                case "$Event":
                    return k;
                case "string":
                    var l = new c.event.$Event(k);
                    typeof j == "object" && c.forEach(l, j);
                    return l
                }
            }
        } (), 
        function(l) {
            var o,
            j,
            m;
            var k = this;
            this._type_ = "$Event";
            if (typeof l == "object" && l.type) {
                k.originalEvent = o = l;
                c.forEach("altKey bubbles button buttons cancelable clientX clientY ctrlKey commandKey currentTarget fromElement metaKey screenX screenY shiftKey toElement type view which triggerData".split(" "), 
                function(p) {
                    k[p] = o[p]
                });
                k.target = k.srcElement = o.srcElement || ((j = o.target) && (j.nodeType == 3 ? j.parentNode: j));
                k.relatedTarget = o.relatedTarget || ((j = o.fromElement) && (j === k.target ? o.toElement: j));
                k.keyCode = k.which = o.keyCode || o.which;
                if (!k.which && o.button !== undefined) {
                    k.which = o.button & 1 ? 1: (o.button & 2 ? 3: (o.button & 4 ? 2: 0))
                }
                var n = document.documentElement,
                i = document.body;
                k.pageX = o.pageX || (o.clientX + (n && n.scrollLeft || i && i.scrollLeft || 0) - (n && n.clientLeft || i && i.clientLeft || 0));
                k.pageY = o.pageY || (o.clientY + (n && n.scrollTop || i && i.scrollTop || 0) - (n && n.clientTop || i && i.clientTop || 0));
                k.data
            }
            typeof l == "string" && (this.type = l);
            this.timeStamp = new Date().getTime()
        }).extend({
            stopPropagation: function() {
                var i = this.originalEvent;
                i && (i.stopPropagation ? i.stopPropagation() : i.cancelBubble = true)
            },
            preventDefault: function() {
                var i = this.originalEvent;
                i && (i.preventDefault ? i.preventDefault() : i.returnValue = false)
            }
        });
        c.dom.extend({
            each: function(l) {
                c.check("function", "baidu.dom.each");
                var k,
                j,
                m = this.length;
                for (k = 0; k < m; k++) {
                    j = l.call(this[k], k, this[k], this);
                    if (j === false || j == "break") {
                        break
                    }
                }
                return this
            }
        });
        c.dom.match = function() {
            var j = /^[\w\#\-\$\.\*]+$/,
            k = document.createElement("DIV");
            k.id = "__tangram__";
            return function(t, m, r) {
                var n,
                p = c.array();
                switch (c.type(m)) {
                case "$DOM":
                    for (var l = t.length - 1; l > -1; l--) {
                        for (var s = m.length - 1; s > -1; s--) {
                            t[l] === m[s] && p.push(t[l])
                        }
                    }
                    break;
                case "function":
                    c.forEach(t, 
                    function(w, u) {
                        m.call(w, u) && p.push(w)
                    });
                    break;
                case "HTMLElement":
                    c.forEach(t, 
                    function(u) {
                        u == m && p.push(u)
                    });
                    break;
                case "string":
                    var o = c.query(m, r || document);
                    c.forEach(t, 
                    function(x) {
                        if (n = i(x)) {
                            var w = n.nodeType == 1 ? c.query(m, n) : o;
                            for (var u = 0, y = w.length; u < y; u++) {
                                if (w[u] === x) {
                                    p.push(x);
                                    break
                                }
                            }
                        }
                    });
                    p = p.unique();
                    break;
                default:
                    p = c.array(t).unique();
                    break
                }
                return p
            };
            function i(n) {
                var l = [],
                m;
                while (n = n.parentNode) {
                    n.nodeType && l.push(n)
                }
                for (var m = l.length - 1; m > -1; m--) {
                    if (l[m].nodeType == 1 || l[m].nodeType == 9) {
                        return l[m]
                    }
                }
                return null
            }
        } ();
        c.dom.extend({
            is: function(i) {
                return c.dom.match(this, i).length > 0
            }
        });
        c.dom.extend({
            triggerHandler: function(k, j) {
                var i = c._util_.eventBase;
                c.forEach(this, 
                function(l) {
                    i.fireHandler(l, k, j)
                });
                return this
            }
        });
        c.dom._g = function(i) {
            return c.type(i) === "string" ? document.getElementById(i) : i
        };
        c.dom.extend({
            contains: function(j) {
                j = c.dom(j);
                if (this.size() <= 0 || j.size() <= 0) {
                    return false
                }
                var i = this[0];
                j = j[0];
                return i.contains ? i != j && i.contains(j) : !!(i.compareDocumentPosition(j) & 16)
            }
        });
        c.dom.contains = function(i, j) {
            var k = c.dom._g;
            i = k(i);
            j = k(j);
            return i.contains ? i != j && i.contains(j) : !!(i.compareDocumentPosition(j) & 16)
        };
        c.dom.extend({
            closest: function(i, k) {
                var j = c.array();
                c.forEach(this, 
                function(m) {
                    var l = [m];
                    while (m = m.parentNode) {
                        m.nodeType && l.push(m)
                    }
                    l = c.dom.match(l, i, k);
                    l.length && j.push(l[0])
                });
                return c.dom(j.unique())
            }
        });
        c._util_.eventBase = function() {
            var i = {};
            var m = {};
            var s = window.addEventListener ? 
            function(w, t, u) {
                w.addEventListener(t, u, false)
            }: window.attachEvent ? 
            function(w, t, u) {
                w.attachEvent("on" + t, u)
            }: function() {};
            var r = function(x, t, w) {
                var z = c.id(x);
                var y = m[z] = m[z] || {};
                if (y[t]) {
                    return
                }
                y[t] = 1;
                var u = function(D) {
                    var B = Array.prototype.slice.call(arguments, 1);
                    B.unshift(D = c.event(D));
                    if (!D.currentTarget) {
                        D.currentTarget = x
                    }
                    for (var C = 0, A = w.length; C < A; C += 2) {
                        w[C].apply(this, B)
                    }
                };
                s(x, t, u)
            };
            var k = function(z, u, B, x, y) {
                var C = function(E) {
                    var D = c.dom(E.target);
                    if (y && !E.data) {
                        E.data = y
                    }
                    if (E.triggerData) { [].push.apply(arguments, E.triggerData)
                    }
                    if (!x) {
                        return E.result = B.apply(z, arguments)
                    }
                    if (D.is(x) || D.is(x + " *")) {
                        return E.result = B.apply(c.dom(E.target).closest(x)[0], arguments)
                    }
                };
                var w = c.id(z);
                var A = i[w] || (i[w] = {});
                var t = A[u] || (A[u] = []);
                t.push(C, B);
                r(z, u, t);
                return C
            };
            var p = function(A, t, D, x) {
                var u;
                if (! (u = c.id(A, "get"))) {
                    return
                }
                var C = i[u] || (i[u] = {});
                var B = {
                    mouseenter: "mouseover",
                    mouseleave: "mouseout",
                    focusin: "focus",
                    focusout: "blur"
                };
                var w = C[t] || (C[t] = []);
                if (B[t]) {
                    C[B[t]] = []
                }
                for (var y = w.length - 1, z; y >= 0; y--) {
                    if (z = w[y], z === D) {
                        w.splice(y - 1, 2)
                    }
                }
            };
            var l = function(x, w) {
                var u;
                if (! (u = c.id(x, "get"))) {
                    return
                }
                var y = i[u] || (i[u] = {});
                var t = function(A) {
                    var z = y[A] || (y[A] = []);
                    for (var B = z.length - 1, C; B >= 0; B -= 2) {
                        C = z[B],
                        p(x, A, C)
                    }
                };
                if (w) {
                    t(w)
                } else {
                    for (var w in y) {
                        t(w)
                    }
                }
            };
            var j = function(B, w, u) {
                var x;
                if (! (x = c.id(B, "get"))) {
                    return
                }
                var D = i[x] || (i[x] = {});
                var y = D[w] || (D[w] = []);
                var t = c.event({
                    type: w
                });
                var C = [t];
                if (u) {
                    t.triggerData = u,
                    C.push.apply(C, u)
                }
                for (var A = 0, z = y.length; A < z; A += 2) {
                    y[A].apply(this, C)
                }
            };
            var o = function(A) {
                var w;
                if (! (w = c.id(A, "get"))) {
                    return
                }
                var B = i[w] || (i[w] = {});
                var x = {},
                t;
                for (var z in B) {
                    t = x[z] = [];
                    ce = B[z];
                    for (var y = 1, u = ce.length; y < u; y += 2) {
                        t.push(ce[y])
                    }
                }
                return x
            };
            var n = function(w) {
                switch (w) {
                case "focusin":
                case "focusout":
                    if (!/firefox/i.test(navigator.userAgent)) {
                        return false
                    }
                    var u = {},
                    t = w == "focusin" ? "focus": "blur";
                    u[w] = function(B, z) {
                        if (typeof B == "function") {
                            z = B,
                            B = null
                        }
                        var A = this;
                        if (!z) {
                            return this.triggerHandler(w, B)
                        } else {
                            var y = function() {
                                A.triggerHandler(w)
                            };
                            c.forEach(this, 
                            function(C) {
                                c("textarea,select,input,button,a", C).on(t, y)
                            });
                            return this._on(w, B, z),
                            this
                        }
                    };
                    return c.dom.extend(u),
                    true;
                case "mouseenter":
                case "mouseleave":
                    if (/msie/i.test(navigator.userAgent)) {
                        return false
                    }
                    var u = {},
                    t = w == "mouseenter" ? "mouseover": "mouseout";
                    var x = c.dom.contains;
                    u[w] = function(B, z) {
                        if (arguments.length == 0) {
                            return this.trigger(w)
                        }
                        if (typeof B == "function") {
                            z = B,
                            B = null
                        }
                        var A = this;
                        var y = function(C) {
                            related = C.relatedTarget;
                            if (!related || (related !== this && !x(this, related))) {
                                A.triggerHandler(w)
                            }
                        };
                        c.forEach(this, 
                        function(C) {
                            this.on(t, y)
                        },
                        this);
                        return this._on(w, B, z),
                        this
                    };
                    return c.dom.extend(u),
                    true
                }
                return false
            };
            return {
                add: function(y, w, u, t, x) {
                    return k(y, w, u, t, x)
                },
                get: function(t) {
                    return o(t)
                },
                remove: function(x, w, u, t) {
                    var y;
                    if ((y = c.id(x, "get")) && u && u["_" + y + "_" + w]) {
                        u = u["_" + y + "_" + w],
                        delete u["_" + y + "_" + w]
                    }
                    if (typeof u == "function") {
                        return p(x, w, u, t)
                    } else {
                        return l(x, w, t)
                    }
                },
                removeAll: function(t) {
                    return l(t)
                },
                fireHandler: function(w, u, t) {
                    return j(w, u, t)
                },
                method: function(w) {
                    if (arguments.length > 1) {
                        for (var x = 0, t = arguments.length; x < t; x++) {
                            this.method(arguments[x])
                        }
                        return this
                    }
                    if (!n(w)) {
                        var u = {};
                        u[w] = function(z, y) {
                            if (arguments.length == 0) {
                                return this.trigger(w)
                            } else {
                                if (typeof z == "function") {
                                    y = z,
                                    z = null
                                }
                                return this._on(w, z, y)
                            }
                        };
                        c.dom.extend(u)
                    }
                },
                _getEventsLength: function(y, z) {
                    var t = 0,
                    x;
                    if (y) {
                        x = i[c.id(y[0] || y, "get")];
                        if (z) {
                            x[z] && (t = x[z].length)
                        } else {
                            for (var w in x) {
                                t += x[w].length
                            }
                        }
                    } else {
                        for (var w in i) {
                            x = i[w];
                            for (var u in x) {
                                t += x[u].length
                            }
                        }
                    }
                    return t / 2
                }
            }
        } ();
        c._util_.eventBase.method("blur", "change", "click", "dblclick", "error", "focus", "focusin", "focusout", "keydown", "keypress", "keyup", "mousedown", "mouseenter", "mouseleave", "mousemove", "mouseout", "mouseover", "mouseup", "resize", "scroll", "select", "submit", "load", "unload", "contextmenu");
        c.dom.g = function(i) {
            if (!i) {
                return null
            }
            if ("string" == typeof i || i instanceof String) {
                return document.getElementById(i)
            } else {
                if (i.nodeName && (i.nodeType == 1 || i.nodeType == 9)) {
                    return i
                }
            }
            return null
        };
        c.dom.extend({
            on: function(l, i, n, m) {
                var k = c._util_.eventBase;
                var j = {
                    mouseenter: 1,
                    mouseleave: 1,
                    focusin: 1,
                    focusout: 1
                };
                if (typeof i == "object" && i) {
                    m = n,
                    n = i,
                    i = null
                } else {
                    if (typeof n == "function") {
                        m = n,
                        n = null
                    } else {
                        if (typeof i == "function") {
                            m = i,
                            i = n = null
                        }
                    }
                }
                if (typeof l == "string") {
                    l = l.split(/[ ,]+/);
                    this.each(function() {
                        c.forEach(l, 
                        function(o) {
                            if (j[o]) {
                                c(this)[o](n, m)
                            } else {
                                k.add(this, o, m, i, n)
                            }
                        },
                        this)
                    })
                } else {
                    if (typeof l == "object") {
                        if (m) {
                            m = null
                        }
                        c.forEach(l, 
                        function(p, o) {
                            this.on(o, i, n, p)
                        },
                        this)
                    }
                }
                return this
            },
            _on: function(i, l, k) {
                var j = c._util_.eventBase;
                this.each(function() {
                    j.add(this, i, k, undefined, l)
                });
                return this
            }
        });
        c.event.on = c.on = function(i, k, j) {
            i = c.dom.g(i);
            c.dom(i).on(k.replace(/^\s*on/, ""), j);
            return i
        };
        void
        function() {
            var E = location ? location.href: document.location.href,
            Q = /^(?:about|app|app\-storage|.+\-extension|file|res|widget):$/,
            R = /^\/\//,
            S = /^([\w\+\.\-]+:)(?:\/\/([^\/?#:]*)(?::(\d+)|)|)/,
            D = /#.*$/,
            B = /\[\]$/,
            z = /^(?:GET|HEAD)$/,
            K = /([?&])_=[^&]*/,
            F = /^(.*?):[ \t]*([^\r\n]*)\r?$/mg,
            P = /^[\],:{}\s]*$/,
            n = /(?:^|:|,)(?:\s*\[)+/g,
            i = /\\(?:["\\\/bfnrt]|u[\da-fA-F]{4})/g,
            o = /"[^"\\\r\n]*"|true|false|null|-?(?:\d\d*\.|)\d+(?:[eE][\-+]?\d+|)/g,
            m = ["*/"] + ["*"],
            l = {},
            C = {},
            I = {},
            s = {},
            O = S.exec(E.toLowerCase()) || [];
            function J(X) {
                var V,
                W;
                if (!X || c.type(X) !== "string") {
                    return null
                }
                try {
                    if (window.DOMParser) {
                        W = new DOMParser();
                        V = W.parseFromString(X, "text/xml")
                    } else {
                        V = new ActiveXObject("Microsoft.XMLDOM");
                        V.async = "false";
                        V.loadXML(X)
                    }
                } catch(Y) {
                    V = undefined
                }
                if (!V || !V.documentElement || V.getElementsByTagName("parsererror").length) {
                    throw new Error("Invalid XML: " + X)
                }
                return V
            }
            function A(V) {
                if (!V || c.type(V) !== "string") {
                    return null
                }
                V = c.string(V).trim();
                if (window.JSON && window.JSON.parse) {
                    return window.JSON.parse(V)
                }
                if (P.test(V.replace(i, "@").replace(o, "]").replace(n, ""))) {
                    return (new Function("return " + V))()
                }
                throw new Error("Invalid JSON: " + V)
            }
            function G(V) {
                if (V && /\S/.test(V)) { (window.execScript || 
                    function(W) {
                        window["eval"].call(window, W)
                    })(V)
                }
            }
            function L(V) {
                return function(ab, Z) {
                    if (c.type(ab) !== "string") {
                        Z = ab;
                        ab = "*"
                    }
                    var X = ab.toLowerCase().split(/\s+/),
                    aa,
                    ac;
                    if (c.type(Z) === "function") {
                        for (var W = 0, Y; Y = X[W]; W++) {
                            aa = /^\+/.test(Y);
                            aa && (Y = Y.substr(1) || "*");
                            ac = V[Y] = V[Y] || [];
                            ac[aa ? "unshift": "push"](Z)
                        }
                    }
                }
            }
            function u(V, ae, ab) {
                var aa,
                ac,
                Z,
                W,
                X = V.contents,
                ad = V.dataTypes,
                Y = V.responseFields;
                for (ac in Y) {
                    if (ac in ab) {
                        ae[Y[ac]] = ab[ac]
                    }
                }
                while (ad[0] === "*") {
                    ad.shift();
                    if (aa === undefined) {
                        aa = V.mimeType || ae.getResponseHeader("content-type")
                    }
                }
                if (aa) {
                    for (ac in X) {
                        if (X[ac] && X[ac].test(aa)) {
                            ad.unshift(ac);
                            break
                        }
                    }
                }
                if (ad[0] in ab) {
                    Z = ad[0]
                } else {
                    for (ac in ab) {
                        if (!ad[0] || V.converters[ac + " " + ad[0]]) {
                            Z = ac;
                            break
                        }
                        if (!W) {
                            W = ac
                        }
                    }
                    Z = Z || W
                }
                if (Z) {
                    if (Z !== ad[0]) {
                        ad.unshift(Z)
                    }
                    return ab[Z]
                }
            }
            function N(V, X) {
                var ab = V.dataTypes.slice(),
                W = ab[0],
                ae = {},
                ad,
                aa;
                V.dataFilter && (X = V.dataFilter(X, V.dataType));
                if (ab[1]) {
                    for (var Y in V.converters) {
                        ae[Y.toLowerCase()] = V.converters[Y]
                    }
                }
                for (var Y = 0, af; af = ab[++Y];) {
                    if (af !== "*") {
                        if (W !== "*" && W !== af) {
                            ad = ae[W + " " + af] || ae["* " + af];
                            if (!ad) {
                                for (var ac in ae) {
                                    aa = ac.split(" ");
                                    if (aa[1] === af) {
                                        ad = ae[W + " " + aa[0]] || ae["* " + aa[0]];
                                        if (ad) {
                                            if (ad === true) {
                                                ad = ae[ac]
                                            } else {
                                                if (ae[ac] !== true) {
                                                    af = aa[0];
                                                    ab.splice(Y--, 0, af)
                                                }
                                            }
                                            break
                                        }
                                    }
                                }
                            }
                            if (ad !== true) {
                                if (ad && V["throws"]) {
                                    X = ad(X)
                                } else {
                                    try {
                                        X = ad(X)
                                    } catch(Z) {
                                        return {
                                            state: "parsererror",
                                            error: ad ? Z: "No conversion from " + W + " to " + af
                                        }
                                    }
                                }
                            }
                        }
                        W = af
                    }
                }
                return {
                    state: "success",
                    data: X
                }
            }
            function r(W, af, aa, ad, ac, Y) {
                ac = ac || af.dataTypes[0];
                Y = Y || {};
                Y[ac] = true;
                var ae,
                ab = W[ac],
                V = ab ? ab.length: 0,
                Z = (W === l);
                for (var X = 0; X < V && (Z || !ae); X++) {
                    ae = ab[X](af, aa, ad);
                    if (typeof ae === "string") {
                        if (!Z || Y[ae]) {
                            ae = undefined
                        } else {
                            af.dataTypes.unshift(ae);
                            ae = r(W, af, aa, ad, ae, Y)
                        }
                    }
                }
                if ((Z || !ae) && !Y["*"]) {
                    ae = r(W, af, aa, ad, "*", Y)
                }
                return ae
            }
            c.createChain("ajax", 
            function(Z, X) {
                if (c.object.isPlain(Z)) {
                    X = Z;
                    Z = undefined
                }
                X = X || {};
                var ae = c.ajax.setup({},
                X),
                at = ae.context || ae,
                W,
                ad,
                ai,
                ar = c.Deferred(),
                am = c.Callbacks("once memory"),
                ab = ae.statusCode || {},
                aa = 0,
                an = {},
                ah = {},
                ac = "canceled",
                ap,
                Y,
                ak,
                aq = c.extend(new c.ajax.$Ajax(Z, ae), {
                    readyState: 0,
                    setRequestHeader: function(av, aw) {
                        if (!aa) {
                            var au = av.toLowerCase();
                            av = an[au] = an[au] || av;
                            ah[av] = aw
                        }
                    },
                    getAllResponseHeaders: function() {
                        return aa === 2 ? ap: null
                    },
                    getResponseHeader: function(av) {
                        var au;
                        if (aa === 2) {
                            if (!Y) {
                                Y = {};
                                while (au = F.exec(ap)) {
                                    Y[au[1].toLowerCase()] = au[2]
                                }
                            }
                            au = Y[av.toLowerCase()]
                        }
                        return au === undefined ? null: au
                    },
                    overrideMimeType: function(au) { ! aa && (ae.mimeType = au);
                        return this
                    },
                    abort: function(au) {
                        au = au || ac;
                        ak && ak.abort(au);
                        ag(0, au);
                        return this
                    }
                });
                var af;
                function ag(az, av, aA, aw) {
                    var ax = av,
                    au,
                    aD,
                    aB,
                    ay,
                    aC;
                    if (aa === 2) {
                        return
                    }
                    aa = 2;
                    af && clearTimeout(af);
                    ak = undefined;
                    ap = aw || "";
                    aq.readyState = az > 0 ? 4: 0;
                    aA && (ay = u(ae, aq, aA));
                    if (az >= 200 && az < 300 || az === 304) {
                        if (ae.ifModified) {
                            aC = aq.getResponseHeader("Last-Modified");
                            aC && (I[ad] = aC);
                            aC = aq.getResponseHeader("Etag");
                            aC && (s[ad] = aC)
                        }
                        if (az === 304) {
                            ax = "notmodified";
                            au = true
                        } else {
                            au = N(ae, ay);
                            ax = au.state;
                            aD = au.data;
                            aB = au.error;
                            au = !aB
                        }
                    } else {
                        aB = ax;
                        if (!ax || az) {
                            ax = "error";
                            az < 0 && (az = 0)
                        }
                    }
                    aq.status = az;
                    aq.statusText = "" + (av || ax);
                    if (au) {
                        ar.resolveWith(at, [aD, ax, aq])
                    } else {
                        ar.rejectWith(at, [aq, ax, aB])
                    }
                    aq.statusCode(ab);
                    ab = undefined;
                    am.fireWith(at, [aq, ax])
                }
                ar.promise(aq);
                aq.success = aq.done;
                aq.error = aq.fail;
                aq.complete = am.add;
                aq.statusCode = function(av) {
                    if (av) {
                        if (aa < 2) {
                            for (var au in av) {
                                ab[au] = [ab[au], av[au]]
                            }
                        } else {
                            aq.always(av[aq.status])
                        }
                    }
                    return this
                };
                ae.url = ((Z || ae.url) + "").replace(D, "").replace(R, O[1] + "//");
                ae.dataTypes = c.string(ae.dataType || "*").trim().toLowerCase().split(/\s+/);
                if (ae.crossDomain == null) {
                    ai = S.exec(ae.url.toLowerCase());
                    ae.crossDomain = !!(ai && (ai[1] != O[1] || ai[2] != O[2] || (ai[3] || (ai[1] === "http:" ? 80: 443)) != (O[3] || (O[1] === "http:" ? 80: 443))))
                }
                if (ae.data && ae.processData && c.type(ae.data) !== "string") {
                    ae.data = c.ajax.param(ae.data, ae.traditional)
                }
                r(l, ae, X, aq);
                if (aa === 2) {
                    return ""
                }
                W = ae.global;
                ae.type = ae.type.toUpperCase();
                ae.hasContent = !z.test(ae.type);
                if (!ae.hasContent) {
                    if (ae.data) {
                        ae.url += (~ae.url.indexOf("?") ? "&": "?") + ae.data;
                        delete ae.data
                    }
                    ad = ae.url;
                    if (ae.cache === false) {
                        var V = new Date().getTime(),
                        ao = ae.url.replace(K, "$1_=" + V);
                        ae.url = ao + (ao === ae.url ? (~ae.url.indexOf("?") ? "&": "?") + "_=" + V: "")
                    }
                }
                if (ae.data && ae.hasContent && ae.contentType !== false || X.contentType) {
                    aq.setRequestHeader("Content-Type", ae.contentType)
                }
                if (ae.ifModified) {
                    ad = ad || ae.url;
                    I[ad] && aq.setRequestHeader("If-Modified-Since", I[ad]);
                    s[ad] && aq.setRequestHeader("If-None-Match", s[ad])
                }
                aq.setRequestHeader("Accept", ae.dataTypes[0] && ae.accepts[ae.dataTypes[0]] ? ae.accepts[ae.dataTypes[0]] + (ae.dataTypes[0] !== "*" ? ", " + m + "; q=0.01": "") : ae.accepts["*"]);
                for (var aj in ae.headers) {
                    aq.setRequestHeader(aj, ae.headers[aj])
                }
                if (ae.beforeSend && (ae.beforeSend.call(at, aq, ae) === false || aa === 2)) {
                    return aq.abort()
                }
                ac = "abort";
                for (var aj in {
                    success: 1,
                    error: 1,
                    complete: 1
                }) {
                    aq[aj](ae[aj])
                }
                ak = r(C, ae, X, aq);
                if (!ak) {
                    ag( - 1, "No Transport")
                } else {
                    aq.readyState = 1;
                    if (ae.async && ae.timeout > 0) {
                        af = setTimeout(function() {
                            aq.abort("timeout")
                        },
                        ae.timeout)
                    }
                    try {
                        aa = 1;
                        ak.send(ah, ag)
                    } catch(al) {
                        if (aa < 2) {
                            ag( - 1, al)
                        } else {
                            throw al
                        }
                    }
                }
                return aq
            },
            function(W, V) {
                this.url = W;
                this.options = V
            });
            c.ajax.settings = {
                url: E,
                isLocal: Q.test(O[1]),
                global: true,
                type: "GET",
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                processData: true,
                async: true,
                accepts: {
                    xml: "application/xml, text/xml",
                    html: "text/html",
                    text: "text/plain",
                    json: "application/json, text/javascript",
                    "*": m
                },
                contents: {
                    xml: /xml/,
                    html: /html/,
                    json: /json/
                },
                responseFields: {
                    xml: "responseXML",
                    text: "responseText"
                },
                converters: {
                    "* text": window.String,
                    "text html": true,
                    "text json": A,
                    "text xml": J
                },
                flatOptions: {
                    context: true,
                    url: true
                }
            };
            function U(X, Z) {
                var Y = c.ajax.settings.flatOptions || {},
                V;
                for (var W in Z) {
                    if (Z[W] !== undefined) { (Y[W] ? X: (V || (V = {})))[W] = Z[W]
                    }
                }
                V && c.extend(true, X, V)
            }
            c.ajax.setup = function(W, V) {
                if (V) {
                    U(W, c.ajax.settings)
                } else {
                    V = W;
                    W = c.ajax.settings
                }
                U(W, V);
                return W
            };
            function y(X, V, W) {
                W = c.type(W) === "function" ? W() : (W || "");
                X.push(encodeURIComponent(V) + "=" + encodeURIComponent(W))
            }
            function j(Z, W, Y, X) {
                if (c.type(Y) === "array") {
                    c.forEach(Y, 
                    function(ab, aa) {
                        if (X || B.test(W)) {
                            y(Z, W, ab)
                        } else {
                            j(Z, W + "[" + (typeof ab === "object" ? aa: "") + "]", ab, X)
                        }
                    })
                } else {
                    if (!X && c.type(Y) === "object") {
                        for (var V in Y) {
                            j(Z, W + "[" + V + "]", Y[V], X)
                        }
                    } else {
                        y(Z, W, Y)
                    }
                }
            }
            c.ajax.param = function(Y, X) {
                var V = [];
                if (c.type(Y) === "array") {
                    c.forEach(Y, 
                    function(Z) {
                        y(V, Z.name, Z.value)
                    })
                } else {
                    for (var W in Y) {
                        j(V, W, Y[W], X)
                    }
                }
                return V.join("&").replace(/%20/g, "+")
            };
            c.ajax.prefilter = L(l);
            c.ajax.transport = L(C);
            var p = [],
            k = /(=)\?(?=&|$)|\?\?/,
            w = new Date().getTime();
            c.ajax.setup({
                jsonp: "callback",
                jsonpCallback: function() {
                    var V = p.pop() || (c.id.key + "_" + (w++));
                    this[V] = true;
                    return V
                }
            });
            c.ajax.prefilter("json jsonp", 
            function(V, ab, af) {
                var ae,
                W,
                ad,
                Z = V.data,
                X = V.url,
                Y = V.jsonp !== false,
                ac = Y && k.test(X),
                aa = Y && !ac && c.type(Z) === "string" && !(V.contentType || "").indexOf("application/x-www-form-urlencoded") && k.test(Z);
                if (V.dataTypes[0] === "jsonp" || ac || aa) {
                    ae = V.jsonpCallback = c.type(V.jsonpCallback) === "function" ? V.jsonpCallback() : V.jsonpCallback;
                    W = window[ae];
                    if (ac) {
                        V.url = X.replace(k, "$1" + ae)
                    } else {
                        if (aa) {
                            V.data = Z.replace(k, "$1" + ae)
                        } else {
                            if (Y) {
                                V.url += (/\?/.test(X) ? "&": "?") + V.jsonp + "=" + ae
                            }
                        }
                    }
                    V.converters["script json"] = function() {
                        return ad[0]
                    };
                    V.dataTypes[0] = "json";
                    window[ae] = function() {
                        ad = arguments
                    };
                    af.always(function() {
                        window[ae] = W;
                        if (V[ae]) {
                            V.jsonpCallback = ab.jsonpCallback;
                            p.push(ae)
                        }
                        if (ad && c.type(W) === "function") {
                            W(ad[0])
                        }
                        ad = W = undefined
                    });
                    return "script"
                }
            });
            c.ajax.setup({
                accepts: {
                    script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
                },
                contents: {
                    script: /javascript|ecmascript/
                },
                converters: {
                    "text script": function(V) {
                        G(V);
                        return V
                    }
                }
            });
            c.ajax.prefilter("script", 
            function(V) {
                V.cache === undefined && (V.cache = false);
                if (V.crossDomain) {
                    V.type = "GET";
                    V.global = false
                }
            });
            c.ajax.transport("script", 
            function(X) {
                if (X.crossDomain) {
                    var V,
                    W = document.head || document.getElementsByTagName("head")[0] || document.documentElement;
                    return {
                        send: function(Y, Z) {
                            V = document.createElement("script");
                            V.async = "async";
                            X.scriptCharset && (V.charset = X.scriptCharset);
                            V.src = X.url;
                            V.onload = V.onreadystatechange = function(aa, ab) {
                                if (ab || !V.readyState || /loaded|complete/.test(V.readyState)) {
                                    V.onload = V.onreadystatechange = null;
                                    W && V.parentNode && W.removeChild(V);
                                    V = undefined; ! ab && Z(200, "success")
                                }
                            };
                            W.insertBefore(V, W.firstChild)
                        },
                        abort: function() {
                            V && V.onload(0, 1)
                        }
                    }
                }
            });
            var t,
            M = 0,
            x = window.ActiveXObject ? 
            function() {
                for (var V in t) {
                    t[V](0, 1)
                }
            }: false;
            function H() {
                try {
                    return new window.XMLHttpRequest()
                } catch(V) {}
            }
            function T() {
                try {
                    return new window.ActiveXObject("Microsoft.XMLHTTP")
                } catch(V) {}
            }
            c.ajax.settings.xhr = window.ActiveXObject ? 
            function() {
                return ! this.isLocal && H() || T()
            }: H;
            void
            function(V) {
                c.extend(c.support, {
                    ajax: !!V,
                    cors: !!V && ("withCredentials" in V)
                })
            } (c.ajax.settings.xhr());
            if (c.support.ajax) {
                c.ajax.transport(function(V) {
                    if (!V.crossDomain || c.support.cors) {
                        var W;
                        return {
                            send: function(ac, X) {
                                var Z,
                                ab = V.xhr();
                                if (V.username) {
                                    ab.open(V.type, V.url, V.async, V.username, V.password)
                                } else {
                                    ab.open(V.type, V.url, V.async)
                                }
                                if (V.xhrFields) {
                                    for (var Y in V.xhrFields) {
                                        ab[Y] = V.xhrFields[Y]
                                    }
                                }
                                if (V.mimeType && ab.overrideMimeType) {
                                    ab.overrideMimeType(V.mimeType)
                                }
                                if (!V.crossDomain && !ac["X-Requested-With"]) {
                                    ac["X-Requested-With"] = "XMLHttpRequest"
                                }
                                try {
                                    for (var Y in ac) {
                                        ab.setRequestHeader(Y, ac[Y])
                                    }
                                } catch(aa) {}
                                ab.send((V.hasContent && V.data) || null);
                                W = function(al, af) {
                                    var ag,
                                    ae,
                                    ad,
                                    aj,
                                    ai;
                                    try {
                                        if (W && (af || ab.readyState === 4)) {
                                            W = undefined;
                                            if (Z) {
                                                ab.onreadystatechange = function() {};
                                                x && (delete t[Z])
                                            }
                                            if (af) {
                                                ab.readyState !== 4 && ab.abort()
                                            } else {
                                                ag = ab.status;
                                                ad = ab.getAllResponseHeaders();
                                                aj = {};
                                                ai = ab.responseXML;
                                                ai && ai.documentElement && (aj.xml = ai);
                                                try {
                                                    aj.text = ab.responseText
                                                } catch(ak) {}
                                                try {
                                                    ae = ab.statusText
                                                } catch(ak) {
                                                    ae = ""
                                                }
                                                if (!ag && V.isLocal && !V.crossDomain) {
                                                    ag = aj.text ? 200: 404
                                                } else {
                                                    if (ag === 1223) {
                                                        ag = 204
                                                    }
                                                }
                                            }
                                        }
                                    } catch(ah) { ! af && X( - 1, ah)
                                    }
                                    aj && X(ag, ae, aj, ad)
                                };
                                if (!V.async) {
                                    W()
                                } else {
                                    if (ab.readyState === 4) {
                                        setTimeout(W, 0)
                                    } else {
                                        Z = ++M;
                                        if (x) {
                                            if (!t) {
                                                t = {};
                                                c.dom(window).on("unload", x)
                                            }
                                            t[Z] = W
                                        }
                                        ab.onreadystatechange = W
                                    }
                                }
                            },
                            abort: function() {
                                W && W(0, 1)
                            }
                        }
                    }
                })
            }
        } ();
        c.createChain("fn", 
        function(i) {
            return new c.fn.$Fn(~"function|string".indexOf(c.type(i)) ? i: function() {})
        },
        function(i) {
            this.fn = i
        });
        c.fn.blank = function() {};
        c.ajax.request = function(n, s) {
            var l = s || {},
            A = l.data || "",
            o = !(l.async === false),
            m = l.username || "",
            i = l.password || "",
            k = (l.method || "GET").toUpperCase(),
            j = l.headers || {},
            r = l.timeout || 0,
            t = {},
            x,
            B,
            p;
            function w() {
                if (p.readyState == 4) {
                    try {
                        var D = p.status
                    } catch(C) {
                        z("failure");
                        return
                    }
                    z(D);
                    if ((D >= 200 && D < 300) || D == 304 || D == 1223) {
                        z("success")
                    } else {
                        z("failure")
                    }
                    window.setTimeout(function() {
                        p.onreadystatechange = c.fn.blank;
                        if (o) {
                            p = null
                        }
                    },
                    0)
                }
            }
            function u() {
                if (window.ActiveXObject) {
                    try {
                        return new ActiveXObject("Msxml2.XMLHTTP")
                    } catch(C) {
                        try {
                            return new ActiveXObject("Microsoft.XMLHTTP")
                        } catch(C) {}
                    }
                }
                if (window.XMLHttpRequest) {
                    return new XMLHttpRequest()
                }
            }
            function z(E) {
                E = "on" + E;
                var D = t[E],
                F = c.ajax[E];
                if (D) {
                    if (x) {
                        clearTimeout(x)
                    }
                    if (E != "onsuccess") {
                        D(p)
                    } else {
                        try {
                            p.responseText
                        } catch(C) {
                            return D(p)
                        }
                        D(p, p.responseText)
                    }
                } else {
                    if (F) {
                        if (E == "onsuccess") {
                            return
                        }
                        F(p)
                    }
                }
            }
            for (B in l) {
                t[B] = l[B]
            }
            j["X-Requested-With"] = "XMLHttpRequest";
            try {
                p = u();
                if (k == "GET") {
                    if (A) {
                        n += (n.indexOf("?") >= 0 ? "&": "?") + A;
                        A = null
                    }
                    if (l.noCache) {
                        n += (n.indexOf("?") >= 0 ? "&": "?") + "b" + ( + new Date) + "=1"
                    }
                }
                if (m) {
                    p.open(k, n, o, m, i)
                } else {
                    p.open(k, n, o)
                }
                if (o) {
                    p.onreadystatechange = w
                }
                if (k == "POST") {
                    p.setRequestHeader("Content-Type", (j["Content-Type"] || "application/x-www-form-urlencoded"))
                }
                for (B in j) {
                    if (j.hasOwnProperty(B)) {
                        p.setRequestHeader(B, j[B])
                    }
                }
                z("beforerequest");
                if (r) {
                    x = setTimeout(function() {
                        p.onreadystatechange = c.fn.blank;
                        p.abort();
                        z("timeout")
                    },
                    r)
                }
                p.send(A);
                if (!o) {
                    w()
                }
            } catch(y) {
                z("failure")
            }
            return p
        };
        c.url = c.url || {};
        c.url.escapeSymbol = function(i) {
            return String(i).replace(/[#%&+=\/\\\ \銆€\f\r\n\t]/g, 
            function(j) {
                return "%" + (256 + j.charCodeAt()).toString(16).substring(1).toUpperCase()
            })
        };
        c.ajax.form = function(j, l) {
            l = l || {};
            var o = j.elements,
            x = o.length,
            k = j.getAttribute("method"),
            n = j.getAttribute("action"),
            D = l.replacer || 
            function(E, i) {
                return E
            },
            A = {},
            C = [],
            u,
            z,
            B,
            w,
            m,
            p,
            r,
            t,
            s;
            function y(i, E) {
                C.push(i + "=" + E)
            }
            for (u in l) {
                if (l.hasOwnProperty(u)) {
                    A[u] = l[u]
                }
            }
            for (u = 0; u < x; u++) {
                z = o[u];
                w = z.name;
                if (!z.disabled && w) {
                    B = z.type;
                    m = c.url.escapeSymbol(z.value);
                    switch (B) {
                    case "radio":
                    case "checkbox":
                        if (!z.checked) {
                            break
                        }
                    case "textarea":
                    case "text":
                    case "password":
                    case "hidden":
                    case "select-one":
                        y(w, D(m, w));
                        break;
                    case "select-multiple":
                        p = z.options;
                        t = p.length;
                        for (r = 0; r < t; r++) {
                            s = p[r];
                            if (s.selected) {
                                y(w, D(s.value, w))
                            }
                        }
                        break
                    }
                }
            }
            A.data = C.join("&");
            A.method = j.getAttribute("method") || "GET";
            return c.ajax.request(n, A)
        };
        c.ajax.get = function(j, i) {
            return c.ajax.request(j, {
                onsuccess: i
            })
        };
        c.ajax.post = function(j, k, i) {
            return c.ajax.request(j, {
                onsuccess: i,
                method: "POST",
                data: k
            })
        };
        c.array.extend({
            contains: function(i) {
                return this.indexOf(i) > -1
            }
        });
        c.each = function(k, p, o) {
            var m,
            r,
            l,
            j;
            if (typeof p == "function" && k) {
                if (typeof k.length == "number") {
                    for (m = 0, r = k.length; m < r; m++) {
                        l = k[m] || (k.charAt && k.charAt(m));
                        j = p.call(o || l, m, l, k);
                        if (j === false || j == "break") {
                            break
                        }
                    }
                } else {
                    if (typeof k == "number") {
                        for (m = 0; m < k; m++) {
                            j = p.call(o || m, m, m, m);
                            if (j === false || j == "break") {
                                break
                            }
                        }
                    } else {
                        if (typeof k == "object") {
                            for (m in k) {
                                if (k.hasOwnProperty(m)) {
                                    j = p.call(o || k[m], m, k[m], k);
                                    if (j === false || j == "break") {
                                        break
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return k
        };
        c.array.extend({
            empty: function() {
                this.length = 0;
                return this
            }
        });
        Array.prototype.every = function(l, k) {
            c.check("function(,.+)?", "baidu.array.every");
            var j,
            m;
            for (j = 0, m = this.length; j < m; j++) {
                if (!l.call(k || this, this[j], j, this)) {
                    return false
                }
            }
            return true
        };
        c.array.every = function(k, j, i) {
            return c.isArray(k) ? k.every(j, i) : k
        };
        Array.prototype.filter = function(o, m) {
            var j = c.array([]),
            l,
            r,
            p,
            k = 0;
            if (c.type(o) === "function") {
                for (l = 0, r = this.length; l < r; l++) {
                    p = this[l];
                    if (o.call(m || this, p, l, this) === true) {
                        j[k++] = p
                    }
                }
            }
            return j
        };
        c.array.filter = function(k, j, i) {
            return c.isArray(k) ? k.filter(j, i) : []
        };
        c.array.extend({
            find: function(k) {
                var j,
                l,
                m = this.length;
                if (c.type(k) == "function") {
                    for (j = 0; j < m; j++) {
                        l = this[j];
                        if (k.call(this, l, j, this) === true) {
                            return l
                        }
                    }
                }
                return null
            }
        });
        c.array.extend({
            hash: function(k) {
                var j = {},
                m = k && k.length,
                l,
                o;
                for (l = 0, o = this.length; l < o; l++) {
                    j[this[l]] = (m && m > l) ? k[l] : true
                }
                return j
            }
        });
        c.array.extend({
            lastIndexOf: function(j, k) {
                c.check(".+(,number)?", "baidu.array.lastIndexOf");
                var i = this.length; (!(k = k | 0) || k >= i) && (k = i - 1);
                k < 0 && (k += i);
                for (; k >= 0; k--) {
                    if (k in this && this[k] === j) {
                        return k
                    }
                }
                return - 1
            }
        });
        Array.prototype.map = function(l, k) {
            c.check("function(,.+)?", "baidu.array.map");
            var j,
            o,
            m = c.array([]);
            for (j = 0, o = this.length; j < o; j++) {
                m[j] = l.call(k || this, this[j], j, this)
            }
            return m
        };
        c.array.map = function(k, j, i) {
            return c.isArray(k) ? k.map(j, i) : k
        };
        Array.prototype.reduce = function(k, l) {
            c.check("function(,.+)?", "baidu.array.reduce");
            var j = 0,
            o = this.length,
            m = false;
            if (typeof l == "undefined") {
                l = this[j++];
                if (typeof l == "undefined") {
                    return
                }
            }
            for (; j < o; j++) {
                l = k(l, this[j], j, this)
            }
            return l
        };
        c.array.reduce = function(k, i, j) {
            return c.isArray(k) ? k.reduce(i, j) : k
        };
        c.array.extend({
            remove: function(i) {
                var j = this.length;
                while (j--) {
                    if (this[j] === i) {
                        this.splice(j, 1)
                    }
                }
                return this
            }
        });
        c.array.extend({
            removeAt: function(i) {
                c.check("number", "baidu.array.removeAt");
                return this.splice(i, 1)[0]
            }
        });
        Array.prototype.some = function(l, k) {
            c.check("function(,.+)?", "baidu.array.some");
            var j,
            m;
            for (j = 0, m = this.length; j < m; j++) {
                if (l.call(k || this, this[j], j, this)) {
                    return true
                }
            }
            return false
        };
        c.array.some = function(k, j, i) {
            return k.some(j, i)
        };
        c.createChain("async", 
        function(i) {
            return typeof i === "string" ? new c.async.$Async(i) : new c.async.$Async()
        },
        function(i) {
            this.url = i
        });
        c.object.extend = c.extend;
        c.lang.isFunction = c.isFunction;
        c.async._isDeferred = function(j) {
            var i = c.lang.isFunction;
            return j && i(j.success) && i(j.then) && i(j.fail) && i(j.cancel)
        };
        c.async.extend({
            Deferred: function() {
                c.async.Deferred.apply(this, arguments);
                return this
            }
        });
        c.async.Deferred = function() {
            var j = this;
            c.extend(j, {
                _fired: 0,
                _firing: 0,
                _cancelled: 0,
                _resolveChain: [],
                _rejectChain: [],
                _result: [],
                _isError: 0
            });
            function i() {
                if (j._cancelled || j._firing) {
                    return
                }
                if (j._nextDeferred) {
                    j._nextDeferred.then(j._resolveChain[0], j._rejectChain[0]);
                    return
                }
                j._firing = 1;
                var n = j._isError ? j._rejectChain: j._resolveChain,
                k = j._result[j._isError ? 1: 0];
                while (n[0] && (!j._cancelled)) {
                    try {
                        var l = n.shift().call(j, k);
                        if (c.async._isDeferred(l)) {
                            j._nextDeferred = l; [].push.apply(l._resolveChain, j._resolveChain); [].push.apply(l._rejectChain, j._rejectChain);
                            n = j._resolveChain = [];
                            j._rejectChain = []
                        }
                    } catch(m) {
                        throw m
                    } finally {
                        j._fired = 1;
                        j._firing = 0
                    }
                }
            }
            j.resolve = j.fireSuccess = function(k) {
                j._result[0] = k;
                i();
                return j
            };
            j.reject = j.fireFail = function(k) {
                j._result[1] = k;
                j._isError = 1;
                i();
                return j
            };
            j.then = function(k, l) {
                j._resolveChain.push(k);
                j._rejectChain.push(l);
                if (j._fired) {
                    i()
                }
                return j
            };
            j.success = function(k) {
                return j.then(k, c.fn.blank)
            };
            j.fail = function(k) {
                return j.then(c.fn.blank, k)
            };
            j.cancel = function() {
                j._cancelled = 1
            }
        };
        c.async.extend({
            get: function() {
                var j = this.url;
                var i = new c.async.Deferred();
                c.ajax.request(j, {
                    onsuccess: function(l, k) {
                        i.resolve({
                            xhr: l,
                            responseText: k
                        })
                    },
                    onfailure: function(k) {
                        i.reject({
                            xhr: k
                        })
                    }
                });
                return i
            }
        });
        c.async.extend({
            post: function(k) {
                var j = this.url;
                var i = new c.async.Deferred();
                c.ajax.request(j, {
                    method: "POST",
                    data: k,
                    onsuccess: function(m, l) {
                        i.resolve({
                            xhr: m,
                            responseText: l
                        })
                    },
                    onfailure: function(l) {
                        i.reject({
                            xhr: l
                        })
                    }
                });
                return i
            }
        });
        c.async.when = function(k, j, l) {
            if (c.async._isDeferred(k)) {
                k.then(j, l);
                return k
            }
            var i = new c.async.Deferred();
            i.then(j, l).resolve(k);
            return i
        };
        c.base = c.base || {};
        c.cookie = c.cookie || {};
        c.cookie._isValidKey = function(i) {
            return (new RegExp('^[^\\x00-\\x20\\x7f\\(\\)<>@,;:\\\\\\"\\[\\]\\?=\\{\\}\\/\\u0080-\\uffff]+\x24')).test(i)
        };
        c.cookie.getRaw = function(j) {
            if (c.cookie._isValidKey(j)) {
                var k = new RegExp("(^| )" + j + "=([^;]*)(;|\x24)"),
                i = k.exec(document.cookie);
                if (i) {
                    return i[2] || null
                }
            }
            return null
        };
        c.cookie.get = function(i) {
            var j = c.cookie.getRaw(i);
            if ("string" == typeof j) {
                j = decodeURIComponent(j);
                return j
            }
            return null
        };
        c.cookie.setRaw = function(k, l, j) {
            if (!c.cookie._isValidKey(k)) {
                return
            }
            j = j || {};
            var i = j.expires;
            if ("number" == typeof j.expires) {
                i = new Date();
                i.setTime(i.getTime() + j.expires)
            }
            document.cookie = k + "=" + l + (j.path ? "; path=" + j.path: "") + (i ? "; expires=" + i.toGMTString() : "") + (j.domain ? "; domain=" + j.domain: "") + (j.secure ? "; secure": "")
        };
        c.cookie.remove = function(j, i) {
            i = i || {};
            i.expires = new Date(0);
            c.cookie.setRaw(j, "", i)
        };
        c.cookie.set = function(j, k, i) {
            c.cookie.setRaw(j, encodeURIComponent(k), i)
        };
        c.object.isEmpty = function(k) {
            var i = true;
            if ("[object Array]" === Object.prototype.toString.call(k)) {
                i = !k.length
            } else {
                k = new Object(k);
                for (var j in k) {
                    return false
                }
            }
            return i
        };
        c._util_.cleanData = function(m) {
            var j;
            for (var k = 0, l; l = m[k]; k++) {
                j = c.id(l, "get");
                if (!j) {
                    continue
                }
                c._util_.eventBase.removeAll(l);
                c.id(l, "remove")
            }
        };
        c.json = c.json || {};
        c.json.parse = function(i) {
            return (new Function("return (" + i + ")"))()
        }; (function() {
            var n = /^(?:\{.*\}|\[.*\])$/,
            i = /([A-Z])/g,
            k = /^-ms-/,
            m = /-([\da-z])/gi,
            j = function(p, r) {
                return (r + "").toUpperCase()
            };
            c.extend(c._util_, {
                camelCase: function(p) {
                    return p.replace(k, "ms-").replace(m, j)
                }
            });
            c.extend(c._util_, {
                cache: {},
                deletedIds: [],
                uuid: 0,
                expando: "baidu" + ("2.0.0" + Math.random()).replace(/\D/g, ""),
                noData: {
                    embed: true,
                    object: "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",
                    applet: true
                },
                hasData: function(p) {
                    p = p.nodeType ? c._util_.cache[p[c._util_.expando]] : p[c._util_.expando];
                    return !! p && !l(p)
                },
                data: function(t, r, w, u) {
                    if (!c._util_.acceptData(t)) {
                        return
                    }
                    var x,
                    z,
                    A = c._util_.expando,
                    y = typeof r === "string",
                    B = t.nodeType,
                    p = B ? c._util_.cache: t,
                    s = B ? t[A] : t[A] && A;
                    if ((!s || !p[s] || (!u && !p[s].data)) && y && w === undefined) {
                        return
                    }
                    if (!s) {
                        if (B) {
                            t[A] = s = c._util_.deletedIds.pop() || ++c._util_.uuid
                        } else {
                            s = A
                        }
                    }
                    if (!p[s]) {
                        p[s] = {};
                        if (!B) {
                            p[s].toJSON = function() {}
                        }
                    }
                    if (typeof r === "object" || typeof r === "function") {
                        if (u) {
                            p[s] = c.extend(p[s], r)
                        } else {
                            p[s].data = c.extend(p[s].data, r)
                        }
                    }
                    x = p[s];
                    if (!u) {
                        if (!x.data) {
                            x.data = {}
                        }
                        x = x.data
                    }
                    if (w !== undefined) {
                        x[c._util_.camelCase(r)] = w
                    }
                    if (y) {
                        z = x[r];
                        if (z == null) {
                            z = x[c._util_.camelCase(r)]
                        }
                    } else {
                        z = x
                    }
                    return z
                },
                removeData: function(t, r, u) {
                    if (!c._util_.acceptData(t)) {
                        return
                    }
                    var y,
                    x,
                    w,
                    z = t.nodeType,
                    p = z ? c._util_.cache: t,
                    s = z ? t[c._util_.expando] : c._util_.expando;
                    if (!p[s]) {
                        return
                    }
                    if (r) {
                        y = u ? p[s] : p[s].data;
                        if (y) {
                            if (!c.isArray(r)) {
                                if (r in y) {
                                    r = [r]
                                } else {
                                    r = c._util_.camelCase(r);
                                    if (r in y) {
                                        r = [r]
                                    } else {
                                        r = r.split(" ")
                                    }
                                }
                            }
                            for (x = 0, w = r.length; x < w; x++) {
                                delete y[r[x]]
                            }
                            if (! (u ? l: c.object.isEmpty)(y)) {
                                return
                            }
                        }
                    }
                    if (!u) {
                        delete p[s].data;
                        if (!l(p[s])) {
                            return
                        }
                    }
                    if (z) {
                        c._util_.cleanData([t], true)
                    } else {
                        if (c.support.deleteExpando || p != p.window) {
                            delete p[s]
                        } else {
                            p[s] = null
                        }
                    }
                },
                _data: function(r, p, s) {
                    return c._util_.data(r, p, s, true)
                },
                acceptData: function(r) {
                    var p = r.nodeName && c._util_.noData[r.nodeName.toLowerCase()];
                    return ! p || p !== true && r.getAttribute("classid") === p
                }
            });
            function o(s, r, t) {
                if (t === undefined && s.nodeType === 1) {
                    var p = "data-" + r.replace(i, "-$1").toLowerCase();
                    t = s.getAttribute(p);
                    if (typeof t === "string") {
                        try {
                            t = t === "true" ? true: t === "false" ? false: t === "null" ? null: c.isNumber(t) ? +t: n.test(t) ? c.json.parse(t) : t
                        } catch(u) {}
                        c._util_.data(s, r, t)
                    } else {
                        t = undefined
                    }
                }
                return t
            }
            function l(r) {
                var p;
                for (p in r) {
                    if (p === "data" && c.object.isEmpty(r[p])) {
                        continue
                    }
                    if (p !== "toJSON") {
                        return false
                    }
                }
                return true
            }
        })();
        c.date = c.date || {};
        c.createChain("number", 
        function(j) {
            var i = parseFloat(j),
            k = isNaN(i) ? i: j;
            clazz = typeof k === "number" ? Number: String,
            pro = clazz.prototype;
            k = new clazz(k);
            c.forEach(c.number.$Number.prototype, 
            function(m, l) {
                pro[l] || (k[l] = m)
            });
            return k
        });
        c.number.extend({
            pad: function(k) {
                var l = this;
                var m = "",
                j = (l < 0),
                i = String(Math.abs(l));
                if (i.length < k) {
                    m = (new Array(k - i.length + 1)).join("0")
                }
                return (j ? "-": "") + m + i
            }
        });
        c.date.format = function(i, n) {
            if ("string" != typeof n) {
                return i.toString()
            }
            function l(u, t) {
                n = n.replace(u, t)
            }
            var j = c.number.pad,
            o = i.getFullYear(),
            m = i.getMonth() + 1,
            s = i.getDate(),
            p = i.getHours(),
            k = i.getMinutes(),
            r = i.getSeconds();
            l(/yyyy/g, j(o, 4));
            l(/yy/g, j(parseInt(o.toString().slice(2), 10), 2));
            l(/MM/g, j(m, 2));
            l(/M/g, m);
            l(/dd/g, j(s, 2));
            l(/d/g, s);
            l(/HH/g, j(p, 2));
            l(/H/g, p);
            l(/hh/g, j(p % 12, 2));
            l(/h/g, p % 12);
            l(/mm/g, j(k, 2));
            l(/m/g, k);
            l(/ss/g, j(r, 2));
            l(/s/g, r);
            return n
        };
        c.date.parse = function(k) {
            var i = new RegExp("^\\d+(\\-|\\/)\\d+(\\-|\\/)\\d+\x24");
            if ("string" == typeof k) {
                if (i.test(k) || isNaN(Date.parse(k))) {
                    var m = k.split(/ |T/),
                    j = m.length > 1 ? m[1].split(/[^\d]/) : [0, 0, 0],
                    l = m[0].split(/[^\d]/);
                    return new Date(l[0] - 0, l[1] - 1, l[2] - 0, j[0] - 0, j[1] - 0, j[2] - 0)
                } else {
                    return new Date(k)
                }
            }
            return new Date()
        };
        c.dom.createElements = function() {
            var k = /<(\w+)/i,
            j = /<|&#?\w+;/,
            l = {
                area: [1, "<map>", "</map>"],
                col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
                legend: [1, "<fieldset>", "</fieldset>"],
                option: [1, "<select multiple='multiple'>", "</select>"],
                td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
                thead: [1, "<table>", "</table>"],
                tr: [2, "<table><tbody>", "</tbody></table>"],
                _default: [0, "", ""]
            };
            l.optgroup = l.option;
            l.tbody = l.tfoot = l.colgroup = l.caption = l.thead;
            l.th = l.td;
            function i(p, s) {
                var r = p.getElementsByTagName("SCRIPT"),
                n,
                m,
                o;
                for (n = r.length - 1; n >= 0; n--) {
                    o = r[n];
                    m = s.createElement("SCRIPT");
                    o.id && (m.id = o.id);
                    o.src && (m.src = o.src);
                    o.type && (m.type = o.type);
                    m[o.text ? "text": "textContent"] = o.text || o.textContent;
                    o.parentNode.replaceChild(m, o)
                }
            }
            return function(s, w) {
                c.isNumber(s) && (s = s.toString());
                w = w || document;
                var o,
                r,
                t,
                x = s,
                p = x.length,
                m = w.createElement("div"),
                u = w.createDocumentFragment(),
                y = [];
                if (c.isString(x)) {
                    if (!j.test(x)) {
                        y.push(w.createTextNode(x))
                    } else {
                        o = l[x.match(k)[1].toLowerCase()] || l._default;
                        m.innerHTML = "<i>mz</i>" + o[1] + x + o[2];
                        m.removeChild(m.firstChild);
                        i(m, w);
                        r = o[0];
                        t = m;
                        while (r--) {
                            t = t.firstChild
                        }
                        c.merge(y, t.childNodes);
                        c.forEach(y, 
                        function(n) {
                            u.appendChild(n)
                        });
                        m = t = null
                    }
                }
                m = null;
                return y
            }
        } ();
        c.dom.extend({
            add: function(j, k) {
                var i = c.array(this.get());
                switch (c.type(j)) {
                case "HTMLElement":
                    i.push(j);
                    break;
                case "$DOM":
                case "array":
                    c.merge(i, j);
                    break;
                case "string":
                    c.merge(i, c.dom(j, k));
                    break;
                default:
                    if (typeof j == "object" && j.length) {
                        c.merge(i, j)
                    }
                }
                return c.dom(i.unique())
            }
        });
        c.dom.extend({
            addClass: function(j) {
                if (arguments.length <= 0) {
                    return this
                }
                switch (typeof j) {
                case "string":
                    j = j.replace(/^\s+/g, "").replace(/\s+$/g, "").replace(/\s+/g, " ");
                    var i = j.split(" ");
                    c.forEach(this, 
                    function(m, k) {
                        var n = "";
                        if (m.className) {
                            n = m.className
                        }
                        for (var l = 0; l < i.length; l++) {
                            if ((" " + n + " ").indexOf(" " + i[l] + " ") == -1) {
                                n += (" " + i[l])
                            }
                        }
                        m.className = n.replace(/^\s+/g, "")
                    });
                    break;
                case "function":
                    c.forEach(this, 
                    function(l, k) {
                        c.dom(l).addClass(j.call(l, k, l.className))
                    });
                    break
                }
                return this
            }
        });
        c.dom.extend({
            getDocument: function() {
                if (this.size() <= 0) {
                    return undefined
                }
                var i = this[0];
                return i.nodeType == 9 ? i: i.ownerDocument || i.document
            }
        });
        c.dom.extend({
            empty: function() {
                for (var j = 0, k; k = this[j]; j++) {
                    k.nodeType === 1 && c._util_.cleanData(k.getElementsByTagName("*"));
                    while (k.firstChild) {
                        k.removeChild(k.firstChild)
                    }
                }
                return this
            }
        });
        c.dom.extend({
            append: function() {
                c.check("^(?:string|function|HTMLElement|\\$DOM)(?:,(?:string|array|HTMLElement|\\$DOM))*$", "baidu.dom.append");
                c._util_.smartInsert(this, arguments, 
                function(i) {
                    this.nodeType === 1 && this.appendChild(i)
                });
                return this
            }
        });
        c.dom.extend({
            html: function(s) {
                var l = c.dom,
                t = c._util_,
                o = this,
                p = false,
                w;
                if (this.size() <= 0) {
                    switch (typeof s) {
                    case "undefined":
                        return undefined;
                        break;
                    default:
                        return o;
                        break
                    }
                }
                var k = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
                r = /<(?:script|style|link)/i,
                j = new RegExp("<(?:" + k + ")[\\s/>]", "i"),
                n = /^\s+/,
                i = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
                m = /<([\w:]+)/,
                u = {
                    option: [1, "<select multiple='multiple'>", "</select>"],
                    legend: [1, "<fieldset>", "</fieldset>"],
                    thead: [1, "<table>", "</table>"],
                    tr: [2, "<table><tbody>", "</tbody></table>"],
                    td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
                    col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
                    area: [1, "<map>", "</map>"],
                    _default: [0, "", ""]
                };
                u.optgroup = u.option;
                u.tbody = u.tfoot = u.colgroup = u.caption = u.thead;
                u.th = u.td;
                if (!c.support.htmlSerialize) {
                    u._default = [1, "X<div>", "</div>"]
                }
                c.forEach(o, 
                function(y, x) {
                    if (w) {
                        return
                    }
                    var A = l(y);
                    switch (typeof s) {
                    case "undefined":
                        w = (y.nodeType === 1 ? y.innerHTML: undefined);
                        return w;
                        break;
                    case "number":
                        s = String(s);
                    case "string":
                        p = true;
                        if (!r.test(s) && (c.support.htmlSerialize || !j.test(s)) && (c.support.leadingWhitespace || !n.test(s)) && !u[(m.exec(s) || ["", ""])[1].toLowerCase()]) {
                            s = s.replace(i, "<$1></$2>");
                            try {
                                if (y.nodeType === 1) {
                                    A.empty();
                                    y.innerHTML = s
                                }
                                y = 0
                            } catch(z) {}
                        }
                        if (y) {
                            o.empty().append(s)
                        }
                        break;
                    case "function":
                        p = true;
                        A.html(s.call(y, x, A.html()));
                        break
                    }
                });
                return p ? o: w
            }
        });
        c._util_.smartInsert = function(j, n, t) {
            if (n.length <= 0 || j.size() <= 0) {
                return
            }
            if (c.type(n[0]) === "function") {
                var p = n[0],
                s;
                return c.forEach(j, 
                function(w, i) {
                    s = c.dom(w);
                    n[0] = p.call(w, i, s.html());
                    c._util_.smartInsert(s, n, t)
                })
            }
            var r = j.getDocument() || document,
            m = r.createDocumentFragment(),
            l = j.length - 1,
            o;
            for (var k = 0, u; u = n[k]; k++) {
                if (u.nodeType) {
                    m.appendChild(u)
                } else {
                    c.forEach(~"string|number".indexOf(c.type(u)) ? c.dom.createElements(u, r) : u, 
                    function(i) {
                        m.appendChild(i)
                    })
                }
            }
            if (! (o = m.firstChild)) {
                return
            }
            c.forEach(j, 
            function(w, i) {
                t.call(w.nodeName.toLowerCase() === "table" && o.nodeName.toLowerCase() === "tr" ? w.tBodies[0] || w.appendChild(w.ownerDocument.createElement("tbody")) : w, i < l ? m.cloneNode(true) : m)
            })
        };
        c.dom.extend({
            after: function() {
                c.check("^(?:string|function|HTMLElement|\\$DOM)(?:,(?:string|array|HTMLElement|\\$DOM))*$", "baidu.dom.after");
                var i = this[0] && this[0].parentNode,
                j = !i && [];
                c._util_.smartInsert(this, arguments, 
                function(k) {
                    i ? i.insertBefore(k, this.nextSibling) : c.merge(j, k.childNodes)
                });
                j && c.merge(this, j);
                return this
            }
        });
        c.dom.extend({
            map: function(i) {
                c.check("function", "baidu.dom.map");
                var j = this,
                k = c.dom();
                c.forEach(this, 
                function(m, l) {
                    k[k.length++] = i.call(m, l, m, m)
                });
                return k
            }
        });
        c._util_.isXML = function(j) {
            var i = (j ? j.ownerDocument || j: 0).documentElement;
            return i ? i.nodeName !== "HTML": false
        };
        c.dom.extend({
            clone: function() {
                var l = c._util_.eventBase;
                function j(n) {
                    return n.getElementsByTagName ? n.getElementsByTagName("*") : (n.querySelectorAll ? n.querySelectorAll("*") : [])
                }
                function k(o, n) {
                    n.clearAttributes && n.clearAttributes();
                    n.mergeAttributes && n.mergeAttributes(o);
                    switch (n.nodeName.toLowerCase()) {
                    case "object":
                        n.outerHTML = o.outerHTML;
                        break;
                    case "textarea":
                    case "input":
                        if (~"checked|radio".indexOf(o.type)) {
                            o.checked && (n.defaultChecked = n.checked = o.checked);
                            n.value !== o.value && (n.value = o.value)
                        }
                        n.defaultValue = o.defaultValue;
                        break;
                    case "options":
                        n.selected = o.defaultSelected;
                        break;
                    case "script":
                        n.text !== o.text && (n.text = o.text);
                        break
                    }
                    n[c.key] && n.removeAttribute(c.key)
                }
                function i(t, p) {
                    if (p.nodeType !== 1 || !c.id(t, "get")) {
                        return
                    }
                    var n = l.get(t);
                    for (var r in n) {
                        for (var o = 0, s; s = n[r][o]; o++) {
                            l.add(p, r, s)
                        }
                    }
                }
                function m(u, w, s) {
                    var r = u.cloneNode(true),
                    o,
                    p,
                    n;
                    if ((!c.support.noCloneEvent || !c.support.noCloneChecked) && (u.nodeType === 1 || u.nodeType === 11) && !c._util_.isXML(u)) {
                        k(u, r);
                        o = j(u);
                        p = j(r);
                        n = o.length;
                        for (var t = 0; t < n; t++) {
                            p[t] && k(o[t], p[t])
                        }
                    }
                    if (w) {
                        i(u, r);
                        if (s) {
                            o = j(u);
                            p = j(r);
                            n = o.length;
                            for (var t = 0; t < n; t++) {
                                i(o[t], p[t])
                            }
                        }
                    }
                    return r
                }
                return function(o, n) {
                    o = !!o;
                    n = !!n;
                    return this.map(function() {
                        return m(this, o, n)
                    })
                }
            } ()
        });
        c._util_.smartInsertTo = function(j, m, o, r) {
            var p = c.dom(m),
            l = p[0],
            n;
            if (r && l && (!l.parentNode || l.parentNode.nodeType === 11)) {
                r = r === "before";
                n = c.merge(r ? j: p, !r ? j: p);
                if (j !== n) {
                    j.length = 0;
                    c.merge(j, n)
                }
            } else {
                for (var k = 0, s; s = p[k]; k++) {
                    c._util_.smartInsert(c.dom(s), k > 0 ? j.clone(true) : j, o)
                }
            }
        };
        c.dom.extend({
            appendTo: function(i) {
                c.check("^(?:string|HTMLElement|\\$DOM)$", "baidu.dom.appendTo");
                c._util_.smartInsertTo(this, i, 
                function(j) {
                    this.appendChild(j)
                });
                return this
            }
        });
        c.extend(c._util_, {
            rfocusable: /^(?:button|input|object|select|textarea)$/i,
            rclickable: /^a(?:rea)?$/i,
            rboolean: /^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i,
            propFix: {
                tabindex: "tabIndex",
                readonly: "readOnly",
                "for": "htmlFor",
                "class": "className",
                maxlength: "maxLength",
                cellspacing: "cellSpacing",
                cellpadding: "cellPadding",
                rowspan: "rowSpan",
                colspan: "colSpan",
                usemap: "useMap",
                frameborder: "frameBorder",
                contenteditable: "contentEditable"
            },
            propHooks: {
                tabIndex: {
                    get: function(k) {
                        var i = c._util_;
                        var j = k.getAttributeNode("tabindex");
                        return j && j.specified ? parseInt(j.value, 10) : i.rfocusable.test(k.nodeName) || i.rclickable.test(k.nodeName) && k.href ? 0: undefined
                    }
                }
            }
        });
        if (!c.support.enctype) {
            var a = c._util_;
            a.propFix.enctype = "encoding"
        }
        if (!c.support.optSelected) {
            var a = c._util_;
            a.propHooks.selected = c.extend(a.propHooks.selected, {
                get: function(j) {
                    var i = j.parentNode;
                    if (i) {
                        i.selectedIndex;
                        if (i.parentNode) {
                            i.parentNode.selectedIndex
                        }
                    }
                    return null
                }
            })
        }
        c.extend(c, {
            _error: function(i) {
                throw new Error(i)
            },
            _nodeName: function(j, i) {
                return j.nodeName && j.nodeName.toUpperCase() === i.toUpperCase()
            }
        });
        c.extend(c._util_, {
            rfocusable: /^(?:button|input|object|select|textarea)$/i,
            rtype: /^(?:button|input)$/i,
            rclickable: /^a(?:rea)?$/i,
            nodeHook: {},
            attrHooks: {
                type: {
                    set: function(j, k) {
                        var i = c._util_;
                        if (i.rtype.test(j.nodeName) && j.parentNode) {
                            c._error("type property can't be changed")
                        } else {
                            if (!c.support.radioValue && k === "radio" && c._nodeName(j, "input")) {
                                var l = j.value;
                                j.setAttribute("type", k);
                                if (l) {
                                    j.value = l
                                }
                                return k
                            }
                        }
                    }
                },
                value: {
                    get: function(k, j) {
                        var i = c._util_;
                        if (i.nodeHook && c._nodeName(k, "button")) {
                            return i.nodeHook.get(k, j)
                        }
                        return j in k ? k.value: null
                    },
                    set: function(j, k, i) {
                        if (a.nodeHook && c._nodeName(j, "button")) {
                            return a.nodeHook.set(j, k, i)
                        }
                        j.value = k
                    }
                }
            },
            boolHook: {
                get: function(j, i) {
                    var l,
                    k = c(j).prop(i);
                    return k === true || typeof k !== "boolean" && (l = j.getAttributeNode(i)) && l.nodeValue !== false ? i.toLowerCase() : undefined
                },
                set: function(j, l, i) {
                    var k;
                    if (l === false) {
                        c(j).removeAttr(i)
                    } else {
                        k = c._util_.propFix[i] || i;
                        if (k in j) {
                            j[k] = true
                        }
                        j.setAttribute(i, i.toLowerCase())
                    }
                    return i
                }
            }
        });
        c._util_.attrHooks.tabindex = c._util_.propHooks.tabIndex;
        if (!c.support.getSetAttribute) {
            var a = c._util_,
            f = {
                name: true,
                id: true,
                coords: true
            };
            a.nodeHook = {
                get: function(k, j) {
                    var i;
                    i = k.getAttributeNode(j);
                    return i && (f[j] ? i.nodeValue !== "": i.specified) ? i.nodeValue: undefined
                },
                set: function(k, l, j) {
                    var i = k.getAttributeNode(j);
                    if (!i) {
                        i = document.createAttribute(j);
                        k.setAttributeNode(i)
                    }
                    return (i.nodeValue = l + "")
                }
            };
            a.attrHooks.tabindex.set = a.nodeHook.set;
            c.forEach(["width", "height"], 
            function(i) {
                a.attrHooks[i] = c.extend(a.attrHooks[i], {
                    set: function(j, k) {
                        if (k === "") {
                            j.setAttribute(i, "auto");
                            return k
                        }
                    }
                })
            });
            a.attrHooks.contenteditable = {
                get: a.nodeHook.get,
                set: function(j, k, i) {
                    if (k === "") {
                        k = "false"
                    }
                    a.nodeHook.set(j, k, i)
                }
            }
        }
        if (!c.support.hrefNormalized) {
            var a = c._util_;
            c.forEach(["href", "src", "width", "height"], 
            function(i) {
                a.attrHooks[i] = c.extend(a.attrHooks[i], {
                    get: function(k) {
                        var j = k.getAttribute(i, 2);
                        return j === null ? undefined: j
                    }
                })
            })
        }
        if (!c.support.style) {
            var a = c._util_;
            a.attrHooks.style = {
                get: function(i) {
                    return i.style.cssText.toLowerCase() || undefined
                },
                set: function(i, j) {
                    return (i.style.cssText = "" + j)
                }
            }
        }
        c.dom.extend({
            prop: function(j, m) {
                if (arguments.length <= 0 || typeof j === "function") {
                    return this
                }
                var i,
                l = this,
                k = false;
                if (this.size() <= 0) {
                    if (j && m) {
                        return l
                    } else {
                        if (j && !m) {
                            return undefined
                        } else {
                            return l
                        }
                    }
                }
                c.forEach(this, 
                function(w, r) {
                    if (i) {
                        return
                    }
                    var s,
                    u,
                    n,
                    p = c.dom,
                    t = c._util_,
                    o = w.nodeType;
                    if (!w || o === 3 || o === 8 || o === 2) {
                        return
                    }
                    n = o !== 1 || !c._util_.isXML(w);
                    if (n) {
                        j = t.propFix[j] || j;
                        u = t.propHooks[j]
                    }
                    switch (typeof j) {
                    case "string":
                        if (typeof m === "undefined") {
                            if (u && "get" in u && (s = u.get(w, j)) !== null) {
                                i = s
                            } else {
                                i = w[j]
                            }
                        } else {
                            if (typeof m === "function") {
                                k = true;
                                var x = p(w);
                                x.prop(j, m.call(w, r, x.prop(j)))
                            } else {
                                k = true;
                                if (u && "set" in u && (s = u.set(w, m, j)) !== undefined) {
                                    return s
                                } else {
                                    w[j] = m
                                }
                            }
                        }
                        break;
                    case "object":
                        k = true;
                        var x = p(w);
                        for (key in j) {
                            x.prop(key, j[key])
                        }
                        break;
                    default:
                        i = l;
                        break
                    }
                });
                return k ? this: i
            }
        });
        c.makeArray = function(k, j) {
            var i = j || [];
            if (!k) {
                return i
            }
            k.length == null || ~"string|function|regexp".indexOf(c.type(k)) ? Array.prototype.push.call(i, k) : c.merge(i, k);
            return i
        };
        c.extend(c._util_, {
            nodeName: function(j, i) {
                return j.nodeName && j.nodeName.toUpperCase() === i.toUpperCase()
            },
            valHooks: {
                option: {
                    get: function(i) {
                        var j = i.attributes.value;
                        return ! j || j.specified ? i.value: i.text
                    }
                },
                select: {
                    get: function(j) {
                        var p,
                        k,
                        o,
                        m,
                        n = j.selectedIndex,
                        r = [],
                        s = j.options,
                        l = j.type === "select-one";
                        if (n < 0) {
                            return null
                        }
                        k = l ? n: 0;
                        o = l ? n + 1: s.length;
                        for (; k < o; k++) {
                            m = s[k];
                            if (m.selected && (c.support.optDisabled ? !m.disabled: m.getAttribute("disabled") === null) && (!m.parentNode.disabled || !c._util_.nodeName(m.parentNode, "optgroup"))) {
                                p = c.dom(m).val();
                                if (l) {
                                    return p
                                }
                                r.push(p)
                            }
                        }
                        if (l && !r.length && s.length) {
                            return c(s[n]).val()
                        }
                        return r
                    },
                    set: function(j, k) {
                        var i = c.makeArray(k);
                        c(j).find("option").each(function() {
                            this.selected = c.array(i).indexOf(c(this).val()) >= 0
                        });
                        if (!i.length) {
                            j.selectedIndex = -1
                        }
                        return i
                    }
                }
            }
        });
        if (!c.support.getSetAttribute) {
            var f = {
                name: true,
                id: true,
                coords: true
            };
            c._util_.valHooks.button = {
                get: function(k, j) {
                    var i;
                    i = k.getAttributeNode(j);
                    return i && (f[j] ? i.value !== "": i.specified) ? i.value: undefined
                },
                set: function(k, l, j) {
                    var i = k.getAttributeNode(j);
                    if (!i) {
                        i = document.createAttribute(j);
                        k.setAttributeNode(i)
                    }
                    return (i.value = l + "")
                }
            }
        }
        if (!c.support.checkOn) {
            c.forEach(["radio", "checkbox"], 
            function() {
                c._util_.valHooks[this] = {
                    get: function(i) {
                        return i.getAttribute("value") === null ? "on": i.value
                    }
                }
            })
        }
        c.forEach(["radio", "checkbox"], 
        function(i) {
            c._util_.valHooks[i] = c.extend(c._util_.valHooks[i], {
                set: function(j, k) {
                    if (c.isArray(k)) {
                        return (j.checked = c.array(k).indexOf(c(j).val()) >= 0)
                    }
                }
            })
        });
        c.dom.extend({
            val: function(n) {
                var m = c.dom,
                j = c._util_,
                l = this,
                k = false,
                i;
                if (this.size() <= 0) {
                    switch (typeof n) {
                    case "undefined":
                        return undefined;
                        break;
                    default:
                        return l;
                        break
                    }
                }
                c.forEach(l, 
                function(r, p) {
                    var o,
                    s = /\r/g;
                    if (i) {
                        return
                    }
                    switch (typeof n) {
                    case "undefined":
                        if (r) {
                            o = j.valHooks[r.type] || j.valHooks[r.nodeName.toLowerCase()];
                            if (o && "get" in o && (i = o.get(r, "value")) !== undefined) {
                                return i
                            }
                            i = r.value;
                            return typeof i === "string" ? i.replace(s, "") : i == null ? "": i
                        }
                        return i;
                        break;
                    case "function":
                        k = true;
                        var t = m(r);
                        t.val(n.call(r, p, t.val()));
                        break;
                    default:
                        k = true;
                        if (r.nodeType !== 1) {
                            return
                        }
                        if (n == null) {
                            n = ""
                        } else {
                            if (typeof n === "number") {
                                n += ""
                            } else {
                                if (c.isArray(n)) {
                                    n = c.forEach(n, 
                                    function(w, u) {
                                        return w == null ? "": w + ""
                                    })
                                }
                            }
                        }
                        o = j.valHooks[r.type] || j.valHooks[r.nodeName.toLowerCase()];
                        if (!o || !("set" in o) || o.set(r, n, "value") === undefined) {
                            r.value = n
                        }
                        break
                    }
                });
                return k ? l: i
            }
        });
        c._util_.access = function(k, m, n) {
            if (this.size() <= 0) {
                return this
            }
            switch (c.type(k)) {
            case "string":
                if (m === undefined) {
                    return n.call(this, this[0], k)
                } else {
                    for (var j = 0, l; l = this[j]; j++) {
                        n.call(this, l, k, c.type(m) === "function" ? m.call(l, j, n.call(this, l, k)) : m)
                    }
                }
                break;
            case "object":
                for (var j in k) {
                    c._util_.access.call(this, j, k[j], n)
                }
                break
            }
            return this
        };
        c.dom.extend({
            getComputedStyle: function(j) {
                var k = this[0].ownerDocument.defaultView,
                i = k && k.getComputedStyle && k.getComputedStyle(this[0], null),
                l = i ? (i.getPropertyValue(j) || i[j]) : "";
                return l || this[0].style[j]
            }
        });
        c.dom.extend({
            getCurrentStyle: function() {
                var i = document.documentElement.currentStyle ? 
                function(j) {
                    return this[0].currentStyle ? this[0].currentStyle[j] : this[0].style[j]
                }: function(j) {
                    return this.getComputedStyle(j)
                };
                return function(j) {
                    return i.call(this, j)
                }
            } ()
        });
        c._util_.getWidthOrHeight = function() {
            var i = {},
            k = {
                position: "absolute",
                visibility: "hidden",
                display: "block"
            };
            function j(n, l) {
                var o = {};
                for (var m in l) {
                    o[m] = n.style[m];
                    n.style[m] = l[m]
                }
                return o
            }
            c.forEach(["Width", "Height"], 
            function(l) {
                var m = {
                    Width: ["Right", "Left"],
                    Height: ["Top", "Bottom"]
                } [l];
                i["get" + l] = function(t, n) {
                    var s = c.dom(t),
                    r = t["offset" + l],
                    o = r === 0 && j(t, k),
                    p = "padding|border";
                    o && (r = t["offset" + l]);
                    n && c.forEach(n.split("|"), 
                    function(u) {
                        if (!~p.indexOf(u)) {
                            r += parseFloat(s.getCurrentStyle(u + m[0])) || 0;
                            r += parseFloat(s.getCurrentStyle(u + m[1])) || 0
                        } else {
                            p = p.replace(new RegExp("\\|?" + u + "\\|?"), "")
                        }
                    });
                    p && c.forEach(p.split("|"), 
                    function(u) {
                        r -= parseFloat(s.getCurrentStyle(u + m[0] + (u === "border" ? "Width": ""))) || 0;
                        r -= parseFloat(s.getCurrentStyle(u + m[1] + (u === "border" ? "Width": ""))) || 0
                    });
                    o && j(t, o);
                    return r
                }
            });
            return function(n, m, l) {
                return i[m === "width" ? "getWidth": "getHeight"](n, l)
            }
        } ();
        c.string.extend({
            toCamelCase: function() {
                var i = this.valueOf();
                if (i.indexOf("-") < 0 && i.indexOf("_") < 0) {
                    return i
                }
                return i.replace(/[-_][^-_]/g, 
                function(j) {
                    return j.charAt(1).toUpperCase()
                })
            }
        });
        c.dom.styleFixer = function() {
            var m = /alpha\s*\(\s*opacity\s*=\s*(\d{1,3})/i,
            i = /^-?(?:\d*\.)?\d+(?!px)[^\d\s]+$/i,
            n = "fillOpacity,fontWeight,opacity,orphans,widows,zIndex,zoom",
            o = {
                "float": c.support.cssFloat ? "cssFloat": "styleFloat"
            },
            j = {
                fontWeight: {
                    normal: 400,
                    bold: 700,
                    bolder: 700,
                    lighter: 100
                }
            },
            l = {
                opacity: {},
                width: {},
                height: {},
                fontWeight: {
                    get: function(s, r) {
                        var p = k.get(s, r);
                        return j.fontWeight[p] || p
                    }
                }
            },
            k = {
                set: function(r, p, s) {
                    r.style[p] = s
                }
            };
            c.extend(l.opacity, c.support.opacity ? {
                get: function(s, r) {
                    var p = c.dom(s).getCurrentStyle(r);
                    return p === "" ? "1": p
                }
            }: {
                get: function(p) {
                    return m.test((p.currentStyle || p.style).filter || "") ? parseFloat(RegExp.$1) / 100: "1"
                },
                set: function(u, r, t) {
                    var s = (u.currentStyle || u.style).filter || "",
                    p = t * 100;
                    u.style.zoom = 1;
                    u.style.filter = m.test(s) ? s.replace(m, "Alpha(opacity=" + p) : s + " progid:dximagetransform.microsoft.Alpha(opacity=" + p + ")"
                }
            });
            c.forEach(["width", "height"], 
            function(p) {
                l[p] = {
                    get: function(r) {
                        return c._util_.getWidthOrHeight(r, p) + "px"
                    },
                    set: function(s, r, t) {
                        c.type(t) === "number" && t < 0 && (t = 0);
                        k.set(s, r, t)
                    }
                }
            });
            c.extend(k, document.documentElement.currentStyle ? {
                get: function(s, r) {
                    var t = c.dom(s).getCurrentStyle(r),
                    p;
                    if (i.test(t)) {
                        p = s.style.left;
                        s.style.left = r === "fontSize" ? "1em": t;
                        t = s.style.pixelLeft + "px";
                        s.style.left = p
                    }
                    return t
                }
            }: {
                get: function(r, p) {
                    return c.dom(r).getCurrentStyle(p)
                }
            });
            return function(u, s, w) {
                var t = c.string(s).toCamelCase(),
                x = w === undefined ? "get": "set",
                r,
                p;
                t = o[t] || t;
                r = c.type(w) === "number" && !~n.indexOf(t) ? w + "px": w;
                p = l.hasOwnProperty(t) && l[t][x] || k[x];
                return p(u, t, r)
            }
        } ();
        c.dom.extend({
            css: function(i, j) {
                c.check("^(?:(?:string(?:,(?:number|string|function))?)|object)$", "baidu.dom.css");
                return c._util_.access.call(this, i, j, 
                function(l, k, n) {
                    var m = c.dom.styleFixer;
                    return m ? m(l, k, n) : (n === undefined ? this.getCurrentStyle(k) : l.style[k] = n)
                })
            }
        });
        c.dom.extend({
            text: function(n) {
                var m = c.dom,
                l = this,
                k = false,
                i;
                if (this.size() <= 0) {
                    switch (typeof n) {
                    case "undefined":
                        return undefined;
                        break;
                    default:
                        return l;
                        break
                    }
                }
                var j = function(t) {
                    var s,
                    p = "",
                    r = 0,
                    o = t.nodeType;
                    if (o) {
                        if (o === 1 || o === 9 || o === 11) {
                            if (typeof t.textContent === "string") {
                                return t.textContent
                            } else {
                                for (t = t.firstChild; t; t = t.nextSibling) {
                                    p += j(t)
                                }
                            }
                        } else {
                            if (o === 3 || o === 4) {
                                return t.nodeValue
                            }
                        }
                    } else {
                        for (; (s = t[r]); r++) {
                            p += j(s)
                        }
                    }
                    return p
                };
                c.forEach(l, 
                function(p, o) {
                    var r = m(p);
                    if (i) {
                        return
                    }
                    switch (typeof n) {
                    case "undefined":
                        i = j(p);
                        return i;
                        break;
                    case "number":
                        n = String(n);
                    case "string":
                        k = true;
                        r.empty().append((p && p.ownerDocument || document).createTextNode(n));
                        break;
                    case "function":
                        k = true;
                        r.text(n.call(p, o, r.text()));
                        break
                    }
                });
                return k ? l: i
            }
        });
        c._util_.getWindowOrDocumentWidthOrHeight = c._util_.getWindowOrDocumentWidthOrHeight || 
        function() {
            var i = {
                window: {},
                document: {}
            };
            c.forEach(["Width", "Height"], 
            function(l) {
                var k = "client" + l,
                m = "offset" + l,
                j = "scroll" + l;
                i.window["get" + l] = function(n) {
                    var p = n.document,
                    o = p.documentElement[k];
                    return c.browser.isStrict && o || p.body && p.body[k] || o
                };
                i.document["get" + l] = function(n) {
                    var o = n.documentElement;
                    return o[k] >= o[j] ? o[k] : Math.max(n.body[j], o[j], n.body[m], o[m])
                }
            });
            return function(l, k, j) {
                return i[k][j === "width" ? "getWidth": "getHeight"](l)
            }
        } ();
        c.dom.extend({
            width: function(i) {
                return c._util_.access.call(this, "width", i, 
                function(n, l, o) {
                    var k = o !== undefined,
                    j = k && parseFloat(o),
                    m = n != null && n == n.window ? "window": (n.nodeType === 9 ? "document": false);
                    if (k && j < 0 || isNaN(j)) {
                        return
                    }
                    k && /^(?:\d*\.)?\d+$/.test(o += "") && (o += "px");
                    return m ? c._util_.getWindowOrDocumentWidthOrHeight(n, m, l) : (k ? n.style.width = o: c._util_.getWidthOrHeight(n, l))
                })
            }
        });
        c.dom.extend({
            height: function(i) {
                return c._util_.access.call(this, "height", i, 
                function(n, l, o) {
                    var k = o !== undefined,
                    j = k && parseFloat(o),
                    m = n != null && n == n.window ? "window": (n.nodeType === 9 ? "document": false);
                    if (k && j < 0 || isNaN(j)) {
                        return
                    }
                    k && /^(?:\d*\.)?\d+$/.test(o += "") && (o += "px");
                    return m ? c._util_.getWindowOrDocumentWidthOrHeight(n, m, l) : (k ? n.style.height = o: c._util_.getWidthOrHeight(n, l))
                })
            }
        });
        c.dom.extend({
            getWindow: function() {
                var i = this.getDocument();
                return (this.size() <= 0) ? undefined: (i.parentWindow || i.defaultView)
            }
        });
        c.dom.extend({
            offset: function() {
                var i = {
                    getDefaultOffset: function(w, s) {
                        var k = s.documentElement,
                        n = s.body,
                        o = s.defaultView,
                        p = o ? o.getComputedStyle(w, null) : w.currentStyle,
                        r = /^t(?:able|h|d)/i,
                        j = w.offsetParent,
                        m = w.offsetLeft,
                        u = w.offsetTop;
                        while ((w = w.parentNode) && w !== n && w !== k) {
                            if (c.support.fixedPosition && p.position === "fixed") {
                                break
                            }
                            p = o ? o.getComputedStyle(w, null) : w.currentStyle;
                            m -= w.scrollLeft;
                            u -= w.scrollTop;
                            if (w === j) {
                                m += w.offsetLeft;
                                u += w.offsetTop;
                                if (!c.support.hasBorderWidth && !(c.support.hasTableCellBorderWidth && r.test(w.nodeName))) {
                                    m += parseFloat(p.borderLeftWidth) || 0;
                                    u += parseFloat(p.borderTopWidth) || 0
                                }
                                j = w.offsetParent
                            }
                        }
                        if (~"static,relative".indexOf(p.position)) {
                            m += n.offsetLeft;
                            u += n.offsetTop
                        }
                        if (c.support.fixedPosition && p.position === "fixed") {
                            m += Math.max(k.scrollLeft, n.scrollLeft);
                            u += Math.max(k.scrollTop, n.scrollTop)
                        }
                        return {
                            left: m,
                            top: u
                        }
                    },
                    setOffset: function(r, m, l) {
                        var p = c.dom(r),
                        o = c.type(m),
                        k = p.offset(),
                        j = p.getCurrentStyle("left"),
                        n = p.getCurrentStyle("top");
                        o === "function" && (m = m.call(r, l, k));
                        if (!m || m.left === undefined && m.top === undefined) {
                            return
                        }
                        j = parseFloat(j) || 0;
                        n = parseFloat(n) || 0;
                        m.left != undefined && (r.style.left = m.left - k.left + j + "px");
                        m.top != undefined && (r.style.top = m.top - k.top + n + "px");
                        p.getCurrentStyle("position") === "static" && (r.style.position = "relative")
                    },
                    bodyOffset: function(j) {
                        var k = c.dom(j);
                        return {
                            left: j.offsetLeft + parseFloat(k.getCurrentStyle("marginLeft")) || 0,
                            top: j.offsetTop + parseFloat(k.getCurrentStyle("marginTop")) || 0
                        }
                    }
                };
                i.getOffset = "getBoundingClientRect" in document.documentElement ? 
                function(m, o) {
                    if (!m.getBoundingClientRect) {
                        return i.getDefaultOffset(m, o)
                    }
                    var l = m.getBoundingClientRect(),
                    n = c.dom(o).getWindow(),
                    k = o.documentElement,
                    j = o.body;
                    return {
                        left: l.left + (n.pageXOffset || Math.max(k.scrollLeft, j.scrollLeft)) - (k.clientLeft || j.clientLeft),
                        top: l.top + (n.pageYOffset || Math.max(k.scrollTop, j.scrollTop)) - (k.clientTop || j.clientTop)
                    }
                }: i.getDefaultOffset;
                return function(j) {
                    if (!j) {
                        var m = this[0],
                        n = c.dom(m).getDocument();
                        return i[m === n.body ? "bodyOffset": "getOffset"](m, n)
                    } else {
                        c.check("^(?:object|function)$", "baidu.dom.offset");
                        for (var k = 0, l; l = this[k]; k++) {
                            i.setOffset(l, j, k)
                        }
                        return this
                    }
                }
            } ()
        });
        c.dom.extend({
            removeAttr: function(i) {
                if (arguments.length <= 0 || !i || typeof i !== "string") {
                    return this
                }
                c.forEach(this, 
                function(t) {
                    var o,
                    m,
                    j,
                    k,
                    s,
                    n = 0;
                    if (t.nodeType === 1) {
                        var p = c.dom,
                        r = c._util_;
                        m = i.toLowerCase().split(/\s+/);
                        k = m.length;
                        for (; n < k; n++) {
                            j = m[n];
                            if (j) {
                                o = r.propFix[j] || j;
                                s = r.rboolean.test(j);
                                if (!s) {
                                    p(t).attr(j, "")
                                }
                                t.removeAttribute(c.support.getSetAttribute ? j: o);
                                if (s && o in t) {
                                    t[o] = false
                                }
                            }
                        }
                    }
                });
                return this
            }
        });
        c.dom.extend({
            attr: function(j, m) {
                if (arguments.length <= 0 || typeof j === "function") {
                    return this
                }
                var i,
                l = this,
                k = false;
                if (this.size() <= 0) {
                    if (j && m) {
                        return l
                    } else {
                        if (j && !m) {
                            return undefined
                        } else {
                            return l
                        }
                    }
                }
                c.forEach(this, 
                function(x, s) {
                    if (i) {
                        return
                    }
                    var t,
                    w,
                    o,
                    r = c.dom,
                    u = c._util_,
                    p = x.nodeType;
                    if (!x || p === 3 || p === 8 || p === 2) {
                        return
                    }
                    if (typeof x.getAttribute === "undefined") {
                        var y = r(x);
                        i = y.prop(j, m)
                    }
                    switch (typeof j) {
                    case "string":
                        o = p !== 1 || !c._util_.isXML(x);
                        if (o) {
                            j = j.toLowerCase();
                            w = u.attrHooks[j] || (u.rboolean.test(j) ? u.boolHook: u.nodeHook)
                        }
                        if (typeof m === "undefined") {
                            if (w && "get" in w && o && (t = w.get(x, j)) !== null) {
                                i = t
                            } else {
                                t = x.getAttribute(j);
                                i = t === null ? undefined: t
                            }
                        } else {
                            if (typeof m === "function") {
                                k = true;
                                var y = r(x);
                                y.attr(j, m.call(x, s, y.attr(j)))
                            } else {
                                k = true;
                                var n = {
                                    val: true,
                                    css: true,
                                    html: true,
                                    text: true,
                                    width: true,
                                    height: true,
                                    offset: true
                                };
                                if (j in n) {
                                    i = r(x)[j](m);
                                    return
                                }
                                if (m === null) {
                                    r(x).removeAttr(j);
                                    return
                                } else {
                                    if (w && "set" in w && o && (t = w.set(x, m, j)) !== undefined) {
                                        return t
                                    } else {
                                        x.setAttribute(j, "" + m);
                                        return m
                                    }
                                }
                            }
                        }
                        break;
                    case "object":
                        k = true;
                        var y = r(x);
                        for (key in j) {
                            y.attr(key, j[key])
                        }
                        break;
                    default:
                        i = l;
                        break
                    }
                });
                return k ? this: i
            }
        });
        c.dom.extend({
            before: function() {
                c.check("^(?:string|function|HTMLElement|\\$DOM)(?:,(?:string|array|HTMLElement|\\$DOM))*$", "baidu.dom.before");
                var i = this[0] && this[0].parentNode,
                k = !i && [],
                j;
                c._util_.smartInsert(this, arguments, 
                function(l) {
                    i ? i.insertBefore(l, this) : c.merge(k, l.childNodes)
                });
                if (k) {
                    k = c.merge(k, this);
                    this.length = 0;
                    c.merge(this, k)
                }
                return this
            }
        });
        c.dom.extend({
            bind: function(j, k, i) {
                return this.on(j, undefined, k, i)
            }
        });
        c.dom.extend({
            children: function(j) {
                var i,
                k = [];
                this.each(function(l) {
                    c.forEach(this.children || this.childNodes, 
                    function(m) {
                        m.nodeType == 1 && k.push(m)
                    })
                });
                return c.dom(c.dom.match(k, j))
            }
        });
        c.dom.children = function(i) {
            c.check("string|HTMLElement", "baidu.dom.children");
            return c.dom(c.isString(i) ? "#" + i: i).children().toArray()
        };
        c.dom.extend({
            contents: function() {
                var n = Array.prototype.slice.call(this),
                j = [],
                m;
                for (var k = 0, l; l = n[k]; k++) {
                    m = l.nodeName;
                    j.push.apply(j, c.makeArray(m && m.toLowerCase() === "iframe" ? l.contentDocument || l.contentWindow.document: l.childNodes))
                }
                this.length = 0;
                return c.merge(this, j)
            }
        });
        c.dom._NAME_ATTRS = (function() {
            var i = {
                cellpadding: "cellPadding",
                cellspacing: "cellSpacing",
                colspan: "colSpan",
                rowspan: "rowSpan",
                valign: "vAlign",
                usemap: "useMap",
                frameborder: "frameBorder"
            };
            if (c.browser.ie < 8) {
                i["for"] = "htmlFor";
                i["class"] = "className"
            } else {
                i.htmlFor = "for";
                i.className = "class"
            }
            return i
        })();
        c.dom.extend({
            setAttr: function(j, k) {
                var i = this[0];
                if ("style" == j) {
                    i.style.cssText = k
                } else {
                    j = c.dom._NAME_ATTRS[j] || j;
                    i.setAttribute(j, k)
                }
                return i
            }
        });
        c.dom.create = function(m, j) {
            var n = document.createElement(m),
            k = j || {};
            for (var l in k) {
                c.dom.setAttr(n, l, k[l])
            }
            return n
        };
        c.dom.extend({
            data: function() {
                var i = c.key,
                j = c.global("_maps_HTMLElementData");
                return function(k, m) {
                    c.forEach(this, 
                    function(n) { ! n[i] && (n[i] = c.id())
                    });
                    if (c.isString(k)) {
                        if (typeof m == "undefined") {
                            var l;
                            return this[0] && (l = j[this[0][i]]) && l[k]
                        }
                        c.forEach(this, 
                        function(o) {
                            var n = j[o[i]] = j[o[i]] || {};
                            n[k] = m
                        })
                    } else {
                        if (c.type(k) == "object") {
                            c.forEach(this, 
                            function(o) {
                                var n = j[o[i]] = j[o[i]] || {};
                                c.forEach(k, 
                                function(p) {
                                    n[p] = k[p]
                                })
                            })
                        }
                    }
                    return this
                }
            } ()
        });
        c.lang.Class = function() {
            this.guid = c.id(this)
        };
        c.lang.Class.prototype.dispose = function() {
            c.id(this.guid, "delete");
            for (var i in this) {
                typeof this[i] != "function" && delete this[i]
            }
            this.disposed = true
        };
        c.lang.Class.prototype.toString = function() {
            return "[object " + (this._type_ || this.__type || this._className || "Object") + "]"
        };
        d = function(i) {
            return c.id(i)
        };
        c.lang.Class.prototype.un = c.lang.Class.prototype.removeEventListener = function(m, l) {
            var k,
            j = this.__listeners;
            if (!j) {
                return
            }
            if (typeof m == "undefined") {
                for (k in j) {
                    delete j[k]
                }
                return
            }
            m.indexOf("on") && (m = "on" + m);
            if (typeof l == "undefined") {
                delete j[m]
            } else {
                if (j[m]) {
                    typeof l == "string" && (l = j[m][l]) && delete j[m][l];
                    for (k = j[m].length - 1; k >= 0; k--) {
                        if (j[m][k] === l) {
                            j[m].splice(k, 1)
                        }
                    }
                }
            }
        };
        c.lang.guid = function() {
            return c.id()
        };
        c.lang.isString = c.isString;
        c.lang.Event = function(i, j) {
            this.type = i;
            this.returnValue = true;
            this.target = j || null;
            this.currentTarget = null
        };
        c.lang.Class.prototype.fire = c.lang.Class.prototype.dispatchEvent = function(o, j) {
            c.lang.isString(o) && (o = new c.lang.Event(o)); ! this.__listeners && (this.__listeners = {});
            j = j || {};
            for (var l in j) {
                o[l] = j[l]
            }
            var l,
            s,
            m = this,
            k = m.__listeners,
            r = o.type;
            o.target = o.target || (o.currentTarget = m);
            r.indexOf("on") && (r = "on" + r);
            typeof m[r] == "function" && m[r].apply(m, arguments);
            if (typeof k[r] == "object") {
                for (l = 0, s = k[r].length; l < s; l++) {
                    k[r][l] && k[r][l].apply(m, arguments)
                }
            }
            return o.returnValue
        };
        c.lang.Class.prototype.on = c.lang.Class.prototype.addEventListener = function(n, m, l) {
            if (typeof m != "function") {
                return
            } ! this.__listeners && (this.__listeners = {});
            var k,
            j = this.__listeners;
            n.indexOf("on") && (n = "on" + n);
            typeof j[n] != "object" && (j[n] = []);
            for (k = j[n].length - 1; k >= 0; k--) {
                if (j[n][k] === m) {
                    return m
                }
            }
            j[n].push(m);
            l && typeof l == "string" && (j[n][l] = m);
            return m
        };
        c.lang.createSingle = function(j) {
            var k = new c.lang.Class();
            for (var i in j) {
                k[i] = j[i]
            }
            return k
        };
        c.dom.ddManager = c.lang.createSingle({
            _targetsDroppingOver: {}
        });
        c.dom.extend({
            delegate: function(i, k, l, j) {
                if (typeof l == "function") {
                    j = l,
                    l = null
                }
                return this.on(k, i, l, j)
            }
        });
        c.dom.extend({
            filter: function(i) {
                return c.dom(c.dom.match(this, i))
            }
        });
        c.dom.extend({
            remove: function(j, m) {
                arguments.length > 0 && c.check("^string(?:,boolean)?$", "baidu.dom.remove");
                var n = j ? this.filter(j) : this;
                for (var k = 0, l; l = n[k]; k++) {
                    if (!m && l.nodeType === 1) {
                        c._util_.cleanData(l.getElementsByTagName("*"));
                        c._util_.cleanData([l])
                    }
                    l.parentNode && l.parentNode.removeChild(l)
                }
                return this
            }
        });
        c.dom.extend({
            detach: function(i) {
                i && c.check("^string$", "baidu.dom.detach");
                return this.remove(i, true)
            }
        });
        c.dom._styleFixer = c.dom._styleFixer || {};
        c.dom._styleFilter = c.dom._styleFilter || [];
        c.dom._styleFilter.filter = function(k, n, o) {
            for (var j = 0, m = c.dom._styleFilter, l; l = m[j]; j++) {
                if (l = l[o]) {
                    n = l(k, n)
                }
            }
            return n
        };
        c.dom.getStyle = function(k, j) {
            var m = c.dom;
            k = m.g(k);
            j = c.string.toCamelCase(j);
            var l = k.style[j] || (k.currentStyle ? k.currentStyle[j] : "") || m.getComputedStyle(k, j);
            if (!l || l == "auto") {
                var i = m._styleFixer[j];
                if (i) {
                    l = i.get ? i.get(k, j, l) : c.dom.getStyle(k, i)
                }
            }
            if (i = m._styleFilter) {
                l = i.filter(j, l, "get")
            }
            return l
        };
        c.page = c.page || {};
        c.page.getScrollTop = function() {
            var i = document;
            return window.pageYOffset || i.documentElement.scrollTop || i.body.scrollTop
        };
        c.page.getScrollLeft = function() {
            var i = document;
            return window.pageXOffset || i.documentElement.scrollLeft || i.body.scrollLeft
        }; (function() {
            c.page.getMousePosition = function() {
                return {
                    x: c.page.getScrollLeft() + i.x,
                    y: c.page.getScrollTop() + i.y
                }
            };
            var i = {
                x: 0,
                y: 0
            };
            c.event.on(document, "onmousemove", 
            function(j) {
                j = window.event || j;
                i.x = j.clientX;
                i.y = j.clientY
            })
        })();
        c.dom.extend({
            off: function(k, i, l) {
                var j = c._util_.eventBase,
                m = this;
                if (!k) {
                    c.forEach(this, 
                    function(n) {
                        j.removeAll(n)
                    })
                } else {
                    if (typeof k == "string") {
                        if (typeof i == "function") {
                            l = i,
                            i = null
                        }
                        k = k.split(/[ ,]/);
                        c.forEach(this, 
                        function(n) {
                            c.forEach(k, 
                            function(o) {
                                j.remove(n, o, l, i)
                            })
                        })
                    } else {
                        if (typeof k == "object") {
                            c.forEach(k, 
                            function(n, o) {
                                m.off(o, i, n)
                            })
                        }
                    }
                }
                return this
            }
        });
        c.event.un = c.un = function(i, k, j) {
            i = c.dom.g(i);
            c.dom(i).off(k.replace(/^\s*on/, ""), j);
            return i
        };
        c.event.preventDefault = function(i) {
            i.originalEvent && (i = i.originalEvent);
            if (i.preventDefault) {
                i.preventDefault()
            } else {
                i.returnValue = false
            }
        }; (function() {
            var w = false,
            r,
            p,
            m,
            l,
            k,
            n,
            u,
            i,
            t,
            x;
            c.dom.drag = function(z, y) {
                if (! (r = c.dom.g(z))) {
                    return false
                }
                p = c.object.extend({
                    autoStop: true,
                    capture: true,
                    interval: 16
                },
                y);
                i = n = parseInt(c.dom.getStyle(r, "left")) || 0;
                t = u = parseInt(c.dom.getStyle(r, "top")) || 0;
                w = true;
                setTimeout(function() {
                    var B = c.page.getMousePosition();
                    m = p.mouseEvent ? (c.page.getScrollLeft() + p.mouseEvent.clientX) : B.x;
                    l = p.mouseEvent ? (c.page.getScrollTop() + p.mouseEvent.clientY) : B.y;
                    clearInterval(k);
                    k = setInterval(j, p.interval)
                },
                1);
                var A = c.dom(document);
                p.autoStop && A.on("mouseup", s);
                A.on("selectstart", o);
                if (p.capture && r.setCapture) {
                    r.setCapture()
                } else {
                    if (p.capture && window.captureEvents) {
                        window.captureEvents(Event.MOUSEMOVE | Event.MOUSEUP)
                    }
                }
                x = document.body.style.MozUserSelect;
                document.body.style.MozUserSelect = "none";
                c.isFunction(p.ondragstart) && p.ondragstart(r, p);
                return {
                    stop: s,
                    dispose: s,
                    update: function(B) {
                        c.object.extend(p, B)
                    }
                }
            };
            function s() {
                w = false;
                clearInterval(k);
                if (p.capture && r.releaseCapture) {
                    r.releaseCapture()
                } else {
                    if (p.capture && window.captureEvents) {
                        window.captureEvents(Event.MOUSEMOVE | Event.MOUSEUP)
                    }
                }
                document.body.style.MozUserSelect = x;
                var y = c.dom(document);
                y.off("selectstart", o);
                p.autoStop && y.off("mouseup", s);
                c.isFunction(p.ondragend) && p.ondragend(r, p, {
                    left: i,
                    top: t
                })
            }
            function j(C) {
                if (!w) {
                    clearInterval(k);
                    return
                }
                var z = p.range || [],
                y = c.page.getMousePosition(),
                A = n + y.x - m,
                B = u + y.y - l;
                if (c.isObject(z) && z.length == 4) {
                    A = Math.max(z[3], A);
                    A = Math.min(z[1] - r.offsetWidth, A);
                    B = Math.max(z[0], B);
                    B = Math.min(z[2] - r.offsetHeight, B)
                }
                r.style.left = A + "px";
                r.style.top = B + "px";
                i = A;
                t = B;
                c.isFunction(p.ondrag) && p.ondrag(r, p, {
                    left: i,
                    top: t
                })
            }
            function o(y) {
                return c.event.preventDefault(y, false)
            }
        })();
        c.dom.extend({
            setStyle: function(j, k) {
                var l = c.dom,
                i;
                element = this[0];
                j = c.string.toCamelCase(j);
                if (i = l._styleFilter) {
                    k = i.filter(j, k, "set")
                }
                i = l._styleFixer[j]; (i && i.set) ? i.set(element, k, j) : (element.style[i || j] = k);
                return element
            }
        });
        c.dom.draggable = function(k, t) {
            t = c.object.extend({
                toggle: function() {
                    return true
                }
            },
            t);
            t.autoStop = true;
            k = c.dom.g(k);
            t.handler = t.handler || k;
            var j,
            r = ["ondragstart", "ondrag", "ondragend"],
            l = r.length - 1,
            m,
            s,
            o = {
                dispose: function() {
                    s && s.stop();
                    c.event.un(t.handler, "onmousedown", p);
                    c.lang.Class.prototype.dispose.call(o)
                }
            },
            n = this;
            if (j = c.dom.ddManager) {
                for (; l >= 0; l--) {
                    m = r[l];
                    t[m] = (function(i) {
                        var u = t[i];
                        return function() {
                            c.lang.isFunction(u) && u.apply(n, arguments);
                            j.dispatchEvent(i, {
                                DOM: k
                            })
                        }
                    })(m)
                }
            }
            if (k) {
                function p(u) {
                    var i = t.mouseEvent = window.event || u;
                    t.mouseEvent = {
                        clientX: i.clientX,
                        clientY: i.clientY
                    };
                    if (i.button > 1 || (c.lang.isFunction(t.toggle) && !t.toggle())) {
                        return
                    }
                    if (c.lang.isFunction(t.onbeforedragstart)) {
                        t.onbeforedragstart(k)
                    }
                    s = c.dom.drag(k, t);
                    o.stop = s.stop;
                    o.update = s.update;
                    c.event.preventDefault(i)
                }
                c.event.on(t.handler, "onmousedown", p)
            }
            return {
                cancel: function() {
                    o.dispose()
                }
            }
        };
        c.dom.getPosition = function(i) {
            i = c.dom.g(i);
            var s = c.dom.getDocument(i),
            l = c.browser,
            o = c.dom.getStyle,
            k = l.isGecko > 0 && s.getBoxObjectFor && o(i, "position") == "absolute" && (i.style.top === "" || i.style.left === ""),
            p = {
                left: 0,
                top: 0
            },
            n = (l.ie && !l.isStrict) ? s.body: s.documentElement,
            t,
            j;
            if (i == n) {
                return p
            }
            if (i.getBoundingClientRect) {
                j = i.getBoundingClientRect();
                p.left = Math.floor(j.left) + Math.max(s.documentElement.scrollLeft, s.body.scrollLeft);
                p.top = Math.floor(j.top) + Math.max(s.documentElement.scrollTop, s.body.scrollTop);
                p.left -= s.documentElement.clientLeft;
                p.top -= s.documentElement.clientTop;
                var r = s.body,
                u = parseInt(o(r, "borderLeftWidth")),
                m = parseInt(o(r, "borderTopWidth"));
                if (l.ie && !l.isStrict) {
                    p.left -= isNaN(u) ? 2: u;
                    p.top -= isNaN(m) ? 2: m
                }
            } else {
                t = i;
                do {
                    p.left += t.offsetLeft;
                    p.top += t.offsetTop;
                    if (l.isWebkit > 0 && o(t, "position") == "fixed") {
                        p.left += s.body.scrollLeft;
                        p.top += s.body.scrollTop;
                        break
                    }
                    t = t.offsetParent
                }
                while (t && t != i);
                if (l.opera > 0 || (l.isWebkit > 0 && o(i, "position") == "absolute")) {
                    p.top -= s.body.offsetTop
                }
                t = i.offsetParent;
                while (t && t != s.body) {
                    p.left -= t.scrollLeft;
                    if (!l.opera || t.tagName != "TR") {
                        p.top -= t.scrollTop
                    }
                    t = t.offsetParent
                }
            }
            return p
        };
        c.dom.intersect = function(p, o) {
            var n = c.dom.g,
            m = c.dom.getPosition,
            i = Math.max,
            k = Math.min;
            p = n(p);
            o = n(o);
            var l = m(p),
            j = m(o);
            return i(l.left, j.left) <= k(l.left + p.offsetWidth, j.left + o.offsetWidth) && i(l.top, j.top) <= k(l.top + p.offsetHeight, j.top + o.offsetHeight)
        };
        c.dom.droppable = function(m, k) {
            k = k || {};
            var l = c.dom.ddManager,
            o = c.dom.g(m),
            j = c.lang.guid(),
            n = function(s) {
                var r = l._targetsDroppingOver,
                p = {
                    trigger: s.DOM,
                    reciever: o
                };
                if (c.dom.intersect(o, s.DOM)) {
                    if (!r[j]) { (typeof k.ondropover == "function") && k.ondropover.call(o, p);
                        l.dispatchEvent("ondropover", p);
                        r[j] = true
                    }
                } else {
                    if (r[j]) { (typeof k.ondropout == "function") && k.ondropout.call(o, p);
                        l.dispatchEvent("ondropout", p)
                    }
                    delete r[j]
                }
            },
            i = function(r) {
                var p = {
                    trigger: r.DOM,
                    reciever: o
                };
                if (c.dom.intersect(o, r.DOM)) {
                    typeof k.ondrop == "function" && k.ondrop.call(o, p);
                    l.dispatchEvent("ondrop", p)
                }
                delete l._targetsDroppingOver[j]
            };
            l.addEventListener("ondrag", n);
            l.addEventListener("ondragend", i);
            return {
                cancel: function() {
                    l.removeEventListener("ondrag", n);
                    l.removeEventListener("ondragend", i)
                }
            }
        };
        c.dom.extend({
            eq: function(i) {
                c.check("number", "baidu.dom.eq");
                return c.dom(this.get(i))
            }
        });
        c.dom.extend({
            find: function(i) {
                var j = [],
                k,
                m = "__tangram__find__",
                l = c.dom();
                switch (c.type(i)) {
                case "string":
                    this.each(function() {
                        c.merge(l, c.query(i, this))
                    });
                    break;
                case "HTMLElement":
                    k = i.tagName + "#" + (i.id ? i.id: (i.id = m));
                    this.each(function() {
                        if (c.query(k, this).length > 0) {
                            j.push(i)
                        }
                    });
                    i.id == m && (i.id = "");
                    if (j.length > 0) {
                        c.merge(l, j)
                    }
                    break;
                case "$DOM":
                    j = i.get();
                    this.each(function() {
                        c.forEach(c.query("*", this), 
                        function(p) {
                            for (var o = 0, r = j.length; o < r; o++) {
                                p === j[o] && (l[l.length++] = j[o])
                            }
                        })
                    });
                    break
                }
                return l
            }
        });
        c.dom.extend({
            first: function() {
                return c.dom(this[0])
            }
        });
        c.dom.first = function(i) {
            c.isString(i) && (i = "#" + i);
            return c.dom(i).children()[0]
        };
        c.dom.getAncestorBy = function(i, j) {
            i = c.dom.g(i);
            while ((i = i.parentNode) && i.nodeType == 1) {
                if (j(i)) {
                    return i
                }
            }
            return null
        };
        c.dom.getAncestorByClass = function(i, j) {
            i = c.dom.g(i);
            j = new RegExp("(^|\\s)" + c.string.trim(j) + "(\\s|\x24)");
            while ((i = i.parentNode) && i.nodeType == 1) {
                if (j.test(i.className)) {
                    return i
                }
            }
            return null
        };
        c.dom.getAncestorByTag = function(j, i) {
            j = c.dom.g(j);
            i = i.toUpperCase();
            while ((j = j.parentNode) && j.nodeType == 1) {
                if (j.tagName == i) {
                    return j
                }
            }
            return null
        };
        c.dom.extend({
            getAttr: function(i) {
                element = this[0];
                if ("style" == i) {
                    return element.style.cssText
                }
                i = c.dom._NAME_ATTRS[i] || i;
                return element.getAttribute(i)
            }
        });
        c.dom.getParent = function(i) {
            i = c.dom._g(i);
            return i.parentElement || i.parentNode || null
        };
        c.dom.extend({
            getText: function() {
                var k = "",
                n,
                m = 0,
                j;
                element = this[0];
                if (element.nodeType === 3 || element.nodeType === 4) {
                    k += element.nodeValue
                } else {
                    if (element.nodeType !== 8) {
                        n = element.childNodes;
                        for (j = n.length; m < j; m++) {
                            k += c.dom.getText(n[m])
                        }
                    }
                }
                return k
            }
        });
        c.dom.extend({
            has: function(i) {
                var j = [],
                k = c.dom(document.body);
                c.forEach(this, 
                function(l) {
                    k[0] = l;
                    k.find(i).length && j.push(l)
                });
                return c.dom(j)
            }
        });
        c.dom.extend({
            hasAttr: function(j) {
                element = this[0];
                var i = element.attributes.getNamedItem(j);
                return !! (i && i.specified)
            }
        });
        c.dom.extend({
            hasClass: function(k) {
                if (arguments.length <= 0 || typeof k === "function") {
                    return this
                }
                if (this.size() <= 0) {
                    return false
                }
                k = k.replace(/^\s+/g, "").replace(/\s+$/g, "").replace(/\s+/g, " ");
                var j = k.split(" ");
                var i;
                c.forEach(this, 
                function(m) {
                    var n = m.className;
                    for (var l = 0; l < j.length; l++) {
                        if ((" " + n + " ").indexOf(" " + j[l] + " ") == -1) {
                            i = false;
                            return
                        }
                    }
                    if (i !== false) {
                        i = true;
                        return
                    }
                });
                return i
            }
        }); (function() {
            var n,
            k,
            i,
            p = /^margin/,
            m = /^-?(?:\d*\.)?\d+(?!px)[^\d\s]+$/i,
            o = /^(top|right|bottom|left)$/,
            j = {};
            c.extend(c._util_, {
                showHide: function(x, r) {
                    var w,
                    y,
                    s = [],
                    t = 0,
                    u = x.length;
                    for (; t < u; t++) {
                        w = x[t];
                        if (!w.style) {
                            continue
                        }
                        s[t] = c._util_._data(w, "olddisplay");
                        if (r) {
                            if (!s[t] && w.style.display === "none") {
                                w.style.display = ""
                            }
                            if ((w.style.display === "" && i(w, "display") === "none") || !c.dom.contains(w.ownerDocument.documentElement, w)) {
                                s[t] = c._util_._data(w, "olddisplay", l(w.nodeName))
                            }
                        } else {
                            y = i(w, "display");
                            if (!s[t] && y !== "none") {
                                c._util_._data(w, "olddisplay", y)
                            }
                        }
                    }
                    for (t = 0; t < u; t++) {
                        w = x[t];
                        if (!w.style) {
                            continue
                        }
                        if (!r || w.style.display === "none" || w.style.display === "") {
                            w.style.display = r ? s[t] || "": "none"
                        }
                    }
                    return x
                }
            });
            if (window.getComputedStyle) {
                i = function(x, s) {
                    var r,
                    u,
                    w = getComputedStyle(x, null),
                    t = x.style;
                    if (w) {
                        r = w[s];
                        if (r === "" && !c.dom.contains(x.ownerDocument.documentElement, x)) {
                            r = c.dom(x).css(s)
                        }
                        if (!c.support.pixelMargin && p.test(s) && m.test(r)) {
                            u = t.width;
                            t.width = r;
                            r = w.width;
                            t.width = u
                        }
                    }
                    return r
                }
            } else {
                if (document.documentElement.currentStyle) {
                    i = function(w, t) {
                        var x,
                        r,
                        s = w.currentStyle && w.currentStyle[t],
                        u = w.style;
                        if (s == null && u && u[t]) {
                            s = u[t]
                        }
                        if (m.test(s) && !o.test(t)) {
                            x = u.left;
                            r = w.runtimeStyle && w.runtimeStyle.left;
                            if (r) {
                                w.runtimeStyle.left = w.currentStyle.left
                            }
                            u.left = t === "fontSize" ? "1em": s;
                            s = u.pixelLeft + "px";
                            u.left = x;
                            if (r) {
                                w.runtimeStyle.left = r
                            }
                        }
                        return s === "" ? "auto": s
                    }
                }
            }
            function l(t) {
                if (j[t]) {
                    return j[t]
                }
                var r = c.dom("<" + t + ">").appendTo(document.body),
                s = r.css("display");
                r.remove();
                if (s === "none" || s === "") {
                    k = document.body.appendChild(k || c.extend(document.createElement("iframe"), {
                        frameBorder: 0,
                        width: 0,
                        height: 0
                    }));
                    if (!n || !k.createElement) {
                        n = (k.contentWindow || k.contentDocument).document;
                        n.write("<!doctype html><html><body>");
                        n.close()
                    }
                    r = n.body.appendChild(n.createElement(t));
                    s = i(r, "display");
                    document.body.removeChild(k)
                }
                j[t] = s;
                return s
            }
        })();
        c.dom.extend({
            hide: function() {
                c._util_.showHide(this);
                return this
            }
        });
        c.dom.extend({
            innerHeight: function() {
                if (this.size() <= 0) {
                    return 0
                }
                var j = this[0],
                i = j != null && j === j.window ? "window": (j.nodeType === 9 ? "document": false);
                return i ? c._util_.getWindowOrDocumentWidthOrHeight(j, i, "height") : c._util_.getWidthOrHeight(j, "height", "padding")
            }
        });
        c.dom.extend({
            innerWidth: function() {
                if (this.size() <= 0) {
                    return 0
                }
                var j = this[0],
                i = j != null && j === j.window ? "window": (j.nodeType === 9 ? "document": false);
                return i ? c._util_.getWindowOrDocumentWidthOrHeight(j, i, "width") : c._util_.getWidthOrHeight(j, "width", "padding")
            }
        });
        c.dom.extend({
            insertAfter: function(i) {
                c.check("^(?:string|HTMLElement|\\$DOM)$", "baidu.dom.insertAfter");
                c._util_.smartInsertTo(this, i, 
                function(j) {
                    this.parentNode.insertBefore(j, this.nextSibling)
                },
                "after");
                return this
            }
        });
        c.dom.insertAfter = function(k, j) {
            var i = c.dom._g;
            return c.dom(i(k)).insertAfter(i(j))[0]
        };
        c.dom.extend({
            insertBefore: function(i) {
                c.check("^(?:string|HTMLElement|\\$DOM)$", "baidu.dom.insertBefore");
                c._util_.smartInsertTo(this, i, 
                function(j) {
                    this.parentNode.insertBefore(j, this)
                },
                "before");
                return this
            }
        });
        c.dom.insertBefore = function(k, j) {
            var i = c.dom._g;
            return c.dom(i(k)).insertBefore(i(j))[0]
        };
        c.dom.extend({
            insertHTML: function(i, k) {
                element = this[0];
                var j,
                l;
                if (element.insertAdjacentHTML && !c.browser.opera) {
                    element.insertAdjacentHTML(i, k)
                } else {
                    j = element.ownerDocument.createRange();
                    i = i.toUpperCase();
                    if (i == "AFTERBEGIN" || i == "BEFOREEND") {
                        j.selectNodeContents(element);
                        j.collapse(i == "AFTERBEGIN")
                    } else {
                        l = i == "BEFOREBEGIN";
                        j[l ? "setStartBefore": "setEndAfter"](element);
                        j.collapse(l)
                    }
                    j.insertNode(j.createContextualFragment(k))
                }
                return element
            }
        });
        c.dom.extend({
            last: function() {
                return c.dom(this.get( - 1))
            }
        });
        c.dom.last = function(i) {
            i = c.dom.g(i);
            for (var j = i.lastChild; j; j = j.previousSibling) {
                if (j.nodeType == 1) {
                    return j
                }
            }
            return null
        };
        c.dom.extend({
            next: function(i) {
                var j = c.dom();
                c.forEach(this, 
                function(k) {
                    while ((k = k.nextSibling) && k && k.nodeType != 1) {}
                    k && (j[j.length++] = k)
                });
                return i ? j.filter(i) : j
            }
        });
        c.dom.extend({
            nextAll: function(i) {
                var j = [];
                c.forEach(this, 
                function(k) {
                    while (k = k.nextSibling) {
                        k && (k.nodeType == 1) && j.push(k)
                    }
                });
                return c.dom(c.dom.match(j, i))
            }
        });
        c.dom.extend({
            nextUntil: function(i, j) {
                var k = c.array();
                c.forEach(this, 
                function(n) {
                    var m = c.array();
                    while (n = n.nextSibling) {
                        n && (n.nodeType == 1) && m.push(n)
                    }
                    if (i && m.length) {
                        var l = c.dom.match(m, i);
                        if (l.length) {
                            m = m.slice(0, m.indexOf(l[0]))
                        }
                    }
                    c.merge(k, m)
                });
                return c.dom(c.dom.match(k, j))
            }
        });
        c.dom.extend({
            not: function(k) {
                var o,
                m,
                r,
                p = this.get(),
                l = c.isArray(k) ? k: c.dom.match(this, k);
                for (o = p.length - 1; o > -1; o--) {
                    for (m = 0, r = l.length; m < r; m++) {
                        l[m] === p[o] && p.splice(o, 1)
                    }
                }
                return c.dom(p)
            }
        });
        c.dom.extend({
            offsetParent: function() {
                return this.map(function() {
                    var j = this.offsetParent || document.body,
                    i = /^(?:body|html)$/i;
                    while (j && c.dom(j).getCurrentStyle("position") === "static" && !i.test(j.nodeName)) {
                        j = j.offsetParent
                    }
                    return j
                })
            }
        });
        c.dom.extend({
            one: function(j, m, i) {
                var k = this;
                if (typeof m == "function") {
                    i = m,
                    m = undefined
                }
                if (typeof j == "object" && j) {
                    c.forEach(j, 
                    function(o, n) {
                        this.one(n, m, o)
                    },
                    this);
                    return this
                }
                var l = function() {
                    c.dom(this).off(j, l);
                    return i.apply(k, arguments)
                };
                this.each(function() {
                    var n = c.id(this);
                    i["_" + n + "_" + j] = l
                });
                return this.on(j, m, l)
            }
        });
        c.dom.opacity = function(j, i) {
            j = c.dom.g(j);
            if (!c.browser.ie) {
                j.style.opacity = i;
                j.style.KHTMLOpacity = i
            } else {
                j.style.filter = "progid:DXImageTransform.Microsoft.Alpha(opacity:" + Math.floor(i * 100) + ")"
            }
        };
        c.dom.extend({
            outerHeight: function(k) {
                if (this.size() <= 0) {
                    return 0
                }
                var j = this[0],
                i = j != null && j === j.window ? "window": (j.nodeType === 9 ? "document": false);
                return i ? c._util_.getWindowOrDocumentWidthOrHeight(j, i, "height") : c._util_.getWidthOrHeight(j, "height", "padding|border" + (k ? "|margin": ""))
            }
        });
        c.dom.extend({
            outerWidth: function(k) {
                if (this.size() <= 0) {
                    return 0
                }
                var j = this[0],
                i = j != null && j === j.window ? "window": (j.nodeType === 9 ? "document": false);
                return i ? c._util_.getWindowOrDocumentWidthOrHeight(j, i, "width") : c._util_.getWidthOrHeight(j, "width", "padding|border" + (k ? "|margin": ""))
            }
        });
        c.dom.extend({
            parent: function(i) {
                var j = [];
                c.forEach(this, 
                function(k) { (k = k.parentNode) && k.nodeType == 1 && j.push(k)
                });
                return c.dom(c.dom.match(j, i))
            }
        });
        c.dom.extend({
            parents: function(i) {
                var j = [];
                c.forEach(this, 
                function(l) {
                    var k = [];
                    while ((l = l.parentNode) && l.nodeType == 1) {
                        k.push(l)
                    }
                    c.merge(j, k)
                });
                return c.dom(c.dom.match(j, i))
            }
        });
        c.dom.extend({
            parentsUntil: function(i, j) {
                c.check("(string|HTMLElement)(,.+)?", "baidu.dom.parentsUntil");
                var k = [];
                c.forEach(this, 
                function(n) {
                    var m = c.array();
                    while ((n = n.parentNode) && n.nodeType == 1) {
                        m.push(n)
                    }
                    if (i && m.length) {
                        var l = c.dom.match(m, i);
                        if (l.length) {
                            m = m.slice(0, m.indexOf(l[0]))
                        }
                    }
                    c.merge(k, m)
                });
                return c.dom(c.dom.match(k, j))
            }
        });
        c.dom.extend({
            position: function() {
                if (this.size() <= 0) {
                    return 0
                }
                var i = /^(?:body|html)$/i,
                l = this.offset(),
                j = this.offsetParent(),
                k = i.test(j[0].nodeName) ? {
                    left: 0,
                    top: 0
                }: j.offset();
                l.left -= parseFloat(this.getCurrentStyle("marginLeft")) || 0;
                l.top -= parseFloat(this.getCurrentStyle("marginTop")) || 0;
                k.left += parseFloat(j.getCurrentStyle("borderLeftWidth")) || 0;
                k.top += parseFloat(j.getCurrentStyle("borderTopWidth")) || 0;
                return {
                    left: l.left - k.left,
                    top: l.top - k.top
                }
            }
        });
        c.dom.extend({
            prepend: function() {
                c.check("^(?:string|function|HTMLElement|\\$DOM)(?:,(?:string|array|HTMLElement|\\$DOM))*$", "baidu.dom.prepend");
                c._util_.smartInsert(this, arguments, 
                function(i) {
                    this.nodeType === 1 && this.insertBefore(i, this.firstChild)
                });
                return this
            }
        });
        c.dom.extend({
            prependTo: function(i) {
                c.check("^(?:string|HTMLElement|\\$DOM)$", "baidu.dom.prependTo");
                c._util_.smartInsertTo(this, i, 
                function(j) {
                    this.insertBefore(j, this.firstChild)
                });
                return this
            }
        });
        c.dom.extend({
            prev: function(i) {
                var j = [];
                c.forEach(this, 
                function(k) {
                    while (k = k.previousSibling) {
                        if (k.nodeType == 1) {
                            j.push(k);
                            break
                        }
                    }
                });
                return c.dom(c.dom.match(j, i))
            }
        });
        c.dom.extend({
            prevAll: function(i) {
                var j = c.array();
                c.forEach(this, 
                function(l) {
                    var k = [];
                    while (l = l.previousSibling) {
                        l.nodeType == 1 && k.push(l)
                    }
                    c.merge(j, k.reverse())
                });
                return c.dom(typeof i == "string" ? c.dom.match(j, i) : j.unique())
            }
        });
        c.dom.extend({
            prevUntil: function(i, j) {
                c.check("(string|HTMLElement)(,.+)?", "baidu.dom.prevUntil");
                var k = [];
                c.forEach(this, 
                function(n) {
                    var m = c.array();
                    while (n = n.previousSibling) {
                        n && (n.nodeType == 1) && m.push(n)
                    }
                    if (i && m.length) {
                        var l = c.dom.match(m, i);
                        if (l.length) {
                            m = m.slice(0, m.indexOf(l[0]))
                        }
                    }
                    c.merge(k, m)
                });
                return c.dom(c.dom.match(k, j))
            }
        });
        c.string.extend({
            escapeReg: function() {
                return this.replace(new RegExp("([.*+?^=!:\x24{}()|[\\]/\\\\])", "g"), "\\\x241")
            }
        });
        c.dom.q = function(r, n, k) {
            var s = [],
            m = c.string.trim,
            p,
            o,
            j,
            l;
            if (! (r = m(r))) {
                return s
            }
            if ("undefined" == typeof n) {
                n = document
            } else {
                n = c.dom.g(n);
                if (!n) {
                    return s
                }
            }
            k && (k = m(k).toUpperCase());
            if (n.getElementsByClassName) {
                j = n.getElementsByClassName(r);
                p = j.length;
                for (o = 0; o < p; o++) {
                    l = j[o];
                    if (k && l.tagName != k) {
                        continue
                    }
                    s[s.length] = l
                }
            } else {
                r = new RegExp("(^|\\s)" + c.string.escapeReg(r) + "(\\s|\x24)");
                j = k ? n.getElementsByTagName(k) : (n.all || n.getElementsByTagName("*"));
                p = j.length;
                for (o = 0; o < p; o++) {
                    l = j[o];
                    r.test(l.className) && (s[s.length] = l)
                }
            }
            return s
        };
        /*!
 * Sizzle CSS Selector Engine
 *  Copyright 2011, The Dojo Foundation
 *  Released under the MIT, BSD, and GPL Licenses.
 *  More information: http://sizzlejs.com/
 */
        (function() {
            var w = /((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^\[\]]*\]|['"][^'"]*['"]|[^\[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?((?:.|\r|\n)*)/g,
            p = "sizcache" + (Math.random() + "").replace(".", ""),
            x = 0,
            A = Object.prototype.toString,
            o = false,
            n = true,
            z = /\\/g,
            D = /\r\n/g,
            F = /\W/; [0, 0].sort(function() {
                n = false;
                return 0
            });
            var l = function(L, G, O, P) {
                O = O || [];
                G = G || document;
                var R = G;
                if (G.nodeType !== 1 && G.nodeType !== 9) {
                    return []
                }
                if (!L || typeof L !== "string") {
                    return O
                }
                var I,
                T,
                W,
                H,
                S,
                V,
                U,
                N,
                K = true,
                J = l.isXML(G),
                M = [],
                Q = L;
                do {
                    w.exec("");
                    I = w.exec(Q);
                    if (I) {
                        Q = I[3];
                        M.push(I[1]);
                        if (I[2]) {
                            H = I[3];
                            break
                        }
                    }
                }
                while (I);
                if (M.length > 1 && r.exec(L)) {
                    if (M.length === 2 && s.relative[M[0]]) {
                        T = B(M[0] + M[1], G, P)
                    } else {
                        T = s.relative[M[0]] ? [G] : l(M.shift(), G);
                        while (M.length) {
                            L = M.shift();
                            if (s.relative[L]) {
                                L += M.shift()
                            }
                            T = B(L, T, P)
                        }
                    }
                } else {
                    if (!P && M.length > 1 && G.nodeType === 9 && !J && s.match.ID.test(M[0]) && !s.match.ID.test(M[M.length - 1])) {
                        S = l.find(M.shift(), G, J);
                        G = S.expr ? l.filter(S.expr, S.set)[0] : S.set[0]
                    }
                    if (G) {
                        S = P ? {
                            expr: M.pop(),
                            set: t(P)
                        }: l.find(M.pop(), M.length === 1 && (M[0] === "~" || M[0] === "+") && G.parentNode ? G.parentNode: G, J);
                        T = S.expr ? l.filter(S.expr, S.set) : S.set;
                        if (M.length > 0) {
                            W = t(T)
                        } else {
                            K = false
                        }
                        while (M.length) {
                            V = M.pop();
                            U = V;
                            if (!s.relative[V]) {
                                V = ""
                            } else {
                                U = M.pop()
                            }
                            if (U == null) {
                                U = G
                            }
                            s.relative[V](W, U, J)
                        }
                    } else {
                        W = M = []
                    }
                }
                if (!W) {
                    W = T
                }
                if (!W) {
                    l.error(V || L)
                }
                if (A.call(W) === "[object Array]") {
                    if (!K) {
                        O.push.apply(O, W)
                    } else {
                        if (G && G.nodeType === 1) {
                            for (N = 0; W[N] != null; N++) {
                                if (W[N] && (W[N] === true || W[N].nodeType === 1 && l.contains(G, W[N]))) {
                                    O.push(T[N])
                                }
                            }
                        } else {
                            for (N = 0; W[N] != null; N++) {
                                if (W[N] && W[N].nodeType === 1) {
                                    O.push(T[N])
                                }
                            }
                        }
                    }
                } else {
                    t(W, O)
                }
                if (H) {
                    l(H, R, O, P);
                    l.uniqueSort(O)
                }
                return O
            };
            l.uniqueSort = function(H) {
                if (y) {
                    o = n;
                    H.sort(y);
                    if (o) {
                        for (var G = 1; G < H.length; G++) {
                            if (H[G] === H[G - 1]) {
                                H.splice(G--, 1)
                            }
                        }
                    }
                }
                return H
            };
            l.matches = function(G, H) {
                return l(G, null, null, H)
            };
            l.matchesSelector = function(G, H) {
                return l(H, null, null, [G]).length > 0
            };
            l.find = function(N, G, O) {
                var M,
                I,
                K,
                J,
                L,
                H;
                if (!N) {
                    return []
                }
                for (I = 0, K = s.order.length; I < K; I++) {
                    L = s.order[I];
                    if ((J = s.leftMatch[L].exec(N))) {
                        H = J[1];
                        J.splice(1, 1);
                        if (H.substr(H.length - 1) !== "\\") {
                            J[1] = (J[1] || "").replace(z, "");
                            M = s.find[L](J, G, O);
                            if (M != null) {
                                N = N.replace(s.match[L], "");
                                break
                            }
                        }
                    }
                }
                if (!M) {
                    M = typeof G.getElementsByTagName !== "undefined" ? G.getElementsByTagName("*") : []
                }
                return {
                    set: M,
                    expr: N
                }
            };
            l.filter = function(R, Q, U, K) {
                var M,
                G,
                P,
                W,
                T,
                H,
                J,
                L,
                S,
                I = R,
                V = [],
                O = Q,
                N = Q && Q[0] && l.isXML(Q[0]);
                while (R && Q.length) {
                    for (P in s.filter) {
                        if ((M = s.leftMatch[P].exec(R)) != null && M[2]) {
                            H = s.filter[P];
                            J = M[1];
                            G = false;
                            M.splice(1, 1);
                            if (J.substr(J.length - 1) === "\\") {
                                continue
                            }
                            if (O === V) {
                                V = []
                            }
                            if (s.preFilter[P]) {
                                M = s.preFilter[P](M, O, U, V, K, N);
                                if (!M) {
                                    G = W = true
                                } else {
                                    if (M === true) {
                                        continue
                                    }
                                }
                            }
                            if (M) {
                                for (L = 0; (T = O[L]) != null; L++) {
                                    if (T) {
                                        W = H(T, M, L, O);
                                        S = K ^ W;
                                        if (U && W != null) {
                                            if (S) {
                                                G = true
                                            } else {
                                                O[L] = false
                                            }
                                        } else {
                                            if (S) {
                                                V.push(T);
                                                G = true
                                            }
                                        }
                                    }
                                }
                            }
                            if (W !== undefined) {
                                if (!U) {
                                    O = V
                                }
                                R = R.replace(s.match[P], "");
                                if (!G) {
                                    return []
                                }
                                break
                            }
                        }
                    }
                    if (R === I) {
                        if (G == null) {
                            l.error(R)
                        } else {
                            break
                        }
                    }
                    I = R
                }
                return O
            };
            l.error = function(G) {
                throw "Syntax error, unrecognized expression: " + G
            };
            var j = l.getText = function(K) {
                var I,
                J,
                G = K.nodeType,
                H = "";
                if (G) {
                    if (G === 1) {
                        if (typeof K.textContent === "string") {
                            return K.textContent
                        } else {
                            if (typeof K.innerText === "string") {
                                return K.innerText.replace(D, "")
                            } else {
                                for (K = K.firstChild; K; K = K.nextSibling) {
                                    H += j(K)
                                }
                            }
                        }
                    } else {
                        if (G === 3 || G === 4) {
                            return K.nodeValue
                        }
                    }
                } else {
                    for (I = 0; (J = K[I]); I++) {
                        if (J.nodeType !== 8) {
                            H += j(J)
                        }
                    }
                }
                return H
            };
            var s = l.selectors = {
                order: ["ID", "NAME", "TAG"],
                match: {
                    ID: /#((?:[\w\u00c0-\uFFFF\-]|\\.)+)/,
                    CLASS: /\.((?:[\w\u00c0-\uFFFF\-]|\\.)+)/,
                    NAME: /\[name=['"]*((?:[\w\u00c0-\uFFFF\-]|\\.)+)['"]*\]/,
                    ATTR: /\[\s*((?:[\w\u00c0-\uFFFF\-]|\\.)+)\s*(?:(\S?=)\s*(?:(['"])(.*?)\3|(#?(?:[\w\u00c0-\uFFFF\-]|\\.)*)|)|)\s*\]/,
                    TAG: /^((?:[\w\u00c0-\uFFFF\*\-]|\\.)+)/,
                    CHILD: /:(only|nth|last|first)-child(?:\(\s*(even|odd|(?:[+\-]?\d+|(?:[+\-]?\d*)?n\s*(?:[+\-]\s*\d+)?))\s*\))?/,
                    POS: /:(nth|eq|gt|lt|first|last|even|odd)(?:\((\d*)\))?(?=[^\-]|$)/,
                    PSEUDO: /:((?:[\w\u00c0-\uFFFF\-]|\\.)+)(?:\((['"]?)((?:\([^\)]+\)|[^\(\)]*)+)\2\))?/
                },
                leftMatch: {},
                attrMap: {
                    "class": "className",
                    "for": "htmlFor"
                },
                attrHandle: {
                    href: function(G) {
                        return G.getAttribute("href")
                    },
                    type: function(G) {
                        return G.getAttribute("type")
                    }
                },
                relative: {
                    "+": function(M, H) {
                        var J = typeof H === "string",
                        L = J && !F.test(H),
                        N = J && !L;
                        if (L) {
                            H = H.toLowerCase()
                        }
                        for (var I = 0, G = M.length, K; I < G; I++) {
                            if ((K = M[I])) {
                                while ((K = K.previousSibling) && K.nodeType !== 1) {}
                                M[I] = N || K && K.nodeName.toLowerCase() === H ? K || false: K === H
                            }
                        }
                        if (N) {
                            l.filter(H, M, true)
                        }
                    },
                    ">": function(M, H) {
                        var L,
                        K = typeof H === "string",
                        I = 0,
                        G = M.length;
                        if (K && !F.test(H)) {
                            H = H.toLowerCase();
                            for (; I < G; I++) {
                                L = M[I];
                                if (L) {
                                    var J = L.parentNode;
                                    M[I] = J.nodeName.toLowerCase() === H ? J: false
                                }
                            }
                        } else {
                            for (; I < G; I++) {
                                L = M[I];
                                if (L) {
                                    M[I] = K ? L.parentNode: L.parentNode === H
                                }
                            }
                            if (K) {
                                l.filter(H, M, true)
                            }
                        }
                    },
                    "": function(J, H, L) {
                        var K,
                        I = x++,
                        G = C;
                        if (typeof H === "string" && !F.test(H)) {
                            H = H.toLowerCase();
                            K = H;
                            G = i
                        }
                        G("parentNode", H, I, J, K, L)
                    },
                    "~": function(J, H, L) {
                        var K,
                        I = x++,
                        G = C;
                        if (typeof H === "string" && !F.test(H)) {
                            H = H.toLowerCase();
                            K = H;
                            G = i
                        }
                        G("previousSibling", H, I, J, K, L)
                    }
                },
                find: {
                    ID: function(H, I, J) {
                        if (typeof I.getElementById !== "undefined" && !J) {
                            var G = I.getElementById(H[1]);
                            return G && G.parentNode ? [G] : []
                        }
                    },
                    NAME: function(I, L) {
                        if (typeof L.getElementsByName !== "undefined") {
                            var H = [],
                            K = L.getElementsByName(I[1]);
                            for (var J = 0, G = K.length; J < G; J++) {
                                if (K[J].getAttribute("name") === I[1]) {
                                    H.push(K[J])
                                }
                            }
                            return H.length === 0 ? null: H
                        }
                    },
                    TAG: function(G, H) {
                        if (typeof H.getElementsByTagName !== "undefined") {
                            return H.getElementsByTagName(G[1])
                        }
                    }
                },
                preFilter: {
                    CLASS: function(J, H, I, G, M, N) {
                        J = " " + J[1].replace(z, "") + " ";
                        if (N) {
                            return J
                        }
                        for (var K = 0, L; (L = H[K]) != null; K++) {
                            if (L) {
                                if (M ^ (L.className && (" " + L.className + " ").replace(/[\t\n\r]/g, " ").indexOf(J) >= 0)) {
                                    if (!I) {
                                        G.push(L)
                                    }
                                } else {
                                    if (I) {
                                        H[K] = false
                                    }
                                }
                            }
                        }
                        return false
                    },
                    ID: function(G) {
                        return G[1].replace(z, "")
                    },
                    TAG: function(H, G) {
                        return H[1].replace(z, "").toLowerCase()
                    },
                    CHILD: function(G) {
                        if (G[1] === "nth") {
                            if (!G[2]) {
                                l.error(G[0])
                            }
                            G[2] = G[2].replace(/^\+|\s*/g, "");
                            var H = /(-?)(\d*)(?:n([+\-]?\d*))?/.exec(G[2] === "even" && "2n" || G[2] === "odd" && "2n+1" || !/\D/.test(G[2]) && "0n+" + G[2] || G[2]);
                            G[2] = (H[1] + (H[2] || 1)) - 0;
                            G[3] = H[3] - 0
                        } else {
                            if (G[2]) {
                                l.error(G[0])
                            }
                        }
                        G[0] = x++;
                        return G
                    },
                    ATTR: function(K, H, I, G, L, M) {
                        var J = K[1] = K[1].replace(z, "");
                        if (!M && s.attrMap[J]) {
                            K[1] = s.attrMap[J]
                        }
                        K[4] = (K[4] || K[5] || "").replace(z, "");
                        if (K[2] === "~=") {
                            K[4] = " " + K[4] + " "
                        }
                        return K
                    },
                    PSEUDO: function(K, H, I, G, L) {
                        if (K[1] === "not") {
                            if ((w.exec(K[3]) || "").length > 1 || /^\w/.test(K[3])) {
                                K[3] = l(K[3], null, null, H)
                            } else {
                                var J = l.filter(K[3], H, I, true ^ L);
                                if (!I) {
                                    G.push.apply(G, J)
                                }
                                return false
                            }
                        } else {
                            if (s.match.POS.test(K[0]) || s.match.CHILD.test(K[0])) {
                                return true
                            }
                        }
                        return K
                    },
                    POS: function(G) {
                        G.unshift(true);
                        return G
                    }
                },
                filters: {
                    enabled: function(G) {
                        return G.disabled === false && G.type !== "hidden"
                    },
                    disabled: function(G) {
                        return G.disabled === true
                    },
                    checked: function(G) {
                        return G.checked === true
                    },
                    selected: function(G) {
                        if (G.parentNode) {
                            G.parentNode.selectedIndex
                        }
                        return G.selected === true
                    },
                    parent: function(G) {
                        return !! G.firstChild
                    },
                    empty: function(G) {
                        return ! G.firstChild
                    },
                    has: function(I, H, G) {
                        return !! l(G[3], I).length
                    },
                    header: function(G) {
                        return (/h\d/i).test(G.nodeName)
                    },
                    text: function(I) {
                        var G = I.getAttribute("type"),
                        H = I.type;
                        return I.nodeName.toLowerCase() === "input" && "text" === H && (G === H || G === null)
                    },
                    radio: function(G) {
                        return G.nodeName.toLowerCase() === "input" && "radio" === G.type
                    },
                    checkbox: function(G) {
                        return G.nodeName.toLowerCase() === "input" && "checkbox" === G.type
                    },
                    file: function(G) {
                        return G.nodeName.toLowerCase() === "input" && "file" === G.type
                    },
                    password: function(G) {
                        return G.nodeName.toLowerCase() === "input" && "password" === G.type
                    },
                    submit: function(H) {
                        var G = H.nodeName.toLowerCase();
                        return (G === "input" || G === "button") && "submit" === H.type
                    },
                    image: function(G) {
                        return G.nodeName.toLowerCase() === "input" && "image" === G.type
                    },
                    reset: function(H) {
                        var G = H.nodeName.toLowerCase();
                        return (G === "input" || G === "button") && "reset" === H.type
                    },
                    button: function(H) {
                        var G = H.nodeName.toLowerCase();
                        return G === "input" && "button" === H.type || G === "button"
                    },
                    input: function(G) {
                        return (/input|select|textarea|button/i).test(G.nodeName)
                    },
                    focus: function(G) {
                        return G === G.ownerDocument.activeElement
                    }
                },
                setFilters: {
                    first: function(H, G) {
                        return G === 0
                    },
                    last: function(I, H, G, J) {
                        return H === J.length - 1
                    },
                    even: function(H, G) {
                        return G % 2 === 0
                    },
                    odd: function(H, G) {
                        return G % 2 === 1
                    },
                    lt: function(I, H, G) {
                        return H < G[3] - 0
                    },
                    gt: function(I, H, G) {
                        return H > G[3] - 0
                    },
                    nth: function(I, H, G) {
                        return G[3] - 0 === H
                    },
                    eq: function(I, H, G) {
                        return G[3] - 0 === H
                    }
                },
                filter: {
                    PSEUDO: function(I, N, M, O) {
                        var G = N[1],
                        H = s.filters[G];
                        if (H) {
                            return H(I, M, N, O)
                        } else {
                            if (G === "contains") {
                                return (I.textContent || I.innerText || j([I]) || "").indexOf(N[3]) >= 0
                            } else {
                                if (G === "not") {
                                    var J = N[3];
                                    for (var L = 0, K = J.length; L < K; L++) {
                                        if (J[L] === I) {
                                            return false
                                        }
                                    }
                                    return true
                                } else {
                                    l.error(G)
                                }
                            }
                        }
                    },
                    CHILD: function(I, K) {
                        var J,
                        Q,
                        M,
                        P,
                        G,
                        L,
                        O,
                        N = K[1],
                        H = I;
                        switch (N) {
                        case "only":
                        case "first":
                            while ((H = H.previousSibling)) {
                                if (H.nodeType === 1) {
                                    return false
                                }
                            }
                            if (N === "first") {
                                return true
                            }
                            H = I;
                        case "last":
                            while ((H = H.nextSibling)) {
                                if (H.nodeType === 1) {
                                    return false
                                }
                            }
                            return true;
                        case "nth":
                            J = K[2];
                            Q = K[3];
                            if (J === 1 && Q === 0) {
                                return true
                            }
                            M = K[0];
                            P = I.parentNode;
                            if (P && (P[p] !== M || !I.nodeIndex)) {
                                L = 0;
                                for (H = P.firstChild; H; H = H.nextSibling) {
                                    if (H.nodeType === 1) {
                                        H.nodeIndex = ++L
                                    }
                                }
                                P[p] = M
                            }
                            O = I.nodeIndex - Q;
                            if (J === 0) {
                                return O === 0
                            } else {
                                return (O % J === 0 && O / J >= 0)
                            }
                        }
                    },
                    ID: function(H, G) {
                        return H.nodeType === 1 && H.getAttribute("id") === G
                    },
                    TAG: function(H, G) {
                        return (G === "*" && H.nodeType === 1) || !!H.nodeName && H.nodeName.toLowerCase() === G
                    },
                    CLASS: function(H, G) {
                        return (" " + (H.className || H.getAttribute("class")) + " ").indexOf(G) > -1
                    },
                    ATTR: function(L, J) {
                        var I = J[1],
                        G = l.attr ? l.attr(L, I) : s.attrHandle[I] ? s.attrHandle[I](L) : L[I] != null ? L[I] : L.getAttribute(I),
                        M = G + "",
                        K = J[2],
                        H = J[4];
                        return G == null ? K === "!=": !K && l.attr ? G != null: K === "=" ? M === H: K === "*=" ? M.indexOf(H) >= 0: K === "~=" ? (" " + M + " ").indexOf(H) >= 0: !H ? M && G !== false: K === "!=" ? M !== H: K === "^=" ? M.indexOf(H) === 0: K === "$=" ? M.substr(M.length - H.length) === H: K === "|=" ? M === H || M.substr(0, H.length + 1) === H + "-": false
                    },
                    POS: function(K, H, I, L) {
                        var G = H[2],
                        J = s.setFilters[G];
                        if (J) {
                            return J(K, I, H, L)
                        }
                    }
                }
            };
            var r = s.match.POS,
            k = function(H, G) {
                return "\\" + (G - 0 + 1)
            };
            for (var m in s.match) {
                s.match[m] = new RegExp(s.match[m].source + (/(?![^\[]*\])(?![^\(]*\))/.source));
                s.leftMatch[m] = new RegExp(/(^(?:.|\r|\n)*?)/.source + s.match[m].source.replace(/\\(\d+)/g, k))
            }
            var t = function(H, G) {
                H = Array.prototype.slice.call(H, 0);
                if (G) {
                    G.push.apply(G, H);
                    return G
                }
                return H
            };
            try {
                Array.prototype.slice.call(document.documentElement.childNodes, 0)[0].nodeType
            } catch(E) {
                t = function(K, J) {
                    var I = 0,
                    H = J || [];
                    if (A.call(K) === "[object Array]") {
                        Array.prototype.push.apply(H, K)
                    } else {
                        if (typeof K.length === "number") {
                            for (var G = K.length; I < G; I++) {
                                H.push(K[I])
                            }
                        } else {
                            for (; K[I]; I++) {
                                H.push(K[I])
                            }
                        }
                    }
                    return H
                }
            }
            var y,
            u;
            if (document.documentElement.compareDocumentPosition) {
                y = function(H, G) {
                    if (H === G) {
                        o = true;
                        return 0
                    }
                    if (!H.compareDocumentPosition || !G.compareDocumentPosition) {
                        return H.compareDocumentPosition ? -1: 1
                    }
                    return H.compareDocumentPosition(G) & 4 ? -1: 1
                }
            } else {
                y = function(O, N) {
                    if (O === N) {
                        o = true;
                        return 0
                    } else {
                        if (O.sourceIndex && N.sourceIndex) {
                            return O.sourceIndex - N.sourceIndex
                        }
                    }
                    var L,
                    H,
                    I = [],
                    G = [],
                    K = O.parentNode,
                    M = N.parentNode,
                    P = K;
                    if (K === M) {
                        return u(O, N)
                    } else {
                        if (!K) {
                            return - 1
                        } else {
                            if (!M) {
                                return 1
                            }
                        }
                    }
                    while (P) {
                        I.unshift(P);
                        P = P.parentNode
                    }
                    P = M;
                    while (P) {
                        G.unshift(P);
                        P = P.parentNode
                    }
                    L = I.length;
                    H = G.length;
                    for (var J = 0; J < L && J < H; J++) {
                        if (I[J] !== G[J]) {
                            return u(I[J], G[J])
                        }
                    }
                    return J === L ? u(O, G[J], -1) : u(I[J], N, 1)
                };
                u = function(H, G, I) {
                    if (H === G) {
                        return I
                    }
                    var J = H.nextSibling;
                    while (J) {
                        if (J === G) {
                            return - 1
                        }
                        J = J.nextSibling
                    }
                    return 1
                }
            } (function() {
                var H = document.createElement("div"),
                I = "script" + (new Date()).getTime(),
                G = document.documentElement;
                H.innerHTML = "<a name='" + I + "'/>";
                G.insertBefore(H, G.firstChild);
                if (document.getElementById(I)) {
                    s.find.ID = function(K, L, M) {
                        if (typeof L.getElementById !== "undefined" && !M) {
                            var J = L.getElementById(K[1]);
                            return J ? J.id === K[1] || typeof J.getAttributeNode !== "undefined" && J.getAttributeNode("id").nodeValue === K[1] ? [J] : undefined: []
                        }
                    };
                    s.filter.ID = function(L, J) {
                        var K = typeof L.getAttributeNode !== "undefined" && L.getAttributeNode("id");
                        return L.nodeType === 1 && K && K.nodeValue === J
                    }
                }
                G.removeChild(H);
                G = H = null
            })(); (function() {
                var G = document.createElement("div");
                G.appendChild(document.createComment(""));
                if (G.getElementsByTagName("*").length > 0) {
                    s.find.TAG = function(H, L) {
                        var K = L.getElementsByTagName(H[1]);
                        if (H[1] === "*") {
                            var J = [];
                            for (var I = 0; K[I]; I++) {
                                if (K[I].nodeType === 1) {
                                    J.push(K[I])
                                }
                            }
                            K = J
                        }
                        return K
                    }
                }
                G.innerHTML = "<a href='#'></a>";
                if (G.firstChild && typeof G.firstChild.getAttribute !== "undefined" && G.firstChild.getAttribute("href") !== "#") {
                    s.attrHandle.href = function(H) {
                        return H.getAttribute("href", 2)
                    }
                }
                G = null
            })();
            if (document.querySelectorAll) { (function() {
                    var G = l,
                    J = document.createElement("div"),
                    I = "__sizzle__";
                    J.innerHTML = "<p class='TEST'></p>";
                    if (J.querySelectorAll && J.querySelectorAll(".TEST").length === 0) {
                        return
                    }
                    l = function(U, L, P, T) {
                        L = L || document;
                        if (!T && !l.isXML(L)) {
                            var S = /^(\w+$)|^\.([\w\-]+$)|^#([\w\-]+$)/.exec(U);
                            if (S && (L.nodeType === 1 || L.nodeType === 9)) {
                                if (S[1]) {
                                    return t(L.getElementsByTagName(U), P)
                                } else {
                                    if (S[2] && s.find.CLASS && L.getElementsByClassName) {
                                        return t(L.getElementsByClassName(S[2]), P)
                                    }
                                }
                            }
                            if (L.nodeType === 9) {
                                if (U === "body" && L.body) {
                                    return t([L.body], P)
                                } else {
                                    if (S && S[3]) {
                                        var O = L.getElementById(S[3]);
                                        if (O && O.parentNode) {
                                            if (O.id === S[3]) {
                                                return t([O], P)
                                            }
                                        } else {
                                            return t([], P)
                                        }
                                    }
                                }
                                try {
                                    return t(L.querySelectorAll(U), P)
                                } catch(Q) {}
                            } else {
                                if (L.nodeType === 1 && L.nodeName.toLowerCase() !== "object") {
                                    var M = L,
                                    N = L.getAttribute("id"),
                                    K = N || I,
                                    W = L.parentNode,
                                    V = /^\s*[+~]/.test(U);
                                    if (!N) {
                                        L.setAttribute("id", K)
                                    } else {
                                        K = K.replace(/'/g, "\\$&")
                                    }
                                    if (V && W) {
                                        L = L.parentNode
                                    }
                                    try {
                                        if (!V || W) {
                                            return t(L.querySelectorAll("[id='" + K + "'] " + U), P)
                                        }
                                    } catch(R) {} finally {
                                        if (!N) {
                                            M.removeAttribute("id")
                                        }
                                    }
                                }
                            }
                        }
                        return G(U, L, P, T)
                    };
                    for (var H in G) {
                        l[H] = G[H]
                    }
                    J = null
                })()
            } (function() {
                var G = document.documentElement,
                I = G.matchesSelector || G.mozMatchesSelector || G.webkitMatchesSelector || G.msMatchesSelector;
                if (I) {
                    var K = !I.call(document.createElement("div"), "div"),
                    H = false;
                    try {
                        I.call(document.documentElement, "[test!='']:sizzle")
                    } catch(J) {
                        H = true
                    }
                    l.matchesSelector = function(M, O) {
                        O = O.replace(/\=\s*([^'"\]]*)\s*\]/g, "='$1']");
                        if (!l.isXML(M)) {
                            try {
                                if (H || !s.match.PSEUDO.test(O) && !/!=/.test(O)) {
                                    var L = I.call(M, O);
                                    if (L || !K || M.document && M.document.nodeType !== 11) {
                                        return L
                                    }
                                }
                            } catch(N) {}
                        }
                        return l(O, null, null, [M]).length > 0
                    }
                }
            })(); (function() {
                var G = document.createElement("div");
                G.innerHTML = "<div class='test e'></div><div class='test'></div>";
                if (!G.getElementsByClassName || G.getElementsByClassName("e").length === 0) {
                    return
                }
                G.lastChild.className = "e";
                if (G.getElementsByClassName("e").length === 1) {
                    return
                }
                s.order.splice(1, 0, "CLASS");
                s.find.CLASS = function(H, I, J) {
                    if (typeof I.getElementsByClassName !== "undefined" && !J) {
                        return I.getElementsByClassName(H[1])
                    }
                };
                G = null
            })();
            function i(H, M, L, P, N, O) {
                for (var J = 0, I = P.length; J < I; J++) {
                    var G = P[J];
                    if (G) {
                        var K = false;
                        G = G[H];
                        while (G) {
                            if (G[p] === L) {
                                K = P[G.sizset];
                                break
                            }
                            if (G.nodeType === 1 && !O) {
                                G[p] = L;
                                G.sizset = J
                            }
                            if (G.nodeName.toLowerCase() === M) {
                                K = G;
                                break
                            }
                            G = G[H]
                        }
                        P[J] = K
                    }
                }
            }
            function C(H, M, L, P, N, O) {
                for (var J = 0, I = P.length; J < I; J++) {
                    var G = P[J];
                    if (G) {
                        var K = false;
                        G = G[H];
                        while (G) {
                            if (G[p] === L) {
                                K = P[G.sizset];
                                break
                            }
                            if (G.nodeType === 1) {
                                if (!O) {
                                    G[p] = L;
                                    G.sizset = J
                                }
                                if (typeof M !== "string") {
                                    if (G === M) {
                                        K = true;
                                        break
                                    }
                                } else {
                                    if (l.filter(M, [G]).length > 0) {
                                        K = G;
                                        break
                                    }
                                }
                            }
                            G = G[H]
                        }
                        P[J] = K
                    }
                }
            }
            if (document.documentElement.contains) {
                l.contains = function(H, G) {
                    return H !== G && (H.contains ? H.contains(G) : true)
                }
            } else {
                if (document.documentElement.compareDocumentPosition) {
                    l.contains = function(H, G) {
                        return !! (H.compareDocumentPosition(G) & 16)
                    }
                } else {
                    l.contains = function() {
                        return false
                    }
                }
            }
            l.isXML = function(G) {
                var H = (G ? G.ownerDocument || G: 0).documentElement;
                return H ? H.nodeName !== "HTML": false
            };
            var B = function(I, G, M) {
                var L,
                N = [],
                K = "",
                O = G.nodeType ? [G] : G;
                while ((L = s.match.PSEUDO.exec(I))) {
                    K += L[0];
                    I = I.replace(s.match.PSEUDO, "")
                }
                I = s.relative[I] ? I + "*": I;
                for (var J = 0, H = O.length; J < H; J++) {
                    l(I, O[J], N, M)
                }
                return l.filter(K, N)
            };
            c.dom.query = l
        })();
        c.dom.extend({
            removeClass: function(j) {
                if (arguments.length <= 0) {
                    c.forEach(this, 
                    function(k) {
                        k.className = ""
                    })
                }
                switch (typeof j) {
                case "string":
                    j = String(j).replace(/^\s+/g, "").replace(/\s+$/g, "").replace(/\s+/g, " ");
                    var i = j.split(" ");
                    c.forEach(this, 
                    function(l) {
                        var m = l.className;
                        for (var k = 0; k < i.length; k++) {
                            while ((" " + m + " ").indexOf(" " + i[k] + " ") >= 0) {
                                m = (" " + m + " ").replace(" " + i[k] + " ", " ")
                            }
                        }
                        l.className = m.replace(/^\s+/g, "").replace(/\s+$/g, "")
                    });
                    break;
                case "function":
                    c.forEach(this, 
                    function(m, k, l) {
                        c.dom(m).removeClass(j.call(m, k, m.className))
                    });
                    break
                }
                return this
            }
        });
        c.dom.extend({
            removeData: function() {
                var i = c.key,
                j = c.global("_maps_HTMLElementData");
                return function(k) {
                    c.forEach(this, 
                    function(l) { ! l[i] && (l[i] = c.id())
                    });
                    c.forEach(this, 
                    function(m) {
                        var l = j[m[i]];
                        if (typeof k == "string") {
                            l && delete l[k]
                        } else {
                            if (c.type(k) == "array") {
                                c.forEach(k, 
                                function(n) {
                                    l && delete l[n]
                                })
                            }
                        }
                    });
                    return this
                }
            } ()
        });
        c.dom.extend({
            removeProp: function(k) {
                if (arguments.length <= 0 || !k || typeof k !== "string") {
                    return this
                }
                var j = c.dom,
                i = c._util_;
                k = i.propFix[k] || k;
                c.forEach(this, 
                function(l) {
                    try {
                        l[k] = undefined;
                        delete l[k]
                    } catch(m) {}
                });
                return this
            }
        });
        c.dom.removeStyle = function() {
            var j = document.createElement("DIV"),
            i,
            k = c.dom._g;
            if (j.style.removeProperty) {
                i = function(m, l) {
                    m = k(m);
                    m.style.removeProperty(l);
                    return m
                }
            } else {
                if (j.style.removeAttribute) {
                    i = function(m, l) {
                        m = k(m);
                        m.style.removeAttribute(c.string.toCamelCase(l));
                        return m
                    }
                }
            }
            j = null;
            return i
        } ();
        c.object.each = function(m, k) {
            var j,
            i,
            l;
            if ("function" == typeof k) {
                for (i in m) {
                    if (m.hasOwnProperty(i)) {
                        l = m[i];
                        j = k.call(m, l, i);
                        if (j === false) {
                            break
                        }
                    }
                }
            }
            return m
        };
        c.dom.extend({
            setStyles: function(j) {
                element = this[0];
                for (var i in j) {
                    c.dom.setStyle(element, i, j[i])
                }
                return element
            }
        });
        c.dom._styleFilter[c.dom._styleFilter.length] = {
            set: function(i, j) {
                if (j.constructor == Number && !/zIndex|fontWeight|opacity|zoom|lineHeight/i.test(i)) {
                    j = j + "px"
                }
                return j
            }
        };
        c.event.getTarget = function(i) {
            i.originalEvent && (i = i.originalEvent);
            return i.target || i.srcElement
        };
        c.dom.setBorderBoxSize = function(k, j) {
            var i = {};
            j.width && (i.width = parseFloat(j.width));
            j.height && (i.height = parseFloat(j.height));
            function l(n, m) {
                return parseFloat(c.dom.getStyle(n, m)) || 0
            }
            if (c.browser.isStrict) {
                if (j.width) {
                    i.width = parseFloat(j.width) - l(k, "paddingLeft") - l(k, "paddingRight") - l(k, "borderLeftWidth") - l(k, "borderRightWidth");
                    i.width < 0 && (i.width = 0)
                }
                if (j.height) {
                    i.height = parseFloat(j.height) - l(k, "paddingTop") - l(k, "paddingBottom") - l(k, "borderTopWidth") - l(k, "borderBottomWidth");
                    i.height < 0 && (i.height = 0)
                }
            }
            return c.dom.setStyles(k, i)
        };
        c.dom.setOuterHeight = c.dom.setBorderBoxHeight = function(j, i) {
            return c.dom.setBorderBoxSize(j, {
                height: i
            })
        };
        c.dom.setOuterWidth = c.dom.setBorderBoxWidth = function(i, j) {
            return c.dom.setBorderBoxSize(i, {
                width: j
            })
        };
        c.dom.resizable = function(l, o) {
            var J,
            w,
            r = {},
            k,
            i = {},
            B,
            H,
            E,
            j,
            m,
            t,
            y,
            C = false,
            s = false,
            F = {
                direction: ["e", "s", "se"],
                minWidth: 16,
                minHeight: 16,
                classPrefix: "tangram",
                directionHandlePosition: {}
            };
            if (! (J = c.dom.g(l)) && c.dom.getStyle(J, "position") == "static") {
                return false
            }
            j = J.offsetParent;
            var x = c.dom.getStyle(J, "position");
            w = c.extend(F, o);
            c.forEach(["minHeight", "minWidth", "maxHeight", "maxWidth"], 
            function(K) {
                w[K] && (w[K] = parseFloat(w[K]))
            });
            B = [w.minWidth || 0, w.maxWidth || Number.MAX_VALUE, w.minHeight || 0, w.maxHeight || Number.MAX_VALUE];
            I();
            function I() {
                t = c.extend({
                    e: {
                        right: "-5px",
                        top: "0px",
                        width: "7px",
                        height: J.offsetHeight
                    },
                    s: {
                        left: "0px",
                        bottom: "-5px",
                        height: "7px",
                        width: J.offsetWidth
                    },
                    n: {
                        left: "0px",
                        top: "-5px",
                        height: "7px",
                        width: J.offsetWidth
                    },
                    w: {
                        left: "-5px",
                        top: "0px",
                        height: J.offsetHeight,
                        width: "7px"
                    },
                    se: {
                        right: "1px",
                        bottom: "1px",
                        height: "16px",
                        width: "16px"
                    },
                    sw: {
                        left: "1px",
                        bottom: "1px",
                        height: "16px",
                        width: "16px"
                    },
                    ne: {
                        right: "1px",
                        top: "1px",
                        height: "16px",
                        width: "16px"
                    },
                    nw: {
                        left: "1px",
                        top: "1px",
                        height: "16px",
                        width: "16px"
                    }
                },
                w.directionHandlePosition);
                c.forEach(w.direction, 
                function(K) {
                    var L = w.classPrefix.split(" ");
                    L[0] = L[0] + "-resizable-" + K;
                    var N = c.dom.create("div", {
                        className: L.join(" ")
                    }),
                    M = t[K];
                    M.cursor = K + "-resize";
                    M.position = "absolute";
                    c.dom.setStyles(N, M);
                    N.key = K;
                    N.style.MozUserSelect = "none";
                    J.appendChild(N);
                    r[K] = N;
                    c.dom(N).on("mousedown", p)
                });
                C = false
            }
            function n() {
                m && D();
                c.object.each(r, 
                function(K) {
                    c.dom(K).off("mousedown", p);
                    c.dom.remove(K)
                });
                C = true
            }
            function u(K) {
                if (!C) {
                    w = c.extend(w, K || {});
                    n();
                    I()
                }
            }
            function p(M) {
                s && D();
                var L = c.event.getTarget(M),
                K = L.key;
                m = L;
                s = true;
                if (L.setCapture) {
                    L.setCapture()
                } else {
                    if (window.captureEvents) {
                        window.captureEvents(Event.MOUSEMOVE | Event.MOUSEUP)
                    }
                }
                E = c.dom.getStyle(document.body, "cursor");
                c.dom.setStyle(document.body, "cursor", K + "-resize");
                var O = c.dom(document.body);
                O.on("mouseup", D);
                O.on("selectstart", z);
                H = document.body.style.MozUserSelect;
                document.body.style.MozUserSelect = "none";
                var N = c.page.getMousePosition();
                i = A();
                y = setInterval(function() {
                    G(K, N)
                },
                20);
                c.isFunction(w.onresizestart) && w.onresizestart();
                c.event.preventDefault(M)
            }
            function D() {
                if (m && m.releaseCapture) {
                    m.releaseCapture()
                } else {
                    if (window.releaseEvents) {
                        window.releaseEvents(Event.MOUSEMOVE | Event.MOUSEUP)
                    }
                }
                c.dom(document.body).off("mouseup", D);
                c.dom(document).off("selectstart", z);
                document.body.style.MozUserSelect = H;
                c.dom(document.body).off("selectstart", z);
                clearInterval(y);
                c.dom.setStyle(document.body, "cursor", E);
                m = null;
                s = false;
                c.isFunction(w.onresizeend) && w.onresizeend()
            }
            function G(L, R) {
                var Q = c.page.getMousePosition(),
                M = i.width,
                K = i.height,
                P = i.top,
                O = i.left,
                N;
                if (L.indexOf("e") >= 0) {
                    M = Math.max(Q.x - R.x + i.width, B[0]);
                    M = Math.min(M, B[1])
                } else {
                    if (L.indexOf("w") >= 0) {
                        M = Math.max(R.x - Q.x + i.width, B[0]);
                        M = Math.min(M, B[1]);
                        O -= M - i.width
                    }
                }
                if (L.indexOf("s") >= 0) {
                    K = Math.max(Q.y - R.y + i.height, B[2]);
                    K = Math.min(K, B[3])
                } else {
                    if (L.indexOf("n") >= 0) {
                        K = Math.max(R.y - Q.y + i.height, B[2]);
                        K = Math.min(K, B[3]);
                        P -= K - i.height
                    }
                }
                N = {
                    width: M,
                    height: K,
                    top: P,
                    left: O
                };
                c.dom.setOuterHeight(J, K);
                c.dom.setOuterWidth(J, M);
                c.dom.setStyles(J, {
                    top: P,
                    left: O
                });
                r.n && c.dom.setStyle(r.n, "width", M);
                r.s && c.dom.setStyle(r.s, "width", M);
                r.e && c.dom.setStyle(r.e, "height", K);
                r.w && c.dom.setStyle(r.w, "height", K);
                c.isFunction(w.onresize) && w.onresize({
                    current: N,
                    original: i
                })
            }
            function z(K) {
                return c.event.preventDefault(K, false)
            }
            function A() {
                var K = c.dom.getPosition(J.offsetParent),
                L = c.dom.getPosition(J),
                N,
                M;
                if (x == "absolute") {
                    N = L.top - (J.offsetParent == document.body ? 0: K.top);
                    M = L.left - (J.offsetParent == document.body ? 0: K.left)
                } else {
                    N = parseFloat(c.dom.getStyle(J, "top")) || -parseFloat(c.dom.getStyle(J, "bottom")) || 0;
                    M = parseFloat(c.dom.getStyle(J, "left")) || -parseFloat(c.dom.getStyle(J, "right")) || 0
                }
                c.dom.setStyles(J, {
                    top: N,
                    left: M
                });
                return {
                    width: J.offsetWidth,
                    height: J.offsetHeight,
                    top: N,
                    left: M
                }
            }
            return {
                cancel: n,
                update: u,
                enable: I
            }
        };
        c._util_.smartScroll = function(l) {
            var i = {
                scrollLeft: "pageXOffset",
                scrollTop: "pageYOffset"
            } [l],
            m = l === "scrollLeft",
            j = {};
            function n(o) {
                return o && o.nodeType === 9
            }
            function k(o) {
                return c.type(o) == "Window" ? o: n(o) ? o.defaultView || o.parentWindow: false
            }
            return {
                get: function(o) {
                    var p = k(o);
                    return p ? (i in p) ? p[i] : c.browser.isStrict && p.document.documentElement[l] || p.document.body[l] : o[l]
                },
                set: function(o, r) {
                    if (!o) {
                        return
                    }
                    var p = k(o);
                    p ? p.scrollTo(m ? r: this.get(o), !m ? r: this.get(o)) : o[l] = r
                }
            }
        };
        c.dom.extend({
            scrollLeft: function() {
                var i = c._util_.smartScroll("scrollLeft");
                return function(j) {
                    j && c.check("^(?:number|string)$", "baidu.dom.scrollLeft");
                    if (this.size() <= 0) {
                        return j === undefined ? 0: this
                    }
                    return j === undefined ? i.get(this[0]) : i.set(this[0], j) || this
                }
            } ()
        });
        c.dom.extend({
            scrollTop: function() {
                var i = c._util_.smartScroll("scrollTop");
                return function(j) {
                    j && c.check("^(?:number|string)$", "baidu.dom.scrollTop");
                    if (this.size() <= 0) {
                        return j === undefined ? 0: this
                    }
                    return j === undefined ? i.get(this[0]) : i.set(this[0], j) || this
                }
            } ()
        });
        c.dom.extend({
            setAttrs: function(i) {
                element = this[0];
                for (var j in i) {
                    c.dom.setAttr(element, j, i[j])
                }
                return element
            }
        });
        c.dom.setPixel = function(j, i, k) {
            typeof k != "undefined" && (c.dom.g(j).style[i] = k + (!isNaN(k) ? "px": ""))
        };
        c.dom.setPosition = function(m, j) {
            var l = {
                left: j.left - (parseFloat(c.dom.getStyle(m, "margin-left")) || 0),
                top: j.top - (parseFloat(c.dom.getStyle(m, "margin-top")) || 0)
            };
            m = c.dom.g(m);
            for (var k in l) {
                c.dom.setStyle(m, k, l[k])
            }
            return m
        };
        c.dom.extend({
            show: function() {
                c._util_.showHide(this, true);
                return this
            }
        });
        c.dom.extend({
            siblings: function(i) {
                var j = [];
                c.forEach(this, 
                function(m) {
                    var l = [],
                    o = [],
                    k = m;
                    while (k = k.previousSibling) {
                        k.nodeType == 1 && l.push(k)
                    }
                    while (m = m.nextSibling) {
                        m.nodeType == 1 && o.push(m)
                    }
                    c.merge(j, l.reverse().concat(o))
                });
                return c.dom(c.dom.match(j, i))
            }
        });
        c.dom.extend({
            slice: function() {
                var i = Array.prototype.slice;
                return function(k, j) {
                    c.check("number(,number)?", "baidu.dom.slice");
                    return c.dom(i.apply(this, arguments))
                }
            } ()
        });
        c.dom.toggle = function(i) {
            i = c.dom.g(i);
            i.style.display = i.style.display == "none" ? "": "none";
            return i
        };
        c.dom.extend({
            toggleClass: function(l, j) {
                var k = typeof l;
                var j = (typeof j === "undefined") ? j: Boolean(j);
                if (arguments.length <= 0) {
                    c.forEach(this, 
                    function(m) {
                        m.className = ""
                    })
                }
                switch (typeof l) {
                case "string":
                    l = l.replace(/^\s+/g, "").replace(/\s+$/g, "").replace(/\s+/g, " ");
                    var i = l.split(" ");
                    c.forEach(this, 
                    function(n) {
                        var o = n.className;
                        for (var m = 0; m < i.length; m++) {
                            if (((" " + o + " ").indexOf(" " + i[m] + " ") > -1) && (typeof j === "undefined")) {
                                o = (" " + o + " ").replace(" " + i[m] + " ", " ")
                            } else {
                                if (((" " + o + " ").indexOf(" " + i[m] + " ") === -1) && (typeof j === "undefined")) {
                                    o += " " + i[m]
                                } else {
                                    if (((" " + o + " ").indexOf(" " + i[m] + " ") === -1) && (j === true)) {
                                        o += " " + i[m]
                                    } else {
                                        if (((" " + o + " ").indexOf(" " + i[m] + " ") > -1) && (j === false)) {
                                            o = o.replace(i[m], "")
                                        }
                                    }
                                }
                            }
                        }
                        n.className = o.replace(/^\s+/g, "").replace(/\s+$/g, "")
                    });
                    break;
                case "function":
                    c.forEach(this, 
                    function(n, m) {
                        c.dom(n).toggleClass(l.call(n, m, n.className), j)
                    });
                    break
                }
                return this
            }
        });
        c.dom.toggleClass = function(i, j) {
            if (c.dom.hasClass(i, j)) {
                c.dom.removeClass(i, j)
            } else {
                c.dom.addClass(i, j)
            }
        };
        c.dom.extend({
            trigger: function() {
                var r = c._util_.eventBase;
                var i = /msie/i.test(navigator.userAgent);
                var z = {
                    keydown: 1,
                    keyup: 1,
                    keypress: 1
                };
                var j = {
                    click: 1,
                    dblclick: 1,
                    mousedown: 1,
                    mousemove: 1,
                    mouseup: 1,
                    mouseover: 1,
                    mouseout: 1,
                    mouseenter: 1,
                    mouseleave: 1,
                    contextmenu: 1
                };
                var u = {
                    abort: 1,
                    blur: 1,
                    change: 1,
                    error: 1,
                    focus: 1,
                    focusin: 1,
                    focusout: 1,
                    load: 1,
                    unload: 1,
                    reset: 1,
                    resize: 1,
                    scroll: 1,
                    select: 1,
                    submit: 1
                };
                var s = {
                    scroll: 1,
                    resize: 1,
                    reset: 1,
                    submit: 1,
                    change: 1,
                    select: 1,
                    error: 1,
                    abort: 1
                };
                var n = {
                    submit: 1
                };
                var y = {
                    KeyEvents: ["bubbles", "cancelable", "view", "ctrlKey", "altKey", "shiftKey", "metaKey", "keyCode", "charCode"],
                    MouseEvents: ["bubbles", "cancelable", "view", "detail", "screenX", "screenY", "clientX", "clientY", "ctrlKey", "altKey", "shiftKey", "metaKey", "button", "relatedTarget"],
                    HTMLEvents: ["bubbles", "cancelable"],
                    UIEvents: ["bubbles", "cancelable", "view", "detail"],
                    Events: ["bubbles", "cancelable"]
                };
                c.extend(s, z);
                c.extend(s, j);
                var p = function(B) {
                    return B.replace(/^\w/, 
                    function(C) {
                        return C.toUpperCase()
                    })
                };
                var w = function(E) {
                    var B = [],
                    D = 0,
                    C;
                    for (C in E) {
                        if (E.hasOwnProperty(C)) {
                            B[D++] = E[C]
                        }
                    }
                    return B
                };
                var m = function(F, D) {
                    var C = 0,
                    B = F.length,
                    E = {};
                    for (; C < B; C++) {
                        E[F[C]] = D[F[C]];
                        delete D[F[C]]
                    }
                    return E
                };
                var o = function(D, C, B) {
                    B = c.extend({},
                    B);
                    var E = w(m(y[C], B)),
                    F = document.createEvent(C);
                    E.unshift(D);
                    switch (C) {
                    case "KeyEvents":
                        F.initKeyEvent.apply(F, E);
                        break;
                    case "MouseEvents":
                        F.initMouseEvent.apply(F, E);
                        break;
                    case "UIEvents":
                        F.initUIEvent.apply(F, E);
                        break;
                    default:
                        F.initEvent.apply(F, E);
                        break
                    }
                    if (B.triggerData) {
                        F.triggerData = B.triggerData
                    }
                    c.extend(F, B);
                    return F
                };
                var l = function(B) {
                    var C;
                    if (document.createEventObject) {
                        C = document.createEventObject();
                        c.extend(C, B)
                    }
                    return C
                };
                var t = function(C, B) {
                    B = m(y.KeyEvents, B);
                    var E;
                    if (document.createEvent) {
                        try {
                            E = o(C, "KeyEvents", B)
                        } catch(D) {
                            try {
                                E = o(C, "Events", B)
                            } catch(D) {
                                E = o(C, "UIEvents", B)
                            }
                        }
                    } else {
                        B.keyCode = B.charCode > 0 ? B.charCode: B.keyCode;
                        E = l(B)
                    }
                    return E
                };
                var A = function(C, B) {
                    B = m(y.MouseEvents, B);
                    var D;
                    if (document.createEvent) {
                        D = o(C, "MouseEvents", B);
                        if (B.relatedTarget && !D.relatedTarget) {
                            if ("mouseout" == C.toLowerCase()) {
                                D.toElement = B.relatedTarget
                            } else {
                                if ("mouseover" == C.toLowerCase()) {
                                    D.fromElement = B.relatedTarget
                                }
                            }
                        }
                    } else {
                        B.button = B.button == 0 ? 1: B.button == 1 ? 4: c.lang.isNumber(B.button) ? B.button: 0;
                        D = l(B)
                    }
                    return D
                };
                var x = function(C, B) {
                    B.bubbles = s.hasOwnProperty(C);
                    B = m(y.HTMLEvents, B);
                    var E;
                    if (document.createEvent) {
                        try {
                            E = o(C, "HTMLEvents", B)
                        } catch(D) {
                            try {
                                E = o(C, "UIEvents", B)
                            } catch(D) {
                                E = o(C, "Events", B)
                            }
                        }
                    } else {
                        E = l(B)
                    }
                    return E
                };
                var k = function(C, E, D) {
                    var G;
                    var G = {
                        bubbles: true,
                        cancelable: true,
                        view: window,
                        detail: 1,
                        screenX: 0,
                        screenY: 0,
                        clientX: 0,
                        clientY: 0,
                        ctrlKey: false,
                        altKey: false,
                        shiftKey: false,
                        metaKey: false,
                        keyCode: 0,
                        charCode: 0,
                        button: 0,
                        relatedTarget: null
                    };
                    if (z[E]) {
                        G = t(E, G)
                    } else {
                        if (j[E]) {
                            G = A(E, G)
                        } else {
                            if (u[E]) {
                                G = x(E, G)
                            } else {
                                return c(C).triggerHandler(E, D)
                            }
                        }
                    }
                    if (G) {
                        if (D) {
                            G.triggerData = D
                        }
                        var B;
                        if (C.dispatchEvent) {
                            B = C.dispatchEvent(G)
                        } else {
                            if (C.fireEvent) {
                                B = C.fireEvent("on" + E, G)
                            }
                        }
                        if (B !== false && n[E]) {
                            try {
                                if (C[E]) {
                                    C[E]()
                                } else {
                                    if (E = p(E), C[E]) {
                                        C[E]()
                                    }
                                }
                            } catch(F) {}
                        }
                    }
                };
                return function(C, B) {
                    this.each(function() {
                        k(this, C, B)
                    });
                    return this
                }
            } ()
        });
        c.dom.extend({
            unbind: function(j, i) {
                return this.off(j, i)
            }
        });
        c.dom.extend({
            undelegate: function(i, k, j) {
                return this.off(k, i, j)
            }
        });
        c.dom.extend({
            unique: function(i) {
                return c.dom(c.array(this.toArray()).unique(i))
            }
        });
        c.dom._matchNode = function(i, k, l) {
            i = c.dom.g(i);
            for (var j = i[l]; j; j = j[k]) {
                if (j.nodeType == 1) {
                    return j
                }
            }
            return null
        };
        c.dom._styleFilter[c.dom._styleFilter.length] = {
            get: function(l, m) {
                if (/color/i.test(l) && m.indexOf("rgb(") != -1) {
                    var n = m.split(",");
                    m = "#";
                    for (var k = 0, j; j = n[k]; k++) {
                        j = parseInt(j.replace(/[^\d]/gi, ""), 10).toString(16);
                        m += j.length == 1 ? "0" + j: j
                    }
                    m = m.toUpperCase()
                }
                return m
            }
        };
        c.dom._styleFixer.display = c.browser.ie && c.browser.ie < 8 ? {
            set: function(i, j) {
                i = i.style;
                if (j == "inline-block") {
                    i.display = "inline";
                    i.zoom = 1
                } else {
                    i.display = j
                }
            }
        }: c.browser.firefox && c.browser.firefox < 3 ? {
            set: function(i, j) {
                i.style.display = j == "inline-block" ? "-moz-inline-box": j
            }
        }: null;
        c.dom._styleFixer["float"] = c.browser.ie ? "styleFloat": "cssFloat";
        c.dom._styleFixer.opacity = c.browser.ie ? {
            get: function(i) {
                var j = i.style.filter;
                return j && j.indexOf("opacity=") >= 0 ? (parseFloat(j.match(/opacity=([^)]*)/)[1]) / 100) + "": "1"
            },
            set: function(i, k) {
                var j = i.style;
                j.filter = (j.filter || "").replace(/alpha\([^\)]*\)/gi, "") + (k == 1 ? "": "alpha(opacity=" + k * 100 + ")");
                j.zoom = 1
            }
        }: null;
        c.dom._styleFixer.width = c.dom._styleFixer.height = {
            get: function(j, i, k) {
                var i = i.replace(/^[a-z]/, 
                function(m) {
                    return m.toUpperCase()
                }),
                l = j["client" + i] || j["offset" + i];
                return l > 0 ? l + "px": !k || k == "auto" ? 0 + "px": l
            },
            set: function(j, k, i) {
                j.style[i] = k
            }
        };
        c.dom._styleFixer.textOverflow = (function() {
            var j = {};
            function i(m) {
                var n = m.length;
                if (n > 0) {
                    n = m[n - 1];
                    m.length--
                } else {
                    n = null
                }
                return n
            }
            function k(m, n) {
                m[c.browser.firefox ? "textContent": "innerText"] = n
            }
            function l(w, r, B) {
                var t = c.browser.ie ? w.currentStyle || w.style: getComputedStyle(w, null),
                A = t.fontWeight,
                z = "font-family:" + t.fontFamily + ";font-size:" + t.fontSize + ";word-spacing:" + t.wordSpacing + ";font-weight:" + ((parseInt(A) || 0) == 401 ? 700: A) + ";font-style:" + t.fontStyle + ";font-variant:" + t.fontVariant,
                m = j[z];
                if (!m) {
                    t = w.appendChild(document.createElement("div"));
                    t.style.cssText = "float:left;" + z;
                    m = j[z] = [];
                    for (var x = 0; x < 256; x++) {
                        x == 32 ? (t.innerHTML = "&nbsp;") : k(t, String.fromCharCode(x));
                        m[x] = t.offsetWidth
                    }
                    k(t, "\u4e00");
                    m[256] = t.offsetWidth;
                    k(t, "\u4e00\u4e00");
                    m[257] = t.offsetWidth - m[256] * 2;
                    m[258] = m[".".charCodeAt(0)] * 3 + m[257] * 3;
                    w.removeChild(t)
                }
                for (var u = w.firstChild, y = m[256], p = m[257], n = m[258], D = [], B = B ? n: 0; u; u = u.nextSibling) {
                    if (r < B) {
                        w.removeChild(u)
                    } else {
                        if (u.nodeType == 3) {
                            for (var x = 0, C = u.nodeValue, s = C.length; x < s; x++) {
                                t = C.charCodeAt(x);
                                D[D.length] = [r, u, x];
                                r -= (x ? p: 0) + (t < 256 ? m[t] : y);
                                if (r < B) {
                                    break
                                }
                            }
                        } else {
                            t = u.tagName;
                            if (t == "IMG" || t == "TABLE") {
                                t = u;
                                u = u.previousSibling;
                                w.removeChild(t)
                            } else {
                                D[D.length] = [r, u];
                                r -= u.offsetWidth
                            }
                        }
                    }
                }
                if (r < B) {
                    while (t = i(D)) {
                        r = t[0];
                        u = t[1];
                        t = t[2];
                        if (u.nodeType == 3) {
                            if (r >= n) {
                                u.nodeValue = u.nodeValue.substring(0, t) + "...";
                                return true
                            } else {
                                if (!t) {
                                    w.removeChild(u)
                                }
                            }
                        } else {
                            if (l(u, r, true)) {
                                return true
                            } else {
                                w.removeChild(u)
                            }
                        }
                    }
                    w.innerHTML = ""
                }
            }
            return {
                get: function(o) {
                    var n = c.browser,
                    m = dom.getStyle;
                    return (n.opera ? m("OTextOverflow") : n.firefox ? o._baiduOverflow: m("textOverflow")) || "clip"
                },
                set: function(n, r) {
                    var m = c.browser;
                    if (n.tagName == "TD" || n.tagName == "TH" || m.firefox) {
                        n._baiduHTML && (n.innerHTML = n._baiduHTML);
                        if (r == "ellipsis") {
                            n._baiduHTML = n.innerHTML;
                            var s = document.createElement("div"),
                            p = n.appendChild(s).offsetWidth;
                            n.removeChild(s);
                            l(n, p)
                        } else {
                            n._baiduHTML = ""
                        }
                    }
                    s = n.style;
                    m.opera ? (s.OTextOverflow = r) : m.firefox ? (n._baiduOverflow = r) : (s.textOverflow = r)
                }
            }
        })();
        c.lang.isArray = c.isArray;
        c.lang.toArray = function(j) {
            if (j === null || j === undefined) {
                return []
            }
            if (c.lang.isArray(j)) {
                return j
            }
            if (typeof j.length !== "number" || typeof j === "string" || c.lang.isFunction(j)) {
                return [j]
            }
            if (j.item) {
                var i = j.length,
                k = new Array(i);
                while (i--) {
                    k[i] = j[i]
                }
                return k
            }
            return [].slice.call(j)
        };
        c.fn.extend({
            methodize: function(i) {
                var j = this.fn;
                return function() {
                    return j.apply(this, [(i ? this[i] : this)].concat([].slice.call(arguments)))
                }
            }
        });
        c.fn.extend({
            wrapReturnValue: function(k, j) {
                var i = this.fn;
                j = j | 0;
                return function() {
                    var l = i.apply(this, arguments);
                    if (!j) {
                        return new k(l)
                    }
                    if (j > 0) {
                        return new k(arguments[j - 1])
                    }
                    return l
                }
            }
        });
        c.fn.wrapReturnValue = function(i, k, j) {
            return c.fn(i).wrapReturnValue(k, j)
        };
        c.fn.extend({
            multize: function(j, i) {
                var l = this.fn;
                function k() {
                    var t = arguments[0],
                    p = j ? k: l,
                    n = [],
                    s = Array.prototype.slice.call(arguments, 0),
                    m;
                    if (t instanceof Array) {
                        for (var o = 0, r; r = t[o]; o++) {
                            s[0] = r;
                            m = p.apply(this, s);
                            if (i) {
                                m && (n = n.concat(m))
                            } else {
                                n.push(m)
                            }
                        }
                        return n
                    } else {
                        return l.apply(this, arguments)
                    }
                }
                return k
            }
        });
        c.fn.multize = function(k, j, i) {
            return c.fn(k).multize(j, i)
        };
        c.element = function(j) {
            var i = c.dom._g(j);
            if (!i && c.dom.query) {
                i = c.dom.query(j)
            }
            return new c.element.Element(i)
        };
        c.element.Element = function(i) {
            if (!c.element._init) {
                c.element._makeChain();
                c.element._init = true
            }
            this._dom = (i.tagName || "").toLowerCase() == "select" ? [i] : c.lang.toArray(i)
        };
        c.element._toChainFunction = function(k, j, i) {
            return c.fn.methodize(c.fn.wrapReturnValue(c.fn.multize(k, 0, 1), c.element.Element, j), "_dom")
        };
        c.element._makeChain = function() {
            var j = c.element.Element.prototype,
            k = c.element._toChainFunction;
            c.forEach(("draggable droppable resizable fixable").split(" "), 
            function(l) {
                j[l] = k(c.dom[l], 1)
            });
            c.forEach(("remove getText contains getAttr getPosition getStyle hasClass intersect hasAttr getComputedStyle").split(" "), 
            function(l) {
                j[l] = j[l.replace(/^get[A-Z]/g, i)] = k(c.dom[l], -1)
            });
            c.forEach(("addClass empty hide show insertAfter insertBefore insertHTML removeClass setAttr setAttrs setStyle setStyles show toggleClass toggle next first getAncestorByClass getAncestorBy getAncestorByTag getDocument getParent getWindow last next prev g removeStyle setBorderBoxSize setOuterWidth setOuterHeight setBorderBoxWidth setBorderBoxHeight setPosition children query").split(" "), 
            function(l) {
                j[l] = j[l.replace(/^get[A-Z]/g, i)] = k(c.dom[l], 0)
            });
            j.q = j.Q = k(function(m, l) {
                return c.dom.q.apply(this, [l, m].concat([].slice.call(arguments, 2)))
            },
            0);
            c.forEach(("on un").split(" "), 
            function(l) {
                j[l] = k(c.event[l], 0)
            });
            c.forEach(("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error").split(" "), 
            function(l) {
                j[l] = function(m) {
                    return this.on(l, m)
                }
            });
            function i(l) {
                return l.charAt(3).toLowerCase()
            }
        };
        c.element.extend = function(i) {
            var j = c.element;
            c.object.each(i, 
            function(l, k) {
                j.Element.prototype[k] = c.element._toChainFunction(l, -1)
            })
        };
        c.event.EventArg = function(l, n) {
            n = n || window;
            l = l || n.event;
            var m = n.document;
            this.target = (l.target) || l.srcElement;
            this.keyCode = l.which || l.keyCode;
            for (var i in l) {
                var j = l[i];
                if ("function" != typeof j) {
                    this[i] = j
                }
            }
            if (!this.pageX && this.pageX !== 0) {
                this.pageX = (l.clientX || 0) + (m.documentElement.scrollLeft || m.body.scrollLeft);
                this.pageY = (l.clientY || 0) + (m.documentElement.scrollTop || m.body.scrollTop)
            }
            this._event = l
        };
        c.event.EventArg.prototype.preventDefault = function() {
            if (this._event.preventDefault) {
                this._event.preventDefault()
            } else {
                this._event.returnValue = false
            }
            return this
        };
        c.event.EventArg.prototype.stopPropagation = function() {
            if (this._event.stopPropagation) {
                this._event.stopPropagation()
            } else {
                this._event.cancelBubble = true
            }
            return this
        };
        c.event.EventArg.prototype.stop = function() {
            return this.stopPropagation().preventDefault()
        };
        c.object.values = function(m) {
            var i = [],
            l = 0,
            j;
            for (j in m) {
                if (m.hasOwnProperty(j)) {
                    i[l++] = m[j]
                }
            }
            return i
        };
        c.lang.isNumber = c.isNumber;
        c.event.fire = function() {
            var l = c.browser,
            t = {
                keydown: 1,
                keyup: 1,
                keypress: 1
            },
            i = {
                click: 1,
                dblclick: 1,
                mousedown: 1,
                mousemove: 1,
                mouseup: 1,
                mouseover: 1,
                mouseout: 1
            },
            p = {
                abort: 1,
                blur: 1,
                change: 1,
                error: 1,
                focus: 1,
                load: l.ie ? 0: 1,
                reset: 1,
                resize: 1,
                scroll: 1,
                select: 1,
                submit: 1,
                unload: l.ie ? 0: 1
            },
            n = {
                scroll: 1,
                resize: 1,
                reset: 1,
                submit: 1,
                change: 1,
                select: 1,
                error: 1,
                abort: 1
            },
            s = {
                KeyEvents: ["bubbles", "cancelable", "view", "ctrlKey", "altKey", "shiftKey", "metaKey", "keyCode", "charCode"],
                MouseEvents: ["bubbles", "cancelable", "view", "detail", "screenX", "screenY", "clientX", "clientY", "ctrlKey", "altKey", "shiftKey", "metaKey", "button", "relatedTarget"],
                HTMLEvents: ["bubbles", "cancelable"],
                UIEvents: ["bubbles", "cancelable", "view", "detail"],
                Events: ["bubbles", "cancelable"]
            };
            c.object.extend(n, t);
            c.object.extend(n, i);
            function k(A, y) {
                var x = 0,
                w = A.length,
                z = {};
                for (; x < w; x++) {
                    z[A[x]] = y[A[x]];
                    delete y[A[x]]
                }
                return z
            }
            function m(y, x, w) {
                w = c.object.extend({},
                w);
                var z = c.object.values(k(s[x], w)),
                A = document.createEvent(x);
                z.unshift(y);
                if ("KeyEvents" == x) {
                    A.initKeyEvent.apply(A, z)
                } else {
                    if ("MouseEvents" == x) {
                        A.initMouseEvent.apply(A, z)
                    } else {
                        if ("UIEvents" == x) {
                            A.initUIEvent.apply(A, z)
                        } else {
                            A.initEvent.apply(A, z)
                        }
                    }
                }
                c.object.extend(A, w);
                return A
            }
            function j(w) {
                var x;
                if (document.createEventObject) {
                    x = document.createEventObject();
                    c.object.extend(x, w)
                }
                return x
            }
            function o(z, w) {
                w = k(s.KeyEvents, w);
                var A;
                if (document.createEvent) {
                    try {
                        A = m(z, "KeyEvents", w)
                    } catch(y) {
                        try {
                            A = m(z, "Events", w)
                        } catch(x) {
                            A = m(z, "UIEvents", w)
                        }
                    }
                } else {
                    w.keyCode = w.charCode > 0 ? w.charCode: w.keyCode;
                    A = j(w)
                }
                return A
            }
            function u(x, w) {
                w = k(s.MouseEvents, w);
                var y;
                if (document.createEvent) {
                    y = m(x, "MouseEvents", w);
                    if (w.relatedTarget && !y.relatedTarget) {
                        if ("mouseout" == x.toLowerCase()) {
                            y.toElement = w.relatedTarget
                        } else {
                            if ("mouseover" == x.toLowerCase()) {
                                y.fromElement = w.relatedTarget
                            }
                        }
                    }
                } else {
                    w.button = w.button == 0 ? 1: w.button == 1 ? 4: c.lang.isNumber(w.button) ? w.button: 0;
                    y = j(w)
                }
                return y
            }
            function r(y, w) {
                w.bubbles = n.hasOwnProperty(y);
                w = k(s.HTMLEvents, w);
                var A;
                if (document.createEvent) {
                    try {
                        A = m(y, "HTMLEvents", w)
                    } catch(z) {
                        try {
                            A = m(y, "UIEvents", w)
                        } catch(x) {
                            A = m(y, "Events", w)
                        }
                    }
                } else {
                    A = j(w)
                }
                return A
            }
            return function(x, y, w) {
                var z;
                y = y.replace(/^on/i, "");
                x = c.dom._g(x);
                w = c.object.extend({
                    bubbles: true,
                    cancelable: true,
                    view: window,
                    detail: 1,
                    screenX: 0,
                    screenY: 0,
                    clientX: 0,
                    clientY: 0,
                    ctrlKey: false,
                    altKey: false,
                    shiftKey: false,
                    metaKey: false,
                    keyCode: 0,
                    charCode: 0,
                    button: 0,
                    relatedTarget: null
                },
                w);
                if (t[y]) {
                    z = o(y, w)
                } else {
                    if (i[y]) {
                        z = u(y, w)
                    } else {
                        if (p[y]) {
                            z = r(y, w)
                        } else {
                            throw (new Error(y + " is not support!"))
                        }
                    }
                }
                if (z) {
                    if (x.dispatchEvent) {
                        x.dispatchEvent(z)
                    } else {
                        if (x.fireEvent) {
                            x.fireEvent("on" + y, z)
                        }
                    }
                }
            }
        } ();
        c.event.get = function(i, j) {
            return new c.event.EventArg(i, j)
        };
        c.event.getEvent = function(j) {
            if (window.event) {
                return window.event
            } else {
                var k = arguments.callee,
                i;
                do {
                    i = k.arguments[0];
                    if (i && (/Event/.test(i) || i.originalEvent)) {
                        return i.originalEvent || i
                    }
                }
                while (k = k.caller);
                return null
            }
        };
        c.event.getKeyCode = function(i) {
            i.originalEvent && (i = i.originalEvent);
            return i.which || i.keyCode
        };
        c.event.getPageX = function(j) {
            j.originalEvent && (j = j.originalEvent);
            var i = j.pageX,
            k = document;
            if (!i && i !== 0) {
                i = (j.clientX || 0) + (k.documentElement.scrollLeft || k.body.scrollLeft)
            }
            return i
        };
        c.event.getPageY = function(j) {
            j.originalEvent && (j = j.originalEvent);
            var i = j.pageY,
            k = document;
            if (!i && i !== 0) {
                i = (j.clientY || 0) + (k.documentElement.scrollTop || k.body.scrollTop)
            }
            return i
        };
        c.event.once = function(i, j, k) {
            return c.dom(c.dom._g(i)).one(j, k)[0]
        };
        c.event.stopPropagation = function(i) {
            i.originalEvent && (i = i.originalEvent);
            if (i.stopPropagation) {
                i.stopPropagation()
            } else {
                i.cancelBubble = true
            }
        };
        c.event.stop = function(i) {
            i.originalEvent && (i = i.originalEvent);
            c.event.stopPropagation(i);
            c.event.preventDefault(i)
        };
        c.fn.abstractMethod = function() {
            throw Error("unimplemented abstract method")
        };
        c.fn.extend({
            bind: function(i) {
                var j = this.fn,
                k = arguments.length > 1 ? Array.prototype.slice.call(arguments, 1) : null;
                return function() {
                    var m = c.type(j) === "string" ? i[j] : j,
                    l = k ? k.concat(Array.prototype.slice.call(arguments, 0)) : arguments;
                    return m.apply(i || m, l)
                }
            }
        });
        c.fn.bind = function(k, j) {
            var i = c.fn(k);
            return i.bind.apply(i, Array.prototype.slice.call(arguments, 1))
        };
        c.lang.register = function(j, l, i) {
            var k = j["\x06r"] || (j["\x06r"] = []);
            k[k.length] = l;
            for (var m in i) {
                j.prototype[m] = i[m]
            }
        };
        c.createChain("form", 
        function(i) {
            return typeof i === "undefined" ? new c.form.$Form() : new c.form.$Form(i)
        },
        function(i) {
            this.form = i
        });
        c.form.extend({
            json: function(n) {
                var l = this.form;
                var k = l.elements,
                n = n || 
                function(A, i) {
                    return A
                },
                r = {},
                y,
                t,
                z,
                m,
                j,
                x,
                w,
                u;
                function o(i, A) {
                    var B = r[i];
                    if (B) {
                        B.push || (r[i] = [B]);
                        r[i].push(A)
                    } else {
                        r[i] = A
                    }
                }
                for (var p = 0, s = k.length; p < s; p++) {
                    y = k[p];
                    z = y.name;
                    if (!y.disabled && z) {
                        t = y.type;
                        m = c.url.escapeSymbol(y.value);
                        switch (t) {
                        case "radio":
                        case "checkbox":
                            if (!y.checked) {
                                break
                            }
                        case "textarea":
                        case "text":
                        case "password":
                        case "hidden":
                        case "file":
                        case "select-one":
                            o(z, n(m, z));
                            break;
                        case "select-multiple":
                            j = y.options;
                            w = j.length;
                            for (x = 0; x < w; x++) {
                                u = j[x];
                                if (u.selected) {
                                    o(z, n(u.value, z))
                                }
                            }
                            break
                        }
                    }
                }
                return r
            }
        });
        c.form.extend({
            serialize: function(n) {
                var l = this.form;
                var k = l.elements,
                n = n || 
                function(A, i) {
                    return A
                },
                r = [],
                y,
                t,
                z,
                m,
                j,
                x,
                w,
                u;
                function o(i, A) {
                    r.push(i + "=" + A)
                }
                for (var p = 0, s = k.length; p < s; p++) {
                    y = k[p];
                    z = y.name;
                    if (!y.disabled && z) {
                        t = y.type;
                        m = c.url.escapeSymbol(y.value);
                        switch (t) {
                        case "radio":
                        case "checkbox":
                            if (!y.checked) {
                                break
                            }
                        case "textarea":
                        case "text":
                        case "password":
                        case "hidden":
                        case "file":
                        case "select-one":
                            o(z, n(m, z));
                            break;
                        case "select-multiple":
                            j = y.options;
                            w = j.length;
                            for (x = 0; x < w; x++) {
                                u = j[x];
                                if (u.selected) {
                                    o(z, n(u.value, z))
                                }
                            }
                            break
                        }
                    }
                }
                return r
            }
        });
        c.fx = c.fx || {};
        c.lang.inherits = function(o, m, l) {
            var k,
            n,
            i = o.prototype,
            j = new Function();
            j.prototype = m.prototype;
            n = o.prototype = new j();
            for (k in i) {
                n[k] = i[k]
            }
            o.prototype.constructor = o;
            o.superClass = m.prototype;
            typeof l == "string" && (n.__type = l);
            o.extend = function(r) {
                for (var p in r) {
                    n[p] = r[p]
                }
                return o
            };
            return o
        };
        c.fx.Timeline = function(i) {
            c.lang.Class.call(this);
            this.interval = 16;
            this.duration = 500;
            this.dynamic = true;
            c.object.extend(this, i)
        };
        c.lang.inherits(c.fx.Timeline, c.lang.Class, "baidu.fx.Timeline").extend({
            launch: function() {
                var i = this;
                i.dispatchEvent("onbeforestart");
                typeof i.initialize == "function" && i.initialize();
                i["\x06btime"] = new Date().getTime();
                i["\x06etime"] = i["\x06btime"] + (i.dynamic ? i.duration: 0);
                i["\x06pulsed"]();
                return i
            },
            "\x06pulsed": function() {
                var j = this;
                var i = new Date().getTime();
                j.percent = (i - j["\x06btime"]) / j.duration;
                j.dispatchEvent("onbeforeupdate");
                if (i >= j["\x06etime"]) {
                    typeof j.render == "function" && j.render(j.transition(j.percent = 1));
                    typeof j.finish == "function" && j.finish();
                    j.dispatchEvent("onafterfinish");
                    j.dispose();
                    return
                }
                typeof j.render == "function" && j.render(j.transition(j.percent));
                j.dispatchEvent("onafterupdate");
                j["\x06timer"] = setTimeout(function() {
                    j["\x06pulsed"]()
                },
                j.interval)
            },
            transition: function(i) {
                return i
            },
            cancel: function() {
                this["\x06timer"] && clearTimeout(this["\x06timer"]);
                this["\x06etime"] = this["\x06btime"];
                typeof this.restore == "function" && this.restore();
                this.dispatchEvent("oncancel");
                this.dispose()
            },
            end: function() {
                this["\x06timer"] && clearTimeout(this["\x06timer"]);
                this["\x06etime"] = this["\x06btime"];
                this["\x06pulsed"]()
            }
        });
        c.fx.create = function(l, j, k) {
            var m = new c.fx.Timeline(j);
            m.element = l;
            m.__type = k || m.__type;
            m["\x06original"] = {};
            var i = "baidu_current_effect";
            m.addEventListener("onbeforestart", 
            function() {
                var o = this,
                n;
                o.attribName = "att_" + o.__type.replace(/\W/g, "_");
                n = o.element.getAttribute(i);
                o.element.setAttribute(i, (n || "") + "|" + o.guid + "|", 0);
                if (!o.overlapping) { (n = o.element.getAttribute(o.attribName)) && window[c.guid]._instances[n].cancel();
                    o.element.setAttribute(o.attribName, o.guid, 0)
                }
            });
            m["\x06clean"] = function(p) {
                var o = this,
                n;
                if (p = o.element) {
                    p.removeAttribute(o.attribName);
                    n = p.getAttribute(i);
                    n = n.replace("|" + o.guid + "|", "");
                    if (!n) {
                        p.removeAttribute(i)
                    } else {
                        p.setAttribute(i, n, 0)
                    }
                }
            };
            m.addEventListener("oncancel", 
            function() {
                this["\x06clean"]();
                this["\x06restore"]()
            });
            m.addEventListener("onafterfinish", 
            function() {
                this["\x06clean"]();
                this.restoreAfterFinish && this["\x06restore"]()
            });
            m.protect = function(n) {
                this["\x06original"][n] = this.element.style[n]
            };
            m["\x06restore"] = function() {
                var t = this["\x06original"],
                r = this.element.style,
                n;
                for (var p in t) {
                    n = t[p];
                    if (typeof n == "undefined") {
                        continue
                    }
                    r[p] = n;
                    if (!n && r.removeAttribute) {
                        r.removeAttribute(p)
                    } else {
                        if (!n && r.removeProperty) {
                            r.removeProperty(p)
                        }
                    }
                }
            };
            return m
        };
        c.fx.collapse = function(k, j) {
            if (! (k = c.dom.g(k))) {
                return null
            }
            var o = k,
            m,
            i,
            n = {
                vertical: {
                    value: "height",
                    offset: "offsetHeight",
                    stylesValue: ["paddingBottom", "paddingTop", "borderTopWidth", "borderBottomWidth"]
                },
                horizontal: {
                    value: "width",
                    offset: "offsetWidth",
                    stylesValue: ["paddingLeft", "paddingRight", "borderLeftWidth", "borderRightWidth"]
                }
            };
            var l = c.fx.create(o, c.object.extend({
                orientation: "vertical",
                initialize: function() {
                    i = n[this.orientation];
                    this.protect(i.value);
                    this.protect("overflow");
                    this.restoreAfterFinish = true;
                    m = o[i.offset];
                    o.style.overflow = "hidden"
                },
                transition: function(p) {
                    return Math.pow(1 - p, 2)
                },
                render: function(p) {
                    o.style[i.value] = Math.floor(p * m) + "px"
                },
                finish: function() {
                    c.dom.hide(o)
                }
            },
            j || {}), "baidu.fx.expand_collapse");
            return l.launch()
        };
        c.lang.instance = function() {
            var i = c.global("_maps_id");
            return function(j) {
                return i[j] || null
            }
        } ();
        c.fx.current = function(l) {
            if (! (l = c.dom.g(l))) {
                return null
            }
            var j,
            n,
            m = /\|([^\|]+)\|/g;
            do {
                if (n = l.getAttribute("baidu_current_effect")) {
                    break
                }
            }
            while ((l = l.parentNode) && l.nodeType == 1);
            if (!n) {
                return null
            }
            if ((j = n.match(m))) {
                m = /\|([^\|]+)\|/;
                for (var k = 0; k < j.length; k++) {
                    m.test(j[k]);
                    j[k] = c.lang.instance(RegExp["\x241"])
                }
            }
            return j
        };
        c.fx.expand = function(k, j) {
            if (! (k = c.dom.g(k))) {
                return null
            }
            var o = k,
            m,
            i,
            n = {
                vertical: {
                    value: "height",
                    offset: "offsetHeight",
                    stylesValue: ["paddingBottom", "paddingTop", "borderTopWidth", "borderBottomWidth"]
                },
                horizontal: {
                    value: "width",
                    offset: "offsetWidth",
                    stylesValue: ["paddingLeft", "paddingRight", "borderLeftWidth", "borderRightWidth"]
                }
            };
            var l = c.fx.create(o, c.object.extend({
                orientation: "vertical",
                initialize: function() {
                    i = n[this.orientation];
                    c.dom.show(o);
                    this.protect(i.value);
                    this.protect("overflow");
                    this.restoreAfterFinish = true;
                    m = o[i.offset];
                    function p(t, s) {
                        var r = parseInt(c.dom.getStyle(t, s));
                        r = isNaN(r) ? 0: r;
                        r = c.lang.isNumber(r) ? r: 0;
                        return r
                    }
                    c.forEach(i.stylesValue, 
                    function(r) {
                        m -= p(o, r)
                    });
                    o.style.overflow = "hidden";
                    o.style[i.value] = "1px"
                },
                transition: function(p) {
                    return Math.sqrt(p)
                },
                render: function(p) {
                    o.style[i.value] = Math.floor(p * m) + "px"
                }
            },
            j || {}), "baidu.fx.expand_collapse");
            return l.launch()
        };
        c.fx.opacity = function(j, i) {
            if (! (j = c.dom.g(j))) {
                return null
            }
            i = c.object.extend({
                from: 0,
                to: 1
            },
            i || {});
            var l = j;
            var k = c.fx.create(l, c.object.extend({
                initialize: function() {
                    c.dom.show(j);
                    if (c.browser.ie) {
                        this.protect("filter")
                    } else {
                        this.protect("opacity");
                        this.protect("KHTMLOpacity")
                    }
                    this.distance = this.to - this.from
                },
                render: function(m) {
                    var o = this.distance * m + this.from;
                    if (!c.browser.ie) {
                        l.style.opacity = o;
                        l.style.KHTMLOpacity = o
                    } else {
                        l.style.filter = "progid:DXImageTransform.Microsoft.Alpha(opacity:" + Math.floor(o * 100) + ")"
                    }
                }
            },
            i), "baidu.fx.opacity");
            return k.launch()
        };
        c.fx.fadeIn = function(j, i) {
            if (! (j = c.dom.g(j))) {
                return null
            }
            var k = c.fx.opacity(j, c.object.extend({
                from: 0,
                to: 1,
                restoreAfterFinish: true
            },
            i || {}));
            k.__type = "baidu.fx.fadeIn";
            return k
        };
        c.fx.fadeOut = function(j, i) {
            if (! (j = c.dom.g(j))) {
                return null
            }
            var k = c.fx.opacity(j, c.object.extend({
                from: 1,
                to: 0,
                restoreAfterFinish: true
            },
            i || {}));
            k.addEventListener("onafterfinish", 
            function() {
                c.dom.hide(this.element)
            });
            k.__type = "baidu.fx.fadeOut";
            return k
        };
        c.fx.getTransition = function(j) {
            var i = c.fx.transitions;
            if (!j || typeof i[j] != "string") {
                j = "linear"
            }
            return new Function("percent", i[j])
        };
        c.fx.transitions = {
            none: "return 0",
            full: "return 1",
            linear: "return percent",
            reverse: "return 1 - percent",
            parabola: "return Math.pow(percent, 2)",
            antiparabola: "return 1 - Math.pow(1 - percent, 2)",
            sinoidal: "return (-Math.cos(percent * Math.PI)/2) + 0.5",
            wobble: "return (-Math.cos(percent * Math.PI * (9 * percent))/2) + 0.5",
            spring: "return 1 - (Math.cos(percent * 4.5 * Math.PI) * Math.exp(-percent * 6))"
        };
        c.string.extend({
            formatColor: function() {
                var k = /^\#[\da-f]{6}$/i,
                j = /^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/i,
                i = {
                    black: "#000000",
                    silver: "#c0c0c0",
                    gray: "#808080",
                    white: "#ffffff",
                    maroon: "#800000",
                    red: "#ff0000",
                    purple: "#800080",
                    fuchsia: "#ff00ff",
                    green: "#008000",
                    lime: "#00ff00",
                    olive: "#808000",
                    yellow: "#ffff0",
                    navy: "#000080",
                    blue: "#0000ff",
                    teal: "#008080",
                    aqua: "#00ffff"
                };
                return function() {
                    var m = this.valueOf();
                    if (k.test(m)) {
                        return m
                    } else {
                        if (j.test(m)) {
                            for (var r, p = 1, m = "#"; p < 4; p++) {
                                r = parseInt(RegExp["\x24" + p]).toString(16);
                                m += ("00" + r).substr(r.length)
                            }
                            return m
                        } else {
                            if (/^\#[\da-f]{3}$/.test(m)) {
                                var o = m.charAt(1),
                                n = m.charAt(2),
                                l = m.charAt(3);
                                return "#" + o + o + n + n + l + l
                            } else {
                                if (i[m]) {
                                    return i[m]
                                }
                            }
                        }
                    }
                    return ""
                }
            } ()
        });
        c.fx.highlight = function(j, i) {
            if (! (j = c.dom.g(j))) {
                return null
            }
            var l = j;
            var k = c.fx.create(l, c.object.extend({
                initialize: function() {
                    var t = this,
                    s = c.dom.getStyle,
                    r = c.string.formatColor,
                    o = r(s(l, "color")) || "#000000",
                    m = r(s(l, "backgroundColor"));
                    t.beginColor = t.beginColor || m || "#FFFF00";
                    t.endColor = t.endColor || m || "#FFFFFF";
                    t.finalColor = t.finalColor || t.endColor || t.element.style.backgroundColor;
                    t.textColor == o && (t.textColor = "");
                    this.protect("color");
                    this.protect("backgroundColor");
                    t.c_b = [];
                    t.c_d = [];
                    t.t_b = [];
                    t.t_d = [];
                    for (var u, p = 0; p < 3; p++) {
                        u = 2 * p + 1;
                        t.c_b[p] = parseInt(t.beginColor.substr(u, 2), 16);
                        t.c_d[p] = parseInt(t.endColor.substr(u, 2), 16) - t.c_b[p];
                        if (t.textColor) {
                            t.t_b[p] = parseInt(o.substr(u, 2), 16);
                            t.t_d[p] = parseInt(t.textColor.substr(u, 2), 16) - t.t_b[p]
                        }
                    }
                },
                render: function(s) {
                    for (var r = this, o = "#", m = "#", t, p = 0; p < 3; p++) {
                        t = Math.round(r.c_b[p] + r.c_d[p] * s).toString(16);
                        o += ("00" + t).substr(t.length);
                        if (r.textColor) {
                            t = Math.round(r.t_b[p] + r.t_d[p] * s).toString(16);
                            m += ("00" + t).substr(t.length)
                        }
                    }
                    l.style.backgroundColor = o;
                    r.textColor && (l.style.color = m)
                },
                finish: function() {
                    this.textColor && (l.style.color = this.textColor);
                    l.style.backgroundColor = this.finalColor
                }
            },
            i || {}), "baidu.fx.highlight");
            return k.launch()
        };
        c.fx.mask = function(k, i) {
            if (! (k = c.dom.g(k)) || c.dom.getStyle(k, "position") != "absolute") {
                return null
            }
            var n = k,
            j = {};
            i = i || {};
            var m = /^(\d+px|\d?\d(\.\d+)?%|100%|left|center|right)(\s+(\d+px|\d?\d(\.\d+)?%|100%|top|center|bottom))?/i; ! m.test(i.startOrigin) && (i.startOrigin = "0px 0px");
            var i = c.object.extend({
                restoreAfterFinish: true,
                from: 0,
                to: 1
            },
            i || {});
            var l = c.fx.create(n, c.object.extend({
                initialize: function() {
                    n.style.display = "";
                    this.protect("clip");
                    j.width = n.offsetWidth;
                    j.height = n.offsetHeight;
                    m.test(this.startOrigin);
                    var t = RegExp["\x241"].toLowerCase(),
                    s = RegExp["\x244"].toLowerCase(),
                    r = this.element.offsetWidth,
                    u = this.element.offsetHeight,
                    p,
                    o;
                    if (/\d+%/.test(t)) {
                        p = parseInt(t, 10) / 100 * r
                    } else {
                        if (/\d+px/.test(t)) {
                            p = parseInt(t)
                        } else {
                            if (t == "left") {
                                p = 0
                            } else {
                                if (t == "center") {
                                    p = r / 2
                                } else {
                                    if (t == "right") {
                                        p = r
                                    }
                                }
                            }
                        }
                    }
                    if (!s) {
                        o = u / 2
                    } else {
                        if (/\d+%/.test(s)) {
                            o = parseInt(s, 10) / 100 * u
                        } else {
                            if (/\d+px/.test(s)) {
                                o = parseInt(s)
                            } else {
                                if (s == "top") {
                                    o = 0
                                } else {
                                    if (s == "center") {
                                        o = u / 2
                                    } else {
                                        if (s == "bottom") {
                                            o = u
                                        }
                                    }
                                }
                            }
                        }
                    }
                    j.x = p;
                    j.y = o
                },
                render: function(t) {
                    var u = this.to * t + this.from * (1 - t),
                    s = j.y * (1 - u) + "px ",
                    r = j.x * (1 - u) + "px ",
                    p = j.x * (1 - u) + j.width * u + "px ",
                    o = j.y * (1 - u) + j.height * u + "px ";
                    n.style.clip = "rect(" + s + p + o + r + ")"
                },
                finish: function() {
                    if (this.to < this.from) {
                        n.style.display = "none"
                    }
                }
            },
            i), "baidu.fx.mask");
            return l.launch()
        };
        c.fx.move = function(j, i) {
            if (! (j = c.dom.g(j)) || c.dom.getStyle(j, "position") == "static") {
                return null
            }
            i = c.object.extend({
                x: 0,
                y: 0
            },
            i || {});
            if (i.x == 0 && i.y == 0) {
                return null
            }
            var k = c.fx.create(j, c.object.extend({
                initialize: function() {
                    this.protect("top");
                    this.protect("left");
                    this.originX = parseInt(c.dom.getStyle(j, "left")) || 0;
                    this.originY = parseInt(c.dom.getStyle(j, "top")) || 0
                },
                transition: function(l) {
                    return 1 - Math.pow(1 - l, 2)
                },
                render: function(l) {
                    j.style.top = (this.y * l + this.originY) + "px";
                    j.style.left = (this.x * l + this.originX) + "px"
                }
            },
            i), "baidu.fx.move");
            return k.launch()
        };
        c.fx.moveBy = function(j, m, i) {
            if (! (j = c.dom.g(j)) || c.dom.getStyle(j, "position") == "static" || typeof m != "object") {
                return null
            }
            var l = {};
            l.x = m[0] || m.x || 0;
            l.y = m[1] || m.y || 0;
            var k = c.fx.move(j, c.object.extend(l, i || {}));
            return k
        };
        c.fx.moveTo = function(l, j, k) {
            if (! (l = c.dom.g(l)) || c.dom.getStyle(l, "position") == "static" || typeof j != "object") {
                return null
            }
            var n = [j[0] || j.x || 0, j[1] || j.y || 0];
            var i = parseInt(c.dom.getStyle(l, "left")) || 0;
            var o = parseInt(c.dom.getStyle(l, "top")) || 0;
            var m = c.fx.move(l, c.object.extend({
                x: n[0] - i,
                y: n[1] - o
            },
            k || {}));
            return m
        };
        c.fx.scale = function(k, i) {
            if (! (k = c.dom.g(k))) {
                return null
            }
            i = c.object.extend({
                from: 0.1,
                to: 1
            },
            i || {});
            var m = /^(-?\d+px|\d?\d(\.\d+)?%|100%|left|center|right)(\s+(-?\d+px|\d?\d(\.\d+)?%|100%|top|center|bottom))?/i; ! m.test(i.transformOrigin) && (i.transformOrigin = "0px 0px");
            var j = {},
            l = c.fx.create(k, c.object.extend({
                fade: true,
                initialize: function() {
                    c.dom.show(k);
                    var w = this,
                    n = j,
                    A = k.style,
                    u = function(o) {
                        w.protect(o)
                    };
                    if (c.browser.ie) {
                        u("top");
                        u("left");
                        u("position");
                        u("zoom");
                        u("filter");
                        this.offsetX = parseInt(c.dom.getStyle(k, "left")) || 0;
                        this.offsetY = parseInt(c.dom.getStyle(k, "top")) || 0;
                        if (c.dom.getStyle(k, "position") == "static") {
                            A.position = "relative"
                        }
                        m.test(this.transformOrigin);
                        var t = RegExp["\x241"].toLowerCase(),
                        r = RegExp["\x244"].toLowerCase(),
                        x = this.element.offsetWidth,
                        p = this.element.offsetHeight,
                        z,
                        y;
                        if (/\d+%/.test(t)) {
                            z = parseInt(t, 10) / 100 * x
                        } else {
                            if (/\d+px/.test(t)) {
                                z = parseInt(t)
                            } else {
                                if (t == "left") {
                                    z = 0
                                } else {
                                    if (t == "center") {
                                        z = x / 2
                                    } else {
                                        if (t == "right") {
                                            z = x
                                        }
                                    }
                                }
                            }
                        }
                        if (!r) {
                            y = p / 2
                        } else {
                            if (/\d+%/.test(r)) {
                                y = parseInt(r, 10) / 100 * p
                            } else {
                                if (/\d+px/.test(r)) {
                                    y = parseInt(r)
                                } else {
                                    if (r == "top") {
                                        y = 0
                                    } else {
                                        if (r == "center") {
                                            y = p / 2
                                        } else {
                                            if (r == "bottom") {
                                                y = p
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        A.zoom = this.from;
                        n.cx = z;
                        n.cy = y
                    } else {
                        u("WebkitTransform");
                        u("WebkitTransformOrigin");
                        u("MozTransform");
                        u("MozTransformOrigin");
                        u("OTransform");
                        u("OTransformOrigin");
                        u("transform");
                        u("transformOrigin");
                        u("opacity");
                        u("KHTMLOpacity");
                        A.WebkitTransform = A.MozTransform = A.OTransform = A.transform = "scale(" + this.from + ")";
                        A.WebkitTransformOrigin = A.MozTransformOrigin = A.OTransformOrigin = A.transformOrigin = this.transformOrigin
                    }
                },
                render: function(u) {
                    var r = k.style,
                    o = this.to == 1,
                    o = typeof this.opacityTrend == "boolean" ? this.opacityTrend: o,
                    t = o ? this.percent: 1 - this.percent,
                    w = this.to * u + this.from * (1 - u);
                    if (c.browser.ie) {
                        r.zoom = w;
                        if (this.fade) {
                            r.filter = "progid:DXImageTransform.Microsoft.Alpha(opacity:" + Math.floor(t * 100) + ")"
                        }
                        r.top = this.offsetY + j.cy * (1 - w);
                        r.left = this.offsetX + j.cx * (1 - w)
                    } else {
                        r.WebkitTransform = r.MozTransform = r.OTransform = r.transform = "scale(" + w + ")";
                        if (this.fade) {
                            r.KHTMLOpacity = r.opacity = t
                        }
                    }
                }
            },
            i), "baidu.fx.scale");
            return l.launch()
        };
        c.fx.zoomOut = function(j, i) {
            if (! (j = c.dom.g(j))) {
                return null
            }
            i = c.object.extend({
                to: 0.1,
                from: 1,
                opacityTrend: false,
                restoreAfterFinish: true,
                transition: function(l) {
                    return 1 - Math.pow(1 - l, 2)
                }
            },
            i || {});
            var k = c.fx.scale(j, i);
            k.addEventListener("onafterfinish", 
            function() {
                c.dom.hide(this.element)
            });
            return k
        };
        c.fx.puff = function(j, i) {
            return c.fx.zoomOut(j, c.object.extend({
                to: 1.8,
                duration: 800,
                transformOrigin: "50% 50%"
            },
            i || {}))
        };
        c.fx.pulsate = function(k, i, j) {
            if (! (k = c.dom.g(k))) {
                return null
            }
            if (isNaN(i) || i == 0) {
                return null
            }
            var m = k;
            var l = c.fx.create(m, c.object.extend({
                initialize: function() {
                    this.protect("visibility")
                },
                transition: function(n) {
                    return Math.cos(2 * Math.PI * n)
                },
                render: function(n) {
                    m.style.visibility = n > 0 ? "visible": "hidden"
                },
                finish: function() {
                    setTimeout(function() {
                        c.fx.pulsate(k, --i, j)
                    },
                    10)
                }
            },
            j), "baidu.fx.pulsate");
            return l.launch()
        };
        c.fx.remove = function(j, i) {
            var k = i.onafterfinish ? i.onafterfinish: new Function();
            return c.fx.fadeOut(j, c.object.extend(i || {},
            {
                onafterfinish: function() {
                    c.dom.remove(this.element);
                    k.call(this)
                }
            }))
        };
        c.fx.scrollBy = function(j, n, i) {
            if (! (j = c.dom.g(j)) || typeof n != "object") {
                return null
            }
            var m = {},
            l = {};
            m.x = n[0] || n.x || 0;
            m.y = n[1] || n.y || 0;
            var k = c.fx.create(j, c.object.extend({
                initialize: function() {
                    var p = l.sTop = j.scrollTop;
                    var o = l.sLeft = j.scrollLeft;
                    l.sx = Math.min(j.scrollWidth - j.clientWidth - o, m.x);
                    l.sy = Math.min(j.scrollHeight - j.clientHeight - p, m.y)
                },
                transition: function(o) {
                    return 1 - Math.pow(1 - o, 2)
                },
                render: function(o) {
                    j.scrollTop = (l.sy * o + l.sTop);
                    j.scrollLeft = (l.sx * o + l.sLeft)
                },
                restore: function() {
                    j.scrollTop = l.sTop;
                    j.scrollLeft = l.sLeft
                }
            },
            i), "baidu.fx.scroll");
            return k.launch()
        };
        c.fx.scrollTo = function(k, i, j) {
            if (! (k = c.dom.g(k)) || typeof i != "object") {
                return null
            }
            var l = {};
            l.x = (i[0] || i.x || 0) - k.scrollLeft;
            l.y = (i[1] || i.y || 0) - k.scrollTop;
            return c.fx.scrollBy(k, l, j)
        };
        c.fx.shake = function(j, n, i) {
            if (! (j = c.dom.g(j))) {
                return null
            }
            var m = j;
            n = n || [];
            function k() {
                for (var o = 0; o < arguments.length; o++) {
                    if (!isNaN(arguments[o])) {
                        return arguments[o]
                    }
                }
            }
            var l = c.fx.create(m, c.object.extend({
                initialize: function() {
                    this.protect("top");
                    this.protect("left");
                    this.protect("position");
                    this.restoreAfterFinish = true;
                    if (c.dom.getStyle(m, "position") == "static") {
                        m.style.position = "relative"
                    }
                    var o = this["\x06original"];
                    this.originX = parseInt(o.left || 0);
                    this.originY = parseInt(o.top || 0);
                    this.offsetX = k(n[0], n.x, 16);
                    this.offsetY = k(n[1], n.y, 5)
                },
                transition: function(p) {
                    var o = 1 - p;
                    return Math.floor(o * 16) % 2 == 1 ? o: p - 1
                },
                render: function(o) {
                    m.style.top = (this.offsetY * o + this.originY) + "px";
                    m.style.left = (this.offsetX * o + this.originX) + "px"
                }
            },
            i || {}), "baidu.fx.shake");
            return l.launch()
        };
        c.fx.zoomIn = function(j, i) {
            if (! (j = c.dom.g(j))) {
                return null
            }
            i = c.object.extend({
                to: 1,
                from: 0.1,
                restoreAfterFinish: true,
                transition: function(k) {
                    return Math.pow(k, 2)
                }
            },
            i || {});
            return c.fx.scale(j, i)
        };
        c._util_.smartAjax = c._util_.smartAjax || 
        function(i) {
            return function(j, l, m, k) {
                if (c.type(l) === "function") {
                    k = k || m;
                    m = l;
                    l = undefined
                }
                c.ajax({
                    type: i,
                    url: j,
                    data: l,
                    success: m,
                    dataType: k
                })
            }
        };
        c.get = c.get || c._util_.smartAjax("get");
        c.global.get = function(i) {
            return c.global(i)
        };
        c.global.set = function(j, k, i) {
            return c.global(j, k, i)
        };
        c.global.getZIndex = function(i, k) {
            var j = c.global.get("zIndex");
            if (i) {
                j[i] = j[i] + (k || 1)
            }
            return j[i]
        };
        c.global.set("zIndex", {
            popup: 50000,
            dialog: 1000
        },
        true);
        c.i18n = c.i18n || {};
        c.i18n.cultures = c.i18n.cultures || {};
        c.i18n.cultures["en-US"] = c.object.extend(c.i18n.cultures["en-US"] || {},
        {
            calendar: {
                dateFormat: "yyyy-MM-dd",
                titleNames: "#{MM}&nbsp;#{yyyy}",
                monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                dayNames: {
                    mon: "Mon",
                    tue: "Tue",
                    wed: "Wed",
                    thu: "Thu",
                    fri: "Fri",
                    sat: "Sat",
                    sun: "Sun"
                }
            },
            timeZone: -5,
            whitespace: new RegExp("(^[\\s\\t\\xa0\\u3000]+)|([\\u3000\\xa0\\s\\t]+\x24)", "g"),
            number: {
                group: ",",
                groupLength: 3,
                decimal: ".",
                positive: "",
                negative: "-",
                _format: function(j, i) {
                    return c.i18n.number._format(j, {
                        group: this.group,
                        groupLength: this.groupLength,
                        decimal: this.decimal,
                        symbol: i ? this.negative: this.positive
                    })
                }
            },
            currency: {
                symbol: "$"
            },
            language: {
                ok: "ok",
                cancel: "cancel",
                signin: "signin",
                signup: "signup"
            }
        });
        c.i18n.currentLocale = "en-US";
        c.i18n.cultures["zh-CN"] = c.object.extend(c.i18n.cultures["zh-CN"] || {},
        {
            calendar: {
                dateFormat: "yyyy-MM-dd",
                titleNames: "#{yyyy}骞�&nbsp;#{MM}鏈�",
                monthNames: ["涓€", "浜�", "涓�", "鍥�", "浜�", "鍏�", "涓�", "鍏�", "涔�", "鍗�", "鍗佷竴", "鍗佷簩"],
                monthNamesShort: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
                dayNames: {
                    mon: "涓€",
                    tue: "浜�",
                    wed: "涓�",
                    thu: "鍥�",
                    fri: "浜�",
                    sat: "鍏�",
                    sun: "鏃�"
                }
            },
            timeZone: 8,
            whitespace: new RegExp("(^[\\s\\t\\xa0\\u3000]+)|([\\u3000\\xa0\\s\\t]+\x24)", "g"),
            number: {
                group: ",",
                groupLength: 3,
                decimal: ".",
                positive: "",
                negative: "-",
                _format: function(j, i) {
                    return c.i18n.number._format(j, {
                        group: this.group,
                        groupLength: this.groupLength,
                        decimal: this.decimal,
                        symbol: i ? this.negative: this.positive
                    })
                }
            },
            currency: {
                symbol: "锟�"
            },
            language: {
                ok: "纭畾",
                cancel: "鍙栨秷",
                signin: "娉ㄥ唽",
                signup: "鐧诲綍"
            }
        });
        c.i18n.currentLocale = "zh-CN";
        c.i18n.number = c.i18n.number || {
            format: function(m, k, o) {
                var l = this,
                n = k && c.i18n.cultures[k].number,
                j = c.i18n.cultures[o || c.i18n.currentLocale].number,
                i = false;
                if (typeof m === "string") {
                    if (m.indexOf(n.negative) > -1) {
                        i = true;
                        m = m.replace(n.negative, "")
                    } else {
                        if (m.indexOf(n.positive) > -1) {
                            m = m.replace(n.positive, "")
                        }
                    }
                    m = m.replace(new RegExp(n.group, "g"), "")
                } else {
                    if (m < 0) {
                        i = true;
                        m *= -1
                    }
                }
                m = parseFloat(m);
                if (isNaN(m)) {
                    return "NAN"
                }
                return j._format ? j._format(m, i) : l._format(m, {
                    group: j.group || ",",
                    decimal: j.decimal || ".",
                    groupLength: j.groupLength,
                    symbol: i ? j.negative: j.positive
                })
            },
            _format: function(l, r) {
                var j = String(l).split(r.decimal),
                o = j[0].split("").reverse(),
                k = j[1] || "",
                n = 0,
                p = 0,
                s = "";
                n = parseInt(o.length / r.groupLength);
                p = o.length % r.groupLength;
                n = p == 0 ? n - 1: n;
                for (var m = 1; m <= n; m++) {
                    o.splice(r.groupLength * m + (m - 1), 0, r.group)
                }
                o = o.reverse();
                s = r.symbol + o.join("") + (k.length > 0 ? r.decimal + k: "");
                return s
            }
        };
        c.i18n.currency = c.i18n.currency || {
            format: function(m, k, o) {
                var l = this,
                n = k && c.i18n.cultures[k].currency,
                j = c.i18n.cultures[o || c.i18n.currentLocale].currency,
                i;
                if (typeof m === "string") {
                    m = m.replace(n.symbol)
                }
                return j.symbol + this._format(m, k, o)
            },
            _format: function(j, i, k) {
                return c.i18n.number.format(j, i, k || c.i18n.currentLocale)
            }
        };
        c.i18n.date = c.i18n.date || {
            getDaysInMonth: function(i, j) {
                var k = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
                if (j == 1 && c.i18n.date.isLeapYear(i)) {
                    return 29
                }
                return k[j]
            },
            isLeapYear: function(i) {
                return ! (i % 400) || (!(i % 4) && !!(i % 100))
            },
            toLocaleDate: function(j, i, k) {
                return this._basicDate(j, i, k || c.i18n.currentLocale)
            },
            _basicDate: function(n, k, p) {
                var i = c.i18n.cultures[p || c.i18n.currentLocale].timeZone,
                o = i * 60,
                j,
                l,
                m = n.getTime();
                if (k) {
                    j = c.i18n.cultures[k].timeZone;
                    l = j * 60
                } else {
                    l = -1 * n.getTimezoneOffset();
                    j = l / 60
                }
                return new Date(j != i ? (m + (o - l) * 60000) : m)
            },
            format: function(i, j) {
                var k = c.i18n.cultrues[j || c.i18n.currentLocale];
                return c.date.format(c.i18n.date.toLocaleDate(i, "", j), k.calendar.dateFormat)
            }
        };
        c.i18n.string = c.i18n.string || {
            trim: function(k, i) {
                var j = c.i18n.cultures[i || c.i18n.currentLocale].whitespace;
                return String(k).replace(j, "")
            },
            translation: function(k, i) {
                var j = c.i18n.cultures[i || c.i18n.currentLocale].language;
                return j[k] || ""
            }
        };
        c.json.decode = c.json.parse;
        c.json.stringify = (function() {
            var j = {
                "\b": "\\b",
                "\t": "\\t",
                "\n": "\\n",
                "\f": "\\f",
                "\r": "\\r",
                '"': '\\"',
                "\\": "\\\\"
            };
            function i(n) {
                if (/["\\\x00-\x1f]/.test(n)) {
                    n = n.replace(/["\\\x00-\x1f]/g, 
                    function(o) {
                        var p = j[o];
                        if (p) {
                            return p
                        }
                        p = o.charCodeAt();
                        return "\\u00" + Math.floor(p / 16).toString(16) + (p % 16).toString(16)
                    })
                }
                return '"' + n + '"'
            }
            function l(t) {
                var o = ["["],
                p = t.length,
                n,
                r,
                s;
                for (r = 0; r < p; r++) {
                    s = t[r];
                    switch (typeof s) {
                    case "undefined":
                    case "function":
                    case "unknown":
                        break;
                    default:
                        if (n) {
                            o.push(",")
                        }
                        o.push(c.json.stringify(s));
                        n = 1
                    }
                }
                o.push("]");
                return o.join("")
            }
            function k(n) {
                return n < 10 ? "0" + n: n
            }
            function m(n) {
                return '"' + n.getFullYear() + "-" + k(n.getMonth() + 1) + "-" + k(n.getDate()) + "T" + k(n.getHours()) + ":" + k(n.getMinutes()) + ":" + k(n.getSeconds()) + '"'
            }
            return function(t) {
                switch (typeof t) {
                case "undefined":
                    return "undefined";
                case "number":
                    return isFinite(t) ? String(t) : "null";
                case "string":
                    return i(t);
                case "boolean":
                    return String(t);
                default:
                    if (t === null) {
                        return "null"
                    } else {
                        if (c.type(t) === "array") {
                            return l(t)
                        } else {
                            if (c.type(t) === "date") {
                                return m(t)
                            } else {
                                var o = ["{"],
                                s = c.json.stringify,
                                n,
                                r;
                                for (var p in t) {
                                    if (Object.prototype.hasOwnProperty.call(t, p)) {
                                        r = t[p];
                                        switch (typeof r) {
                                        case "undefined":
                                        case "unknown":
                                        case "function":
                                            break;
                                        default:
                                            if (n) {
                                                o.push(",")
                                            }
                                            n = 1;
                                            o.push(s(p) + ":" + s(r))
                                        }
                                    }
                                }
                                o.push("}");
                                return o.join("")
                            }
                        }
                    }
                }
            }
        })();
        c.json.encode = c.json.stringify;
        c.lang.Class.prototype.addEventListeners = function(l, m) {
            if (typeof m == "undefined") {
                for (var k in l) {
                    this.addEventListener(k, l[k])
                }
            } else {
                l = l.split(",");
                var k = 0,
                j = l.length,
                n;
                for (; k < j; k++) {
                    this.addEventListener(c.trim(l[k]), m)
                }
            }
        };
        c.lang.createClass = function(k, s) {
            s = s || {};
            var p = s.superClass || c.lang.Class;
            var r = function() {
                var w = this;
                s.decontrolled && (w.__decontrolled = true);
                p.apply(w, arguments);
                for (t in r.options) {
                    w[t] = r.options[t]
                }
                k.apply(w, arguments);
                for (var t = 0, u = r["\x06r"]; u && t < u.length; t++) {
                    u[t].apply(w, arguments)
                }
            };
            r.options = s.options || {};
            var j = function() {},
            o = k.prototype;
            j.prototype = p.prototype;
            var m = r.prototype = new j();
            for (var l in o) {
                m[l] = o[l]
            }
            var n = s.className || s.type;
            typeof n == "string" && (m.__type = n);
            m.constructor = o.constructor;
            r.extend = function(u) {
                for (var t in u) {
                    r.prototype[t] = u[t]
                }
                return r
            };
            return r
        };
        c.lang.decontrol = function() {
            var i = c.global("_maps_id");
            return function(j) {
                delete i[j]
            }
        } ();
        c.lang.eventCenter = c.lang.eventCenter || c.lang.createSingle();
        c.lang.getModule = function(j, k) {
            var l = j.split("."),
            m = k || window,
            i;
            for (; i = l.shift();) {
                if (m[i] != null) {
                    m = m[i]
                } else {
                    return null
                }
            }
            return m
        };
        c.lang.isBoolean = c.isBoolean;
        c.lang.isDate = c.isDate;
        c.lang.isElement = c.isElement;
        c.lang.isObject = c.isObject;
        c.lang.isWindow = function(i) {
            return c.type(i) == "Window"
        };
        c.lang.module = function(m, o, k) {
            var p = m.split("."),
            j = p.length - 1,
            l,
            n = 0;
            if (!k) {
                try {
                    if (! (new RegExp("^[a-zA-Z_\x24][a-zA-Z0-9_\x24]*\x24")).test(p[0])) {
                        throw ""
                    }
                    k = window["eval"](p[0]);
                    n = 1
                } catch(r) {
                    k = window
                }
            }
            for (; n < j; n++) {
                l = p[n];
                if (!k[l]) {
                    k[l] = {}
                }
                k = k[l]
            }
            if (!k[p[j]]) {
                k[p[j]] = o
            }
        };
        c.lang.register = function(j, l, i) {
            var k = j["\x06r"] || (j["\x06r"] = []);
            k[k.length] = l;
            for (var m in i) {
                j.prototype[m] = i[m]
            }
        };
        c.global("_maps_id");
        c.number.extend({
            comma: function(i) {
                var j = this;
                if (!i || i < 1) {
                    i = 3
                }
                j = String(j).split(".");
                j[0] = j[0].replace(new RegExp("(\\d)(?=(\\d{" + i + "})+$)", "ig"), "$1,");
                return j.join(".")
            }
        });
        c.number.randomInt = function(j, i) {
            return Math.floor(Math.random() * (i - j + 1) + j)
        };
        c.object.clone = function(n) {
            var k = n,
            l,
            j;
            if (!n || n instanceof Number || n instanceof String || n instanceof Boolean) {
                return k
            } else {
                if (c.lang.isArray(n)) {
                    k = [];
                    var m = 0;
                    for (l = 0, j = n.length; l < j; l++) {
                        k[m++] = c.object.clone(n[l])
                    }
                } else {
                    if (c.object.isPlain(n)) {
                        k = {};
                        for (l in n) {
                            if (n.hasOwnProperty(l)) {
                                k[l] = c.object.clone(n[l])
                            }
                        }
                    }
                }
            }
            return k
        };
        c.object.keys = function(m) {
            var i = [],
            l = 0,
            j;
            for (j in m) {
                if (m.hasOwnProperty(j)) {
                    i[l++] = j
                }
            }
            return i
        };
        c.object.map = function(l, k) {
            var j = {};
            for (var i in l) {
                if (l.hasOwnProperty(i)) {
                    j[i] = k(l[i], i)
                }
            }
            return j
        }; (function() {
            var j = function(k) {
                return c.lang.isObject(k) && !c.lang.isFunction(k)
            };
            function i(o, n, m, l, k) {
                if (n.hasOwnProperty(m)) {
                    if (k && j(o[m])) {
                        c.object.merge(o[m], n[m], {
                            overwrite: l,
                            recursive: k
                        })
                    } else {
                        if (l || !(m in o)) {
                            o[m] = n[m]
                        }
                    }
                }
            }
            c.object.merge = function(p, k, s) {
                var m = 0,
                t = s || {},
                o = t.overwrite,
                r = t.whiteList,
                l = t.recursive,
                n;
                if (r && r.length) {
                    n = r.length;
                    for (; m < n; ++m) {
                        i(p, k, r[m], o, l)
                    }
                } else {
                    for (m in k) {
                        i(p, k, m, o, l)
                    }
                }
                return p
            }
        })();
        c.page.createStyleSheet = function(i) {
            var n = i || {},
            l = n.document || document,
            k;
            if (c.browser.ie) {
                if (!n.url) {
                    n.url = ""
                }
                return l.createStyleSheet(n.url, n.index)
            } else {
                k = "<style type='text/css'></style>";
                n.url && (k = "<link type='text/css' rel='stylesheet' href='" + n.url + "'/>");
                c.dom.insertHTML(l.getElementsByTagName("HEAD")[0], "beforeEnd", k);
                if (n.url) {
                    return null
                }
                var j = l.styleSheets[l.styleSheets.length - 1],
                m = j.rules || j.cssRules;
                return {
                    self: j,
                    rules: j.rules || j.cssRules,
                    addRule: function(o, r, p) {
                        if (j.addRule) {
                            return j.addRule(o, r, p)
                        } else {
                            if (j.insertRule) {
                                isNaN(p) && (p = m.length);
                                return j.insertRule(o + "{" + r + "}", p)
                            }
                        }
                    },
                    removeRule: function(o) {
                        if (j.removeRule) {
                            j.removeRule(o)
                        } else {
                            if (j.deleteRule) {
                                isNaN(o) && (o = 0);
                                j.deleteRule(o)
                            }
                        }
                    }
                }
            }
        };
        c.page.getHeight = function() {
            var l = document,
            i = l.body,
            k = l.documentElement,
            j = l.compatMode == "BackCompat" ? i: l.documentElement;
            return Math.max(k.scrollHeight, i.scrollHeight, j.clientHeight)
        };
        c.page.getViewHeight = function() {
            var j = document,
            i = j.compatMode == "BackCompat" ? j.body: j.documentElement;
            return i.clientHeight
        };
        c.page.getViewWidth = function() {
            var j = document,
            i = j.compatMode == "BackCompat" ? j.body: j.documentElement;
            return i.clientWidth
        };
        c.page.getWidth = function() {
            var l = document,
            i = l.body,
            k = l.documentElement,
            j = l.compatMode == "BackCompat" ? i: l.documentElement;
            return Math.max(k.scrollWidth, i.scrollWidth, j.clientWidth)
        };
        c.page.lazyLoadImage = function(i) {
            i = i || {};
            i.preloadHeight = i.preloadHeight || 0;
            c.dom.ready(function() {
                var m = document.getElementsByTagName("IMG"),
                n = m,
                o = m.length,
                l = 0,
                s = k(),
                r = "data-tangram-ori-src",
                p;
                if (i.className) {
                    n = [];
                    for (; l < o; ++l) {
                        if (c.dom.hasClass(m[l], i.className)) {
                            n.push(m[l])
                        }
                    }
                }
                function k() {
                    return c.page.getScrollTop() + c.page.getViewHeight() + i.preloadHeight
                }
                for (l = 0, o = n.length; l < o; ++l) {
                    p = n[l];
                    if (c.dom.getPosition(p).top > s) {
                        p.setAttribute(r, p.src);
                        i.placeHolder ? p.src = i.placeHolder: p.removeAttribute("src")
                    }
                }
                var j = function() {
                    var u = k(),
                    x,
                    y = true,
                    w = 0,
                    t = n.length;
                    for (; w < t; ++w) {
                        p = n[w];
                        x = p.getAttribute(r);
                        x && (y = false);
                        if (c.dom.getPosition(p).top < u && x) {
                            p.src = x;
                            p.removeAttribute(r);
                            c.lang.isFunction(i.onlazyload) && i.onlazyload(p)
                        }
                    }
                    y && c.dom(window).off("scroll", j)
                };
                c.dom(window).on("scroll", j)
            })
        };
        c.page.load = function(k, s, m) {
            s = s || {};
            var p = c.page.load,
            i = p._cache = p._cache || {},
            o = p._loadingCache = p._loadingCache || {},
            n = s.parallel;
            function l() {
                for (var u = 0, t = k.length; u < t; ++u) {
                    if (!i[k[u].url]) {
                        setTimeout(arguments.callee, 10);
                        return
                    }
                }
                s.onload()
            }
            function j(w, z) {
                var y,
                u,
                t;
                switch (w.type.toLowerCase()) {
                case "css":
                    y = document.createElement("link");
                    y.setAttribute("rel", "stylesheet");
                    y.setAttribute("type", "text/css");
                    break;
                case "js":
                    y = document.createElement("script");
                    y.setAttribute("type", "text/javascript");
                    y.setAttribute("charset", w.charset || p.charset);
                    break;
                case "html":
                    y = document.createElement("iframe");
                    y.frameBorder = "none";
                    break;
                default:
                    return
                }
                var x = c.dom(y);
                t = function() {
                    if (!u && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
                        u = true;
                        x.off("load", t);
                        x.off("readystatechange", t);
                        z.call(window, y)
                    }
                };
                x.on("load", t);
                x.on("readystatechange", t);
                if (w.type == "css") { (function() {
                        if (u) {
                            return
                        }
                        try {
                            y.sheet.cssRule
                        } catch(A) {
                            setTimeout(arguments.callee, 20);
                            return
                        }
                        u = true;
                        z.call(window, y)
                    })()
                }
                y.href = y.src = w.url;
                document.getElementsByTagName("head")[0].appendChild(y)
            }
            c.lang.isString(k) && (k = [{
                url: k
            }]);
            if (! (k && k.length)) {
                return
            }
            function r(w) {
                var u = w.url,
                x = !!n,
                t,
                y = function(z) {
                    i[w.url] = z;
                    delete o[w.url];
                    if (c.lang.isFunction(w.onload)) {
                        if (false === w.onload.call(window, z)) {
                            return
                        }
                    } ! n && p(k.slice(1), s, true);
                    if ((!m) && c.lang.isFunction(s.onload)) {
                        l()
                    }
                };
                w.type = w.type || u.replace(/^[^\?#]+\.(css|js|html)(\?|#| |$)[^\?#]*/i, "$1");
                w.requestType = w.requestType || (w.type == "html" ? "ajax": "dom");
                if (t = i[w.url]) {
                    y(t);
                    return x
                }
                if (!s.refresh && o[w.url]) {
                    setTimeout(function() {
                        r(w)
                    },
                    10);
                    return x
                }
                o[w.url] = true;
                if (w.requestType.toLowerCase() == "dom") {
                    j(w, y)
                } else {
                    c.ajax.get(w.url, 
                    function(A, z) {
                        y(z)
                    })
                }
                return x
            }
            c.forEach(k, r)
        };
        c.page.load.charset = "UTF8";
        c.page.loadCssFile = function(j) {
            var i = document.createElement("link");
            i.setAttribute("rel", "stylesheet");
            i.setAttribute("type", "text/css");
            i.setAttribute("href", j);
            document.getElementsByTagName("head")[0].appendChild(i)
        };
        c.page.loadJsFile = function(j) {
            var i = document.createElement("script");
            i.setAttribute("type", "text/javascript");
            i.setAttribute("src", j);
            i.setAttribute("defer", "defer");
            document.getElementsByTagName("head")[0].appendChild(i)
        };
        c.param = function(j) {
            j = j || arguments.callee.caller.arguments;
            var l = "",
            m = j.length;
            for (var k = 0; k < m; k++) {
                l += "," + c.type(j[k])
            }
            return l ? l.substr(1) : ""
        };
        c.platform = c.platform || 
        function() {
            var j = navigator.userAgent,
            i = function() {};
            c.forEach("Android iPad iPhone Linux Macintosh Windows X11".split(" "), 
            function(l) {
                var k = l.charAt(0).toUpperCase() + l.toLowerCase().substr(1);
                c["is" + k] = i["is" + k] = j.indexOf(l) > -1
            });
            return i
        } ();
        c.post = c.post || c._util_.smartAjax("post");
        c.regexp = c.regexp || {};
        c.regexp.compile = function(i) {
            return function(j, m) {
                var l,
                k = j + "$$" + m; ! (l = i[k]) && (l = i[k] = new RegExp(j, m));
                l.lastIndex > 0 && (l.lastIndex = 0);
                return l
            }
        } (c.global("_maps_RegExp"));
        c.createChain("sio", 
        function(i) {
            switch (typeof i) {
            case "string":
                return new c.sio.$Sio(i);
                break
            }
        },
        function(i) {
            this.url = i
        });
        c.sio._createScriptTag = function(j, i, k) {
            j.setAttribute("type", "text/javascript");
            k && j.setAttribute("charset", k);
            j.setAttribute("src", i);
            document.getElementsByTagName("head")[0].appendChild(j)
        };
        c.sio._removeScriptTag = function(j) {
            if (j.clearAttributes) {
                j.clearAttributes()
            } else {
                for (var i in j) {
                    if (j.hasOwnProperty(i)) {
                        delete j[i]
                    }
                }
            }
            if (j && j.parentNode) {
                j.parentNode.removeChild(j)
            }
            j = null
        };
        c.sio.extend({
            callByBrowser: function(o, r) {
                var i = this.url;
                var l = document.createElement("SCRIPT"),
                m = 0,
                s = r || {},
                k = s.charset,
                p = o || 
                function() {},
                n = s.timeOut || 0,
                j;
                l.onload = l.onreadystatechange = function() {
                    if (m) {
                        return
                    }
                    var t = l.readyState;
                    if ("undefined" == typeof t || t == "loaded" || t == "complete") {
                        m = 1;
                        try {
                            p();
                            clearTimeout(j)
                        } finally {
                            l.onload = l.onreadystatechange = null;
                            c.sio._removeScriptTag(l)
                        }
                    }
                };
                if (n) {
                    j = setTimeout(function() {
                        l.onload = l.onreadystatechange = null;
                        c.sio._removeScriptTag(l);
                        s.onfailure && s.onfailure()
                    },
                    n)
                }
                c.sio._createScriptTag(l, i, k)
            }
        });
        c.sio.extend({
            callByServer: function(w, x) {
                var i = this.url;
                var r = document.createElement("SCRIPT"),
                p = "bd__cbs__",
                t,
                m,
                y = x || {},
                l = y.charset,
                n = y.queryField || "callback",
                u = y.timeOut || 0,
                j,
                k = new RegExp("(\\?|&)" + n + "=([^&]*)"),
                o;
                if (c.lang.isFunction(w)) {
                    t = p + Math.floor(Math.random() * 2147483648).toString(36);
                    window[t] = s(0)
                } else {
                    if (c.lang.isString(w)) {
                        t = w
                    } else {
                        if (o = k.exec(i)) {
                            t = o[2]
                        }
                    }
                }
                if (u) {
                    j = setTimeout(s(1), u)
                }
                i = i.replace(k, "\x241" + n + "=" + t);
                if (i.search(k) < 0) {
                    i += (i.indexOf("?") < 0 ? "?": "&") + n + "=" + t
                }
                c.sio._createScriptTag(r, i, l);
                function s(z) {
                    return function() {
                        try {
                            if (z) {
                                y.onfailure && y.onfailure()
                            } else {
                                w.apply(window, arguments);
                                clearTimeout(j)
                            }
                            window[t] = null;
                            delete window[t]
                        } catch(A) {} finally {
                            c.sio._removeScriptTag(r)
                        }
                    }
                }
            }
        });
        c.sio.extend({
            log: function() {
                url = this.url;
                var i = new Image(),
                j = "tangram_sio_log_" + Math.floor(Math.random() * 2147483648).toString(36);
                window[j] = i;
                i.onload = i.onerror = i.onabort = function() {
                    i.onload = i.onerror = i.onabort = null;
                    window[j] = null;
                    i = null
                };
                i.src = url
            }
        });
        /*!
 * Sizzle CSS Selector Engine
 *  Copyright 2011, The Dojo Foundation
 *  Released under the MIT, BSD, and GPL Licenses.
 *  More information: http://sizzlejs.com/
 */
        (function(W, A) {
            c.query = function(aa, ac, ab) {
                return c.merge(ab || [], c.sizzle(aa, ac))
            };
            var m = W.document,
            s = m.documentElement,
            Y = "sizcache" + (Math.random() + "").replace(".", ""),
            y = 0,
            l = Object.prototype.toString,
            K = "undefined",
            u = false,
            p = true,
            R = /^#([\w\-]+$)|^(\w+$)|^\.([\w\-]+$)/,
            J = /((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^\[\]]*\]|['"][^'"]*['"]|[^\[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?((?:.|\r|\n)*)/g,
            I = /\\/g,
            Z = /\W/,
            o = /^\w/,
            C = /\D/,
            n = /(-?)(\d*)(?:n([+\-]?\d*))?/,
            G = /^\+|\s*/g,
            F = /h\d/i,
            S = /input|select|textarea|button/i,
            w = /[\t\n\f\r]/g,
            B = "(?:[-\\w]|[^\\x00-\\xa0]|\\\\.)",
            O = {
                ID: new RegExp("#(" + B + "+)"),
                CLASS: new RegExp("\\.(" + B + "+)"),
                NAME: new RegExp("\\[name=['\"]*(" + B + "+)['\"]*\\]"),
                TAG: new RegExp("^(" + B.replace("[-", "[-\\*") + "+)"),
                ATTR: new RegExp("\\[\\s*(" + B + "+)\\s*(?:(\\S?=)\\s*(?:(['\"])(.*?)\\3|(#?" + B + "*)|)|)\\s*\\]"),
                PSEUDO: new RegExp(":(" + B + "+)(?:\\((['\"]?)((?:\\([^\\)]+\\)|[^\\(\\)]*)+)\\2\\))?"),
                CHILD: /:(only|nth|last|first)-child(?:\(\s*(even|odd|(?:[+\-]?\d+|(?:[+\-]?\d*)?n\s*(?:[+\-]\s*\d+)?))\s*\))?/,
                POS: /:(nth|eq|gt|lt|first|last|even|odd)(?:\((\d*)\))?(?=[^\-]|$)/
            },
            T = O.POS,
            U = (function() {
                var ab,
                ac = function(ae, ad) {
                    return "\\" + (ad - 0 + 1)
                },
                aa = {};
                for (ab in O) {
                    O[ab] = new RegExp(O[ab].source + (/(?![^\[]*\])(?![^\(]*\))/.source));
                    aa[ab] = new RegExp(/(^(?:.|\r|\n)*?)/.source + O[ab].source.replace(/\\(\d+)/g, ac))
                }
                O.globalPOS = T;
                return aa
            })(),
            Q = function(aa) {
                var ab = false,
                ad = m.createElement("div");
                try {
                    ab = aa(ad)
                } catch(ac) {}
                ad = null;
                return ab
            },
            r = Q(function(ac) {
                var aa = true,
                ab = "script" + (new Date()).getTime();
                ac.innerHTML = "<a name ='" + ab + "'/>";
                s.insertBefore(ac, s.firstChild);
                if (m.getElementById(ab)) {
                    aa = false
                }
                s.removeChild(ac);
                return aa
            }),
            k = Q(function(aa) {
                aa.appendChild(m.createComment(""));
                return aa.getElementsByTagName("*").length === 0
            }),
            N = Q(function(aa) {
                aa.innerHTML = "<a href='#'></a>";
                return aa.firstChild && typeof aa.firstChild.getAttribute !== K && aa.firstChild.getAttribute("href") === "#"
            }),
            M = Q(function(aa) {
                aa.innerHTML = "<div class='test e'></div><div class='test'></div>";
                if (!aa.getElementsByClassName || aa.getElementsByClassName("e").length === 0) {
                    return false
                }
                aa.lastChild.className = "e";
                return aa.getElementsByClassName("e").length !== 1
            }); [0, 0].sort(function() {
                p = false;
                return 0
            });
            var V = function(aa, af, ae) {
                ae = ae || [];
                af = af || m;
                var ac,
                ag,
                ad,
                ab = af.nodeType;
                if (ab !== 1 && ab !== 9) {
                    return []
                }
                if (!aa || typeof aa !== "string") {
                    return ae
                }
                ad = E(af);
                if (!ad) {
                    if ((ac = R.exec(aa))) {
                        if (ac[1]) {
                            if (ab === 9) {
                                ag = af.getElementById(ac[1]);
                                if (ag && ag.parentNode) {
                                    if (ag.id === ac[1]) {
                                        return D([ag], ae)
                                    }
                                } else {
                                    return D([], ae)
                                }
                            } else {
                                if (af.ownerDocument && (ag = af.ownerDocument.getElementById(ac[1])) && L(af, ag) && ag.id === ac[1]) {
                                    return D([ag], ae)
                                }
                            }
                        } else {
                            if (ac[2]) {
                                if (aa === "body" && af.body) {
                                    return D([af.body], ae)
                                }
                                return D(af.getElementsByTagName(aa), ae)
                            } else {
                                if (M && ac[3] && af.getElementsByClassName) {
                                    return D(af.getElementsByClassName(ac[3]), ae)
                                }
                            }
                        }
                    }
                }
                return X(aa, af, ae, A, ad)
            };
            var X = function(af, aa, ai, aj, ae) {
                var ac,
                an,
                aq,
                ab,
                am,
                ap,
                ao,
                ah,
                al = aa,
                ad = true,
                ag = [],
                ak = af;
                do {
                    J.exec("");
                    ac = J.exec(ak);
                    if (ac) {
                        ak = ac[3];
                        ag.push(ac[1]);
                        if (ac[2]) {
                            ab = ac[3];
                            break
                        }
                    }
                }
                while (ac);
                if (ag.length > 1 && T.exec(af)) {
                    if (ag.length === 2 && P.relative[ag[0]]) {
                        an = H(ag[0] + ag[1], aa, aj, ae)
                    } else {
                        an = P.relative[ag[0]] ? [aa] : V(ag.shift(), aa);
                        while (ag.length) {
                            af = ag.shift();
                            if (P.relative[af]) {
                                af += ag.shift()
                            }
                            an = H(af, an, aj, ae)
                        }
                    }
                } else {
                    if (!aj && ag.length > 1 && aa.nodeType === 9 && !ae && O.ID.test(ag[0]) && !O.ID.test(ag[ag.length - 1])) {
                        am = V.find(ag.shift(), aa, ae);
                        aa = am.expr ? V.filter(am.expr, am.set)[0] : am.set[0]
                    }
                    if (aa) {
                        am = aj ? {
                            expr: ag.pop(),
                            set: D(aj)
                        }: V.find(ag.pop(), (ag.length >= 1 && (ag[0] === "~" || ag[0] === "+") && aa.parentNode) || aa, ae);
                        an = am.expr ? V.filter(am.expr, am.set) : am.set;
                        if (ag.length > 0) {
                            aq = D(an)
                        } else {
                            ad = false
                        }
                        while (ag.length) {
                            ap = ag.pop();
                            ao = ap;
                            if (!P.relative[ap]) {
                                ap = ""
                            } else {
                                ao = ag.pop()
                            }
                            if (ao == null) {
                                ao = aa
                            }
                            P.relative[ap](aq, ao, ae)
                        }
                    } else {
                        aq = ag = []
                    }
                }
                if (!aq) {
                    aq = an
                }
                if (!aq) {
                    V.error(ap || af)
                }
                if (l.call(aq) === "[object Array]") {
                    if (!ad) {
                        ai.push.apply(ai, aq)
                    } else {
                        if (aa && aa.nodeType === 1) {
                            for (ah = 0; aq[ah] != null; ah++) {
                                if (aq[ah] && (aq[ah] === true || aq[ah].nodeType === 1 && L(aa, aq[ah]))) {
                                    ai.push(an[ah])
                                }
                            }
                        } else {
                            for (ah = 0; aq[ah] != null; ah++) {
                                if (aq[ah] && aq[ah].nodeType === 1) {
                                    ai.push(an[ah])
                                }
                            }
                        }
                    }
                } else {
                    D(aq, ai)
                }
                if (ab) {
                    X(ab, al, ai, aj, ae);
                    x(ai)
                }
                return ai
            };
            var E = V.isXML = function(aa) {
                var ab = (aa ? aa.ownerDocument || aa: 0).documentElement;
                return ab ? ab.nodeName !== "HTML": false
            };
            var D = function(ad, ac) {
                ac = ac || [];
                var ab = 0,
                aa = ad.length;
                if (typeof aa === "number") {
                    for (; ab < aa; ab++) {
                        ac.push(ad[ab])
                    }
                } else {
                    for (; ad[ab]; ab++) {
                        ac.push(ad[ab])
                    }
                }
                return ac
            };
            var x = V.uniqueSort = function(ab) {
                if (z) {
                    u = p;
                    ab.sort(z);
                    if (u) {
                        for (var aa = 1; aa < ab.length; aa++) {
                            if (ab[aa] === ab[aa - 1]) {
                                ab.splice(aa--, 1)
                            }
                        }
                    }
                }
                return ab
            };
            var L = V.contains = s.compareDocumentPosition ? 
            function(ab, aa) {
                return !! (ab.compareDocumentPosition(aa) & 16)
            }: s.contains ? 
            function(ab, aa) {
                return ab !== aa && (ab.contains ? ab.contains(aa) : false)
            }: function(ab, aa) {
                while ((aa = aa.parentNode)) {
                    if (aa === ab) {
                        return true
                    }
                }
                return false
            };
            V.matches = function(aa, ab) {
                return X(aa, m, [], ab, E(m))
            };
            V.matchesSelector = function(aa, ab) {
                return X(ab, m, [], [aa], E(m)).length > 0
            };
            V.find = function(ai, aa, ac) {
                var ah,
                ad,
                af,
                ae,
                ag,
                ab;
                if (!ai) {
                    return []
                }
                for (ad = 0, af = P.order.length; ad < af; ad++) {
                    ag = P.order[ad];
                    if ((ae = U[ag].exec(ai))) {
                        ab = ae[1];
                        ae.splice(1, 1);
                        if (ab.substr(ab.length - 1) !== "\\") {
                            ae[1] = (ae[1] || "").replace(I, "");
                            ah = P.find[ag](ae, aa, ac);
                            if (ah != null) {
                                ai = ai.replace(O[ag], "");
                                break
                            }
                        }
                    }
                }
                if (!ah) {
                    ah = typeof aa.getElementsByTagName !== K ? aa.getElementsByTagName("*") : []
                }
                return {
                    set: ah,
                    expr: ai
                }
            };
            V.filter = function(al, ak, ao, ae) {
                var ag,
                aa,
                aj,
                aq,
                an,
                ab,
                ad,
                af,
                am,
                ac = al,
                ap = [],
                ai = ak,
                ah = ak && ak[0] && E(ak[0]);
                while (al && ak.length) {
                    for (aj in P.filter) {
                        if ((ag = U[aj].exec(al)) != null && ag[2]) {
                            ab = P.filter[aj];
                            ad = ag[1];
                            aa = false;
                            ag.splice(1, 1);
                            if (ad.substr(ad.length - 1) === "\\") {
                                continue
                            }
                            if (ai === ap) {
                                ap = []
                            }
                            if (P.preFilter[aj]) {
                                ag = P.preFilter[aj](ag, ai, ao, ap, ae, ah);
                                if (!ag) {
                                    aa = aq = true
                                } else {
                                    if (ag === true) {
                                        continue
                                    }
                                }
                            }
                            if (ag) {
                                for (af = 0; (an = ai[af]) != null; af++) {
                                    if (an) {
                                        aq = ab(an, ag, af, ai);
                                        am = ae ^ aq;
                                        if (ao && aq != null) {
                                            if (am) {
                                                aa = true
                                            } else {
                                                ai[af] = false
                                            }
                                        } else {
                                            if (am) {
                                                ap.push(an);
                                                aa = true
                                            }
                                        }
                                    }
                                }
                            }
                            if (aq !== A) {
                                if (!ao) {
                                    ai = ap
                                }
                                al = al.replace(O[aj], "");
                                if (!aa) {
                                    return []
                                }
                                break
                            }
                        }
                    }
                    if (al === ac) {
                        if (aa == null) {
                            V.error(al)
                        } else {
                            break
                        }
                    }
                    ac = al
                }
                return ai
            };
            V.error = function(aa) {
                throw new Error("Syntax error, unrecognized expression: " + aa)
            };
            var i = V.getText = function(ae) {
                var ac,
                ad,
                aa = ae.nodeType,
                ab = "";
                if (aa) {
                    if (aa === 1 || aa === 9 || aa === 11) {
                        if (typeof ae.textContent === "string") {
                            return ae.textContent
                        } else {
                            for (ae = ae.firstChild; ae; ae = ae.nextSibling) {
                                ab += i(ae)
                            }
                        }
                    } else {
                        if (aa === 3 || aa === 4) {
                            return ae.nodeValue
                        }
                    }
                } else {
                    for (ac = 0; (ad = ae[ac]); ac++) {
                        if (ad.nodeType !== 8) {
                            ab += i(ad)
                        }
                    }
                }
                return ab
            };
            var P = V.selectors = {
                match: O,
                leftMatch: U,
                order: ["ID", "NAME", "TAG"],
                attrMap: {
                    "class": "className",
                    "for": "htmlFor"
                },
                attrHandle: {
                    href: N ? 
                    function(aa) {
                        return aa.getAttribute("href")
                    }: function(aa) {
                        return aa.getAttribute("href", 2)
                    },
                    type: function(aa) {
                        return aa.getAttribute("type")
                    }
                },
                relative: {
                    "+": function(ag, ab) {
                        var ad = typeof ab === "string",
                        af = ad && !Z.test(ab),
                        ah = ad && !af;
                        if (af) {
                            ab = ab.toLowerCase()
                        }
                        for (var ac = 0, aa = ag.length, ae; ac < aa; ac++) {
                            if ((ae = ag[ac])) {
                                while ((ae = ae.previousSibling) && ae.nodeType !== 1) {}
                                ag[ac] = ah || ae && ae.nodeName.toLowerCase() === ab ? ae || false: ae === ab
                            }
                        }
                        if (ah) {
                            V.filter(ab, ag, true)
                        }
                    },
                    ">": function(ag, ab) {
                        var af,
                        ae = typeof ab === "string",
                        ac = 0,
                        aa = ag.length;
                        if (ae && !Z.test(ab)) {
                            ab = ab.toLowerCase();
                            for (; ac < aa; ac++) {
                                af = ag[ac];
                                if (af) {
                                    var ad = af.parentNode;
                                    ag[ac] = ad.nodeName.toLowerCase() === ab ? ad: false
                                }
                            }
                        } else {
                            for (; ac < aa; ac++) {
                                af = ag[ac];
                                if (af) {
                                    ag[ac] = ae ? af.parentNode: af.parentNode === ab
                                }
                            }
                            if (ae) {
                                V.filter(ab, ag, true)
                            }
                        }
                    },
                    "": function(ac, ab, aa) {
                        t("parentNode", ac, ab, aa)
                    },
                    "~": function(ac, ab, aa) {
                        t("previousSibling", ac, ab, aa)
                    }
                },
                find: {
                    ID: r ? 
                    function(ac, ad, ab) {
                        if (typeof ad.getElementById !== K && !ab) {
                            var aa = ad.getElementById(ac[1]);
                            return aa && aa.parentNode ? [aa] : []
                        }
                    }: function(ac, ad, ab) {
                        if (typeof ad.getElementById !== K && !ab) {
                            var aa = ad.getElementById(ac[1]);
                            return aa ? aa.id === ac[1] || typeof aa.getAttributeNode !== K && aa.getAttributeNode("id").nodeValue === ac[1] ? [aa] : A: []
                        }
                    },
                    NAME: function(ac, af) {
                        if (typeof af.getElementsByName !== K) {
                            var ab = [],
                            ae = af.getElementsByName(ac[1]),
                            ad = 0,
                            aa = ae.length;
                            for (; ad < aa; ad++) {
                                if (ae[ad].getAttribute("name") === ac[1]) {
                                    ab.push(ae[ad])
                                }
                            }
                            return ab.length === 0 ? null: ab
                        }
                    },
                    TAG: k ? 
                    function(aa, ab) {
                        if (typeof ab.getElementsByTagName !== K) {
                            return ab.getElementsByTagName(aa[1])
                        }
                    }: function(aa, ae) {
                        var ad = ae.getElementsByTagName(aa[1]);
                        if (aa[1] === "*") {
                            var ac = [],
                            ab = 0;
                            for (; ad[ab]; ab++) {
                                if (ad[ab].nodeType === 1) {
                                    ac.push(ad[ab])
                                }
                            }
                            ad = ac
                        }
                        return ad
                    }
                },
                preFilter: {
                    CLASS: function(ae, ab, ac, aa, ah, ad) {
                        ae = " " + ae[1].replace(I, "") + " ";
                        if (ad) {
                            return ae
                        }
                        for (var af = 0, ag; (ag = ab[af]) != null; af++) {
                            if (ag) {
                                if (ah ^ (ag.className && (" " + ag.className + " ").replace(w, " ").indexOf(ae) >= 0)) {
                                    if (!ac) {
                                        aa.push(ag)
                                    }
                                } else {
                                    if (ac) {
                                        ab[af] = false
                                    }
                                }
                            }
                        }
                        return false
                    },
                    ID: function(aa) {
                        return aa[1].replace(I, "")
                    },
                    TAG: function(ab, aa) {
                        return ab[1].replace(I, "").toLowerCase()
                    },
                    CHILD: function(aa) {
                        if (aa[1] === "nth") {
                            if (!aa[2]) {
                                V.error(aa[0])
                            }
                            aa[2] = aa[2].replace(G, "");
                            var ab = n.exec(aa[2] === "even" && "2n" || aa[2] === "odd" && "2n+1" || !C.test(aa[2]) && "0n+" + aa[2] || aa[2]);
                            aa[2] = (ab[1] + (ab[2] || 1)) - 0;
                            aa[3] = ab[3] - 0
                        } else {
                            if (aa[2]) {
                                V.error(aa[0])
                            }
                        }
                        aa[0] = y++;
                        return aa
                    },
                    ATTR: function(af, ab, ac, aa, ag, ae) {
                        var ad = af[1] = af[1].replace(I, "");
                        if (!ae && P.attrMap[ad]) {
                            af[1] = P.attrMap[ad]
                        }
                        af[4] = (af[4] || af[5] || "").replace(I, "");
                        if (af[2] === "~=") {
                            af[4] = " " + af[4] + " "
                        }
                        return af
                    },
                    PSEUDO: function(af, ab, ac, aa, ag, ae) {
                        if (af[1] === "not") {
                            if ((J.exec(af[3]) || "").length > 1 || o.test(af[3])) {
                                af[3] = X(af[3], m, [], ab, ae)
                            } else {
                                var ad = V.filter(af[3], ab, ac, !ag);
                                if (!ac) {
                                    aa.push.apply(aa, ad)
                                }
                                return false
                            }
                        } else {
                            if (O.POS.test(af[0]) || O.CHILD.test(af[0])) {
                                return true
                            }
                        }
                        return af
                    },
                    POS: function(aa) {
                        aa.unshift(true);
                        return aa
                    }
                },
                filters: {
                    enabled: function(aa) {
                        return aa.disabled === false
                    },
                    disabled: function(aa) {
                        return aa.disabled === true
                    },
                    checked: function(aa) {
                        var ab = aa.nodeName.toLowerCase();
                        return (ab === "input" && !!aa.checked) || (ab === "option" && !!aa.selected)
                    },
                    selected: function(aa) {
                        if (aa.parentNode) {
                            aa.parentNode.selectedIndex
                        }
                        return aa.selected === true
                    },
                    parent: function(aa) {
                        return !! aa.firstChild
                    },
                    empty: function(aa) {
                        return ! aa.firstChild
                    },
                    has: function(ac, ab, aa) {
                        return !! V(aa[3], ac).length
                    },
                    header: function(aa) {
                        return F.test(aa.nodeName)
                    },
                    text: function(ac) {
                        var aa = ac.getAttribute("type"),
                        ab = ac.type;
                        return ac.nodeName.toLowerCase() === "input" && "text" === ab && (aa === null || aa.toLowerCase() === ab)
                    },
                    radio: function(aa) {
                        return aa.nodeName.toLowerCase() === "input" && "radio" === aa.type
                    },
                    checkbox: function(aa) {
                        return aa.nodeName.toLowerCase() === "input" && "checkbox" === aa.type
                    },
                    file: function(aa) {
                        return aa.nodeName.toLowerCase() === "input" && "file" === aa.type
                    },
                    password: function(aa) {
                        return aa.nodeName.toLowerCase() === "input" && "password" === aa.type
                    },
                    submit: function(ab) {
                        var aa = ab.nodeName.toLowerCase();
                        return (aa === "input" || aa === "button") && "submit" === ab.type
                    },
                    image: function(aa) {
                        return aa.nodeName.toLowerCase() === "input" && "image" === aa.type
                    },
                    reset: function(ab) {
                        var aa = ab.nodeName.toLowerCase();
                        return (aa === "input" || aa === "button") && "reset" === ab.type
                    },
                    button: function(ab) {
                        var aa = ab.nodeName.toLowerCase();
                        return aa === "input" && "button" === ab.type || aa === "button"
                    },
                    input: function(aa) {
                        return S.test(aa.nodeName)
                    },
                    focus: function(aa) {
                        var ab = aa.ownerDocument;
                        return aa === ab.activeElement && (!ab.hasFocus || ab.hasFocus()) && !!(aa.type || aa.href)
                    },
                    active: function(aa) {
                        return aa === aa.ownerDocument.activeElement
                    },
                    contains: function(ac, ab, aa) {
                        return (ac.textContent || ac.innerText || i(ac)).indexOf(aa[3]) >= 0
                    }
                },
                setFilters: {
                    first: function(ab, aa) {
                        return aa === 0
                    },
                    last: function(ac, ab, aa, ad) {
                        return ab === ad.length - 1
                    },
                    even: function(ab, aa) {
                        return aa % 2 === 0
                    },
                    odd: function(ab, aa) {
                        return aa % 2 === 1
                    },
                    lt: function(ac, ab, aa) {
                        return ab < aa[3] - 0
                    },
                    gt: function(ac, ab, aa) {
                        return ab > aa[3] - 0
                    },
                    nth: function(ac, ab, aa) {
                        return aa[3] - 0 === ab
                    },
                    eq: function(ac, ab, aa) {
                        return aa[3] - 0 === ab
                    }
                },
                filter: {
                    PSEUDO: function(ac, ag, af, ai) {
                        var aa = ag[1],
                        ab = P.filters[aa];
                        if (ab) {
                            return ab(ac, af, ag, ai)
                        } else {
                            if (aa === "not") {
                                var ad = ag[3],
                                ae = 0,
                                ah = ad.length;
                                for (; ae < ah; ae++) {
                                    if (ad[ae] === ac) {
                                        return false
                                    }
                                }
                                return true
                            } else {
                                V.error(aa)
                            }
                        }
                    },
                    CHILD: function(ac, ae) {
                        var ad,
                        ak,
                        ag,
                        aj,
                        aa,
                        af,
                        ai,
                        ah = ae[1],
                        ab = ac;
                        switch (ah) {
                        case "only":
                        case "first":
                            while ((ab = ab.previousSibling)) {
                                if (ab.nodeType === 1) {
                                    return false
                                }
                            }
                            if (ah === "first") {
                                return true
                            }
                            ab = ac;
                        case "last":
                            while ((ab = ab.nextSibling)) {
                                if (ab.nodeType === 1) {
                                    return false
                                }
                            }
                            return true;
                        case "nth":
                            ad = ae[2];
                            ak = ae[3];
                            if (ad === 1 && ak === 0) {
                                return true
                            }
                            ag = ae[0];
                            aj = ac.parentNode;
                            if (aj && (aj[Y] !== ag || !ac.nodeIndex)) {
                                af = 0;
                                for (ab = aj.firstChild; ab; ab = ab.nextSibling) {
                                    if (ab.nodeType === 1) {
                                        ab.nodeIndex = ++af
                                    }
                                }
                                aj[Y] = ag
                            }
                            ai = ac.nodeIndex - ak;
                            if (ad === 0) {
                                return ai === 0
                            } else {
                                return (ai % ad === 0 && ai / ad >= 0)
                            }
                        }
                    },
                    ID: r ? 
                    function(ab, aa) {
                        return ab.nodeType === 1 && ab.getAttribute("id") === aa
                    }: function(ac, aa) {
                        var ab = typeof ac.getAttributeNode !== K && ac.getAttributeNode("id");
                        return ac.nodeType === 1 && ab && ab.nodeValue === aa
                    },
                    TAG: function(ab, aa) {
                        return (aa === "*" && ab.nodeType === 1) || !!ab.nodeName && ab.nodeName.toLowerCase() === aa
                    },
                    CLASS: function(ab, aa) {
                        return (" " + (ab.className || ab.getAttribute("class")) + " ").indexOf(aa) > -1
                    },
                    ATTR: function(af, ad) {
                        var ac = ad[1],
                        aa = V.attr ? V.attr(af, ac) : P.attrHandle[ac] ? P.attrHandle[ac](af) : af[ac] != null ? af[ac] : af.getAttribute(ac),
                        ag = aa + "",
                        ae = ad[2],
                        ab = ad[4];
                        return aa == null ? ae === "!=": !ae && V.attr ? aa != null: ae === "=" ? ag === ab: ae === "*=" ? ag.indexOf(ab) >= 0: ae === "~=" ? (" " + ag + " ").indexOf(ab) >= 0: !ab ? ag && aa !== false: ae === "!=" ? ag !== ab: ae === "^=" ? ag.indexOf(ab) === 0: ae === "$=" ? ag.substr(ag.length - ab.length) === ab: ae === "|=" ? ag === ab || ag.substr(0, ab.length + 1) === ab + "-": false
                    },
                    POS: function(ae, ab, ac, af) {
                        var aa = ab[2],
                        ad = P.setFilters[aa];
                        if (ad) {
                            return ad(ae, ac, ab, af)
                        }
                    }
                }
            };
            if (M) {
                P.order.splice(1, 0, "CLASS");
                P.find.CLASS = function(ab, ac, aa) {
                    if (typeof ac.getElementsByClassName !== K && !aa) {
                        return ac.getElementsByClassName(ab[1])
                    }
                }
            }
            var z,
            j;
            if (s.compareDocumentPosition) {
                z = function(ab, aa) {
                    if (ab === aa) {
                        u = true;
                        return 0
                    }
                    if (!ab.compareDocumentPosition || !aa.compareDocumentPosition) {
                        return ab.compareDocumentPosition ? -1: 1
                    }
                    return ab.compareDocumentPosition(aa) & 4 ? -1: 1
                }
            } else {
                z = function(ai, ah) {
                    if (ai === ah) {
                        u = true;
                        return 0
                    } else {
                        if (ai.sourceIndex && ah.sourceIndex) {
                            return ai.sourceIndex - ah.sourceIndex
                        }
                    }
                    var af,
                    ab,
                    ac = [],
                    aa = [],
                    ae = ai.parentNode,
                    ag = ah.parentNode,
                    aj = ae;
                    if (ae === ag) {
                        return j(ai, ah)
                    } else {
                        if (!ae) {
                            return - 1
                        } else {
                            if (!ag) {
                                return 1
                            }
                        }
                    }
                    while (aj) {
                        ac.unshift(aj);
                        aj = aj.parentNode
                    }
                    aj = ag;
                    while (aj) {
                        aa.unshift(aj);
                        aj = aj.parentNode
                    }
                    af = ac.length;
                    ab = aa.length;
                    for (var ad = 0; ad < af && ad < ab; ad++) {
                        if (ac[ad] !== aa[ad]) {
                            return j(ac[ad], aa[ad])
                        }
                    }
                    return ad === af ? j(ai, aa[ad], -1) : j(ac[ad], ah, 1)
                };
                j = function(ab, aa, ac) {
                    if (ab === aa) {
                        return ac
                    }
                    var ad = ab.nextSibling;
                    while (ad) {
                        if (ad === aa) {
                            return - 1
                        }
                        ad = ad.nextSibling
                    }
                    return 1
                }
            }
            if (m.querySelectorAll) { (function() {
                    var ae = X,
                    ad = "__sizzle__",
                    ac = /^\s*[+~]/,
                    ab = /'/g,
                    aa = [];
                    Q(function(af) {
                        af.innerHTML = "<select><option selected></option></select>";
                        if (!af.querySelectorAll("[selected]").length) {
                            aa.push("\\[[\\x20\\t\\n\\r\\f]*(?:checked|disabled|ismap|multiple|readonly|selected|value)")
                        }
                        if (!af.querySelectorAll(":checked").length) {
                            aa.push(":checked")
                        }
                    });
                    Q(function(af) {
                        af.innerHTML = "<p class=''></p>";
                        if (af.querySelectorAll("[class^='']").length) {
                            aa.push("[*^$]=[\\x20\\t\\n\\r\\f]*(?:\"\"|'')")
                        }
                        af.innerHTML = "<input type='hidden'>";
                        if (!af.querySelectorAll(":enabled").length) {
                            aa.push(":enabled", ":disabled")
                        }
                    });
                    aa = aa.length && new RegExp(aa.join("|"));
                    X = function(al, ag, am, an, ak) {
                        if (!an && !ak && (!aa || !aa.test(al))) {
                            if (ag.nodeType === 9) {
                                try {
                                    return D(ag.querySelectorAll(al), am)
                                } catch(aj) {}
                            } else {
                                if (ag.nodeType === 1 && ag.nodeName.toLowerCase() !== "object") {
                                    var ah = ag,
                                    ai = ag.getAttribute("id"),
                                    af = ai || ad,
                                    ap = ag.parentNode,
                                    ao = ac.test(al);
                                    if (!ai) {
                                        ag.setAttribute("id", af)
                                    } else {
                                        af = af.replace(ab, "\\$&")
                                    }
                                    if (ao && ap) {
                                        ag = ap
                                    }
                                    try {
                                        if (!ao || ap) {
                                            return D(ag.querySelectorAll("[id='" + af + "'] " + al), am)
                                        }
                                    } catch(aj) {} finally {
                                        if (!ai) {
                                            ah.removeAttribute("id")
                                        }
                                    }
                                }
                            }
                        }
                        return ae(al, ag, am, an, ak)
                    }
                })()
            }
            function t(ac, ak, aa, ae) {
                var ab,
                af,
                ai,
                aj,
                ah = y++,
                ad = 0,
                ag = ak.length;
                if (typeof aa === "string" && !Z.test(aa)) {
                    aa = aa.toLowerCase();
                    aj = aa
                }
                for (; ad < ag; ad++) {
                    ab = ak[ad];
                    if (ab) {
                        af = false;
                        ab = ab[ac];
                        while (ab) {
                            if (ab[Y] === ah) {
                                af = ak[ab.sizset];
                                break
                            }
                            ai = ab.nodeType === 1;
                            if (ai && !ae) {
                                ab[Y] = ah;
                                ab.sizset = ad
                            }
                            if (aj) {
                                if (ab.nodeName.toLowerCase() === aa) {
                                    af = ab;
                                    break
                                }
                            } else {
                                if (ai) {
                                    if (typeof aa !== "string") {
                                        if (ab === aa) {
                                            af = true;
                                            break
                                        }
                                    } else {
                                        if (V.filter(aa, [ab]).length > 0) {
                                            af = ab;
                                            break
                                        }
                                    }
                                }
                            }
                            ab = ab[ac]
                        }
                        ak[ad] = af
                    }
                }
            }
            var H = function(ac, aa, ag, ab) {
                var af,
                ai = [],
                ae = "",
                aj = aa.nodeType ? [aa] : aa,
                ad = 0,
                ah = aj.length;
                while ((af = O.PSEUDO.exec(ac))) {
                    ae += af[0];
                    ac = ac.replace(O.PSEUDO, "")
                }
                if (P.relative[ac]) {
                    ac += "*"
                }
                for (; ad < ah; ad++) {
                    X(ac, aj[ad], ai, ag, ab)
                }
                return V.filter(ae, ai)
            };
            W.Sizzle = c.sizzle = V
        })(window);
        c.string.extend({
            decodeHTML: function() {
                var i = this.replace(/&quot;/g, '"').replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&amp;/g, "&");
                return i.replace(/&#([\d]+);/g, 
                function(k, j) {
                    return String.fromCharCode(parseInt(j, 10))
                })
            }
        });
        c.string.extend({
            encodeHTML: function() {
                return this.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#39;")
            }
        });
        c.string.filterFormat = function(k, i) {
            var j = Array.prototype.slice.call(arguments, 1),
            l = Object.prototype.toString;
            if (j.length) {
                j = j.length == 1 ? (i !== null && (/\[object Array\]|\[object Object\]/.test(l.call(i))) ? i: j) : j;
                return k.replace(/#\{(.+?)\}/g, 
                function(n, r) {
                    var t,
                    p,
                    o,
                    m,
                    s;
                    if (!j) {
                        return ""
                    }
                    t = r.split("|");
                    p = j[t[0]];
                    if ("[object Function]" == l.call(p)) {
                        p = p(t[0])
                    }
                    for (o = 1, m = t.length; o < m; ++o) {
                        s = c.string.filterFormat[t[o]];
                        if ("[object Function]" == l.call(s)) {
                            p = s(p)
                        }
                    }
                    return (("undefined" == typeof p || p === null) ? "": p)
                })
            }
            return k
        };
        c.string.filterFormat.escapeJs = function(n) {
            if (!n || "string" != typeof n) {
                return n
            }
            var m,
            j,
            k,
            l = [];
            for (m = 0, j = n.length; m < j; ++m) {
                k = n.charCodeAt(m);
                if (k > 255) {
                    l.push(n.charAt(m))
                } else {
                    l.push("\\x" + k.toString(16))
                }
            }
            return l.join("")
        };
        c.string.filterFormat.js = c.string.filterFormat.escapeJs;
        c.string.filterFormat.escapeString = function(i) {
            if (!i || "string" != typeof i) {
                return i
            }
            return i.replace(/["'<>\\\/`]/g, 
            function(j) {
                return "&#" + j.charCodeAt(0) + ";"
            })
        };
        c.string.filterFormat.e = c.string.filterFormat.escapeString;
        c.string.filterFormat.toInt = function(i) {
            return parseInt(i, 10) || 0
        };
        c.string.filterFormat.i = c.string.filterFormat.toInt;
        c.string.extend({
            format: function(i) {
                var k = this.valueOf(),
                j = Array.prototype.slice.call(arguments, 0),
                l = Object.prototype.toString;
                if (j.length) {
                    j = j.length == 1 ? (i !== null && (/\[object Array\]|\[object Object\]/.test(l.call(i))) ? i: j) : j;
                    return k.replace(/#\{(.+?)\}/g, 
                    function(m, o) {
                        var n = j[o];
                        if ("[object Function]" == l.call(n)) {
                            n = n(o)
                        }
                        return ("undefined" == typeof n ? "": n)
                    })
                }
                return k
            }
        });
        c.string.extend({
            getByteLength: function() {
                return this.replace(/[^\x00-\xff]/g, "ci").length
            }
        });
        c.string.extend({
            stripTags: function() {
                return (this || "").replace(/<[^>]+>/g, "")
            }
        });
        c.string.extend({
            subByte: function(i, j) {
                c.check("^(?:number(?:,(?:string|number))?)$", "baidu.string.subByte");
                var k = this.valueOf();
                j = j || "";
                if (i < 0 || c.string(k).getByteLength() <= i) {
                    return k + j
                }
                k = k.substr(0, i).replace(/([^\x00-\xff])/g, "\x241 ").substr(0, i).replace(/[^\x00-\xff]$/, "").replace(/([^\x00-\xff]) /g, "\x241");
                return k + j
            }
        });
        c.string.extend({
            toHalfWidth: function() {
                return this.replace(/[\uFF01-\uFF5E]/g, 
                function(i) {
                    return String.fromCharCode(i.charCodeAt(0) - 65248)
                }).replace(/\u3000/g, " ")
            }
        });
        c.string.extend({
            wbr: function() {
                return this.replace(/(?:<[^>]+>)|(?:&#?[0-9a-z]{2,6};)|(.{1})/gi, "$&<wbr>").replace(/><wbr>/g, ">")
            }
        });
        c.swf = c.swf || {};
        c.swf.version = (function() {
            var p = navigator;
            if (p.plugins && p.mimeTypes.length) {
                var l = p.plugins["Shockwave Flash"];
                if (l && l.description) {
                    return l.description.replace(/([a-zA-Z]|\s)+/, "").replace(/(\s)+r/, ".") + ".0"
                }
            } else {
                if (window.ActiveXObject && !window.opera) {
                    for (var k = 12; k >= 2; k--) {
                        try {
                            var o = new ActiveXObject("ShockwaveFlash.ShockwaveFlash." + k);
                            if (o) {
                                var j = o.GetVariable("$version");
                                return j.replace(/WIN/g, "").replace(/,/g, ".")
                            }
                        } catch(m) {}
                    }
                }
            }
        })();
        c.swf.createHTML = function(C) {
            C = C || {};
            var t = c.swf.version,
            r = C.ver || "6.0.0",
            p,
            n,
            o,
            m,
            s,
            B,
            j = {},
            y = c.string.encodeHTML;
            for (m in C) {
                j[m] = C[m]
            }
            C = j;
            if (t) {
                t = t.split(".");
                r = r.split(".");
                for (o = 0; o < 3; o++) {
                    p = parseInt(t[o], 10);
                    n = parseInt(r[o], 10);
                    if (n < p) {
                        break
                    } else {
                        if (n > p) {
                            return ""
                        }
                    }
                }
            } else {
                return ""
            }
            var w = C.vars,
            u = ["classid", "codebase", "id", "width", "height", "align"];
            C.align = C.align || "middle";
            C.classid = "clsid:d27cdb6e-ae6d-11cf-96b8-444553540000";
            C.codebase = "http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0";
            C.movie = C.url || "";
            delete C.vars;
            delete C.url;
            if ("string" == typeof w) {
                C.flashvars = w
            } else {
                var z = [];
                for (m in w) {
                    B = w[m];
                    z.push(m + "=" + encodeURIComponent(B))
                }
                C.flashvars = z.join("&")
            }
            var x = ["<object "];
            for (o = 0, s = u.length; o < s; o++) {
                B = u[o];
                x.push(" ", B, '="', y(C[B]), '"')
            }
            x.push(">");
            var l = {
                wmode: 1,
                scale: 1,
                quality: 1,
                play: 1,
                loop: 1,
                menu: 1,
                salign: 1,
                bgcolor: 1,
                base: 1,
                allowscriptaccess: 1,
                allownetworking: 1,
                allowfullscreen: 1,
                seamlesstabbing: 1,
                devicefont: 1,
                swliveconnect: 1,
                flashvars: 1,
                movie: 1
            };
            for (m in C) {
                B = C[m];
                m = m.toLowerCase();
                if (l[m] && (B || B === false || B === 0)) {
                    x.push('<param name="' + m + '" value="' + y(B) + '" />')
                }
            }
            C.src = C.movie;
            C.name = C.id;
            delete C.id;
            delete C.movie;
            delete C.classid;
            delete C.codebase;
            C.type = "application/x-shockwave-flash";
            C.pluginspage = "http://www.macromedia.com/go/getflashplayer";
            x.push("<embed");
            var A;
            for (m in C) {
                B = C[m];
                if (B || B === false || B === 0) {
                    if ((new RegExp("^salign\x24", "i")).test(m)) {
                        A = B;
                        continue
                    }
                    x.push(" ", m, '="', y(B), '"')
                }
            }
            if (A) {
                x.push(' salign="', y(A), '"')
            }
            x.push("></embed></object>");
            return x.join("")
        };
        c.swf.create = function(i, k) {
            i = i || {};
            var j = c.swf.createHTML(i) || i.errorMessage || "";
            if (k && "string" == typeof k) {
                k = document.getElementById(k)
            }
            c.dom.insertHTML(k || document.body, "beforeEnd", j)
        };
        c.swf.getMovie = function(k) {
            var i = document[k],
            j;
            return c.browser.ie == 9 ? i && i.length ? (j = c.array.remove(c.lang.toArray(i), 
            function(l) {
                return l.tagName.toLowerCase() != "embed"
            })).length == 1 ? j[0] : j: i: i || window[k]
        };
        c.swf.Proxy = function(n, k, l) {
            var j = this,
            i = this._flash = c.swf.getMovie(n),
            m;
            if (!k) {
                return this
            }
            m = setInterval(function() {
                try {
                    if (i[k]) {
                        j._initialized = true;
                        clearInterval(m);
                        if (l) {
                            l()
                        }
                    }
                } catch(o) {}
            },
            100)
        };
        c.swf.Proxy.prototype.getFlash = function() {
            return this._flash
        };
        c.swf.Proxy.prototype.isReady = function() {
            return !! this._initialized
        };
        c.swf.Proxy.prototype.call = function(i, m) {
            try {
                var k = this.getFlash(),
                j = Array.prototype.slice.call(arguments);
                j.shift();
                if (k[i]) {
                    k[i].apply(k, j)
                }
            } catch(l) {}
        };
        c.url.getQueryValue = function(j, k) {
            var l = new RegExp("(^|&|\\?|#)" + c.string.escapeReg(k) + "=([^&#]*)(&|\x24|#)", "");
            var i = j.match(l);
            if (i) {
                return i[2]
            }
            return null
        };
        c.url.jsonToQuery = function(k, m) {
            var i = [],
            l,
            j = m || 
            function(n) {
                return c.url.escapeSymbol(n)
            };
            c.object.each(k, 
            function(o, n) {
                if (c.lang.isArray(o)) {
                    l = o.length;
                    while (l--) {
                        i.push(n + "=" + j(o[l], n))
                    }
                } else {
                    i.push(n + "=" + j(o, n))
                }
            });
            return i.join("&")
        };
        c.url.queryToJson = function(l) {
            var r = l.substr(l.lastIndexOf("?") + 1).split("&"),
            j = r.length,
            k = null,
            o,
            n,
            p;
            for (var m = 0; m < j; m++) {
                o = r[m].split("=");
                if (o.length < 2) {
                    continue
                } ! k && (k = {});
                n = o[0];
                p = o[1];
                o = k[n];
                if (!o) {
                    k[n] = p
                } else {
                    if (c.lang.isArray(o)) {
                        o.push(p)
                    } else {
                        k[n] = [o, p]
                    }
                }
            }
            return k
        }; (function() {
            var n = /^(?:\{.*\}|\[.*\])$/,
            i = /([A-Z])/g,
            k = /^-ms-/,
            m = /-([\da-z])/gi,
            j = function(p, r) {
                return (r + "").toUpperCase()
            };
            c.extend(c._util_, {
                camelCase: function(p) {
                    return p.replace(k, "ms-").replace(m, j)
                }
            });
            c.extend(c._util_, {
                cache: {},
                deletedIds: [],
                uuid: 0,
                expando: "baidu" + ("2.0.0" + Math.random()).replace(/\D/g, ""),
                noData: {
                    embed: true,
                    object: "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",
                    applet: true
                },
                hasData: function(p) {
                    p = p.nodeType ? c._util_.cache[p[c._util_.expando]] : p[c._util_.expando];
                    return !! p && !l(p)
                },
                data: function(t, r, w, u) {
                    if (!c._util_.acceptData(t)) {
                        return
                    }
                    var x,
                    z,
                    A = c._util_.expando,
                    y = typeof r === "string",
                    B = t.nodeType,
                    p = B ? c._util_.cache: t,
                    s = B ? t[A] : t[A] && A;
                    if ((!s || !p[s] || (!u && !p[s].data)) && y && w === undefined) {
                        return
                    }
                    if (!s) {
                        if (B) {
                            t[A] = s = c._util_.deletedIds.pop() || ++c._util_.uuid
                        } else {
                            s = A
                        }
                    }
                    if (!p[s]) {
                        p[s] = {};
                        if (!B) {
                            p[s].toJSON = function() {}
                        }
                    }
                    if (typeof r === "object" || typeof r === "function") {
                        if (u) {
                            p[s] = c.extend(p[s], r)
                        } else {
                            p[s].data = c.extend(p[s].data, r)
                        }
                    }
                    x = p[s];
                    if (!u) {
                        if (!x.data) {
                            x.data = {}
                        }
                        x = x.data
                    }
                    if (w !== undefined) {
                        x[c._util_.camelCase(r)] = w
                    }
                    if (y) {
                        z = x[r];
                        if (z == null) {
                            z = x[c._util_.camelCase(r)]
                        }
                    } else {
                        z = x
                    }
                    return z
                },
                removeData: function(t, r, u) {
                    if (!c._util_.acceptData(t)) {
                        return
                    }
                    var y,
                    x,
                    w,
                    z = t.nodeType,
                    p = z ? c._util_.cache: t,
                    s = z ? t[c._util_.expando] : c._util_.expando;
                    if (!p[s]) {
                        return
                    }
                    if (r) {
                        y = u ? p[s] : p[s].data;
                        if (y) {
                            if (!c.isArray(r)) {
                                if (r in y) {
                                    r = [r]
                                } else {
                                    r = c._util_.camelCase(r);
                                    if (r in y) {
                                        r = [r]
                                    } else {
                                        r = r.split(" ")
                                    }
                                }
                            }
                            for (x = 0, w = r.length; x < w; x++) {
                                delete y[r[x]]
                            }
                            if (! (u ? l: c.object.isEmpty)(y)) {
                                return
                            }
                        }
                    }
                    if (!u) {
                        delete p[s].data;
                        if (!l(p[s])) {
                            return
                        }
                    }
                    if (z) {
                        c._util_.cleanData([t], true)
                    } else {
                        if (c.support.deleteExpando || p != p.window) {
                            delete p[s]
                        } else {
                            p[s] = null
                        }
                    }
                },
                _data: function(r, p, s) {
                    return c._util_.data(r, p, s, true)
                },
                acceptData: function(r) {
                    var p = r.nodeName && c._util_.noData[r.nodeName.toLowerCase()];
                    return ! p || p !== true && r.getAttribute("classid") === p
                }
            });
            function o(s, r, t) {
                if (t === undefined && s.nodeType === 1) {
                    var p = "data-" + r.replace(i, "-$1").toLowerCase();
                    t = s.getAttribute(p);
                    if (typeof t === "string") {
                        try {
                            t = t === "true" ? true: t === "false" ? false: t === "null" ? null: c.isNumber(t) ? +t: n.test(t) ? c.json.parse(t) : t
                        } catch(u) {}
                        c._util_.data(s, r, t)
                    } else {
                        t = undefined
                    }
                }
                return t
            }
            function l(r) {
                var p;
                for (p in r) {
                    if (p === "data" && c.object.isEmpty(r[p])) {
                        continue
                    }
                    if (p !== "toJSON") {
                        return false
                    }
                }
                return true
            }
        })();
        c.array = c.array || {};
        c.dom = c.dom || {};
        c.addClass = c.dom.addClass || {};
        c.g = c.G = c.dom.g || {};
        c.getAttr = c.dom.getAttr || {};
        c.getStyle = c.dom.getStyle || {};
        c.hide = c.dom.hide || {};
        c.insertHTML = c.dom.insertHTML || {};
        c.q = c.Q = c.dom.q || {};
        c.removeClass = c.dom.removeClass || {};
        c.setAttr = c.dom.setAttr || {};
        c.setAttrs = c.dom.setAttrs || {};
        c.dom.setOuterHeight = c.dom.setBorderBoxHeight || {};
        c.dom.setOuterWidth = c.dom.setBorderBoxWidth || {};
        c.setStyle = c.dom.setStyle || {};
        c.setStyles = c.dom.setStyles || {};
        c.show = c.dom.show || {};
        c.e = c.element = c.element || {};
        c.event = c.event || {};
        c.on = c.event.on || {};
        c.un = c.event.un || {};
        c.lang = c.lang || {};
        c.inherits = c.lang.inherits || {};
        c.object = c.object || {};
        c.string = c.string || {};
        c.decodeHTML = c.string.decodeHTML || {};
        c.encodeHTML = c.string.encodeHTML || {};
        c.format = c.string.format || {};
        c.trim = c.string.trim || {};
        var h = h || window.passport || {},
        c = h.tangramInst || c || window.baidu; (function(i) {
            i.apiDomain = {
                staticDomain: (window.location ? ((window.location.protocol.toLowerCase() == "http:") ? "http://passport.bdimg.com": "https://passport.baidu.com") : ((document.location.protocol.toLowerCase() == "http:") ? "http://passport.bdimg.com": "https://passport.baidu.com"))
            }
        })(h);
        var g = null;
        if (typeof g != "function") {
            var g = function() {}
        }
        g._baiduInstName = g._baiduInstName || "bdInst_" + new Date().getTime();
        var d = d || c.baiduInstance || window.baiduInstance;
        window[g._baiduInstName] = window[g._baiduInstName] || d;
        g.resourcePath = "";
        g.skinName = "default";
        g.version = "1.0.0.0";
        /msie 6/i.test(navigator.userAgent) && document.execCommand("BackgroundImageCache", false, true);
        c.form = c.form || {};
        c.url = c.url || {};
        c.url.escapeSymbol = c.url.escapeSymbol || 
        function(i) {
            return String(i).replace(/[#%&+=\/\\\ \銆€\f\r\n\t]/g, 
            function(j) {
                return "%" + (256 + j.charCodeAt()).toString(16).substring(1).toUpperCase()
            })
        };
        c.form.json = c.form.json || 
        function(l, n) {
            var k = l.elements,
            n = n || 
            function(A, i) {
                return A
            },
            r = {},
            y,
            t,
            z,
            m,
            j,
            x,
            w,
            u;
            function o(i, A) {
                var B = r[i];
                if (B) {
                    B.push || (r[i] = [B]);
                    r[i].push(A)
                } else {
                    r[i] = A
                }
            }
            for (var p = 0, s = k.length; p < s; p++) {
                y = k[p];
                z = y.name;
                if (!y.disabled && z) {
                    t = y.type;
                    m = c.url.escapeSymbol(y.value);
                    switch (t) {
                    case "radio":
                    case "checkbox":
                        if (!y.checked) {
                            break
                        }
                    case "textarea":
                    case "text":
                    case "password":
                    case "hidden":
                    case "file":
                    case "select-one":
                        o(z, n(m, z));
                        break;
                    case "select-multiple":
                        j = y.options;
                        w = j.length;
                        for (x = 0; x < w; x++) {
                            u = j[x];
                            if (u.selected) {
                                o(z, n(u.value, z))
                            }
                        }
                        break
                    }
                }
            }
            return r
        };
        g.Base = function() {
            c.lang.Class.call(this);
            this._ids = {};
            this._eid = this.guid + "__"
        };
        c.lang.inherits(g.Base, c.lang.Class, "magic.Base").extend({
            getElement: function(i) {
                return document.getElementById(this.$getId(i))
            },
            getElements: function() {
                var i = {};
                var k = this._ids;
                for (var j in k) {
                    i[j] = this.getElement(j)
                }
                return i
            },
            $getId: function(i) {
                i = c.lang.isString(i) ? i: "";
                return this._ids[i] || (this._ids[i] = this._eid + i)
            },
            $mappingDom: function(i, j) {
                if (c.lang.isString(j)) {
                    this._ids[i] = j
                } else {
                    if (j && j.nodeType) {
                        j.id ? this._ids[i] = j.id: j.id = this.$getId(i)
                    }
                }
                return this
            },
            $hide: function(i) {
                if ((typeof i).toLowerCase() == "string" || i === "") {
                    i = this.getElement(i)
                }
                if (i && i.style) {
                    i.style.display = "none";
                    i.style.visibility = "hidden"
                }
                return this
            },
            $show: function(i) {
                if ((typeof i).toLowerCase() == "string" || i === "") {
                    i = this.getElement(i)
                }
                if (i && i.style) {
                    i.style.display = "block";
                    i.style.visibility = "visible";
                    i.style.opacity = "1"
                }
                return this
            },
            $dispose: function() {
                this.fire("ondispose") && c.lang.Class.prototype.dispose.call(this)
            }
        });
        g.control = g.control || {}; (function() {
            g.setup = g.setup || 
            function(p, l, m) {
                var o = k(p, "tang-param") || {};
                for (var n in m) {
                    o[n] = m[n]
                }
                var s = new l(o);
                s.$mappingDom("", p);
                j(p, s.guid);
                var r = p.getElementsByTagName("*");
                for (var n = r.length - 1; n >= 0; n--) {
                    j(r[n], s.guid)
                }
                return s
            };
            function k(p, l) {
                var s = p.getAttribute(l),
                r,
                o = false;
                if (s && (r = s.match(i[0]))) {
                    o = {};
                    for (var n = 0, m; n < r.length; n++) {
                        m = r[n].match(i[1]); ! isNaN(m[2]) && (m[2] = +m[2]);
                        i[2].test(m[2]) && (m[2] = m[2].replace(i[3], "\x242"));
                        i[4].test(m[2]) && (m[2] = i[5].test(m[2]));
                        o[m[1]] = m[2]
                    }
                }
                return o
            }
            var i = [/\b[\w\$\-]+\s*:\s*[^;]+/g, /([\w\$\-]+)\s*:\s*([^;]+)\s*/, /\'|\"/, /^\s*(\'|\")([^\1]*)\1\s*/, /^(true|false)\s*$/i, /\btrue\b/i];
            function j(o, l) {
                var n = k(o, "tang-event");
                if (n) {
                    for (var m in n) {
                        var p = n[m].substr(1);
                        p.indexOf("(") < 0 && (p += "()");
                        c.dom(o).on(m, new Function(g._baiduInstName + "('" + l + "') && " + g._baiduInstName + "('" + l + "')" + p))
                    }
                }
            }
        })();
        h = h || {};
        h.lib = h.lib || {};
        h.lib.RSAExport = {}; (function(az) {
            var bO;
            var A = 244837814094590;
            var a4 = ((A & 16777215) == 15715070);
            function bo(z, t, L) {
                if (z != null) {
                    if ("number" == typeof z) {
                        this.fromNumber(z, t, L)
                    } else {
                        if (t == null && "string" != typeof z) {
                            this.fromString(z, 256)
                        } else {
                            this.fromString(z, t)
                        }
                    }
                }
            }
            function bv() {
                return new bo(null)
            }
            function bg(b7, t, z, b6, b9, b8) {
                while (--b8 >= 0) {
                    var L = t * this[b7++] + z[b6] + b9;
                    b9 = Math.floor(L / 67108864);
                    z[b6++] = L & 67108863
                }
                return b9
            }
            function bf(b7, cc, cd, b6, ca, t) {
                var b9 = cc & 32767,
                cb = cc >> 15;
                while (--t >= 0) {
                    var L = this[b7] & 32767;
                    var b8 = this[b7++] >> 15;
                    var z = cb * L + b8 * b9;
                    L = b9 * L + ((z & 32767) << 15) + cd[b6] + (ca & 1073741823);
                    ca = (L >>> 30) + (z >>> 15) + cb * b8 + (ca >>> 30);
                    cd[b6++] = L & 1073741823
                }
                return ca
            }
            function be(b7, cc, cd, b6, ca, t) {
                var b9 = cc & 16383,
                cb = cc >> 14;
                while (--t >= 0) {
                    var L = this[b7] & 16383;
                    var b8 = this[b7++] >> 14;
                    var z = cb * L + b8 * b9;
                    L = b9 * L + ((z & 16383) << 14) + cd[b6] + ca;
                    ca = (L >> 28) + (z >> 14) + cb * b8;
                    cd[b6++] = L & 268435455
                }
                return ca
            }
            if (a4 && (navigator.appName == "Microsoft Internet Explorer")) {
                bo.prototype.am = bf;
                bO = 30
            } else {
                if (a4 && (navigator.appName != "Netscape")) {
                    bo.prototype.am = bg;
                    bO = 26
                } else {
                    bo.prototype.am = be;
                    bO = 28
                }
            }
            bo.prototype.DB = bO;
            bo.prototype.DM = ((1 << bO) - 1);
            bo.prototype.DV = (1 << bO);
            var b0 = 52;
            bo.prototype.FV = Math.pow(2, b0);
            bo.prototype.F1 = b0 - bO;
            bo.prototype.F2 = 2 * bO - b0;
            var i = "0123456789abcdefghijklmnopqrstuvwxyz";
            var o = new Array();
            var aQ,
            P;
            aQ = "0".charCodeAt(0);
            for (P = 0; P <= 9; ++P) {
                o[aQ++] = P
            }
            aQ = "a".charCodeAt(0);
            for (P = 10; P < 36; ++P) {
                o[aQ++] = P
            }
            aQ = "A".charCodeAt(0);
            for (P = 10; P < 36; ++P) {
                o[aQ++] = P
            }
            function ah(t) {
                return i.charAt(t)
            }
            function a6(z, t) {
                var L = o[z.charCodeAt(t)];
                return (L == null) ? -1: L
            }
            function l(z) {
                for (var t = this.t - 1; t >= 0; --t) {
                    z[t] = this[t]
                }
                z.t = this.t;
                z.s = this.s
            }
            function p(t) {
                this.t = 1;
                this.s = (t < 0) ? -1: 0;
                if (t > 0) {
                    this[0] = t
                } else {
                    if (t < -1) {
                        this[0] = t + DV
                    } else {
                        this.t = 0
                    }
                }
            }
            function br(t) {
                var z = bv();
                z.fromInt(t);
                return z
            }
            function bS(b9, z) {
                var b6;
                if (z == 16) {
                    b6 = 4
                } else {
                    if (z == 8) {
                        b6 = 3
                    } else {
                        if (z == 256) {
                            b6 = 8
                        } else {
                            if (z == 2) {
                                b6 = 1
                            } else {
                                if (z == 32) {
                                    b6 = 5
                                } else {
                                    if (z == 4) {
                                        b6 = 2
                                    } else {
                                        this.fromRadix(b9, z);
                                        return
                                    }
                                }
                            }
                        }
                    }
                }
                this.t = 0;
                this.s = 0;
                var b8 = b9.length,
                L = false,
                b7 = 0;
                while (--b8 >= 0) {
                    var t = (b6 == 8) ? b9[b8] & 255: a6(b9, b8);
                    if (t < 0) {
                        if (b9.charAt(b8) == "-") {
                            L = true
                        }
                        continue
                    }
                    L = false;
                    if (b7 == 0) {
                        this[this.t++] = t
                    } else {
                        if (b7 + b6 > this.DB) {
                            this[this.t - 1] |= (t & ((1 << (this.DB - b7)) - 1)) << b7;
                            this[this.t++] = (t >> (this.DB - b7))
                        } else {
                            this[this.t - 1] |= t << b7
                        }
                    }
                    b7 += b6;
                    if (b7 >= this.DB) {
                        b7 -= this.DB
                    }
                }
                if (b6 == 8 && (b9[0] & 128) != 0) {
                    this.s = -1;
                    if (b7 > 0) {
                        this[this.t - 1] |= ((1 << (this.DB - b7)) - 1) << b7
                    }
                }
                this.clamp();
                if (L) {
                    bo.ZERO.subTo(this, this)
                }
            }
            function bJ() {
                var t = this.s & this.DM;
                while (this.t > 0 && this[this.t - 1] == t) {--this.t
                }
            }
            function G(z) {
                if (this.s < 0) {
                    return "-" + this.negate().toString(z)
                }
                var L;
                if (z == 16) {
                    L = 4
                } else {
                    if (z == 8) {
                        L = 3
                    } else {
                        if (z == 2) {
                            L = 1
                        } else {
                            if (z == 32) {
                                L = 5
                            } else {
                                if (z == 4) {
                                    L = 2
                                } else {
                                    return this.toRadix(z)
                                }
                            }
                        }
                    }
                }
                var b7 = (1 << L) - 1,
                ca,
                t = false,
                b8 = "",
                b6 = this.t;
                var b9 = this.DB - (b6 * this.DB) % L;
                if (b6-->0) {
                    if (b9 < this.DB && (ca = this[b6] >> b9) > 0) {
                        t = true;
                        b8 = ah(ca)
                    }
                    while (b6 >= 0) {
                        if (b9 < L) {
                            ca = (this[b6] & ((1 << b9) - 1)) << (L - b9);
                            ca |= this[--b6] >> (b9 += this.DB - L)
                        } else {
                            ca = (this[b6] >> (b9 -= L)) & b7;
                            if (b9 <= 0) {
                                b9 += this.DB; --b6
                            }
                        }
                        if (ca > 0) {
                            t = true
                        }
                        if (t) {
                            b8 += ah(ca)
                        }
                    }
                }
                return t ? b8: "0"
            }
            function bM() {
                var t = bv();
                bo.ZERO.subTo(this, t);
                return t
            }
            function bK() {
                return (this.s < 0) ? this.negate() : this
            }
            function bX(t) {
                var L = this.s - t.s;
                if (L != 0) {
                    return L
                }
                var z = this.t;
                L = z - t.t;
                if (L != 0) {
                    return (this.s < 0) ? -L: L
                }
                while (--z >= 0) {
                    if ((L = this[z] - t[z]) != 0) {
                        return L
                    }
                }
                return 0
            }
            function D(z) {
                var b6 = 1,
                L;
                if ((L = z >>> 16) != 0) {
                    z = L;
                    b6 += 16
                }
                if ((L = z >> 8) != 0) {
                    z = L;
                    b6 += 8
                }
                if ((L = z >> 4) != 0) {
                    z = L;
                    b6 += 4
                }
                if ((L = z >> 2) != 0) {
                    z = L;
                    b6 += 2
                }
                if ((L = z >> 1) != 0) {
                    z = L;
                    b6 += 1
                }
                return b6
            }
            function bC() {
                if (this.t <= 0) {
                    return 0
                }
                return this.DB * (this.t - 1) + D(this[this.t - 1] ^ (this.s & this.DM))
            }
            function bE(L, z) {
                var t;
                for (t = this.t - 1; t >= 0; --t) {
                    z[t + L] = this[t]
                }
                for (t = L - 1; t >= 0; --t) {
                    z[t] = 0
                }
                z.t = this.t + L;
                z.s = this.s
            }
            function bb(L, z) {
                for (var t = L; t < this.t; ++t) {
                    z[t - L] = this[t]
                }
                z.t = Math.max(this.t - L, 0);
                z.s = this.s
            }
            function F(ca, b6) {
                var z = ca % this.DB;
                var t = this.DB - z;
                var b8 = (1 << t) - 1;
                var b7 = Math.floor(ca / this.DB),
                b9 = (this.s << z) & this.DM,
                L;
                for (L = this.t - 1; L >= 0; --L) {
                    b6[L + b7 + 1] = (this[L] >> t) | b9;
                    b9 = (this[L] & b8) << z
                }
                for (L = b7 - 1; L >= 0; --L) {
                    b6[L] = 0
                }
                b6[b7] = b9;
                b6.t = this.t + b7 + 1;
                b6.s = this.s;
                b6.clamp()
            }
            function b3(b9, b6) {
                b6.s = this.s;
                var b7 = Math.floor(b9 / this.DB);
                if (b7 >= this.t) {
                    b6.t = 0;
                    return
                }
                var z = b9 % this.DB;
                var t = this.DB - z;
                var b8 = (1 << z) - 1;
                b6[0] = this[b7] >> z;
                for (var L = b7 + 1; L < this.t; ++L) {
                    b6[L - b7 - 1] |= (this[L] & b8) << t;
                    b6[L - b7] = this[L] >> z
                }
                if (z > 0) {
                    b6[this.t - b7 - 1] |= (this.s & b8) << t
                }
                b6.t = this.t - b7;
                b6.clamp()
            }
            function bB(z, b6) {
                var L = 0,
                b7 = 0,
                t = Math.min(z.t, this.t);
                while (L < t) {
                    b7 += this[L] - z[L];
                    b6[L++] = b7 & this.DM;
                    b7 >>= this.DB
                }
                if (z.t < this.t) {
                    b7 -= z.s;
                    while (L < this.t) {
                        b7 += this[L];
                        b6[L++] = b7 & this.DM;
                        b7 >>= this.DB
                    }
                    b7 += this.s
                } else {
                    b7 += this.s;
                    while (L < z.t) {
                        b7 -= z[L];
                        b6[L++] = b7 & this.DM;
                        b7 >>= this.DB
                    }
                    b7 -= z.s
                }
                b6.s = (b7 < 0) ? -1: 0;
                if (b7 < -1) {
                    b6[L++] = this.DV + b7
                } else {
                    if (b7 > 0) {
                        b6[L++] = b7
                    }
                }
                b6.t = L;
                b6.clamp()
            }
            function bT(z, b6) {
                var t = this.abs(),
                b7 = z.abs();
                var L = t.t;
                b6.t = L + b7.t;
                while (--L >= 0) {
                    b6[L] = 0
                }
                for (L = 0; L < b7.t; ++L) {
                    b6[L + t.t] = t.am(0, b7[L], b6, L, 0, t.t)
                }
                b6.s = 0;
                b6.clamp();
                if (this.s != z.s) {
                    bo.ZERO.subTo(b6, b6)
                }
            }
            function aD(L) {
                var t = this.abs();
                var z = L.t = 2 * t.t;
                while (--z >= 0) {
                    L[z] = 0
                }
                for (z = 0; z < t.t - 1; ++z) {
                    var b6 = t.am(z, t[z], L, 2 * z, 0, 1);
                    if ((L[z + t.t] += t.am(z + 1, 2 * t[z], L, 2 * z + 1, b6, t.t - z - 1)) >= t.DV) {
                        L[z + t.t] -= t.DV;
                        L[z + t.t + 1] = 1
                    }
                }
                if (L.t > 0) {
                    L[L.t - 1] += t.am(z, t[z], L, 2 * z, 0, 1)
                }
                L.s = 0;
                L.clamp()
            }
            function bi(cd, ca, b9) {
                var ck = cd.abs();
                if (ck.t <= 0) {
                    return
                }
                var cb = this.abs();
                if (cb.t < ck.t) {
                    if (ca != null) {
                        ca.fromInt(0)
                    }
                    if (b9 != null) {
                        this.copyTo(b9)
                    }
                    return
                }
                if (b9 == null) {
                    b9 = bv()
                }
                var b7 = bv(),
                z = this.s,
                cc = cd.s;
                var cj = this.DB - D(ck[ck.t - 1]);
                if (cj > 0) {
                    ck.lShiftTo(cj, b7);
                    cb.lShiftTo(cj, b9)
                } else {
                    ck.copyTo(b7);
                    cb.copyTo(b9)
                }
                var cg = b7.t;
                var L = b7[cg - 1];
                if (L == 0) {
                    return
                }
                var cf = L * (1 << this.F1) + ((cg > 1) ? b7[cg - 2] >> this.F2: 0);
                var cn = this.FV / cf,
                cm = (1 << this.F1) / cf,
                cl = 1 << this.F2;
                var ci = b9.t,
                ch = ci - cg,
                b8 = (ca == null) ? bv() : ca;
                b7.dlShiftTo(ch, b8);
                if (b9.compareTo(b8) >= 0) {
                    b9[b9.t++] = 1;
                    b9.subTo(b8, b9)
                }
                bo.ONE.dlShiftTo(cg, b8);
                b8.subTo(b7, b7);
                while (b7.t < cg) {
                    b7[b7.t++] = 0
                }
                while (--ch >= 0) {
                    var b6 = (b9[--ci] == L) ? this.DM: Math.floor(b9[ci] * cn + (b9[ci - 1] + cl) * cm);
                    if ((b9[ci] += b7.am(0, b6, b9, ch, 0, cg)) < b6) {
                        b7.dlShiftTo(ch, b8);
                        b9.subTo(b8, b9);
                        while (b9[ci] < --b6) {
                            b9.subTo(b8, b9)
                        }
                    }
                }
                if (ca != null) {
                    b9.drShiftTo(cg, ca);
                    if (z != cc) {
                        bo.ZERO.subTo(ca, ca)
                    }
                }
                b9.t = cg;
                b9.clamp();
                if (cj > 0) {
                    b9.rShiftTo(cj, b9)
                }
                if (z < 0) {
                    bo.ZERO.subTo(b9, b9)
                }
            }
            function bq(t) {
                var z = bv();
                this.abs().divRemTo(t, null, z);
                if (this.s < 0 && z.compareTo(bo.ZERO) > 0) {
                    t.subTo(z, z)
                }
                return z
            }
            function a2(t) {
                this.m = t
            }
            function aR(t) {
                if (t.s < 0 || t.compareTo(this.m) >= 0) {
                    return t.mod(this.m)
                } else {
                    return t
                }
            }
            function k(t) {
                return t
            }
            function ae(t) {
                t.divRemTo(this.m, null, t)
            }
            function C(t, L, z) {
                t.multiplyTo(L, z);
                this.reduce(z)
            }
            function aO(t, z) {
                t.squareTo(z);
                this.reduce(z)
            }
            a2.prototype.convert = aR;
            a2.prototype.revert = k;
            a2.prototype.reduce = ae;
            a2.prototype.mulTo = C;
            a2.prototype.sqrTo = aO;
            function ak() {
                if (this.t < 1) {
                    return 0
                }
                var t = this[0];
                if ((t & 1) == 0) {
                    return 0
                }
                var z = t & 3;
                z = (z * (2 - (t & 15) * z)) & 15;
                z = (z * (2 - (t & 255) * z)) & 255;
                z = (z * (2 - (((t & 65535) * z) & 65535))) & 65535;
                z = (z * (2 - t * z % this.DV)) % this.DV;
                return (z > 0) ? this.DV - z: -z
            }
            function U(t) {
                this.m = t;
                this.mp = t.invDigit();
                this.mpl = this.mp & 32767;
                this.mph = this.mp >> 15;
                this.um = (1 << (t.DB - 15)) - 1;
                this.mt2 = 2 * t.t
            }
            function bH(t) {
                var z = bv();
                t.abs().dlShiftTo(this.m.t, z);
                z.divRemTo(this.m, null, z);
                if (t.s < 0 && z.compareTo(bo.ZERO) > 0) {
                    this.m.subTo(z, z)
                }
                return z
            }
            function bu(t) {
                var z = bv();
                t.copyTo(z);
                this.reduce(z);
                return z
            }
            function b5(t) {
                while (t.t <= this.mt2) {
                    t[t.t++] = 0
                }
                for (var L = 0; L < this.m.t; ++L) {
                    var z = t[L] & 32767;
                    var b6 = (z * this.mpl + (((z * this.mph + (t[L] >> 15) * this.mpl) & this.um) << 15)) & t.DM;
                    z = L + this.m.t;
                    t[z] += this.m.am(0, b6, t, L, 0, this.m.t);
                    while (t[z] >= t.DV) {
                        t[z] -= t.DV;
                        t[++z]++
                    }
                }
                t.clamp();
                t.drShiftTo(this.m.t, t);
                if (t.compareTo(this.m) >= 0) {
                    t.subTo(this.m, t)
                }
            }
            function al(t, z) {
                t.squareTo(z);
                this.reduce(z)
            }
            function bI(t, L, z) {
                t.multiplyTo(L, z);
                this.reduce(z)
            }
            U.prototype.convert = bH;
            U.prototype.revert = bu;
            U.prototype.reduce = b5;
            U.prototype.mulTo = bI;
            U.prototype.sqrTo = al;
            function am() {
                return ((this.t > 0) ? (this[0] & 1) : this.s) == 0
            }
            function aw(ca, cb) {
                if (ca > 4294967295 || ca < 1) {
                    return bo.ONE
                }
                var b9 = bv(),
                L = bv(),
                b8 = cb.convert(this),
                b7 = D(ca) - 1;
                b8.copyTo(b9);
                while (--b7 >= 0) {
                    cb.sqrTo(b9, L);
                    if ((ca & (1 << b7)) > 0) {
                        cb.mulTo(L, b8, b9)
                    } else {
                        var b6 = b9;
                        b9 = L;
                        L = b6
                    }
                }
                return cb.revert(b9)
            }
            function aP(L, t) {
                var b6;
                if (L < 256 || t.isEven()) {
                    b6 = new a2(t)
                } else {
                    b6 = new U(t)
                }
                return this.exp(L, b6)
            }
            bo.prototype.copyTo = l;
            bo.prototype.fromInt = p;
            bo.prototype.fromString = bS;
            bo.prototype.clamp = bJ;
            bo.prototype.dlShiftTo = bE;
            bo.prototype.drShiftTo = bb;
            bo.prototype.lShiftTo = F;
            bo.prototype.rShiftTo = b3;
            bo.prototype.subTo = bB;
            bo.prototype.multiplyTo = bT;
            bo.prototype.squareTo = aD;
            bo.prototype.divRemTo = bi;
            bo.prototype.invDigit = ak;
            bo.prototype.isEven = am;
            bo.prototype.exp = aw;
            bo.prototype.toString = G;
            bo.prototype.negate = bM;
            bo.prototype.abs = bK;
            bo.prototype.compareTo = bX;
            bo.prototype.bitLength = bC;
            bo.prototype.mod = bq;
            bo.prototype.modPowInt = aP;
            bo.ZERO = br(0);
            bo.ONE = br(1);
            function n() {
                var t = bv();
                this.copyTo(t);
                return t
            }
            function j() {
                if (this.s < 0) {
                    if (this.t == 1) {
                        return this[0] - this.DV
                    } else {
                        if (this.t == 0) {
                            return - 1
                        }
                    }
                } else {
                    if (this.t == 1) {
                        return this[0]
                    } else {
                        if (this.t == 0) {
                            return 0
                        }
                    }
                }
                return ((this[1] & ((1 << (32 - this.DB)) - 1)) << this.DB) | this[0]
            }
            function bP() {
                return (this.t == 0) ? this.s: (this[0] << 24) >> 24
            }
            function ap() {
                return (this.t == 0) ? this.s: (this[0] << 16) >> 16
            }
            function a3(t) {
                return Math.floor(Math.LN2 * this.DB / Math.log(t))
            }
            function a8() {
                if (this.s < 0) {
                    return - 1
                } else {
                    if (this.t <= 0 || (this.t == 1 && this[0] <= 0)) {
                        return 0
                    } else {
                        return 1
                    }
                }
            }
            function S(t) {
                if (t == null) {
                    t = 10
                }
                if (this.signum() == 0 || t < 2 || t > 36) {
                    return "0"
                }
                var b6 = this.chunkSize(t);
                var L = Math.pow(t, b6);
                var b9 = br(L),
                ca = bv(),
                b8 = bv(),
                b7 = "";
                this.divRemTo(b9, ca, b8);
                while (ca.signum() > 0) {
                    b7 = (L + b8.intValue()).toString(t).substr(1) + b7;
                    ca.divRemTo(b9, ca, b8)
                }
                return b8.intValue().toString(t) + b7
            }
            function aE(cb, b8) {
                this.fromInt(0);
                if (b8 == null) {
                    b8 = 10
                }
                var b6 = this.chunkSize(b8);
                var b7 = Math.pow(b8, b6),
                L = false,
                t = 0,
                ca = 0;
                for (var z = 0; z < cb.length; ++z) {
                    var b9 = a6(cb, z);
                    if (b9 < 0) {
                        if (cb.charAt(z) == "-" && this.signum() == 0) {
                            L = true
                        }
                        continue
                    }
                    ca = b8 * ca + b9;
                    if (++t >= b6) {
                        this.dMultiply(b7);
                        this.dAddOffset(ca, 0);
                        t = 0;
                        ca = 0
                    }
                }
                if (t > 0) {
                    this.dMultiply(Math.pow(b8, t));
                    this.dAddOffset(ca, 0)
                }
                if (L) {
                    bo.ZERO.subTo(this, this)
                }
            }
            function aY(b6, L, b8) {
                if ("number" == typeof L) {
                    if (b6 < 2) {
                        this.fromInt(1)
                    } else {
                        this.fromNumber(b6, b8);
                        if (!this.testBit(b6 - 1)) {
                            this.bitwiseTo(bo.ONE.shiftLeft(b6 - 1), au, this)
                        }
                        if (this.isEven()) {
                            this.dAddOffset(1, 0)
                        }
                        while (!this.isProbablePrime(L)) {
                            this.dAddOffset(2, 0);
                            if (this.bitLength() > b6) {
                                this.subTo(bo.ONE.shiftLeft(b6 - 1), this)
                            }
                        }
                    }
                } else {
                    var z = new Array(),
                    b7 = b6 & 7;
                    z.length = (b6 >> 3) + 1;
                    L.nextBytes(z);
                    if (b7 > 0) {
                        z[0] &= ((1 << b7) - 1)
                    } else {
                        z[0] = 0
                    }
                    this.fromString(z, 256)
                }
            }
            function aT() {
                var z = this.t,
                L = new Array();
                L[0] = this.s;
                var b6 = this.DB - (z * this.DB) % 8,
                b7,
                t = 0;
                if (z-->0) {
                    if (b6 < this.DB && (b7 = this[z] >> b6) != (this.s & this.DM) >> b6) {
                        L[t++] = b7 | (this.s << (this.DB - b6))
                    }
                    while (z >= 0) {
                        if (b6 < 8) {
                            b7 = (this[z] & ((1 << b6) - 1)) << (8 - b6);
                            b7 |= this[--z] >> (b6 += this.DB - 8)
                        } else {
                            b7 = (this[z] >> (b6 -= 8)) & 255;
                            if (b6 <= 0) {
                                b6 += this.DB; --z
                            }
                        }
                        if ((b7 & 128) != 0) {
                            b7 |= -256
                        }
                        if (t == 0 && (this.s & 128) != (b7 & 128)) {++t
                        }
                        if (t > 0 || b7 != this.s) {
                            L[t++] = b7
                        }
                    }
                }
                return L
            }
            function bQ(t) {
                return (this.compareTo(t) == 0)
            }
            function af(t) {
                return (this.compareTo(t) < 0) ? this: t
            }
            function bD(t) {
                return (this.compareTo(t) > 0) ? this: t
            }
            function aS(z, b8, b6) {
                var L,
                b7,
                t = Math.min(z.t, this.t);
                for (L = 0; L < t; ++L) {
                    b6[L] = b8(this[L], z[L])
                }
                if (z.t < this.t) {
                    b7 = z.s & this.DM;
                    for (L = t; L < this.t; ++L) {
                        b6[L] = b8(this[L], b7)
                    }
                    b6.t = this.t
                } else {
                    b7 = this.s & this.DM;
                    for (L = t; L < z.t; ++L) {
                        b6[L] = b8(b7, z[L])
                    }
                    b6.t = z.t
                }
                b6.s = b8(this.s, z.s);
                b6.clamp()
            }
            function B(t, z) {
                return t & z
            }
            function bY(t) {
                var z = bv();
                this.bitwiseTo(t, B, z);
                return z
            }
            function au(t, z) {
                return t | z
            }
            function a1(t) {
                var z = bv();
                this.bitwiseTo(t, au, z);
                return z
            }
            function aj(t, z) {
                return t ^ z
            }
            function M(t) {
                var z = bv();
                this.bitwiseTo(t, aj, z);
                return z
            }
            function s(t, z) {
                return t & ~z
            }
            function aM(t) {
                var z = bv();
                this.bitwiseTo(t, s, z);
                return z
            }
            function ac() {
                var z = bv();
                for (var t = 0; t < this.t; ++t) {
                    z[t] = this.DM & ~this[t]
                }
                z.t = this.t;
                z.s = ~this.s;
                return z
            }
            function aW(z) {
                var t = bv();
                if (z < 0) {
                    this.rShiftTo( - z, t)
                } else {
                    this.lShiftTo(z, t)
                }
                return t
            }
            function aa(z) {
                var t = bv();
                if (z < 0) {
                    this.lShiftTo( - z, t)
                } else {
                    this.rShiftTo(z, t)
                }
                return t
            }
            function bl(t) {
                if (t == 0) {
                    return - 1
                }
                var z = 0;
                if ((t & 65535) == 0) {
                    t >>= 16;
                    z += 16
                }
                if ((t & 255) == 0) {
                    t >>= 8;
                    z += 8
                }
                if ((t & 15) == 0) {
                    t >>= 4;
                    z += 4
                }
                if ((t & 3) == 0) {
                    t >>= 2;
                    z += 2
                }
                if ((t & 1) == 0) {++z
                }
                return z
            }
            function aA() {
                for (var t = 0; t < this.t; ++t) {
                    if (this[t] != 0) {
                        return t * this.DB + bl(this[t])
                    }
                }
                if (this.s < 0) {
                    return this.t * this.DB
                }
                return - 1
            }
            function bs(t) {
                var z = 0;
                while (t != 0) {
                    t &= t - 1; ++z
                }
                return z
            }
            function ay() {
                var L = 0,
                t = this.s & this.DM;
                for (var z = 0; z < this.t; ++z) {
                    L += bs(this[z] ^ t)
                }
                return L
            }
            function aU(z) {
                var t = Math.floor(z / this.DB);
                if (t >= this.t) {
                    return (this.s != 0)
                }
                return ((this[t] & (1 << (z % this.DB))) != 0)
            }
            function ad(L, z) {
                var t = bo.ONE.shiftLeft(L);
                this.bitwiseTo(t, z, t);
                return t
            }
            function ba(t) {
                return this.changeBit(t, au)
            }
            function aq(t) {
                return this.changeBit(t, s)
            }
            function aX(t) {
                return this.changeBit(t, aj)
            }
            function ab(z, b6) {
                var L = 0,
                b7 = 0,
                t = Math.min(z.t, this.t);
                while (L < t) {
                    b7 += this[L] + z[L];
                    b6[L++] = b7 & this.DM;
                    b7 >>= this.DB
                }
                if (z.t < this.t) {
                    b7 += z.s;
                    while (L < this.t) {
                        b7 += this[L];
                        b6[L++] = b7 & this.DM;
                        b7 >>= this.DB
                    }
                    b7 += this.s
                } else {
                    b7 += this.s;
                    while (L < z.t) {
                        b7 += z[L];
                        b6[L++] = b7 & this.DM;
                        b7 >>= this.DB
                    }
                    b7 += z.s
                }
                b6.s = (b7 < 0) ? -1: 0;
                if (b7 > 0) {
                    b6[L++] = b7
                } else {
                    if (b7 < -1) {
                        b6[L++] = this.DV + b7
                    }
                }
                b6.t = L;
                b6.clamp()
            }
            function bp(t) {
                var z = bv();
                this.addTo(t, z);
                return z
            }
            function aJ(t) {
                var z = bv();
                this.subTo(t, z);
                return z
            }
            function bR(t) {
                var z = bv();
                this.multiplyTo(t, z);
                return z
            }
            function b4() {
                var t = bv();
                this.squareTo(t);
                return t
            }
            function bm(t) {
                var z = bv();
                this.divRemTo(t, z, null);
                return z
            }
            function bZ(t) {
                var z = bv();
                this.divRemTo(t, null, z);
                return z
            }
            function bt(t) {
                var L = bv(),
                z = bv();
                this.divRemTo(t, L, z);
                return new Array(L, z)
            }
            function m(t) {
                this[this.t] = this.am(0, t - 1, this, 0, 0, this.t); ++this.t;
                this.clamp()
            }
            function a0(z, t) {
                if (z == 0) {
                    return
                }
                while (this.t <= t) {
                    this[this.t++] = 0
                }
                this[t] += z;
                while (this[t] >= this.DV) {
                    this[t] -= this.DV;
                    if (++t >= this.t) {
                        this[this.t++] = 0
                    }++this[t]
                }
            }
            function ai() {}
            function bF(t) {
                return t
            }
            function bU(t, L, z) {
                t.multiplyTo(L, z)
            }
            function ar(t, z) {
                t.squareTo(z)
            }
            ai.prototype.convert = bF;
            ai.prototype.revert = bF;
            ai.prototype.mulTo = bU;
            ai.prototype.sqrTo = ar;
            function Z(t) {
                return this.exp(t, new ai())
            }
            function aZ(t, b7, b6) {
                var L = Math.min(this.t + t.t, b7);
                b6.s = 0;
                b6.t = L;
                while (L > 0) {
                    b6[--L] = 0
                }
                var z;
                for (z = b6.t - this.t; L < z; ++L) {
                    b6[L + this.t] = this.am(0, t[L], b6, L, 0, this.t)
                }
                for (z = Math.min(t.t, b7); L < z; ++L) {
                    this.am(0, t[L], b6, L, 0, b7 - L)
                }
                b6.clamp()
            }
            function a9(t, b6, L) {--b6;
                var z = L.t = this.t + t.t - b6;
                L.s = 0;
                while (--z >= 0) {
                    L[z] = 0
                }
                for (z = Math.max(b6 - this.t, 0); z < t.t; ++z) {
                    L[this.t + z - b6] = this.am(b6 - z, t[z], L, 0, 0, this.t + z - b6)
                }
                L.clamp();
                L.drShiftTo(1, L)
            }
            function b1(t) {
                this.r2 = bv();
                this.q3 = bv();
                bo.ONE.dlShiftTo(2 * t.t, this.r2);
                this.mu = this.r2.divide(t);
                this.m = t
            }
            function R(t) {
                if (t.s < 0 || t.t > 2 * this.m.t) {
                    return t.mod(this.m)
                } else {
                    if (t.compareTo(this.m) < 0) {
                        return t
                    } else {
                        var z = bv();
                        t.copyTo(z);
                        this.reduce(z);
                        return z
                    }
                }
            }
            function bW(t) {
                return t
            }
            function O(t) {
                t.drShiftTo(this.m.t - 1, this.r2);
                if (t.t > this.m.t + 1) {
                    t.t = this.m.t + 1;
                    t.clamp()
                }
                this.mu.multiplyUpperTo(this.r2, this.m.t + 1, this.q3);
                this.m.multiplyLowerTo(this.q3, this.m.t + 1, this.r2);
                while (t.compareTo(this.r2) < 0) {
                    t.dAddOffset(1, this.m.t + 1)
                }
                t.subTo(this.r2, t);
                while (t.compareTo(this.m) >= 0) {
                    t.subTo(this.m, t)
                }
            }
            function aV(t, z) {
                t.squareTo(z);
                this.reduce(z)
            }
            function I(t, L, z) {
                t.multiplyTo(L, z);
                this.reduce(z)
            }
            b1.prototype.convert = R;
            b1.prototype.revert = bW;
            b1.prototype.reduce = O;
            b1.prototype.mulTo = I;
            b1.prototype.sqrTo = aV;
            function W(cf, b9) {
                var cc = cf.bitLength(),
                ca,
                b6 = br(1),
                ci;
                if (cc <= 0) {
                    return b6
                } else {
                    if (cc < 18) {
                        ca = 1
                    } else {
                        if (cc < 48) {
                            ca = 3
                        } else {
                            if (cc < 144) {
                                ca = 4
                            } else {
                                if (cc < 768) {
                                    ca = 5
                                } else {
                                    ca = 6
                                }
                            }
                        }
                    }
                }
                if (cc < 8) {
                    ci = new a2(b9)
                } else {
                    if (b9.isEven()) {
                        ci = new b1(b9)
                    } else {
                        ci = new U(b9)
                    }
                }
                var cd = new Array(),
                b8 = 3,
                cg = ca - 1,
                L = (1 << ca) - 1;
                cd[1] = ci.convert(this);
                if (ca > 1) {
                    var cl = bv();
                    ci.sqrTo(cd[1], cl);
                    while (b8 <= L) {
                        cd[b8] = bv();
                        ci.mulTo(cl, cd[b8 - 2], cd[b8]);
                        b8 += 2
                    }
                }
                var cb = cf.t - 1,
                cj,
                ch = true,
                b7 = bv(),
                ck;
                cc = D(cf[cb]) - 1;
                while (cb >= 0) {
                    if (cc >= cg) {
                        cj = (cf[cb] >> (cc - cg)) & L
                    } else {
                        cj = (cf[cb] & ((1 << (cc + 1)) - 1)) << (cg - cc);
                        if (cb > 0) {
                            cj |= cf[cb - 1] >> (this.DB + cc - cg)
                        }
                    }
                    b8 = ca;
                    while ((cj & 1) == 0) {
                        cj >>= 1; --b8
                    }
                    if ((cc -= b8) < 0) {
                        cc += this.DB; --cb
                    }
                    if (ch) {
                        cd[cj].copyTo(b6);
                        ch = false
                    } else {
                        while (b8 > 1) {
                            ci.sqrTo(b6, b7);
                            ci.sqrTo(b7, b6);
                            b8 -= 2
                        }
                        if (b8 > 0) {
                            ci.sqrTo(b6, b7)
                        } else {
                            ck = b6;
                            b6 = b7;
                            b7 = ck
                        }
                        ci.mulTo(b7, cd[cj], b6)
                    }
                    while (cb >= 0 && (cf[cb] & (1 << cc)) == 0) {
                        ci.sqrTo(b6, b7);
                        ck = b6;
                        b6 = b7;
                        b7 = ck;
                        if (--cc < 0) {
                            cc = this.DB - 1; --cb
                        }
                    }
                }
                return ci.revert(b6)
            }
            function aK(L) {
                var z = (this.s < 0) ? this.negate() : this.clone();
                var b9 = (L.s < 0) ? L.negate() : L.clone();
                if (z.compareTo(b9) < 0) {
                    var b7 = z;
                    z = b9;
                    b9 = b7
                }
                var b6 = z.getLowestSetBit(),
                b8 = b9.getLowestSetBit();
                if (b8 < 0) {
                    return z
                }
                if (b6 < b8) {
                    b8 = b6
                }
                if (b8 > 0) {
                    z.rShiftTo(b8, z);
                    b9.rShiftTo(b8, b9)
                }
                while (z.signum() > 0) {
                    if ((b6 = z.getLowestSetBit()) > 0) {
                        z.rShiftTo(b6, z)
                    }
                    if ((b6 = b9.getLowestSetBit()) > 0) {
                        b9.rShiftTo(b6, b9)
                    }
                    if (z.compareTo(b9) >= 0) {
                        z.subTo(b9, z);
                        z.rShiftTo(1, z)
                    } else {
                        b9.subTo(z, b9);
                        b9.rShiftTo(1, b9)
                    }
                }
                if (b8 > 0) {
                    b9.lShiftTo(b8, b9)
                }
                return b9
            }
            function at(b6) {
                if (b6 <= 0) {
                    return 0
                }
                var L = this.DV % b6,
                z = (this.s < 0) ? b6 - 1: 0;
                if (this.t > 0) {
                    if (L == 0) {
                        z = this[0] % b6
                    } else {
                        for (var t = this.t - 1; t >= 0; --t) {
                            z = (L * z + this[t]) % b6
                        }
                    }
                }
                return z
            }
            function b2(z) {
                var b8 = z.isEven();
                if ((this.isEven() && b8) || z.signum() == 0) {
                    return bo.ZERO
                }
                var b7 = z.clone(),
                b6 = this.clone();
                var L = br(1),
                t = br(0),
                ca = br(0),
                b9 = br(1);
                while (b7.signum() != 0) {
                    while (b7.isEven()) {
                        b7.rShiftTo(1, b7);
                        if (b8) {
                            if (!L.isEven() || !t.isEven()) {
                                L.addTo(this, L);
                                t.subTo(z, t)
                            }
                            L.rShiftTo(1, L)
                        } else {
                            if (!t.isEven()) {
                                t.subTo(z, t)
                            }
                        }
                        t.rShiftTo(1, t)
                    }
                    while (b6.isEven()) {
                        b6.rShiftTo(1, b6);
                        if (b8) {
                            if (!ca.isEven() || !b9.isEven()) {
                                ca.addTo(this, ca);
                                b9.subTo(z, b9)
                            }
                            ca.rShiftTo(1, ca)
                        } else {
                            if (!b9.isEven()) {
                                b9.subTo(z, b9)
                            }
                        }
                        b9.rShiftTo(1, b9)
                    }
                    if (b7.compareTo(b6) >= 0) {
                        b7.subTo(b6, b7);
                        if (b8) {
                            L.subTo(ca, L)
                        }
                        t.subTo(b9, t)
                    } else {
                        b6.subTo(b7, b6);
                        if (b8) {
                            ca.subTo(L, ca)
                        }
                        b9.subTo(t, b9)
                    }
                }
                if (b6.compareTo(bo.ONE) != 0) {
                    return bo.ZERO
                }
                if (b9.compareTo(z) >= 0) {
                    return b9.subtract(z)
                }
                if (b9.signum() < 0) {
                    b9.addTo(z, b9)
                } else {
                    return b9
                }
                if (b9.signum() < 0) {
                    return b9.add(z)
                } else {
                    return b9
                }
            }
            var aI = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997];
            var w = (1 << 26) / aI[aI.length - 1];
            function bV(b8) {
                var b7,
                L = this.abs();
                if (L.t == 1 && L[0] <= aI[aI.length - 1]) {
                    for (b7 = 0; b7 < aI.length; ++b7) {
                        if (L[0] == aI[b7]) {
                            return true
                        }
                    }
                    return false
                }
                if (L.isEven()) {
                    return false
                }
                b7 = 1;
                while (b7 < aI.length) {
                    var z = aI[b7],
                    b6 = b7 + 1;
                    while (b6 < aI.length && z < w) {
                        z *= aI[b6++]
                    }
                    z = L.modInt(z);
                    while (b7 < b6) {
                        if (z % aI[b7++] == 0) {
                            return false
                        }
                    }
                }
                return L.millerRabin(b8)
            }
            function aN(b8) {
                var b9 = this.subtract(bo.ONE);
                var L = b9.getLowestSetBit();
                if (L <= 0) {
                    return false
                }
                var ca = b9.shiftRight(L);
                b8 = (b8 + 1) >> 1;
                if (b8 > aI.length) {
                    b8 = aI.length
                }
                var z = bv();
                for (var b7 = 0; b7 < b8; ++b7) {
                    z.fromInt(aI[Math.floor(Math.random() * aI.length)]);
                    var cb = z.modPow(ca, this);
                    if (cb.compareTo(bo.ONE) != 0 && cb.compareTo(b9) != 0) {
                        var b6 = 1;
                        while (b6++<L && cb.compareTo(b9) != 0) {
                            cb = cb.modPowInt(2, this);
                            if (cb.compareTo(bo.ONE) == 0) {
                                return false
                            }
                        }
                        if (cb.compareTo(b9) != 0) {
                            return false
                        }
                    }
                }
                return true
            }
            bo.prototype.chunkSize = a3;
            bo.prototype.toRadix = S;
            bo.prototype.fromRadix = aE;
            bo.prototype.fromNumber = aY;
            bo.prototype.bitwiseTo = aS;
            bo.prototype.changeBit = ad;
            bo.prototype.addTo = ab;
            bo.prototype.dMultiply = m;
            bo.prototype.dAddOffset = a0;
            bo.prototype.multiplyLowerTo = aZ;
            bo.prototype.multiplyUpperTo = a9;
            bo.prototype.modInt = at;
            bo.prototype.millerRabin = aN;
            bo.prototype.clone = n;
            bo.prototype.intValue = j;
            bo.prototype.byteValue = bP;
            bo.prototype.shortValue = ap;
            bo.prototype.signum = a8;
            bo.prototype.toByteArray = aT;
            bo.prototype.equals = bQ;
            bo.prototype.min = af;
            bo.prototype.max = bD;
            bo.prototype.and = bY;
            bo.prototype.or = a1;
            bo.prototype.xor = M;
            bo.prototype.andNot = aM;
            bo.prototype.not = ac;
            bo.prototype.shiftLeft = aW;
            bo.prototype.shiftRight = aa;
            bo.prototype.getLowestSetBit = aA;
            bo.prototype.bitCount = ay;
            bo.prototype.testBit = aU;
            bo.prototype.setBit = ba;
            bo.prototype.clearBit = aq;
            bo.prototype.flipBit = aX;
            bo.prototype.add = bp;
            bo.prototype.subtract = aJ;
            bo.prototype.multiply = bR;
            bo.prototype.divide = bm;
            bo.prototype.remainder = bZ;
            bo.prototype.divideAndRemainder = bt;
            bo.prototype.modPow = W;
            bo.prototype.modInverse = b2;
            bo.prototype.pow = Z;
            bo.prototype.gcd = aK;
            bo.prototype.isProbablePrime = bV;
            bo.prototype.square = b4;
            function by() {
                this.i = 0;
                this.j = 0;
                this.S = new Array()
            }
            function ao(b7) {
                var b6,
                z,
                L;
                for (b6 = 0; b6 < 256; ++b6) {
                    this.S[b6] = b6
                }
                z = 0;
                for (b6 = 0; b6 < 256; ++b6) {
                    z = (z + this.S[b6] + b7[b6 % b7.length]) & 255;
                    L = this.S[b6];
                    this.S[b6] = this.S[z];
                    this.S[z] = L
                }
                this.i = 0;
                this.j = 0
            }
            function bn() {
                var z;
                this.i = (this.i + 1) & 255;
                this.j = (this.j + this.S[this.i]) & 255;
                z = this.S[this.i];
                this.S[this.i] = this.S[this.j];
                this.S[this.j] = z;
                return this.S[(z + this.S[this.i]) & 255]
            }
            by.prototype.init = ao;
            by.prototype.next = bn;
            function Y() {
                return new by()
            }
            var J = 256;
            var u;
            var x;
            var N;
            function r(t) {
                x[N++] ^= t & 255;
                x[N++] ^= (t >> 8) & 255;
                x[N++] ^= (t >> 16) & 255;
                x[N++] ^= (t >> 24) & 255;
                if (N >= J) {
                    N -= J
                }
            }
            function bL() {
                r(new Date().getTime())
            }
            if (x == null) {
                x = new Array();
                N = 0;
                var bj;
                if (navigator.appName == "Netscape" && navigator.appVersion < "5" && window.crypto) {
                    var bh = window.crypto.random(32);
                    for (bj = 0; bj < bh.length; ++bj) {
                        x[N++] = bh.charCodeAt(bj) & 255
                    }
                }
                while (N < J) {
                    bj = Math.floor(65536 * Math.random());
                    x[N++] = bj >>> 8;
                    x[N++] = bj & 255
                }
                N = 0;
                bL()
            }
            function bk() {
                if (u == null) {
                    bL();
                    u = Y();
                    u.init(x);
                    for (N = 0; N < x.length; ++N) {
                        x[N] = 0
                    }
                    N = 0
                }
                return u.next()
            }
            function a7(z) {
                var t;
                for (t = 0; t < z.length; ++t) {
                    z[t] = bk()
                }
            }
            function Q() {}
            Q.prototype.nextBytes = a7;
            function H(z, t) {
                return new bo(z, t)
            }
            function y(L, b6) {
                var t = "";
                var z = 0;
                while (z + b6 < L.length) {
                    t += L.substring(z, z + b6) + "\n";
                    z += b6
                }
                return t + L.substring(z, L.length)
            }
            function bA(t) {
                if (t < 16) {
                    return "0" + t.toString(16)
                } else {
                    return t.toString(16)
                }
            }
            function bN(b6, b9) {
                if (b9 < b6.length + 11) {
                    console.error("Message too long for RSA");
                    return null
                }
                var b8 = new Array();
                var L = b6.length - 1;
                while (L >= 0 && b9 > 0) {
                    var b7 = b6.charCodeAt(L--);
                    if (b7 < 128) {
                        b8[--b9] = b7
                    } else {
                        if ((b7 > 127) && (b7 < 2048)) {
                            b8[--b9] = (b7 & 63) | 128;
                            b8[--b9] = (b7 >> 6) | 192
                        } else {
                            b8[--b9] = (b7 & 63) | 128;
                            b8[--b9] = ((b7 >> 6) & 63) | 128;
                            b8[--b9] = (b7 >> 12) | 224
                        }
                    }
                }
                b8[--b9] = 0;
                var z = new Q();
                var t = new Array();
                while (b9 > 2) {
                    t[0] = 0;
                    while (t[0] == 0) {
                        z.nextBytes(t)
                    }
                    b8[--b9] = t[0]
                }
                b8[--b9] = 2;
                b8[--b9] = 0;
                return new bo(b8)
            }
            function K() {
                this.n = null;
                this.e = 0;
                this.d = null;
                this.p = null;
                this.q = null;
                this.dmp1 = null;
                this.dmq1 = null;
                this.coeff = null
            }
            function ax(z, t) {
                if (z != null && t != null && z.length > 0 && t.length > 0) {
                    this.n = H(z, 16);
                    this.e = parseInt(t, 16)
                } else {
                    console.error("Invalid RSA public key")
                }
            }
            function bz(t) {
                return t.modPowInt(this.e, this.n)
            }
            function av(L) {
                var t = bN(L, (this.n.bitLength() + 7) >> 3);
                if (t == null) {
                    return null
                }
                var b6 = this.doPublic(t);
                if (b6 == null) {
                    return null
                }
                var z = b6.toString(16);
                if ((z.length & 1) == 0) {
                    return z
                } else {
                    return "0" + z
                }
            }
            K.prototype.doPublic = bz;
            K.prototype.setPublic = ax;
            K.prototype.encrypt = av;
            function bx(b6, b8) {
                var t = b6.toByteArray();
                var L = 0;
                while (L < t.length && t[L] == 0) {++L
                }
                if (t.length - L != b8 - 1 || t[L] != 2) {
                    return null
                }++L;
                while (t[L] != 0) {
                    if (++L >= t.length) {
                        return null
                    }
                }
                var z = "";
                while (++L < t.length) {
                    var b7 = t[L] & 255;
                    if (b7 < 128) {
                        z += String.fromCharCode(b7)
                    } else {
                        if ((b7 > 191) && (b7 < 224)) {
                            z += String.fromCharCode(((b7 & 31) << 6) | (t[L + 1] & 63)); ++L
                        } else {
                            z += String.fromCharCode(((b7 & 15) << 12) | ((t[L + 1] & 63) << 6) | (t[L + 2] & 63));
                            L += 2
                        }
                    }
                }
                return z
            }
            function aL(L, t, z) {
                if (L != null && t != null && L.length > 0 && t.length > 0) {
                    this.n = H(L, 16);
                    this.e = parseInt(t, 16);
                    this.d = H(z, 16)
                } else {
                    console.error("Invalid RSA private key")
                }
            }
            function X(b9, b6, b7, L, z, t, ca, b8) {
                if (b9 != null && b6 != null && b9.length > 0 && b6.length > 0) {
                    this.n = H(b9, 16);
                    this.e = parseInt(b6, 16);
                    this.d = H(b7, 16);
                    this.p = H(L, 16);
                    this.q = H(z, 16);
                    this.dmp1 = H(t, 16);
                    this.dmq1 = H(ca, 16);
                    this.coeff = H(b8, 16)
                } else {
                    console.error("Invalid RSA private key")
                }
            }
            function aG(L, cc) {
                var z = new Q();
                var b9 = L >> 1;
                this.e = parseInt(cc, 16);
                var b6 = new bo(cc, 16);
                for (;;) {
                    for (;;) {
                        this.p = new bo(L - b9, 1, z);
                        if (this.p.subtract(bo.ONE).gcd(b6).compareTo(bo.ONE) == 0 && this.p.isProbablePrime(10)) {
                            break
                        }
                    }
                    for (;;) {
                        this.q = new bo(b9, 1, z);
                        if (this.q.subtract(bo.ONE).gcd(b6).compareTo(bo.ONE) == 0 && this.q.isProbablePrime(10)) {
                            break
                        }
                    }
                    if (this.p.compareTo(this.q) <= 0) {
                        var cb = this.p;
                        this.p = this.q;
                        this.q = cb
                    }
                    var ca = this.p.subtract(bo.ONE);
                    var b7 = this.q.subtract(bo.ONE);
                    var b8 = ca.multiply(b7);
                    if (b8.gcd(b6).compareTo(bo.ONE) == 0) {
                        this.n = this.p.multiply(this.q);
                        this.d = b6.modInverse(b8);
                        this.dmp1 = this.d.mod(ca);
                        this.dmq1 = this.d.mod(b7);
                        this.coeff = this.q.modInverse(this.p);
                        break
                    }
                }
            }
            function aH(t) {
                if (this.p == null || this.q == null) {
                    return t.modPow(this.d, this.n)
                }
                var L = t.mod(this.p).modPow(this.dmp1, this.p);
                var z = t.mod(this.q).modPow(this.dmq1, this.q);
                while (L.compareTo(z) < 0) {
                    L = L.add(this.p)
                }
                return L.subtract(z).multiply(this.coeff).mod(this.p).multiply(this.q).add(z)
            }
            function E(z) {
                var L = H(z, 16);
                var t = this.doPrivate(L);
                if (t == null) {
                    return null
                }
                return bx(t, (this.n.bitLength() + 7) >> 3)
            }
            K.prototype.doPrivate = aH;
            K.prototype.setPrivate = aL;
            K.prototype.setPrivateEx = X;
            K.prototype.generate = aG;
            K.prototype.decrypt = E; (function() {
                var z = function(cd, cb, cc) {
                    var b9 = new Q();
                    var b6 = cd >> 1;
                    this.e = parseInt(cb, 16);
                    var b8 = new bo(cb, 16);
                    var ca = this;
                    var b7 = function() {
                        var cg = function() {
                            if (ca.p.compareTo(ca.q) <= 0) {
                                var cj = ca.p;
                                ca.p = ca.q;
                                ca.q = cj
                            }
                            var cl = ca.p.subtract(bo.ONE);
                            var ci = ca.q.subtract(bo.ONE);
                            var ck = cl.multiply(ci);
                            if (ck.gcd(b8).compareTo(bo.ONE) == 0) {
                                ca.n = ca.p.multiply(ca.q);
                                ca.d = b8.modInverse(ck);
                                ca.dmp1 = ca.d.mod(cl);
                                ca.dmq1 = ca.d.mod(ci);
                                ca.coeff = ca.q.modInverse(ca.p);
                                setTimeout(function() {
                                    cc()
                                },
                                0)
                            } else {
                                setTimeout(b7, 0)
                            }
                        };
                        var ch = function() {
                            ca.q = bv();
                            ca.q.fromNumberAsync(b6, 1, b9, 
                            function() {
                                ca.q.subtract(bo.ONE).gcda(b8, 
                                function(ci) {
                                    if (ci.compareTo(bo.ONE) == 0 && ca.q.isProbablePrime(10)) {
                                        setTimeout(cg, 0)
                                    } else {
                                        setTimeout(ch, 0)
                                    }
                                })
                            })
                        };
                        var cf = function() {
                            ca.p = bv();
                            ca.p.fromNumberAsync(cd - b6, 1, b9, 
                            function() {
                                ca.p.subtract(bo.ONE).gcda(b8, 
                                function(ci) {
                                    if (ci.compareTo(bo.ONE) == 0 && ca.p.isProbablePrime(10)) {
                                        setTimeout(ch, 0)
                                    } else {
                                        setTimeout(cf, 0)
                                    }
                                })
                            })
                        };
                        setTimeout(cf, 0)
                    };
                    setTimeout(b7, 0)
                };
                K.prototype.generateAsync = z;
                var t = function(b7, cd) {
                    var b6 = (this.s < 0) ? this.negate() : this.clone();
                    var cc = (b7.s < 0) ? b7.negate() : b7.clone();
                    if (b6.compareTo(cc) < 0) {
                        var b9 = b6;
                        b6 = cc;
                        cc = b9
                    }
                    var b8 = b6.getLowestSetBit(),
                    ca = cc.getLowestSetBit();
                    if (ca < 0) {
                        cd(b6);
                        return
                    }
                    if (b8 < ca) {
                        ca = b8
                    }
                    if (ca > 0) {
                        b6.rShiftTo(ca, b6);
                        cc.rShiftTo(ca, cc)
                    }
                    var cb = function() {
                        if ((b8 = b6.getLowestSetBit()) > 0) {
                            b6.rShiftTo(b8, b6)
                        }
                        if ((b8 = cc.getLowestSetBit()) > 0) {
                            cc.rShiftTo(b8, cc)
                        }
                        if (b6.compareTo(cc) >= 0) {
                            b6.subTo(cc, b6);
                            b6.rShiftTo(1, b6)
                        } else {
                            cc.subTo(b6, cc);
                            cc.rShiftTo(1, cc)
                        }
                        if (! (b6.signum() > 0)) {
                            if (ca > 0) {
                                cc.lShiftTo(ca, cc)
                            }
                            setTimeout(function() {
                                cd(cc)
                            },
                            0)
                        } else {
                            setTimeout(cb, 0)
                        }
                    };
                    setTimeout(cb, 10)
                };
                bo.prototype.gcda = t;
                var L = function(ca, b7, cd, cc) {
                    if ("number" == typeof b7) {
                        if (ca < 2) {
                            this.fromInt(1)
                        } else {
                            this.fromNumber(ca, cd);
                            if (!this.testBit(ca - 1)) {
                                this.bitwiseTo(bo.ONE.shiftLeft(ca - 1), au, this)
                            }
                            if (this.isEven()) {
                                this.dAddOffset(1, 0)
                            }
                            var b9 = this;
                            var b8 = function() {
                                b9.dAddOffset(2, 0);
                                if (b9.bitLength() > ca) {
                                    b9.subTo(bo.ONE.shiftLeft(ca - 1), b9)
                                }
                                if (b9.isProbablePrime(b7)) {
                                    setTimeout(function() {
                                        cc()
                                    },
                                    0)
                                } else {
                                    setTimeout(b8, 0)
                                }
                            };
                            setTimeout(b8, 0)
                        }
                    } else {
                        var b6 = new Array(),
                        cb = ca & 7;
                        b6.length = (ca >> 3) + 1;
                        b7.nextBytes(b6);
                        if (cb > 0) {
                            b6[0] &= ((1 << cb) - 1)
                        } else {
                            b6[0] = 0
                        }
                        this.fromString(b6, 256)
                    }
                };
                bo.prototype.fromNumberAsync = L
            })();
            var bd = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
            var T = "=";
            function an(L) {
                var z;
                var b6;
                var t = "";
                for (z = 0; z + 3 <= L.length; z += 3) {
                    b6 = parseInt(L.substring(z, z + 3), 16);
                    t += bd.charAt(b6 >> 6) + bd.charAt(b6 & 63)
                }
                if (z + 1 == L.length) {
                    b6 = parseInt(L.substring(z, z + 1), 16);
                    t += bd.charAt(b6 << 2)
                } else {
                    if (z + 2 == L.length) {
                        b6 = parseInt(L.substring(z, z + 2), 16);
                        t += bd.charAt(b6 >> 2) + bd.charAt((b6 & 3) << 4)
                    }
                }
                while ((t.length & 3) > 0) {
                    t += T
                }
                return t
            }
            function a5(b7) {
                var L = "";
                var b6;
                var t = 0;
                var z;
                for (b6 = 0; b6 < b7.length; ++b6) {
                    if (b7.charAt(b6) == T) {
                        break
                    }
                    v = bd.indexOf(b7.charAt(b6));
                    if (v < 0) {
                        continue
                    }
                    if (t == 0) {
                        L += ah(v >> 2);
                        z = v & 3;
                        t = 1
                    } else {
                        if (t == 1) {
                            L += ah((z << 2) | (v >> 4));
                            z = v & 15;
                            t = 2
                        } else {
                            if (t == 2) {
                                L += ah(z);
                                L += ah(v >> 2);
                                z = v & 3;
                                t = 3
                            } else {
                                L += ah((z << 2) | (v >> 4));
                                L += ah(v & 15);
                                t = 0
                            }
                        }
                    }
                }
                if (t == 1) {
                    L += ah(z << 2)
                }
                return L
            }
            function V(b6) {
                var L = a5(b6);
                var z;
                var t = new Array();
                for (z = 0; 2 * z < L.length; ++z) {
                    t[z] = parseInt(L.substring(2 * z, 2 * z + 2), 16)
                }
                return t
            }
            /*! asn1-1.0.2.js (c) 2013 Kenji Urushima | kjur.github.com/jsrsasign/license
     */
            var aC = aC || {};
            aC.env = aC.env || {};
            var bw = aC,
            aF = Object.prototype,
            aB = "[object Function]",
            ag = ["toString", "valueOf"];
            aC.env.parseUA = function(b6) {
                var b7 = function(cb) {
                    var cc = 0;
                    return parseFloat(cb.replace(/\./g, 
                    function() {
                        return (cc++==1) ? "": "."
                    }))
                },
                ca = navigator,
                b9 = {
                    ie: 0,
                    opera: 0,
                    gecko: 0,
                    webkit: 0,
                    chrome: 0,
                    mobile: null,
                    air: 0,
                    ipad: 0,
                    iphone: 0,
                    ipod: 0,
                    ios: null,
                    android: 0,
                    webos: 0,
                    caja: ca && ca.cajaVersion,
                    secure: false,
                    os: null
                },
                L = b6 || (navigator && navigator.userAgent),
                b8 = window && window.location,
                z = b8 && b8.href,
                t;
                b9.secure = z && (z.toLowerCase().indexOf("https") === 0);
                if (L) {
                    if ((/windows|win32/i).test(L)) {
                        b9.os = "windows"
                    } else {
                        if ((/macintosh/i).test(L)) {
                            b9.os = "macintosh"
                        } else {
                            if ((/rhino/i).test(L)) {
                                b9.os = "rhino"
                            }
                        }
                    }
                    if ((/KHTML/).test(L)) {
                        b9.webkit = 1
                    }
                    t = L.match(/AppleWebKit\/([^\s]*)/);
                    if (t && t[1]) {
                        b9.webkit = b7(t[1]);
                        if (/ Mobile\//.test(L)) {
                            b9.mobile = "Apple";
                            t = L.match(/OS ([^\s]*)/);
                            if (t && t[1]) {
                                t = b7(t[1].replace("_", "."))
                            }
                            b9.ios = t;
                            b9.ipad = b9.ipod = b9.iphone = 0;
                            t = L.match(/iPad|iPod|iPhone/);
                            if (t && t[0]) {
                                b9[t[0].toLowerCase()] = b9.ios
                            }
                        } else {
                            t = L.match(/NokiaN[^\/]*|Android \d\.\d|webOS\/\d\.\d/);
                            if (t) {
                                b9.mobile = t[0]
                            }
                            if (/webOS/.test(L)) {
                                b9.mobile = "WebOS";
                                t = L.match(/webOS\/([^\s]*);/);
                                if (t && t[1]) {
                                    b9.webos = b7(t[1])
                                }
                            }
                            if (/ Android/.test(L)) {
                                b9.mobile = "Android";
                                t = L.match(/Android ([^\s]*);/);
                                if (t && t[1]) {
                                    b9.android = b7(t[1])
                                }
                            }
                        }
                        t = L.match(/Chrome\/([^\s]*)/);
                        if (t && t[1]) {
                            b9.chrome = b7(t[1])
                        } else {
                            t = L.match(/AdobeAIR\/([^\s]*)/);
                            if (t) {
                                b9.air = t[0]
                            }
                        }
                    }
                    if (!b9.webkit) {
                        t = L.match(/Opera[\s\/]([^\s]*)/);
                        if (t && t[1]) {
                            b9.opera = b7(t[1]);
                            t = L.match(/Version\/([^\s]*)/);
                            if (t && t[1]) {
                                b9.opera = b7(t[1])
                            }
                            t = L.match(/Opera Mini[^;]*/);
                            if (t) {
                                b9.mobile = t[0]
                            }
                        } else {
                            t = L.match(/MSIE\s([^;]*)/);
                            if (t && t[1]) {
                                b9.ie = b7(t[1])
                            } else {
                                t = L.match(/Gecko\/([^\s]*)/);
                                if (t) {
                                    b9.gecko = 1;
                                    t = L.match(/rv:([^\s\)]*)/);
                                    if (t && t[1]) {
                                        b9.gecko = b7(t[1])
                                    }
                                }
                            }
                        }
                    }
                }
                return b9
            };
            aC.env.ua = aC.env.parseUA();
            aC.isFunction = function(t) {
                return (typeof t === "function") || aF.toString.apply(t) === aB
            };
            aC._IEEnumFix = (aC.env.ua.ie) ? 
            function(L, z) {
                var t,
                b7,
                b6;
                for (t = 0; t < ag.length; t = t + 1) {
                    b7 = ag[t];
                    b6 = z[b7];
                    if (bw.isFunction(b6) && b6 != aF[b7]) {
                        L[b7] = b6
                    }
                }
            }: function() {};
            aC.extend = function(b6, b7, L) {
                if (!b7 || !b6) {
                    throw new Error("extend failed, please check that all dependencies are included.")
                }
                var z = function() {},
                t;
                z.prototype = b7.prototype;
                b6.prototype = new z();
                b6.prototype.constructor = b6;
                b6.superclass = b7.prototype;
                if (b7.prototype.constructor == aF.constructor) {
                    b7.prototype.constructor = b7
                }
                if (L) {
                    for (t in L) {
                        if (bw.hasOwnProperty(L, t)) {
                            b6.prototype[t] = L[t]
                        }
                    }
                    bw._IEEnumFix(b6.prototype, L)
                }
            };
            if (typeof KJUR == "undefined" || !KJUR) {
                KJUR = {}
            }
            if (typeof KJUR.asn1 == "undefined" || !KJUR.asn1) {
                KJUR.asn1 = {}
            }
            KJUR.asn1.ASN1Util = new
            function() {
                this.integerToByteHex = function(t) {
                    var z = t.toString(16);
                    if ((z.length % 2) == 1) {
                        z = "0" + z
                    }
                    return z
                };
                this.bigIntToMinTwosComplementsHex = function(ca) {
                    var b8 = ca.toString(16);
                    if (b8.substr(0, 1) != "-") {
                        if (b8.length % 2 == 1) {
                            b8 = "0" + b8
                        } else {
                            if (!b8.match(/^[0-7]/)) {
                                b8 = "00" + b8
                            }
                        }
                    } else {
                        var t = b8.substr(1);
                        var b7 = t.length;
                        if (b7 % 2 == 1) {
                            b7 += 1
                        } else {
                            if (!b8.match(/^[0-7]/)) {
                                b7 += 2
                            }
                        }
                        var b9 = "";
                        for (var b6 = 0; b6 < b7; b6++) {
                            b9 += "f"
                        }
                        var L = new bo(b9, 16);
                        var z = L.xor(ca).add(bo.ONE);
                        b8 = z.toString(16).replace(/^-/, "")
                    }
                    return b8
                };
                this.getPEMStringFromHex = function(t, z) {
                    var b7 = CryptoJS.enc.Hex.parse(t);
                    var L = CryptoJS.enc.Base64.stringify(b7);
                    var b6 = L.replace(/(.{64})/g, "$1\r\n");
                    b6 = b6.replace(/\r\n$/, "");
                    return "-----BEGIN " + z + "-----\r\n" + b6 + "\r\n-----END " + z + "-----\r\n"
                }
            };
            KJUR.asn1.ASN1Object = function() {
                var L = true;
                var z = null;
                var b6 = "00";
                var b7 = "00";
                var t = "";
                this.getLengthHexFromValue = function() {
                    if (typeof this.hV == "undefined" || this.hV == null) {
                        throw "this.hV is null or undefined."
                    }
                    if (this.hV.length % 2 == 1) {
                        throw "value hex must be even length: n=" + t.length + ",v=" + this.hV
                    }
                    var cb = this.hV.length / 2;
                    var ca = cb.toString(16);
                    if (ca.length % 2 == 1) {
                        ca = "0" + ca
                    }
                    if (cb < 128) {
                        return ca
                    } else {
                        var b9 = ca.length / 2;
                        if (b9 > 15) {
                            throw "ASN.1 length too long to represent by 8x: n = " + cb.toString(16)
                        }
                        var b8 = 128 + b9;
                        return b8.toString(16) + ca
                    }
                };
                this.getEncodedHex = function() {
                    if (this.hTLV == null || this.isModified) {
                        this.hV = this.getFreshValueHex();
                        this.hL = this.getLengthHexFromValue();
                        this.hTLV = this.hT + this.hL + this.hV;
                        this.isModified = false
                    }
                    return this.hTLV
                };
                this.getValueHex = function() {
                    this.getEncodedHex();
                    return this.hV
                };
                this.getFreshValueHex = function() {
                    return ""
                }
            };
            KJUR.asn1.DERAbstractString = function(L) {
                KJUR.asn1.DERAbstractString.superclass.constructor.call(this);
                var z = null;
                var t = null;
                this.getString = function() {
                    return this.s
                };
                this.setString = function(b6) {
                    this.hTLV = null;
                    this.isModified = true;
                    this.s = b6;
                    this.hV = stohex(this.s)
                };
                this.setStringHex = function(b6) {
                    this.hTLV = null;
                    this.isModified = true;
                    this.s = null;
                    this.hV = b6
                };
                this.getFreshValueHex = function() {
                    return this.hV
                };
                if (typeof L != "undefined") {
                    if (typeof L.str != "undefined") {
                        this.setString(L.str)
                    } else {
                        if (typeof L.hex != "undefined") {
                            this.setStringHex(L.hex)
                        }
                    }
                }
            };
            aC.extend(KJUR.asn1.DERAbstractString, KJUR.asn1.ASN1Object);
            KJUR.asn1.DERAbstractTime = function(L) {
                KJUR.asn1.DERAbstractTime.superclass.constructor.call(this);
                var z = null;
                var t = null;
                this.localDateToUTC = function(b7) {
                    utc = b7.getTime() + (b7.getTimezoneOffset() * 60000);
                    var b6 = new Date(utc);
                    return b6
                };
                this.formatDate = function(cb, cd) {
                    var b6 = this.zeroPadding;
                    var cc = this.localDateToUTC(cb);
                    var cf = String(cc.getFullYear());
                    if (cd == "utc") {
                        cf = cf.substr(2, 2)
                    }
                    var ca = b6(String(cc.getMonth() + 1), 2);
                    var cg = b6(String(cc.getDate()), 2);
                    var b7 = b6(String(cc.getHours()), 2);
                    var b8 = b6(String(cc.getMinutes()), 2);
                    var b9 = b6(String(cc.getSeconds()), 2);
                    return cf + ca + cg + b7 + b8 + b9 + "Z"
                };
                this.zeroPadding = function(b7, b6) {
                    if (b7.length >= b6) {
                        return b7
                    }
                    return new Array(b6 - b7.length + 1).join("0") + b7
                };
                this.getString = function() {
                    return this.s
                };
                this.setString = function(b6) {
                    this.hTLV = null;
                    this.isModified = true;
                    this.s = b6;
                    this.hV = stohex(this.s)
                };
                this.setByDateValue = function(ca, cc, b7, b6, b8, b9) {
                    var cb = new Date(Date.UTC(ca, cc - 1, b7, b6, b8, b9, 0));
                    this.setByDate(cb)
                };
                this.getFreshValueHex = function() {
                    return this.hV
                }
            };
            aC.extend(KJUR.asn1.DERAbstractTime, KJUR.asn1.ASN1Object);
            KJUR.asn1.DERAbstractStructured = function(z) {
                KJUR.asn1.DERAbstractString.superclass.constructor.call(this);
                var t = null;
                this.setByASN1ObjectArray = function(L) {
                    this.hTLV = null;
                    this.isModified = true;
                    this.asn1Array = L
                };
                this.appendASN1Object = function(L) {
                    this.hTLV = null;
                    this.isModified = true;
                    this.asn1Array.push(L)
                };
                this.asn1Array = new Array();
                if (typeof z != "undefined") {
                    if (typeof z.array != "undefined") {
                        this.asn1Array = z.array
                    }
                }
            };
            aC.extend(KJUR.asn1.DERAbstractStructured, KJUR.asn1.ASN1Object);
            KJUR.asn1.DERBoolean = function() {
                KJUR.asn1.DERBoolean.superclass.constructor.call(this);
                this.hT = "01";
                this.hTLV = "0101ff"
            };
            aC.extend(KJUR.asn1.DERBoolean, KJUR.asn1.ASN1Object);
            KJUR.asn1.DERInteger = function(t) {
                KJUR.asn1.DERInteger.superclass.constructor.call(this);
                this.hT = "02";
                this.setByBigInteger = function(z) {
                    this.hTLV = null;
                    this.isModified = true;
                    this.hV = KJUR.asn1.ASN1Util.bigIntToMinTwosComplementsHex(z)
                };
                this.setByInteger = function(L) {
                    var z = new bo(String(L), 10);
                    this.setByBigInteger(z)
                };
                this.setValueHex = function(z) {
                    this.hV = z
                };
                this.getFreshValueHex = function() {
                    return this.hV
                };
                if (typeof t != "undefined") {
                    if (typeof t.bigint != "undefined") {
                        this.setByBigInteger(t.bigint)
                    } else {
                        if (typeof t["int"] != "undefined") {
                            this.setByInteger(t["int"])
                        } else {
                            if (typeof t.hex != "undefined") {
                                this.setValueHex(t.hex)
                            }
                        }
                    }
                }
            };
            aC.extend(KJUR.asn1.DERInteger, KJUR.asn1.ASN1Object);
            KJUR.asn1.DERBitString = function(t) {
                KJUR.asn1.DERBitString.superclass.constructor.call(this);
                this.hT = "03";
                this.setHexValueIncludingUnusedBits = function(z) {
                    this.hTLV = null;
                    this.isModified = true;
                    this.hV = z
                };
                this.setUnusedBitsAndHexValue = function(z, b6) {
                    if (z < 0 || 7 < z) {
                        throw "unused bits shall be from 0 to 7: u = " + z
                    }
                    var L = "0" + z;
                    this.hTLV = null;
                    this.isModified = true;
                    this.hV = L + b6
                };
                this.setByBinaryString = function(b6) {
                    b6 = b6.replace(/0+$/, "");
                    var b7 = 8 - b6.length % 8;
                    if (b7 == 8) {
                        b7 = 0
                    }
                    for (var b8 = 0; b8 <= b7; b8++) {
                        b6 += "0"
                    }
                    var b9 = "";
                    for (var b8 = 0; b8 < b6.length - 1; b8 += 8) {
                        var L = b6.substr(b8, 8);
                        var z = parseInt(L, 2).toString(16);
                        if (z.length == 1) {
                            z = "0" + z
                        }
                        b9 += z
                    }
                    this.hTLV = null;
                    this.isModified = true;
                    this.hV = "0" + b7 + b9
                };
                this.setByBooleanArray = function(b6) {
                    var L = "";
                    for (var z = 0; z < b6.length; z++) {
                        if (b6[z] == true) {
                            L += "1"
                        } else {
                            L += "0"
                        }
                    }
                    this.setByBinaryString(L)
                };
                this.newFalseArray = function(b6) {
                    var z = new Array(b6);
                    for (var L = 0; L < b6; L++) {
                        z[L] = false
                    }
                    return z
                };
                this.getFreshValueHex = function() {
                    return this.hV
                };
                if (typeof t != "undefined") {
                    if (typeof t.hex != "undefined") {
                        this.setHexValueIncludingUnusedBits(t.hex)
                    } else {
                        if (typeof t.bin != "undefined") {
                            this.setByBinaryString(t.bin)
                        } else {
                            if (typeof t.array != "undefined") {
                                this.setByBooleanArray(t.array)
                            }
                        }
                    }
                }
            };
            aC.extend(KJUR.asn1.DERBitString, KJUR.asn1.ASN1Object);
            KJUR.asn1.DEROctetString = function(t) {
                KJUR.asn1.DEROctetString.superclass.constructor.call(this, t);
                this.hT = "04"
            };
            aC.extend(KJUR.asn1.DEROctetString, KJUR.asn1.DERAbstractString);
            KJUR.asn1.DERNull = function() {
                KJUR.asn1.DERNull.superclass.constructor.call(this);
                this.hT = "05";
                this.hTLV = "0500"
            };
            aC.extend(KJUR.asn1.DERNull, KJUR.asn1.ASN1Object);
            KJUR.asn1.DERObjectIdentifier = function(L) {
                var z = function(b6) {
                    var b7 = b6.toString(16);
                    if (b7.length == 1) {
                        b7 = "0" + b7
                    }
                    return b7
                };
                var t = function(cc) {
                    var cb = "";
                    var b7 = new bo(cc, 10);
                    var b6 = b7.toString(2);
                    var b9 = 7 - b6.length % 7;
                    if (b9 == 7) {
                        b9 = 0
                    }
                    var cf = "";
                    for (var ca = 0; ca < b9; ca++) {
                        cf += "0"
                    }
                    b6 = cf + b6;
                    for (var ca = 0; ca < b6.length - 1; ca += 7) {
                        var cd = b6.substr(ca, 7);
                        if (ca != b6.length - 7) {
                            cd = "1" + cd
                        }
                        cb += z(parseInt(cd, 2))
                    }
                    return cb
                };
                KJUR.asn1.DERObjectIdentifier.superclass.constructor.call(this);
                this.hT = "06";
                this.setValueHex = function(b6) {
                    this.hTLV = null;
                    this.isModified = true;
                    this.s = null;
                    this.hV = b6
                };
                this.setValueOidString = function(b8) {
                    if (!b8.match(/^[0-9.]+$/)) {
                        throw "malformed oid string: " + b8
                    }
                    var b9 = "";
                    var b6 = b8.split(".");
                    var ca = parseInt(b6[0]) * 40 + parseInt(b6[1]);
                    b9 += z(ca);
                    b6.splice(0, 2);
                    for (var b7 = 0; b7 < b6.length; b7++) {
                        b9 += t(b6[b7])
                    }
                    this.hTLV = null;
                    this.isModified = true;
                    this.s = null;
                    this.hV = b9
                };
                this.setValueName = function(b7) {
                    if (typeof KJUR.asn1.x509.OID.name2oidList[b7] != "undefined") {
                        var b6 = KJUR.asn1.x509.OID.name2oidList[b7];
                        this.setValueOidString(b6)
                    } else {
                        throw "DERObjectIdentifier oidName undefined: " + b7
                    }
                };
                this.getFreshValueHex = function() {
                    return this.hV
                };
                if (typeof L != "undefined") {
                    if (typeof L.oid != "undefined") {
                        this.setValueOidString(L.oid)
                    } else {
                        if (typeof L.hex != "undefined") {
                            this.setValueHex(L.hex)
                        } else {
                            if (typeof L.name != "undefined") {
                                this.setValueName(L.name)
                            }
                        }
                    }
                }
            };
            aC.extend(KJUR.asn1.DERObjectIdentifier, KJUR.asn1.ASN1Object);
            KJUR.asn1.DERUTF8String = function(t) {
                KJUR.asn1.DERUTF8String.superclass.constructor.call(this, t);
                this.hT = "0c"
            };
            aC.extend(KJUR.asn1.DERUTF8String, KJUR.asn1.DERAbstractString);
            KJUR.asn1.DERNumericString = function(t) {
                KJUR.asn1.DERNumericString.superclass.constructor.call(this, t);
                this.hT = "12"
            };
            aC.extend(KJUR.asn1.DERNumericString, KJUR.asn1.DERAbstractString);
            KJUR.asn1.DERPrintableString = function(t) {
                KJUR.asn1.DERPrintableString.superclass.constructor.call(this, t);
                this.hT = "13"
            };
            aC.extend(KJUR.asn1.DERPrintableString, KJUR.asn1.DERAbstractString);
            KJUR.asn1.DERTeletexString = function(t) {
                KJUR.asn1.DERTeletexString.superclass.constructor.call(this, t);
                this.hT = "14"
            };
            aC.extend(KJUR.asn1.DERTeletexString, KJUR.asn1.DERAbstractString);
            KJUR.asn1.DERIA5String = function(t) {
                KJUR.asn1.DERIA5String.superclass.constructor.call(this, t);
                this.hT = "16"
            };
            aC.extend(KJUR.asn1.DERIA5String, KJUR.asn1.DERAbstractString);
            KJUR.asn1.DERUTCTime = function(t) {
                KJUR.asn1.DERUTCTime.superclass.constructor.call(this, t);
                this.hT = "17";
                this.setByDate = function(z) {
                    this.hTLV = null;
                    this.isModified = true;
                    this.date = z;
                    this.s = this.formatDate(this.date, "utc");
                    this.hV = stohex(this.s)
                };
                if (typeof t != "undefined") {
                    if (typeof t.str != "undefined") {
                        this.setString(t.str)
                    } else {
                        if (typeof t.hex != "undefined") {
                            this.setStringHex(t.hex)
                        } else {
                            if (typeof t.date != "undefined") {
                                this.setByDate(t.date)
                            }
                        }
                    }
                }
            };
            aC.extend(KJUR.asn1.DERUTCTime, KJUR.asn1.DERAbstractTime);
            KJUR.asn1.DERGeneralizedTime = function(t) {
                KJUR.asn1.DERGeneralizedTime.superclass.constructor.call(this, t);
                this.hT = "18";
                this.setByDate = function(z) {
                    this.hTLV = null;
                    this.isModified = true;
                    this.date = z;
                    this.s = this.formatDate(this.date, "gen");
                    this.hV = stohex(this.s)
                };
                if (typeof t != "undefined") {
                    if (typeof t.str != "undefined") {
                        this.setString(t.str)
                    } else {
                        if (typeof t.hex != "undefined") {
                            this.setStringHex(t.hex)
                        } else {
                            if (typeof t.date != "undefined") {
                                this.setByDate(t.date)
                            }
                        }
                    }
                }
            };
            aC.extend(KJUR.asn1.DERGeneralizedTime, KJUR.asn1.DERAbstractTime);
            KJUR.asn1.DERSequence = function(t) {
                KJUR.asn1.DERSequence.superclass.constructor.call(this, t);
                this.hT = "30";
                this.getFreshValueHex = function() {
                    var L = "";
                    for (var z = 0; z < this.asn1Array.length; z++) {
                        var b6 = this.asn1Array[z];
                        L += b6.getEncodedHex()
                    }
                    this.hV = L;
                    return this.hV
                }
            };
            aC.extend(KJUR.asn1.DERSequence, KJUR.asn1.DERAbstractStructured);
            KJUR.asn1.DERSet = function(t) {
                KJUR.asn1.DERSet.superclass.constructor.call(this, t);
                this.hT = "31";
                this.getFreshValueHex = function() {
                    var z = new Array();
                    for (var L = 0; L < this.asn1Array.length; L++) {
                        var b6 = this.asn1Array[L];
                        z.push(b6.getEncodedHex())
                    }
                    z.sort();
                    this.hV = z.join("");
                    return this.hV
                }
            };
            aC.extend(KJUR.asn1.DERSet, KJUR.asn1.DERAbstractStructured);
            KJUR.asn1.DERTaggedObject = function(t) {
                KJUR.asn1.DERTaggedObject.superclass.constructor.call(this);
                this.hT = "a0";
                this.hV = "";
                this.isExplicit = true;
                this.asn1Object = null;
                this.setASN1Object = function(z, L, b6) {
                    this.hT = L;
                    this.isExplicit = z;
                    this.asn1Object = b6;
                    if (this.isExplicit) {
                        this.hV = this.asn1Object.getEncodedHex();
                        this.hTLV = null;
                        this.isModified = true
                    } else {
                        this.hV = null;
                        this.hTLV = b6.getEncodedHex();
                        this.hTLV = this.hTLV.replace(/^../, L);
                        this.isModified = false
                    }
                };
                this.getFreshValueHex = function() {
                    return this.hV
                };
                if (typeof t != "undefined") {
                    if (typeof t.tag != "undefined") {
                        this.hT = t.tag
                    }
                    if (typeof t.explicit != "undefined") {
                        this.isExplicit = t.explicit
                    }
                    if (typeof t.obj != "undefined") {
                        this.asn1Object = t.obj;
                        this.setASN1Object(this.isExplicit, this.hT, this.asn1Object)
                    }
                }
            };
            aC.extend(KJUR.asn1.DERTaggedObject, KJUR.asn1.ASN1Object); (function(z) {
                var t = {},
                L;
                t.decode = function(b6) {
                    var b8;
                    if (L === z) {
                        var b9 = "0123456789ABCDEF",
                        cd = " \f\n\r\t\u00A0\u2028\u2029";
                        L = [];
                        for (b8 = 0; b8 < 16; ++b8) {
                            L[b9.charAt(b8)] = b8
                        }
                        b9 = b9.toLowerCase();
                        for (b8 = 10; b8 < 16; ++b8) {
                            L[b9.charAt(b8)] = b8
                        }
                        for (b8 = 0; b8 < cd.length; ++b8) {
                            L[cd.charAt(b8)] = -1
                        }
                    }
                    var b7 = [],
                    ca = 0,
                    cc = 0;
                    for (b8 = 0; b8 < b6.length; ++b8) {
                        var cb = b6.charAt(b8);
                        if (cb == "=") {
                            break
                        }
                        cb = L[cb];
                        if (cb == -1) {
                            continue
                        }
                        if (cb === z) {
                            throw "Illegal character at offset " + b8
                        }
                        ca |= cb;
                        if (++cc >= 2) {
                            b7[b7.length] = ca;
                            ca = 0;
                            cc = 0
                        } else {
                            ca <<= 4
                        }
                    }
                    if (cc) {
                        throw "Hex encoding incomplete: 4 bits missing"
                    }
                    return b7
                };
                window.Hex = t
            })(); (function(z) {
                var t = {},
                L;
                t.decode = function(b6) {
                    var b9;
                    if (L === z) {
                        var b8 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
                        cd = "= \f\n\r\t\u00A0\u2028\u2029";
                        L = [];
                        for (b9 = 0; b9 < 64; ++b9) {
                            L[b8.charAt(b9)] = b9
                        }
                        for (b9 = 0; b9 < cd.length; ++b9) {
                            L[cd.charAt(b9)] = -1
                        }
                    }
                    var b7 = [];
                    var ca = 0,
                    cc = 0;
                    for (b9 = 0; b9 < b6.length; ++b9) {
                        var cb = b6.charAt(b9);
                        if (cb == "=") {
                            break
                        }
                        cb = L[cb];
                        if (cb == -1) {
                            continue
                        }
                        if (cb === z) {
                            throw "Illegal character at offset " + b9
                        }
                        ca |= cb;
                        if (++cc >= 4) {
                            b7[b7.length] = (ca >> 16);
                            b7[b7.length] = (ca >> 8) & 255;
                            b7[b7.length] = ca & 255;
                            ca = 0;
                            cc = 0
                        } else {
                            ca <<= 6
                        }
                    }
                    switch (cc) {
                    case 1:
                        throw "Base64 encoding incomplete: at least 2 bits missing";
                    case 2:
                        b7[b7.length] = (ca >> 10);
                        break;
                    case 3:
                        b7[b7.length] = (ca >> 16);
                        b7[b7.length] = (ca >> 8) & 255;
                        break
                    }
                    return b7
                };
                t.re = /-----BEGIN [^-]+-----([A-Za-z0-9+\/=\s]+)-----END [^-]+-----|begin-base64[^\n]+\n([A-Za-z0-9+\/=\s]+)====/;
                t.unarmor = function(b7) {
                    var b6 = t.re.exec(b7);
                    if (b6) {
                        if (b6[1]) {
                            b7 = b6[1]
                        } else {
                            if (b6[2]) {
                                b7 = b6[2]
                            } else {
                                throw "RegExp out of sync"
                            }
                        }
                    }
                    return t.decode(b7)
                };
                window.Base64 = t
            })(); (function(b8) {
                var z = 100,
                t = "\u2026",
                L = {
                    tag: function(ca, cb) {
                        var b9 = document.createElement(ca);
                        b9.className = cb;
                        return b9
                    },
                    text: function(b9) {
                        return document.createTextNode(b9)
                    }
                };
                function b7(b9, ca) {
                    if (b9 instanceof b7) {
                        this.enc = b9.enc;
                        this.pos = b9.pos
                    } else {
                        this.enc = b9;
                        this.pos = ca
                    }
                }
                b7.prototype.get = function(b9) {
                    if (b9 === b8) {
                        b9 = this.pos++
                    }
                    if (b9 >= this.enc.length) {
                        throw "Requesting byte offset " + b9 + " on a stream of length " + this.enc.length
                    }
                    return this.enc[b9]
                };
                b7.prototype.hexDigits = "0123456789ABCDEF";
                b7.prototype.hexByte = function(b9) {
                    return this.hexDigits.charAt((b9 >> 4) & 15) + this.hexDigits.charAt(b9 & 15)
                };
                b7.prototype.hexDump = function(cd, b9, ca) {
                    var cc = "";
                    for (var cb = cd; cb < b9; ++cb) {
                        cc += this.hexByte(this.get(cb));
                        if (ca !== true) {
                            switch (cb & 15) {
                            case 7:
                                cc += "  ";
                                break;
                            case 15:
                                cc += "\n";
                                break;
                            default:
                                cc += " "
                            }
                        }
                    }
                    return cc
                };
                b7.prototype.parseStringISO = function(cc, b9) {
                    var cb = "";
                    for (var ca = cc; ca < b9; ++ca) {
                        cb += String.fromCharCode(this.get(ca))
                    }
                    return cb
                };
                b7.prototype.parseStringUTF = function(cd, b9) {
                    var cb = "";
                    for (var ca = cd; ca < b9;) {
                        var cc = this.get(ca++);
                        if (cc < 128) {
                            cb += String.fromCharCode(cc)
                        } else {
                            if ((cc > 191) && (cc < 224)) {
                                cb += String.fromCharCode(((cc & 31) << 6) | (this.get(ca++) & 63))
                            } else {
                                cb += String.fromCharCode(((cc & 15) << 12) | ((this.get(ca++) & 63) << 6) | (this.get(ca++) & 63))
                            }
                        }
                    }
                    return cb
                };
                b7.prototype.parseStringBMP = function(cf, ca) {
                    var cd = "";
                    for (var cc = cf; cc < ca; cc += 2) {
                        var b9 = this.get(cc);
                        var cb = this.get(cc + 1);
                        cd += String.fromCharCode((b9 << 8) + cb)
                    }
                    return cd
                };
                b7.prototype.reTime = /^((?:1[89]|2\d)?\d\d)(0[1-9]|1[0-2])(0[1-9]|[12]\d|3[01])([01]\d|2[0-3])(?:([0-5]\d)(?:([0-5]\d)(?:[.,](\d{1,3}))?)?)?(Z|[-+](?:[0]\d|1[0-2])([0-5]\d)?)?$/;
                b7.prototype.parseTime = function(cc, ca) {
                    var cb = this.parseStringISO(cc, ca),
                    b9 = this.reTime.exec(cb);
                    if (!b9) {
                        return "Unrecognized time: " + cb
                    }
                    cb = b9[1] + "-" + b9[2] + "-" + b9[3] + " " + b9[4];
                    if (b9[5]) {
                        cb += ":" + b9[5];
                        if (b9[6]) {
                            cb += ":" + b9[6];
                            if (b9[7]) {
                                cb += "." + b9[7]
                            }
                        }
                    }
                    if (b9[8]) {
                        cb += " UTC";
                        if (b9[8] != "Z") {
                            cb += b9[8];
                            if (b9[9]) {
                                cb += ":" + b9[9]
                            }
                        }
                    }
                    return cb
                };
                b7.prototype.parseInteger = function(cf, ca) {
                    var b9 = ca - cf;
                    if (b9 > 4) {
                        b9 <<= 3;
                        var cc = this.get(cf);
                        if (cc === 0) {
                            b9 -= 8
                        } else {
                            while (cc < 128) {
                                cc <<= 1; --b9
                            }
                        }
                        return "(" + b9 + " bit)"
                    }
                    var cd = 0;
                    for (var cb = cf; cb < ca; ++cb) {
                        cd = (cd << 8) | this.get(cb)
                    }
                    return cd
                };
                b7.prototype.parseBitString = function(b9, ca) {
                    var cf = this.get(b9),
                    cc = ((ca - b9 - 1) << 3) - cf,
                    ci = "(" + cc + " bit)";
                    if (cc <= 20) {
                        var ch = cf;
                        ci += " ";
                        for (var cd = ca - 1; cd > b9; --cd) {
                            var cg = this.get(cd);
                            for (var cb = ch; cb < 8; ++cb) {
                                ci += (cg >> cb) & 1 ? "1": "0"
                            }
                            ch = 0
                        }
                    }
                    return ci
                };
                b7.prototype.parseOctetString = function(cd, ca) {
                    var b9 = ca - cd,
                    cc = "(" + b9 + " byte) ";
                    if (b9 > z) {
                        ca = cd + z
                    }
                    for (var cb = cd; cb < ca; ++cb) {
                        cc += this.hexByte(this.get(cb))
                    }
                    if (b9 > z) {
                        cc += t
                    }
                    return cc
                };
                b7.prototype.parseOID = function(ch, ca) {
                    var cd = "",
                    cg = 0,
                    cf = 0;
                    for (var cc = ch; cc < ca; ++cc) {
                        var cb = this.get(cc);
                        cg = (cg << 7) | (cb & 127);
                        cf += 7;
                        if (! (cb & 128)) {
                            if (cd === "") {
                                var b9 = cg < 80 ? cg < 40 ? 0: 1: 2;
                                cd = b9 + "." + (cg - b9 * 40)
                            } else {
                                cd += "." + ((cf >= 31) ? "bigint": cg)
                            }
                            cg = cf = 0
                        }
                    }
                    return cd
                };
                function b6(cc, cd, cb, b9, ca) {
                    this.stream = cc;
                    this.header = cd;
                    this.length = cb;
                    this.tag = b9;
                    this.sub = ca
                }
                b6.prototype.typeName = function() {
                    if (this.tag === b8) {
                        return "unknown"
                    }
                    var cb = this.tag >> 6,
                    b9 = (this.tag >> 5) & 1,
                    ca = this.tag & 31;
                    switch (cb) {
                    case 0:
                        switch (ca) {
                        case 0:
                            return "EOC";
                        case 1:
                            return "BOOLEAN";
                        case 2:
                            return "INTEGER";
                        case 3:
                            return "BIT_STRING";
                        case 4:
                            return "OCTET_STRING";
                        case 5:
                            return "NULL";
                        case 6:
                            return "OBJECT_IDENTIFIER";
                        case 7:
                            return "ObjectDescriptor";
                        case 8:
                            return "EXTERNAL";
                        case 9:
                            return "REAL";
                        case 10:
                            return "ENUMERATED";
                        case 11:
                            return "EMBEDDED_PDV";
                        case 12:
                            return "UTF8String";
                        case 16:
                            return "SEQUENCE";
                        case 17:
                            return "SET";
                        case 18:
                            return "NumericString";
                        case 19:
                            return "PrintableString";
                        case 20:
                            return "TeletexString";
                        case 21:
                            return "VideotexString";
                        case 22:
                            return "IA5String";
                        case 23:
                            return "UTCTime";
                        case 24:
                            return "GeneralizedTime";
                        case 25:
                            return "GraphicString";
                        case 26:
                            return "VisibleString";
                        case 27:
                            return "GeneralString";
                        case 28:
                            return "UniversalString";
                        case 30:
                            return "BMPString";
                        default:
                            return "Universal_" + ca.toString(16)
                        }
                    case 1:
                        return "Application_" + ca.toString(16);
                    case 2:
                        return "[" + ca + "]";
                    case 3:
                        return "Private_" + ca.toString(16)
                    }
                };
                b6.prototype.reSeemsASCII = /^[ -~]+$/;
                b6.prototype.content = function() {
                    if (this.tag === b8) {
                        return null
                    }
                    var cd = this.tag >> 6,
                    ca = this.tag & 31,
                    cc = this.posContent(),
                    b9 = Math.abs(this.length);
                    if (cd !== 0) {
                        if (this.sub !== null) {
                            return "(" + this.sub.length + " elem)"
                        }
                        var cb = this.stream.parseStringISO(cc, cc + Math.min(b9, z));
                        if (this.reSeemsASCII.test(cb)) {
                            return cb.substring(0, 2 * z) + ((cb.length > 2 * z) ? t: "")
                        } else {
                            return this.stream.parseOctetString(cc, cc + b9)
                        }
                    }
                    switch (ca) {
                    case 1:
                        return (this.stream.get(cc) === 0) ? "false": "true";
                    case 2:
                        return this.stream.parseInteger(cc, cc + b9);
                    case 3:
                        return this.sub ? "(" + this.sub.length + " elem)": this.stream.parseBitString(cc, cc + b9);
                    case 4:
                        return this.sub ? "(" + this.sub.length + " elem)": this.stream.parseOctetString(cc, cc + b9);
                    case 6:
                        return this.stream.parseOID(cc, cc + b9);
                    case 16:
                    case 17:
                        return "(" + this.sub.length + " elem)";
                    case 12:
                        return this.stream.parseStringUTF(cc, cc + b9);
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 26:
                        return this.stream.parseStringISO(cc, cc + b9);
                    case 30:
                        return this.stream.parseStringBMP(cc, cc + b9);
                    case 23:
                    case 24:
                        return this.stream.parseTime(cc, cc + b9)
                    }
                    return null
                };
                b6.prototype.toString = function() {
                    return this.typeName() + "@" + this.stream.pos + "[header:" + this.header + ",length:" + this.length + ",sub:" + ((this.sub === null) ? "null": this.sub.length) + "]"
                };
                b6.prototype.print = function(ca) {
                    if (ca === b8) {
                        ca = ""
                    }
                    document.writeln(ca + this);
                    if (this.sub !== null) {
                        ca += "  ";
                        for (var cb = 0, b9 = this.sub.length; cb < b9; ++cb) {
                            this.sub[cb].print(ca)
                        }
                    }
                };
                b6.prototype.toPrettyString = function(ca) {
                    if (ca === b8) {
                        ca = ""
                    }
                    var cc = ca + this.typeName() + " @" + this.stream.pos;
                    if (this.length >= 0) {
                        cc += "+"
                    }
                    cc += this.length;
                    if (this.tag & 32) {
                        cc += " (constructed)"
                    } else {
                        if (((this.tag == 3) || (this.tag == 4)) && (this.sub !== null)) {
                            cc += " (encapsulates)"
                        }
                    }
                    cc += "\n";
                    if (this.sub !== null) {
                        ca += "  ";
                        for (var cb = 0, b9 = this.sub.length; cb < b9; ++cb) {
                            cc += this.sub[cb].toPrettyString(ca)
                        }
                    }
                    return cc
                };
                b6.prototype.toDOM = function() {
                    var ca = L.tag("div", "node");
                    ca.asn1 = this;
                    var ch = L.tag("div", "head");
                    var cj = this.typeName().replace(/_/g, " ");
                    ch.innerHTML = cj;
                    var cf = this.content();
                    if (cf !== null) {
                        cf = String(cf).replace(/</g, "&lt;");
                        var cd = L.tag("span", "preview");
                        cd.appendChild(L.text(cf));
                        ch.appendChild(cd)
                    }
                    ca.appendChild(ch);
                    this.node = ca;
                    this.head = ch;
                    var ci = L.tag("div", "value");
                    cj = "Offset: " + this.stream.pos + "<br/>";
                    cj += "Length: " + this.header + "+";
                    if (this.length >= 0) {
                        cj += this.length
                    } else {
                        cj += ( - this.length) + " (undefined)"
                    }
                    if (this.tag & 32) {
                        cj += "<br/>(constructed)"
                    } else {
                        if (((this.tag == 3) || (this.tag == 4)) && (this.sub !== null)) {
                            cj += "<br/>(encapsulates)"
                        }
                    }
                    if (cf !== null) {
                        cj += "<br/>Value:<br/><b>" + cf + "</b>";
                        if ((typeof oids === "object") && (this.tag == 6)) {
                            var cb = oids[cf];
                            if (cb) {
                                if (cb.d) {
                                    cj += "<br/>" + cb.d
                                }
                                if (cb.c) {
                                    cj += "<br/>" + cb.c
                                }
                                if (cb.w) {
                                    cj += "<br/>(warning!)"
                                }
                            }
                        }
                    }
                    ci.innerHTML = cj;
                    ca.appendChild(ci);
                    var b9 = L.tag("div", "sub");
                    if (this.sub !== null) {
                        for (var cc = 0, cg = this.sub.length; cc < cg; ++cc) {
                            b9.appendChild(this.sub[cc].toDOM())
                        }
                    }
                    ca.appendChild(b9);
                    ch.onclick = function() {
                        ca.className = (ca.className == "node collapsed") ? "node": "node collapsed"
                    };
                    return ca
                };
                b6.prototype.posStart = function() {
                    return this.stream.pos
                };
                b6.prototype.posContent = function() {
                    return this.stream.pos + this.header
                };
                b6.prototype.posEnd = function() {
                    return this.stream.pos + this.header + Math.abs(this.length)
                };
                b6.prototype.fakeHover = function(b9) {
                    this.node.className += " hover";
                    if (b9) {
                        this.head.className += " hover"
                    }
                };
                b6.prototype.fakeOut = function(ca) {
                    var b9 = / ?hover/;
                    this.node.className = this.node.className.replace(b9, "");
                    if (ca) {
                        this.head.className = this.head.className.replace(b9, "")
                    }
                };
                b6.prototype.toHexDOM_sub = function(cc, cb, cd, cf, b9) {
                    if (cf >= b9) {
                        return
                    }
                    var ca = L.tag("span", cb);
                    ca.appendChild(L.text(cd.hexDump(cf, b9)));
                    cc.appendChild(ca)
                };
                b6.prototype.toHexDOM = function(ca) {
                    var cd = L.tag("span", "hex");
                    if (ca === b8) {
                        ca = cd
                    }
                    this.head.hexNode = cd;
                    this.head.onmouseover = function() {
                        this.hexNode.className = "hexCurrent"
                    };
                    this.head.onmouseout = function() {
                        this.hexNode.className = "hex"
                    };
                    cd.asn1 = this;
                    cd.onmouseover = function() {
                        var cg = !ca.selected;
                        if (cg) {
                            ca.selected = this.asn1;
                            this.className = "hexCurrent"
                        }
                        this.asn1.fakeHover(cg)
                    };
                    cd.onmouseout = function() {
                        var cg = (ca.selected == this.asn1);
                        this.asn1.fakeOut(cg);
                        if (cg) {
                            ca.selected = null;
                            this.className = "hex"
                        }
                    };
                    this.toHexDOM_sub(cd, "tag", this.stream, this.posStart(), this.posStart() + 1);
                    this.toHexDOM_sub(cd, (this.length >= 0) ? "dlen": "ulen", this.stream, this.posStart() + 1, this.posContent());
                    if (this.sub === null) {
                        cd.appendChild(L.text(this.stream.hexDump(this.posContent(), this.posEnd())))
                    } else {
                        if (this.sub.length > 0) {
                            var cf = this.sub[0];
                            var cc = this.sub[this.sub.length - 1];
                            this.toHexDOM_sub(cd, "intro", this.stream, this.posContent(), cf.posStart());
                            for (var cb = 0, b9 = this.sub.length; cb < b9; ++cb) {
                                cd.appendChild(this.sub[cb].toHexDOM(ca))
                            }
                            this.toHexDOM_sub(cd, "outro", this.stream, cc.posEnd(), this.posEnd())
                        }
                    }
                    return cd
                };
                b6.prototype.toHexString = function(b9) {
                    return this.stream.hexDump(this.posStart(), this.posEnd(), true)
                };
                b6.decodeLength = function(cc) {
                    var ca = cc.get(),
                    b9 = ca & 127;
                    if (b9 == ca) {
                        return b9
                    }
                    if (b9 > 3) {
                        throw "Length over 24 bits not supported at position " + (cc.pos - 1)
                    }
                    if (b9 === 0) {
                        return - 1
                    }
                    ca = 0;
                    for (var cb = 0; cb < b9; ++cb) {
                        ca = (ca << 8) | cc.get()
                    }
                    return ca
                };
                b6.hasContent = function(ca, b9, cg) {
                    if (ca & 32) {
                        return true
                    }
                    if ((ca < 3) || (ca > 4)) {
                        return false
                    }
                    var cf = new b7(cg);
                    if (ca == 3) {
                        cf.get()
                    }
                    var cd = cf.get();
                    if ((cd >> 6) & 1) {
                        return false
                    }
                    try {
                        var cc = b6.decodeLength(cf);
                        return ((cf.pos - cg.pos) + cc == b9)
                    } catch(cb) {
                        return false
                    }
                };
                b6.decode = function(ch) {
                    if (! (ch instanceof b7)) {
                        ch = new b7(ch, 0)
                    }
                    var cg = new b7(ch),
                    cj = ch.get(),
                    cd = b6.decodeLength(ch),
                    cc = ch.pos - cg.pos,
                    b9 = null;
                    if (b6.hasContent(cj, cd, ch)) {
                        var ca = ch.pos;
                        if (cj == 3) {
                            ch.get()
                        }
                        b9 = [];
                        if (cd >= 0) {
                            var cb = ca + cd;
                            while (ch.pos < cb) {
                                b9[b9.length] = b6.decode(ch)
                            }
                            if (ch.pos != cb) {
                                throw "Content size is not correct for container starting at offset " + ca
                            }
                        } else {
                            try {
                                for (;;) {
                                    var ci = b6.decode(ch);
                                    if (ci.tag === 0) {
                                        break
                                    }
                                    b9[b9.length] = ci
                                }
                                cd = ca - ch.pos
                            } catch(cf) {
                                throw "Exception while decoding undefined length content: " + cf
                            }
                        }
                    } else {
                        ch.pos += cd
                    }
                    return new b6(cg, cc, cd, cj, b9)
                };
                b6.test = function() {
                    var cf = [{
                        value: [39],
                        expected: 39
                    },
                    {
                        value: [129, 201],
                        expected: 201
                    },
                    {
                        value: [131, 254, 220, 186],
                        expected: 16702650
                    }];
                    for (var cb = 0, b9 = cf.length; cb < b9; ++cb) {
                        var cd = 0,
                        cc = new b7(cf[cb].value, 0),
                        ca = b6.decodeLength(cc);
                        if (ca != cf[cb].expected) {
                            document.write("In test[" + cb + "] expected " + cf[cb].expected + " got " + ca + "\n")
                        }
                    }
                };
                window.ASN1 = b6
            })();
            ASN1.prototype.getHexStringValue = function() {
                var t = this.toHexString();
                var L = this.header * 2;
                var z = this.length * 2;
                return t.substr(L, z)
            };
            K.prototype.parseKey = function(cb) {
                try {
                    var t = /^\s*(?:[0-9A-Fa-f][0-9A-Fa-f]\s*)+$/;
                    var cg = t.test(cb) ? Hex.decode(cb) : Base64.unarmor(cb);
                    var b7 = ASN1.decode(cg);
                    if (b7.sub.length === 9) {
                        var ch = b7.sub[1].getHexStringValue();
                        this.n = H(ch, 16);
                        var b6 = b7.sub[2].getHexStringValue();
                        this.e = parseInt(b6, 16);
                        var z = b7.sub[3].getHexStringValue();
                        this.d = H(z, 16);
                        var ca = b7.sub[4].getHexStringValue();
                        this.p = H(ca, 16);
                        var b9 = b7.sub[5].getHexStringValue();
                        this.q = H(b9, 16);
                        var cd = b7.sub[6].getHexStringValue();
                        this.dmp1 = H(cd, 16);
                        var cc = b7.sub[7].getHexStringValue();
                        this.dmq1 = H(cc, 16);
                        var L = b7.sub[8].getHexStringValue();
                        this.coeff = H(L, 16)
                    } else {
                        if (b7.sub.length === 2) {
                            var ci = b7.sub[1];
                            var b8 = ci.sub[0];
                            var ch = b8.sub[0].getHexStringValue();
                            this.n = H(ch, 16);
                            var b6 = b8.sub[1].getHexStringValue();
                            this.e = parseInt(b6, 16)
                        } else {
                            return false
                        }
                    }
                    return true
                } catch(cf) {
                    return false
                }
            };
            K.prototype.getPrivateBaseKey = function() {
                var z = {
                    array: [new KJUR.asn1.DERInteger({
                        "int": 0
                    }), new KJUR.asn1.DERInteger({
                        bigint: this.n
                    }), new KJUR.asn1.DERInteger({
                        "int": this.e
                    }), new KJUR.asn1.DERInteger({
                        bigint: this.d
                    }), new KJUR.asn1.DERInteger({
                        bigint: this.p
                    }), new KJUR.asn1.DERInteger({
                        bigint: this.q
                    }), new KJUR.asn1.DERInteger({
                        bigint: this.dmp1
                    }), new KJUR.asn1.DERInteger({
                        bigint: this.dmq1
                    }), new KJUR.asn1.DERInteger({
                        bigint: this.coeff
                    })]
                };
                var t = new KJUR.asn1.DERSequence(z);
                return t.getEncodedHex()
            };
            K.prototype.getPrivateBaseKeyB64 = function() {
                return an(this.getPrivateBaseKey())
            };
            K.prototype.getPublicBaseKey = function() {
                var L = {
                    array: [new KJUR.asn1.DERObjectIdentifier({
                        oid: "1.2.840.113549.1.1.1"
                    }), new KJUR.asn1.DERNull()]
                };
                var t = new KJUR.asn1.DERSequence(L);
                L = {
                    array: [new KJUR.asn1.DERInteger({
                        bigint: this.n
                    }), new KJUR.asn1.DERInteger({
                        "int": this.e
                    })]
                };
                var b7 = new KJUR.asn1.DERSequence(L);
                L = {
                    hex: "00" + b7.getEncodedHex()
                };
                var b6 = new KJUR.asn1.DERBitString(L);
                L = {
                    array: [t, b6]
                };
                var z = new KJUR.asn1.DERSequence(L);
                return z.getEncodedHex()
            };
            K.prototype.getPublicBaseKeyB64 = function() {
                return an(this.getPublicBaseKey())
            };
            K.prototype.wordwrap = function(L, t) {
                t = t || 64;
                if (!L) {
                    return L
                }
                var z = "(.{1," + t + "})( +|$\n?)|(.{1," + t + "})";
                return L.match(RegExp(z, "g")).join("\n")
            };
            K.prototype.getPrivateKey = function() {
                var t = "-----BEGIN RSA PRIVATE KEY-----\n";
                t += this.wordwrap(this.getPrivateBaseKeyB64()) + "\n";
                t += "-----END RSA PRIVATE KEY-----";
                return t
            };
            K.prototype.getPublicKey = function() {
                var t = "-----BEGIN PUBLIC KEY-----\n";
                t += this.wordwrap(this.getPublicBaseKeyB64()) + "\n";
                t += "-----END PUBLIC KEY-----";
                return t
            };
            K.prototype.hasPublicKeyProperty = function(t) {
                t = t || {};
                return t.hasOwnProperty("n") && t.hasOwnProperty("e")
            };
            K.prototype.hasPrivateKeyProperty = function(t) {
                t = t || {};
                return t.hasOwnProperty("n") && t.hasOwnProperty("e") && t.hasOwnProperty("d") && t.hasOwnProperty("p") && t.hasOwnProperty("q") && t.hasOwnProperty("dmp1") && t.hasOwnProperty("dmq1") && t.hasOwnProperty("coeff")
            };
            K.prototype.parsePropertiesFrom = function(t) {
                this.n = t.n;
                this.e = t.e;
                if (t.hasOwnProperty("d")) {
                    this.d = t.d;
                    this.p = t.p;
                    this.q = t.q;
                    this.dmp1 = t.dmp1;
                    this.dmq1 = t.dmq1;
                    this.coeff = t.coeff
                }
            };
            var bG = function(t) {
                K.call(this);
                if (t) {
                    if (typeof t === "string") {
                        this.parseKey(t)
                    } else {
                        if (this.hasPrivateKeyProperty(t) || this.hasPublicKeyProperty(t)) {
                            this.parsePropertiesFrom(t)
                        }
                    }
                }
            };
            bG.prototype = new K();
            bG.prototype.constructor = bG;
            var bc = function(t) {
                t = t || {};
                this.default_key_size = parseInt(t.default_key_size) || 1024;
                this.default_public_exponent = t.default_public_exponent || "010001";
                this.log = t.log || false;
                this.key = null
            };
            bc.prototype.setKey = function(t) {
                if (this.log && this.key) {
                    console.warn("A key was already set, overriding existing.")
                }
                this.key = new bG(t)
            };
            bc.prototype.setPrivateKey = function(t) {
                this.setKey(t)
            };
            bc.prototype.setPublicKey = function(t) {
                this.setKey(t)
            };
            bc.prototype.decrypt = function(t) {
                try {
                    return this.getKey().decrypt(a5(t))
                } catch(z) {
                    return false
                }
            };
            bc.prototype.encrypt = function(t) {
                try {
                    return an(this.getKey().encrypt(t))
                } catch(z) {
                    return false
                }
            };
            bc.prototype.getKey = function(t) {
                if (!this.key) {
                    this.key = new bG();
                    if (t && {}.toString.call(t) === "[object Function]") {
                        this.key.generateAsync(this.default_key_size, this.default_public_exponent, t);
                        return
                    }
                    this.key.generate(this.default_key_size, this.default_public_exponent)
                }
                return this.key
            };
            bc.prototype.getPrivateKey = function() {
                return this.getKey().getPrivateKey()
            };
            bc.prototype.getPrivateKeyB64 = function() {
                return this.getKey().getPrivateBaseKeyB64()
            };
            bc.prototype.getPublicKey = function() {
                return this.getKey().getPublicKey()
            };
            bc.prototype.getPublicKeyB64 = function() {
                return this.getKey().getPublicBaseKeyB64()
            };
            az.JSEncrypt = bc
        })(h.lib.RSAExport);
        h.lib.RSA = h.lib.RSAExport.JSEncrypt;
        function b(i) {
            alert("undefined:" + i)
        }
        var h = h || window.passport || {};
        h.err = h.err || {}; (function(i) {
            var j = null;
            if ((typeof(i.getCurrent)).toLowerCase() === "function") {
                j = i.getCurrent()
            } else {
                j = {
                    errMsg: {},
                    labelText: {}
                }
            }
            j.errMsg.login = {
                "-1": {
                    msg: '\u7cfb\u7edf\u9519\u8bef,\u8bf7\u60a8\u7a0d\u540e\u518d\u8bd5,<a href="http://passport.baidu.com/v2/?ucenterfeedback#{urldata}#login"  target="_blank">\u95ee\u9898\u53cd\u9988</a>',
                    field: ""
                },
                "1": {
                    msg: "\u60a8\u8f93\u5165\u7684\u5e10\u53f7\u683c\u5f0f\u4e0d\u6b63\u786e",
                    field: "userName"
                },
                "2": {
                    msg: "\u60a8\u8f93\u5165\u7684\u5e10\u53f7\u4e0d\u5b58\u5728\uff0c\u53ef<a href='http://passport.baidu.com/v2/?ucenterfeedback#{urldata}#login_7' target='_blank'>\u67e5\u770b\u5e2e\u52a9</a>\u6216<a href='http://passport.baidu.com/v2/?reg#{urldata}' target='_blank'>\u7acb\u5373\u6ce8\u518c</a>",
                    field: "userName"
                },
                "3": {
                    msg: "\u9a8c\u8bc1\u7801\u4e0d\u5b58\u5728\u6216\u5df2\u8fc7\u671f,\u8bf7\u91cd\u65b0\u8f93\u5165",
                    field: ""
                },
                "4": {
                    msg: '\u60a8\u8f93\u5165\u7684\u5e10\u53f7\u6216\u5bc6\u7801\u6709\u8bef,<a href="http://passport.baidu.com/?getpassindex#{urldata}"  target="_blank" >\u5fd8\u8bb0\u5bc6\u7801</a>\uff1f',
                    field: "password"
                },
                "5": {
                    msg: "\u8bf7\u5728\u5f39\u51fa\u7684\u7a97\u53e3\u64cd\u4f5c,\u6216\u91cd\u65b0\u767b\u5f55",
                    field: ""
                },
                "6": {
                    msg: "\u60a8\u8f93\u5165\u7684\u9a8c\u8bc1\u7801\u6709\u8bef",
                    field: "verifyCode"
                },
                "16": {
                    msg: '\u60a8\u7684\u5e10\u53f7\u56e0\u5b89\u5168\u95ee\u9898\u5df2\u88ab\u9650\u5236\u767b\u5f55,<a href="http://passport.baidu.com/v2/?ucenterfeedback#{urldata}#login"  target="_blank" >\u95ee\u9898\u53cd\u9988</a>',
                    field: ""
                },
                "257": {
                    msg: "\u8bf7\u8f93\u5165\u9a8c\u8bc1\u7801",
                    field: "verifyCode"
                },
                "120016": {
                    msg: "",
                    field: ""
                },
                "400031": {
                    msg: "\u8bf7\u5728\u5f39\u51fa\u7684\u7a97\u53e3\u64cd\u4f5c,\u6216\u91cd\u65b0\u767b\u5f55",
                    field: ""
                },
                "400032": {
                    msg: "",
                    field: ""
                },
                "400034": {
                    msg: "",
                    field: ""
                },
                "401007": {
                    msg: "\u60a8\u7684\u624b\u673a\u53f7\u5173\u8054\u4e86\u5176\u4ed6\u5e10\u53f7\uff0c\u8bf7\u9009\u62e9\u767b\u5f55",
                    field: ""
                },
                "120021": {
                    msg: "\u767b\u5f55\u5931\u8d25,\u8bf7\u5728\u5f39\u51fa\u7684\u7a97\u53e3\u64cd\u4f5c,\u6216\u91cd\u65b0\u767b\u5f55",
                    field: ""
                },
                "500010": {
                    msg: "\u767b\u5f55\u8fc7\u4e8e\u9891\u7e41,\u8bf724\u5c0f\u65f6\u540e\u518d\u8bd5",
                    field: ""
                },
                "200010": {
                    msg: "\u9a8c\u8bc1\u7801\u4e0d\u5b58\u5728\u6216\u5df2\u8fc7\u671f",
                    field: ""
                },
                "100005": {
                    msg: "\u7cfb\u7edf\u9519\u8bef,\u8bf7\u60a8\u7a0d\u540e\u518d\u8bd5",
                    field: ""
                },
                "120019": {
                    msg: "\u8bf7\u5728\u5f39\u51fa\u7684\u7a97\u53e3\u64cd\u4f5c,\u6216\u91cd\u65b0\u767b\u5f55",
                    field: "userName"
                },
                "110024": {
                    msg: '\u6b64\u5e10\u53f7\u6682\u672a\u6fc0\u6d3b,<a href="#{gotourl}" >\u91cd\u53d1\u9a8c\u8bc1\u90ae\u4ef6</a>',
                    field: ""
                },
                "100023": {
                    msg: '\u5f00\u542fCookie\u4e4b\u540e\u624d\u80fd\u767b\u5f55,<a href="http://passport.baidu.com/v2/?ucenterfeedback#{urldata}#login"  target="_blank" >\u5982\u4f55\u5f00\u542f</a>?',
                    field: ""
                },
                "400401": {
                    msg: "",
                    field: ""
                }
            };
            j.errMsg.checkVerifycode = {
                "500002": {
                    msg: "\u60a8\u8f93\u5165\u7684\u9a8c\u8bc1\u7801\u6709\u8bef",
                    field: "verifyCode"
                },
                "500018": {
                    msg: "\u9a8c\u8bc1\u7801\u5df2\u5931\u6548\uff0c\u8bf7\u91cd\u8bd5",
                    field: "verifyCode"
                }
            };
            j.labelText.login = {
                verifyCode: "\u9a8c\u8bc1\u7801",
                verifyCodeStaErr: "\u60a8\u8f93\u5165\u7684\u9a8c\u8bc1\u7801\u6709\u8bef",
                verifyCodeLenErr: "\u60a8\u8f93\u5165\u7684\u9a8c\u8bc1\u7801\u6709\u8bef",
                captcha: "\u9a8c\u8bc1\u7801",
                captchaErr: "\u60a8\u8f93\u5165\u7684\u52a8\u6001\u5bc6\u7801\u6709\u8bef,\u8bf7\u91cd\u8bd5",
                captchaAlt: "\u9a8c\u8bc1\u7801\u56fe\u7247",
                captchaChange: "\u6362\u4e00\u5f20",
                memberPassLabel: "\u4e0b\u6b21\u81ea\u52a8\u767b\u5f55",
                login: "\u767b\u5f55",
                fgtPwd: "\u5fd8\u8bb0\u5bc6\u7801\uff1f",
                feedback: "\u95ee\u9898\u53cd\u9988",
                register: "\u7acb\u5373\u6ce8\u518c",
                phoneNum: "\u624b\u673a\u53f7",
                account: "\u90ae\u7bb1",
                userName: "\u624b\u673a/\u90ae\u7bb1/\u7528\u6237\u540d",
                password: "\u5bc6\u7801",
                passwordResetWarn: '\u5bc6\u7801\u9519\u8bef,\u60a8\u5728#{resetpwd}\u4fee\u6539\u8fc7\u5bc6\u7801,<a href="http://passport.baidu.com/v2/?ucenterfeedback#{urldata}#login"  target="_blank" >\u95ee\u9898\u53cd\u9988</a>',
                passwordResetIn: "\u4e2a\u6708\u4ee5\u5185",
                passwordResetOut: "\u4e2a\u6708\u4ee5\u524d",
                unameMailLengthError: "\u90ae\u7bb1\u8fc7\u957f,\u8bf7\u91cd\u65b0\u8f93\u5165",
                unameInputError: "\u90ae\u7bb1\u683c\u5f0f\u9519\u8bef,\u82e5\u672a\u7ed1\u5b9a\u90ae\u7bb1,\u8bf7\u4f7f\u7528\u7528\u6237\u540d\u767b\u5f55",
                smsPhone: "\u624b\u673a\u53f7",
                smsPhoneMsg: "\u8bf7\u8f93\u5165\u624b\u673a\u53f7\uff0c\u672a\u6ce8\u518c\u5c06\u81ea\u52a8\u521b\u5efa\u767e\u5ea6\u5e10\u53f7",
                smsVerifyCode: "\u52a8\u6001\u5bc6\u7801",
                logining: "\u767b\u5f55\u4e2d...",
                loginsuccess: "\u767b\u5f55\u6210\u529f",
                submitTimeup: "\u767b\u5f55\u8d85\u65f6,\u8bf7\u7a0d\u540e\u518d\u8bd5",
                backToLogin: "<<\u8fd4\u56de\u767b\u5f55",
                qrcodeTitle: "\u4e8c\u7ef4\u7801\u767b\u5f55",
                qrcodeMsg: '\u8bf7\u4f7f\u7528<a href="http://passport.baidu.com/export/app/index.html" target="_blank">\u767e\u5ea6\u5b89\u5168\u4e2d\u5fc3APP</a>\u626b\u63cf\u767b\u5f55\u3002',
                sysError: "\u7cfb\u7edf\u9519\u8bef\uff0c\u4f11\u606f\u4e00\u4f1a\u513f\uff0c\u8bf7\u7a0d\u540e\u518d\u8bd5",
                sysUpdate: "\u670d\u52a1\u6b63\u5728\u5347\u7ea7\u4e2d,\u8bf7\u60a8\u7a0d\u540e\u518d\u8bd5",
                cookieDisable: '\u5f00\u542fCookie\u4e4b\u540e\u624d\u80fd\u767b\u5f55,<a href="http://passport.baidu.com/v2/?ucenterfeedback#login"  target="_blank" >\u5982\u4f55\u5f00\u542f</a>?',
                captchaErr: "\u52a8\u6001\u5bc6\u7801\u9519\u8bef"
            };
            i.getCurrent = function() {
                return j
            }
        })(h.err);
        var h = h || window.passport || {};
        h.data = h.data || {}; (function(F) {
            var l = function() {};
            function m(p) {
                this._requests = [];
                this._value = null;
                this._exception = null;
                this._isComplete = false;
                var H = this;
                p(function(I) {
                    H._fulfillPromise(I)
                },
                function(I) {
                    H._breakPromise(I)
                })
            }
            m.prototype = {
                get_isComplete: function() {
                    return this._isComplete
                },
                get_value: function() {
                    if (!this._isComplete) {
                        return undefined
                    }
                    if (this._exception) {
                        throw this._exception
                    }
                    return this._value
                },
                call: function(I, K) {
                    var H = [];
                    for (var J = 0, p = arguments.length - 1; J < p; J++) {
                        H[J] = arguments[J + 1]
                    }
                    return this.when(function(L) {
                        return L[I].apply(L, H)
                    })
                },
                getValue: function(p) {
                    return this.when(function(H) {
                        return H[p]
                    })
                },
                setValue: function(p, H) {
                    this.whenOnly(function(I) {
                        I[p] = H
                    })
                },
                when: function(H, I, p) {
                    return m.when(this, H, I, p)
                },
                whenOnly: function(H, I, p) {
                    m.whenOnly(this, H, I, p)
                },
                success: function(H, p) {
                    return this.when(H, l, p)
                },
                fail: function(H, p) {
                    return this.when(l, H, p)
                },
                _enqueueOne: function(p) {
                    if (this._isComplete) {
                        this._notify(p)
                    } else {
                        this._requests.push(p)
                    }
                },
                _notify: function(p) {
                    if (this._exception) {
                        if (p.breakPromise) {
                            p.breakPromise(this._exception)
                        }
                    } else {
                        if (p.fulfillPromise) {
                            p.fulfillPromise(this._value)
                        }
                    }
                },
                _notifyAll: function() {
                    for (var H = 0, p = this._requests.length; H < p; H++) {
                        this._notify(this._requests[H])
                    }
                },
                _fulfillPromise: function(p) {
                    this._value = p;
                    this._exception = null;
                    this._isComplete = true;
                    this._notifyAll()
                },
                _breakPromise: function(p) {
                    this._value = null;
                    this._exception = p || new Error("An error occured");
                    this._isComplete = true;
                    this._notifyAll()
                }
            };
            m.when = function(J, H, I, p) {
                return new m(function(K, L) {
                    m.make(J)._enqueueOne({
                        fulfillPromise: function(M) {
                            if (H) {
                                K(H.call(p, M))
                            } else {
                                K(M)
                            }
                        },
                        breakPromise: function(M) {
                            if (I) {
                                try {
                                    K(I.call(p, M))
                                } catch(N) {
                                    L(N)
                                }
                            } else {
                                L(M)
                            }
                        }
                    })
                })
            };
            m.whenOnly = function(J, H, I, p) {
                m.make(J)._enqueueOne({
                    fulfillPromise: function(K) {
                        if (H) {
                            H.call(p, K)
                        }
                    },
                    breakPromise: function(K) {
                        if (I) {
                            I.call(p, K)
                        }
                    }
                })
            };
            m.make = function(p) {
                if (p instanceof m) {
                    return p
                }
                return m.immediate(p)
            };
            m.immediate = function(p) {
                return new m(function(H, I) {
                    H(p)
                })
            };
            var r = {}; (function(p) {
                var H = new RegExp("(^[\\s\\t\\xa0\\u3000]+)|([\\u3000\\xa0\\s\\t]+\x24)", "g");
                p.trim = function(I) {
                    return String(I).replace(H, "")
                };
                p.getUniqueId = function(I) {
                    return I + Math.floor(Math.random() * 2147483648).toString(36)
                };
                p.g = function(I) {
                    if (!I) {
                        return null
                    }
                    if ("string" == typeof I || I instanceof String) {
                        return document.getElementById(I)
                    } else {
                        if (I.nodeName && (I.nodeType == 1 || I.nodeType == 9)) {
                            return I
                        }
                    }
                    return null
                };
                p.getParent = function(I) {
                    I = p.g(I);
                    return I.parentElement || I.parentNode || null
                };
                p.encodeHTML = function(I) {
                    return String(I).replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#39;")
                };
                p.array = p.array || {};
                p.array.indexOf = function(M, J, L) {
                    var I = M.length,
                    K = J;
                    L = L | 0;
                    if (L < 0) {
                        L = Math.max(0, I + L)
                    }
                    for (; L < I; L++) {
                        if (L in M && M[L] === J) {
                            return L
                        }
                    }
                    return - 1
                };
                p.browser = p.browser || {};
                p.browser.opera = /opera(\/| )(\d+(\.\d+)?)(.+?(version\/(\d+(\.\d+)?)))?/i.test(navigator.userAgent) ? +(RegExp["\x246"] || RegExp["\x242"]) : undefined;
                p.insertHTML = function(L, I, K) {
                    L = p.g(L);
                    var J,
                    M;
                    if (L.insertAdjacentHTML && !p.browser.opera) {
                        L.insertAdjacentHTML(I, K)
                    } else {
                        J = L.ownerDocument.createRange();
                        I = I.toUpperCase();
                        if (I == "AFTERBEGIN" || I == "BEFOREEND") {
                            J.selectNodeContents(L);
                            J.collapse(I == "AFTERBEGIN")
                        } else {
                            M = I == "BEFOREBEGIN";
                            J[M ? "setStartBefore": "setEndAfter"](L);
                            J.collapse(M)
                        }
                        J.insertNode(J.createContextualFragment(K))
                    }
                    return L
                }
            })(r);
            F.base = r;
            var y = {}; (function(p) {
                var H = "__bdpp_pstc__" + new Date().getTime(),
                P = H + "_form",
                I = H + "_ifr";
                var R = function(V) {
                    if (typeof(V) == "object") {
                        var S = [];
                        for (var W in V) {
                            var U = V[W];
                            if (U !== undefined && U !== null) {
                                if (S.length) {
                                    S.push("&")
                                }
                                var T = encodeURIComponent(typeof(U) == "boolean" ? (U ? "1": "0") : U.toString());
                                S.push(encodeURIComponent(W), "=", T)
                            }
                        }
                        return S.join("")
                    }
                    if (typeof(V) == "string") {
                        return V
                    }
                    return null
                };
                var N = function(S, U) {
                    U = R(U);
                    if (typeof(U) == "string") {
                        var T = (/\?/g).test(S);
                        S += (T ? "&": "?") + R(U)
                    }
                    return S
                };
                var Q = function(T, S, U) {
                    T.setAttribute("type", "text/javascript");
                    U && T.setAttribute("charset", U);
                    T.setAttribute("src", S);
                    document.getElementsByTagName("head")[0].appendChild(T)
                };
                var J = function(T) {
                    if (T.clearAttributes) {
                        T.clearAttributes()
                    } else {
                        for (var S in T) {
                            if (T.hasOwnProperty(S)) {
                                delete T[S]
                            }
                        }
                    }
                    if (T && T.parentNode) {
                        T.parentNode.removeChild(T)
                    }
                    T = null
                };
                var L = function(S, ae, af) {
                    var aa = document.createElement("SCRIPT"),
                    Z = "bd__cbs__",
                    ac,
                    W,
                    ag = af || {},
                    V = ag.charset,
                    X = ag.queryField || "callback",
                    ad = ag.timeOut || 0,
                    T,
                    U = new RegExp("(\\?|&)" + X + "=([^&]*)"),
                    Y;
                    ac = r.getUniqueId(Z);
                    window[ac] = ab(0);
                    if (ad) {
                        T = setTimeout(ab(1), ad)
                    }
                    S = S.replace(U, "\x241" + X + "=" + ac);
                    if (S.search(U) < 0) {
                        S += (S.indexOf("?") < 0 ? "?": "&") + X + "=" + ac
                    }
                    Q(aa, S, V);
                    function ab(ah) {
                        return function() {
                            try {
                                if (ah) {
                                    ag.onfailure && ag.onfailure()
                                } else {
                                    ae.apply(window, arguments);
                                    clearTimeout(T)
                                }
                                window[ac] = null;
                                delete window[ac]
                            } catch(ai) {} finally {
                                J(aa)
                            }
                        }
                    }
                };
                var O = function(V, U) {
                    var S = [];
                    S.push("<form id='", P, "' target='", I, "' ");
                    S.push("action='", r.encodeHTML(V), "' method='post'>");
                    for (var X in U) {
                        if (U.hasOwnProperty(X)) {
                            var W = U[X];
                            if (W !== undefined && W !== null) {
                                var T = r.encodeHTML(typeof(W) == "boolean" ? (W ? "1": "0") : W);
                                S.push("<input type='hidden' name='", r.encodeHTML(X), "' value='", T, "' />")
                            }
                        }
                    }
                    S.push("</form>");
                    return S.join("")
                };
                var M = function(S, W, aa, ab) {
                    ab = ab || {};
                    var Z = ab.timeOut || 0,
                    T = false,
                    Y = r.getUniqueId("bd__pcbs__");
                    W[ab.queryField || "callback"] = "parent." + Y;
                    var U = O(S, W);
                    if (r.g(P)) {
                        r.getParent(P).innerHTML = U
                    } else {
                        var V = [];
                        V.push("<div id='", H, "' style='display:none;'>");
                        V.push("<div>", U, "</div>");
                        V.push("<iframe name='", I, "' src='" + ((window.location ? window.location.protocol.toLowerCase() : document.location.protocol.toLowerCase()) == "https:" ? "https://passport.baidu.com/passApi/html/_blank.html": "about:blank") + "' style='display:none;'></iframe>");
                        V.push("</div>");
                        r.insertHTML(document.body, "beforeEnd", V.join(""))
                    }
                    window[Y] = X();
                    if (Z) {
                        T = setTimeout(X(1), Z)
                    }
                    function X(ac) {
                        return function() {
                            try {
                                if (ac) {
                                    ab.onfailure && ab.onfailure()
                                } else {
                                    aa.apply(window, arguments);
                                    T && clearTimeout(T)
                                }
                                window[Y] = null;
                                delete window[Y]
                            } catch(ad) {}
                        }
                    }
                    r.g(P).submit()
                };
                p.jsonp = function(U, V, T) {
                    T = T || {};
                    var S = U;
                    return new m(function(X, W) {
                        U = N(U, V);
                        L(U, 
                        function(Y) {
                            if (T.processData) {
                                Y = T.processData(Y)
                            }
                            X && X(Y)
                        },
                        {
                            charset: T.charset,
                            queryField: T.queryField,
                            timeOut: T.timeOut,
                            onfailure: function() {
                                W && W()
                            }
                        })
                    })
                };
                p.submit = function(T, U, S) {
                    if (T && U) {
                        return new m(function(W, V) {
                            M(T, U, 
                            function(X) {
                                if (S.processData) {
                                    X = S.processData(X)
                                }
                                W && W(X)
                            },
                            S)
                        })
                    }
                };
                var K = [];
                p.load = function(S) {
                    return new m(function(W, V) {
                        var U = K.push(new Image) - 1,
                        T = false,
                        X = setTimeout(function() {
                            T = true;
                            W && W()
                        },
                        1000);
                        K[U].onload = function() {
                            clearTimeout(X);
                            if (!T) {
                                W && W()
                            }
                            T = true;
                            K[U] = K[U].onload = null
                        };
                        K[U].src = S
                    })
                }
            })(y);
            var o = "https://passport.baidu.com",
            B = {
                getApiInfo: "/v2/api/?getapi",
                getLoginHistory: "/v2/api/?loginhistory",
                loginCheck: "/v2/api/?logincheck",
                getVerifyCodeStr: "/v2/?reggetcodestr",
                checkUserName: "/v2/?regnamesugg",
                checkPassword: "/v2/?regpwdcheck",
                checkMail: "/v2/?regmailcheck",
                isUserNoName: "/v2/api/?ucenteradduname",
                checkPhone: "/v2/?regphonecheck",
                sendPhoneCode: "/v2/?regphonesend",
                multiBind: "/v2/?multiaccountassociate",
                multiUnbind: "/v2/?multiaccountdisassociate",
                multiCheckUserName: "/v2/?multiaccountusername",
                multiGetaccounts: "/v2/?multiaccountget",
                multiSwitchuser: "/v2/?loginswitch",
                checkVerifycode: "/v2/?checkvcode",
                getRsaKey: "/v2/getpublickey",
                authwidGetverify: "/v2/sapi/authwidgetverify"
            },
            u = {
                login: "/v2/api/?login",
                reg: "/v2/api/?reg",
                fillUserName: "/v2/api/?ucenteradduname",
                regPhone: "/v2/api/?regphone"
            },
            C = {
                getApiInfo: {
                    apiType: "class"
                },
                login: {
                    memberPass: "mem_pass",
                    safeFlag: "safeflg",
                    isPhone: "isPhone",
                    timeSpan: "ppui_logintime",
                    logLoginType: "logLoginType"
                },
                fillUserName: {
                    selectedSuggestName: "pass_fillinusername_suggestuserradio",
                    timeSpan: "ppui_fillusernametime"
                },
                reg: {
                    password: "loginpass",
                    timeSpan: "ppui_regtime",
                    suggestIndex: "suggestIndex",
                    suggestType: "suggestType",
                    selectedSuggestName: "pass_reg_suggestuserradio_0",
                    logRegType: "logRegType"
                },
                regPhone: {
                    password: "loginpass",
                    timeSpan: "ppui_regtime",
                    suggestIndex: "suggestIndex",
                    suggestType: "suggestType",
                    selectedSuggestName: "pass_reg_suggestuserradio_0",
                    logRegType: "logRegType"
                }
            },
            w = {
                loginCheck: {
                    isPhone: function(H, p) {
                        return H ? "true": "false"
                    }
                },
                login: {
                    memberPass: function(H, p) {
                        return (H ? "on": "")
                    }
                }
            },
            G = {
                checkPassword: {
                    fromreg: 1
                },
                reg: {
                    registerType: 1,
                    verifypass: function(p) {
                        return p.password
                    }
                }
            },
            z = {
                password: true
            },
            k = {
                login: function(p) {}
            },
            j = {
                checkUserName: "reg",
                checkMail: "reg",
                checkPhone: "regPhone",
                sendPhoneCode: "regPhone",
                multiCheckUserName: "multiBind",
                multiSwitchuser: "changeUser",
                checkVerifycode: "checkVerifycode"
            },
            s = h.err.getCurrent().errMsg || h.err.getCurrent(),
            D = {};
            F.setContext = function(p) {
                D.product = p.product || D.product;
                D.charset = p.charset || D.charset;
                D.staticPage = p.staticPage || D.staticPage;
                D.token = p.token || D.token;
                D.subpro = p.subpro || D.subpro
            };
            function n(I, p, H) {
                if (p) {
                    if (!H) {
                        return function(J) {
                            return y.jsonp(o + p, A(J, I, C[I], w[I], false), {
                                charset: "utf-8",
                                processData: function(K) {
                                    return x(I, K)
                                }
                            })
                        }
                    } else {
                        return function(J) {
                            J = J || {};
                            return y.submit(o + p, A(J, I, C[I], w[I], true), {
                                charset: "utf-8",
                                processData: function(K) {
                                    if (K) {
                                        for (var M in K) {
                                            if (K.hasOwnProperty(M)) {
                                                var L = K[M];
                                                if (L) {
                                                    K[M] = decodeURIComponent(L)
                                                }
                                            }
                                        }
                                    }
                                    return x(I, K)
                                }
                            })
                        }
                    }
                } else {
                    return l
                }
            }
            function A(K, O, L, M, N) {
                var R = (N ? {
                    staticpage: D.staticPage,
                    charset: D.charset || document.characterSet || document.charset || ""
                }: {}),
                I = G[O];
                if (I) {
                    for (var H in I) {
                        if (I.hasOwnProperty(H)) {
                            var Q = I[H];
                            R[H] = (typeof(Q) == "function" ? Q(K) : Q)
                        }
                        if (H == "verifypass") {
                            R[H] = decodeURIComponent(R[H])
                        }
                    }
                }
                R.token = D.token;
                R.tpl = D.product || "";
                R.subpro = D.subpro;
                R.apiver = "v3";
                R.tt = new Date().getTime();
                if (K) {
                    L = L || {};
                    M = M || {};
                    for (var H in K) {
                        if (K.hasOwnProperty(H)) {
                            var P = M[H],
                            J = ( !! P ? P(K[H], K) : K[H]);
                            if (typeof(J) == "string") {
                                if (N) {
                                    J = decodeURIComponent(J)
                                }
                                if (!z[H]) {
                                    J = r.trim(J)
                                }
                            }
                            R[L[H] || H.toLowerCase()] = J
                        }
                    }
                }
                return R
            }
            function x(K, H) {
                if (H) {
                    var L = k[K];
                    if (L) {
                        L(H)
                    }
                    var I = H.errInfo,
                    J = H,
                    p = J;
                    if (!I) {
                        I = {
                            no: H.err_no,
                            msg: H.err_msg || ""
                        };
                        delete J.err_no;
                        delete J.err_msg;
                        p = {
                            data: J,
                            errInfo: E(K, I, J)
                        }
                    } else {
                        J.errInfo = E(K, I, J)
                    }
                    return p
                }
                return H
            }
            function E(K, H, J) {
                var p = s[j[K] || K];
                if (p && H && (H.no != 0)) {
                    var I = p[H.no] || p["-1"];
                    if (I) {
                        var L = I.msg;
                        H.msg = L;
                        H.field = I.field
                    }
                }
                return H
            }
            for (var t in B) {
                if (B.hasOwnProperty(t)) {
                    F[t] = n(t, B[t])
                }
            }
            for (var t in u) {
                if (u.hasOwnProperty(t)) {
                    F[t] = n(t, u[t], true)
                }
            }
            function i(H) {
                if (H) {
                    var J = H.errInfo,
                    K = H;
                    if (!J) {
                        for (var L in H) {
                            if (H.hasOwnProperty(L)) {
                                var I = H[L];
                                if (I) {
                                    H[L] = decodeURIComponent(I)
                                }
                            }
                        }
                    }
                    if (!J) {
                        J = {
                            no: H.err_no,
                            msg: H.err_msg || ""
                        };
                        delete K.err_no;
                        delete K.err_msg;
                        H = {
                            data: K,
                            errInfo: J
                        }
                    }
                }
                return H
            }
            F.jsonp = function(p, H) {
                if (p.indexOf("http") != 0) {
                    p = o + p
                }
                H = H || {};
                H.apiver = "v3";
                H.tt = new Date().getTime();
                return y.jsonp(p, H, {
                    charset: "utf-8",
                    processData: function(I) {
                        return i(I)
                    }
                })
            };
            F.post = function(p, H) {
                H = H || {};
                if (H.apitype == "wap") {
                    p = p
                } else {
                    p = o + p
                }
                H.staticpage = H.staticpage || D.staticPage;
                H.charset = H.charset || D.charset || document.characterSet || document.charset || "";
                H.token = H.token || D.token;
                H.tpl = H.tpl || D.product;
                return y.submit(p, H, {
                    charset: "utf-8",
                    processData: function(I) {
                        return i(I)
                    }
                })
            };
            F.request = y
        })(h.data);
        var h = h || window.passport || {};
        h.analysis = h.analysis || {}; (function(j) {
            var i = function(p, o) {
                var k = p.config.diaPassLogin ? "dialogLogin": "basicLogin";
                var u = p.config.loginMerge ? 1: 0;
                var r = p.config.product || "isnull";
                var w = window.location ? window.location.protocol.toLowerCase() : document.location.protocol.toLowerCase();
                var l = "";
                var x = "&_t=" + new Date().getTime();
                for (var n in o) {
                    l = l + "&" + n + "=" + o[n]
                }
                if (w == "http:") {
                    var s = "http://nsclick.baidu.com/v.gif?pid=111&url=&logintype=" + k + "&merge=" + u + "&tpl=" + r + l + x
                } else {
                    if (w == "https:") {
                        var s = "https://passport.baidu.com/img/v.gif?cdnversion=00558676.gif?logintype=" + k + "&merge=" + u + "&tpl=" + r + l + x
                    }
                }
                if (s) {
                    var m = new Image();
                    m.onload = m.onerror = function() {
                        m.onload = m.onerror = null;
                        m = null
                    };
                    m.src = s
                }
            };
            j.login = {
                render: function(m) {
                    i(m, {
                        type: "firstrender",
                        loginurl: encodeURIComponent(document.location.href)
                    });
                    c(m.getPhoenixElement("pass_phoenix_list_login")).on("click", 
                    function(p) {
                        var n = c(p.target),
                        o;
                        if (n && n.attr("title")) {
                            switch (n.attr("title")) {
                            case "鏅€氱櫥褰�":
                                o = "normal";
                                break;
                            case "浜岀淮鐮佺櫥褰�":
                                o = "qrcode";
                                break;
                            case "鐭俊鐧诲綍":
                                o = "sms";
                                break;
                            case "QQ甯愬彿":
                                o = "qq";
                                break;
                            case "鏂版氮寰崥":
                                o = "weibo";
                                break;
                            case "浜轰汉缃�":
                                o = "renren";
                                break;
                            case "鑵捐寰崥":
                                o = "tqq";
                                break;
                            case "椋炰俊":
                                o = "fetion";
                                break
                            }
                            i(m, {
                                phoenix: o
                            })
                        }
                    });
                    var k = m.getElement();
                    c(k).on("click", 
                    function(n) {
                        if (!m.loginfirstclick) {
                            m.loginfirstclick = true;
                            i(m, {
                                type: "loginfirst"
                            })
                        }
                    });
                    var l = m.getElement("form");
                    c(l).on("submit", 
                    function(n) {
                        if (!m.loginfirstsubmit) {
                            m.loginfirstsubmit = true;
                            i(m, {
                                type: "loginfirstsubmit"
                            })
                        }
                    })
                },
                loginSuccess: function(l, k) {
                    i(l, {
                        type: "loginsuccess"
                    })
                },
                loginError: function(l, k) {},
                validateError: function(k, l) {
                    if (l.validate) {
                        i(k, {
                            errno: encodeURIComponent(l.validate.msg),
                            type: "loginerrno"
                        })
                    }
                    return {
                        preventEvent: false,
                        preventDefault: false
                    }
                },
                fieldKeyup: function(l, k) {
                    if (!l.KEYUPFLAG) {
                        i(l, {
                            type: "typein"
                        });
                        l.KEYUPFLAG = true
                    }
                }
            }
        })(h.analysis);
        var h = h || window.passport || {};
        h.hook = h.hook || {}; (function(s) {
            var w = (window.location ? ((window.location.protocol.toLowerCase() == "http:") ? "http://passport.baidu.com": "https://passport.baidu.com") : ((document.location.protocol.toLowerCase() == "http:") ? "http://passport.baidu.com": "https://passport.baidu.com")),
            l = {
                uni_armorwidget: "/passApi/js/uni_armorwidget_f2746e4a.js",
                uni_forceverify: "/passApi/js/uni_forceverify_fb33da4a.js",
                uni_wrapper: "/passApi/js/uni_wrapper_8b26a799.js"
            },
            k = "uni_wrapper",
            u = "uni_forceverify",
            r = "uni_armorwidget",
            x = {};
            var t = function(A, y) {
                var z = A.fire("loginSuccess", y);
                if (!z) {
                    return
                }
                if (window.location) {
                    window.location.href = y.rsp.data.u
                } else {
                    document.location.href = y.rsp.data.u
                }
            };
            var m = function(y) {
                return h.data.request.load(document.location.protocol + "//user.hao123.com/static/crossdomain.php?bdu=" + encodeURIComponent(y) + "&t=" + new Date().getTime())
            };
            function p(L) {
                var I = this;
                var G = {
                    "120016": {
                        isLogin: false,
                        msg: "鎮ㄧ殑甯愬彿瀛樺湪瀹夊叏椋庨櫓锛屾垜浠凡缁忎负鎮ㄩ噰鍙栦繚鎶ょ瓥鐣ワ紝寤鸿鎮ㄥ厛缁戝畾瀵嗕繚鎵嬫満銆�"
                    },
                    "400032": {
                        isLogin: true,
                        msg: "蹇潵缁戝畾瀵嗕繚宸ュ叿鍚э紝鎻愬崌甯愬彿瀹夊叏鎬х殑鍚屾椂鍙互蹇€熸壘鍥炲瘑鐮併€�"
                    },
                    "400034": {
                        isLogin: false,
                        msg: {
                            phone: "璇风粦瀹氭偍鐨勬墜鏈哄彿鐮佷綔涓烘偍鐨勫瘑淇濇墜鏈猴紝鎻愬崌甯愬彿瀹夊叏鎬х殑鍚屾椂杩樺彲浠ュ揩閫熸壘鍥炲瘑鐮併€�",
                            email: "璇风粦瀹氫竴涓偍鐨勫父鐢ㄩ偖绠变綔涓烘偍鐨勫瘑淇濋偖绠憋紝鎻愬崌甯愬彿瀹夊叏鎬х殑鍚屾椂杩樺彲浠ュ揩閫熸壘鍥炲瘑鐮併€�"
                        }
                    }
                } [L.errno];
                var H = L.args,
                J = L.title,
                A = G.msg,
                y = L.auth_title,
                M = L.auth_msg,
                F = G.isLogin,
                E = L.cfg;
                var K,
                D;
                var z = function(Q, O, S) {
                    var N = O.args,
                    R = {
                        action: O.type || "init",
                        u: Q.config.u,
                        tpl: Q.config.product,
                        ltoken: N.rsp.data.ltoken,
                        lstr: N.rsp.data.lstr
                    },
                    P = "";
                    Q.REQUESTBINDTOKENURL = "/v2/?loginspmbindsecureinfo";
                    h.data.jsonp("https://passport.baidu.com" + Q.REQUESTBINDTOKENURL, R).success(function(T) {
                        if (T.errInfo.no == 0) {
                            S && S({
                                bindEmailToken: T.data.bindEmailToken,
                                bindMobileToken: T.data.bindMobileToken,
                                authsid: T.data.authsid,
                                loginproxy: T.data.loginproxy,
                                otherValue: R
                            })
                        } else {
                            alert(Q.lang.sysError)
                        }
                    })
                };
                var C = function(O) {
                    var N = ((typeof A).toLowerCase() == "string") ? A: A.email;
                    if (F) {
                        N += "鎮ㄥ彲浠�<a class='bindLink bindJumpEmail'>璺宠繃姝ゆ楠�</a>鎴�<a class='bindLink bindPhoneBtn'>缁戝畾瀵嗕繚鎵嬫満</a>銆�"
                    } else {
                        N += "鎮ㄤ篃鍙互<a class='bindLink bindPhoneBtn'>缁戝畾瀵嗕繚鎵嬫満</a>銆�"
                    }
                    return h.pop.ArmorWidget("bindemail", {
                        token: O.bindEmailToken,
                        authsid: O.authsid,
                        title: J || "缁戝畾瀵嗕繚閭",
                        otherValue: O.otherValue,
                        msg: N,
                        subpro: I.config.subpro,
                        onSubmitSuccess: function(Q, P) {
                            var Q = Q;
                            z(I, {
                                args: H,
                                type: "check"
                            },
                            function(R) {
                                Q && Q.hide && Q.hide();
                                H.isCompleted = true;
                                if (R.loginproxy) {
                                    h.data.jsonp(R.loginproxy).success(function(S) {
                                        E.onCompleted && E.onCompleted(S, 
                                        function() {
                                            E.onCancel && E.onCancel(H)
                                        })
                                    })
                                } else {
                                    E.onCancel && E.onCancel(H)
                                }
                            })
                        },
                        onRender: function() {
                            var P = this;
                            c(".bindPhoneBtn").on("click", 
                            function() {
                                P.close();
                                D.show()
                            });
                            c(".bindJumpEmail").on("click", 
                            function(Q) {
                                Q.preventDefault();
                                P.close();
                                H.isCompleted = true;
                                E.onCancel && E.onCancel(H)
                            });
                            c("#" + P.getId("header_a")).on("click", 
                            function() {
                                if (F) {
                                    H.isCompleted = true;
                                    E.onCancel && E.onCancel(H)
                                }
                            })
                        }
                    })
                };
                var B = function(O) {
                    var N = ((typeof A).toLowerCase() == "string") ? A: A.phone;
                    if (F && O.bindEmailToken) {
                        N += "鎮ㄥ彲浠�<a class='bindLink bindJumpPhone'>璺宠繃姝ゆ楠�</a>鎴�<a class='bindLink bindEmailBtn'>缁戝畾瀵嗕繚閭</a>銆�"
                    } else {
                        if (O.bindEmailToken) {
                            N += "鎮ㄤ篃鍙互<a class='bindLink bindEmailBtn'>缁戝畾瀵嗕繚閭</a>銆�"
                        }
                    }
                    return h.pop.ArmorWidget("bindmobile", {
                        token: O.bindMobileToken,
                        authsid: O.authsid,
                        title: J || "缁戝畾瀵嗕繚鎵嬫満",
                        otherValue: O.otherValue,
                        msg: N,
                        subpro: I.config.subpro,
                        onSubmitSuccess: function(Q, P) {
                            var Q = Q;
                            z(I, {
                                args: H,
                                type: "check"
                            },
                            function(R) {
                                Q && Q.hide && Q.hide();
                                H.isCompleted = true;
                                if (R.loginproxy) {
                                    h.data.jsonp(R.loginproxy).success(function(S) {
                                        E.onCompleted && E.onCompleted(S, 
                                        function() {
                                            E.onCancel && E.onCancel(H)
                                        })
                                    })
                                } else {
                                    E.onCancel && E.onCancel(H)
                                }
                            })
                        },
                        onRender: function(P) {
                            var P = this;
                            c(".bindEmailBtn").on("click", 
                            function(Q) {
                                Q.preventDefault();
                                P.close();
                                K = K || C(O);
                                K.show()
                            });
                            c(".bindJumpPhone").on("click", 
                            function(Q) {
                                Q.preventDefault();
                                P.close();
                                H.isCompleted = true;
                                E.onCancel && E.onCancel(H)
                            });
                            c("#" + P.getId("header_a")).on("click", 
                            function() {
                                if (F) {
                                    H.isCompleted = true;
                                    E.onCancel && E.onCancel(H)
                                }
                            })
                        }
                    })
                };
                z(I, {
                    args: H,
                    type: "init"
                },
                function(N) {
                    h._use(r, l[r], 
                    function() {
                        if (N.bindMobileToken) {
                            D = B(N);
                            D.show()
                        } else {
                            if (N.bindEmailToken) {
                                K = C(N);
                                K.show()
                            } else {
                                alert(I.lang.sysError)
                            }
                        }
                    })
                })
            }
            function j(G) {
                var D = this,
                z,
                F,
                y = G.rspData,
                A = G.cfg,
                C = G.args;
                var B = function(H) {
                    var H = H || "绯荤粺妫€娴嬪埌鎮ㄧ殑甯愬彿鐤戜技琚洍锛屽瓨鍦ㄥ畨鍏ㄩ闄┿€傝灏藉揩淇敼瀵嗙爜銆�";
                    return '<div class="passport-forceverify-risk"><p class="passport-forceverify-risk-msg">' + H + '</p><div  class="passport-forceverify-risk-con clearfix"><a class="passport-forceverify-risk-btn" href="http://passport.baidu.com/v2/account/password" target="_blank" >绔嬪嵆淇敼</a><a class="passport-forceverify-risk-next" id="passport_forceverify_risk_next" href="###">涓嬫鎻愰啋</a></div></div>'
                };
                if (y && y.secstate) {
                    if (y.secstate == "risk") {
                        z = B("鎮ㄧ殑甯愬彿鐤戜技琚洍锛屽瓨鍦ㄤ弗閲嶇殑瀹夊叏椋庨櫓锛岃鎮ㄥ敖蹇慨鏀瑰瘑鐮併€�<br/>* 鍦ㄦ偍鎴愬姛淇敼瀵嗙爜鍓嶏紝鎮ㄦ瘡娆＄櫥褰曢兘闇€瑕佽繘琛屽畨鍏ㄩ獙璇�");
                        F = '绯荤粺鐩戞祴鍒版偍鐨勫笎鍙风枒浼艰鐩楋紝涓虹‘淇濇湰浜烘搷浣滐紝璇峰厛杩涜瀹夊叏楠岃瘉銆傚鏋滀互涓嬪瘑淇濆伐鍏蜂笉鏄偍鏈汉缁戝畾锛屾垨宸茬粡琚洍鍙疯€呮洿鏀癸紝璇风洿鎺ヨ繘鍏�<a href="#" id="passport_forceverify_risk_appeal" target="_blank">浜哄伐鐢宠瘔</a>'
                    } else {
                        if (y.secstate == "cheat") {
                            F = "鎮ㄧ殑甯愬彿鐤戜技琚洍锛屽瓨鍦ㄤ弗閲嶇殑瀹夊叏椋庨櫓锛岃鍏堣繘琛屽畨鍏ㄩ獙璇侊紝楠岃瘉鎴愬姛鍚庢偍鍙互姝ｅ父浣跨敤璇ュ笎鍙枫€�"
                        } else {
                            F = "鐢变簬鎮ㄧ殑甯愬彿瀛樺湪瀹夊叏椋庨櫓锛屼负浜嗙‘淇濇偍鐨勫笎鍙峰畨鍏紝璇峰厛杩涜瀹夊叏楠岃瘉锛岄獙璇佹垚鍔熷悗鎮ㄥ彲浠ユ甯镐娇鐢ㄨ甯愬彿銆�"
                        }
                    }
                }
                var E = {
                    "400031": {
                        title: "鐧诲綍淇濇姢",
                        msg: "鎮ㄧ殑甯愬彿宸插紑鍚櫥褰曚繚鎶ゆ湇鍔★紝鐧诲綍鍓嶈鍏堣繘琛屽畨鍏ㄩ獙璇併€�"
                    },
                    "5": {
                        title: "鐧诲綍瀹夊叏楠岃瘉",
                        msg: "鎮ㄦ墍澶勭殑缃戠粶鐜瀛樺湪瀹夊叏椋庨櫓锛屼负淇濊瘉甯愬彿瀹夊叏锛岃鍏堣繘琛屽畨鍏ㄩ獙璇�",
                        onSuccess: function(H, J) {
                            var I = y.gotourl + "&authsid=" + J.authsid;
                            h.data.jsonp(I).success(function(K) {
                                D._ownerDialog && D._ownerDialog.show();
                                H.hide();
                                if (K.errInfo.no == 0 || K.data.errno == 0) {
                                    D.getElement("error").innerHTML = '璇烽噸鏂扮櫥闄嗭紝鎴�<a href="https://passport.baidu.com/?getpass_index" target="_blank">鎵惧洖瀵嗙爜</a>'
                                } else {
                                    D.getElement("error").innerHTML = D.lang.sysError
                                }
                            })
                        },
                        onGetapiError: function() {
                            D.getElement("error").innerHTML = "鎮ㄦ墍澶勭殑缃戠粶鐜瀛樺湪瀹夊叏椋庨櫓锛岃10鍒嗛挓鍐嶈瘯"
                        }
                    },
                    "120019": {
                        title: "鐧诲綍瑙ｅ喕楠岃瘉",
                        msg: "鎮ㄦ渶杩戝瘑鐮佽緭鍏ラ敊璇繃浜庨绻侊紝涓轰繚璇佸笎鍙峰畨鍏紝璇峰厛杩涜瀹夊叏楠岃瘉",
                        onSuccess: function(H, J) {
                            var I = y.gotourl + "&authsid=" + J.authsid;
                            h.data.jsonp(I).success(function(K) {
                                D._ownerDialog && D._ownerDialog.show();
                                H.hide();
                                if (K.errInfo.no == 0 || K.data.errno == 0) {
                                    D.getElement("error").innerHTML = '璇烽噸鏂扮櫥闄嗭紝鎴�<a href="https://passport.baidu.com/?getpass_index" target="_blank">鎵惧洖瀵嗙爜</a>'
                                } else {
                                    D.getElement("error").innerHTML = D.lang.sysError
                                }
                            })
                        },
                        onGetapiError: function() {
                            D.getElement("error").innerHTML = '鐧诲綍瀵嗙爜閿欒宸茶揪涓婇檺锛屾偍鍙互<a href="https://passport.baidu.com/?getpass_index" target="_blank">鎵惧洖瀵嗙爜</a>鎴�3灏忔椂鍚庡啀璇�'
                        }
                    },
                    "120021": {
                        title: "瀹夊叏楠岃瘉",
                        msg: F,
                        defaultHTML: z,
                        onSuccess: function(H) {
                            h.data.jsonp(y.loginproxy).success(function(I) {
                                H.show();
                                if (I.errInfo.no == 0) {
                                    C.isCompleted = true;
                                    if (z) {
                                        H.getElement("article").innerHTML = z;
                                        c(H.getElement("header_a")).on("click", 
                                        function() {
                                            H.hide();
                                            A.onCompleted && A.onCompleted(I, 
                                            function() {
                                                A.onCancel && A.onCancel(C)
                                            })
                                        });
                                        c(document.getElementById("passport_forceverify_risk_next")).on("click", 
                                        function() {
                                            H.hide();
                                            A.onCompleted && A.onCompleted(I, 
                                            function() {
                                                A.onCancel && A.onCancel(C)
                                            })
                                        })
                                    } else {
                                        H.hide();
                                        A.onCompleted && A.onCompleted(I, 
                                        function() {
                                            A.onCancel && A.onCancel(C)
                                        })
                                    }
                                } else {
                                    alert(D.lang.sysError)
                                }
                            });
                            return false
                        },
                        onRender: function(H) {
                            if (document.getElementById("passport_forceverify_risk_appeal")) {
                                document.getElementById("passport_forceverify_risk_appeal").href = H.url_forgot
                            }
                        }
                    },
                    riskCheat: {
                        token: "risk",
                        title: "瀹夊叏楠岃瘉",
                        msg: F,
                        defaultHTML: z,
                        onRender: function(H) {
                            c(document.getElementById("passport_forceverify_risk_next")).on("click", 
                            function() {
                                H.hide();
                                A.onCancel && A.onCancel()
                            });
                            if (document.getElementById("passport_forceverify_risk_appeal")) {
                                document.getElementById("passport_forceverify_risk_appeal").href = H.url_forgot
                            }
                        }
                    }
                } [G.errno];
                h._use(u, l[u], 
                function() {
                    forceverifyLoginverify = h.pop.Forceverify({
                        token: y.authtoken,
                        title: E.title,
                        msg: E.msg,
                        subpro: D.config.subpro,
                        onRender: function(H) {
                            E.onRender && E.onRender(H)
                        },
                        onSubmitSuccess: function(H, I) {
                            if (E.onSuccess) {
                                E.onSuccess(H, I);
                                return
                            }
                            var J = y.loginproxy;
                            h.data.jsonp(J).success(function(K) {
                                if (K.errInfo.no == 0 || K.data.errno == 0) {
                                    H.hide();
                                    C.isCompleted = true;
                                    A.onCompleted && A.onCompleted(K, 
                                    function() {
                                        A.onCancel && A.onCancel(C)
                                    })
                                } else {
                                    H.hide();
                                    D._ownerDialog && D._ownerDialog.show();
                                    D.getElement("error").innerHTML = D.lang.sysError
                                }
                            })
                        },
                        onGetapiError: function(H) {
                            D._ownerDialog && D._ownerDialog.show();
                            if (E.onGetapiError) {
                                E.onGetapiError(H);
                                return
                            }
                            D.getElement("error").innerHTML = D.lang.sysError
                        },
                        onHide: function() {
                            A.onCancel && A.onCancel();
                            if (D.config.loginMerge && D.getElement("isPhone")) {
                                D.getElement("isPhone").value = ""
                            }
                        }
                    },
                    true)
                })
            }
            function n(z) {
                var E = this,
                B = z.rspData;
                var y = [],
                D = B.accounts.split(";");
                for (var A = 0; A < D.length; A++) {
                    var C = D[A].split(",");
                    y.push({
                        username: C[0],
                        portrait: C[1]
                    })
                }
                h._load(w + l[k], true, 
                function() {
                    var F = h.pop.init({
                        type: "loginMultichoice",
                        tangram: true,
                        apiOpt: {
                            phone: B.userName,
                            userList: y
                        },
                        onChoicedUser: function(H, G) {
                            F.hide();
                            E._ownerDialog && E._ownerDialog.show();
                            E.getElement("userName").value = G.username;
                            E.getElement("isPhone").value = "false";
                            if (E.config.loginMerge && E.getElement("loginMerge")) {
                                E.getElement("loginMerge").value = ""
                            }
                            if (E.currentLoginType == "sms") {
                                E.getElement("smsHiddenFields_switchuname").value = G.username;
                                E._submitSmsForm()
                            } else {
                                if (!E.config.loginMerge) {
                                    E._collectData();
                                    E.switchTo("normal");
                                    E._restoreData("phone")
                                }
                                E.submit()
                            }
                        },
                        onHide: function() {
                            E.getElement("userName").value = B.userName;
                            E.getElement("isPhone").value = ""
                        }
                    });
                    F.show()
                })
            }
            function i(E, G, F) {
                var C = G.rsp.errInfo.no,
                z = G.rsp.data,
                F = F || {},
                H = z && (z.secstate == "risk" || z.secstate == "cheat");
                if (C == 401007) {
                    var y = E.fire("beforeWarning", G);
                    if (!y) {
                        return
                    }
                    E.getElement("error").innerHTML = "";
                    E._ownerDialog && E._ownerDialog.hide("unHide");
                    n.apply(E, [{
                        rspData: z
                    }]);
                    return false
                }
                if (C == 120016 || C == 400032 || C == 400034) {
                    var y = E.fire("beforeWarning", G);
                    if (!y) {
                        return
                    }
                    E.getElement("error").innerHTML = "";
                    E._ownerDialog && E._ownerDialog.hide("unHide");
                    p.apply(E, [{
                        errno: C,
                        args: G,
                        cfg: F
                    }]);
                    if (z.hao123Param) {
                        m(z.hao123Param)
                    }
                    return false
                }
                if (C == 5 || C == 400031 || C == 120019 || C == 120021 || H) {
                    var D = null,
                    A = null,
                    B = null;
                    var y = E.fire("beforeWarning", {
                        args: G
                    });
                    if (!y) {
                        return
                    }
                    E.getElement("error").innerHTML = "";
                    E._ownerDialog && E._ownerDialog.hide("unHide");
                    o(w + "/passApi/css/uni_forceverify_f606441b.css");
                    j.apply(E, [{
                        args: G,
                        rspData: z,
                        cfg: F,
                        errno: ((H && C != 120021) ? "riskCheat": C)
                    }]);
                    return false
                }
                return true
            }
            function o(z) {
                if (!x[z]) {
                    x[z] = true;
                    var y = document.createElement("link");
                    y.rel = "stylesheet";
                    y.type = "text/css";
                    y.href = z;
                    document.getElementsByTagName("head")[0].appendChild(y)
                }
                return true
            }
            s.login = {
                loginSuccess: function(z, y) {
                    m(y.rsp.data.hao123Param).success(function() {
                        var A = i(z, y, {
                            onCancel: function() {
                                t(z, y)
                            }
                        });
                        if (A) {
                            t(z, y)
                        }
                    });
                    return {
                        preventEvent: true,
                        preventDefault: true
                    }
                },
                loginError: function(z, y) {
                    i(z, y, {
                        onCompleted: function(A, B) {
                            m(A.data.hao123Param).success(B)
                        },
                        onCancel: function(A) {
                            if (A && A.isCompleted) {
                                t(z, {
                                    rsp: A.rsp
                                })
                            }
                        }
                    });
                    return {
                        preventEvent: false,
                        preventDefault: false
                    }
                },
                validateAllError: function(y, z) {
                    var A = z.validates ? y.getElement(z.validates[z.validates.length - 1].field) : "";
                    if (A && A.focus) {
                        A.focus()
                    }
                    return {
                        preventEvent: false,
                        preventDefault: false
                    }
                }
            }
        })(h.hook);
        g.passport = c.lang.createClass(function() {
            this._validateInfo = {}
        },
        {
            type: "magic.passport",
            superClass: g.Base
        }).extend({
            _getRegularField: function(i, k) {
                var o = i.pwd ? "password": "text",
                n = this,
                p = ((k && k == "reg") || (!k && i.field == "userName")) ? ' autocomplete	="off"': "",
                j = i.maxLength ? 'maxlength="' + i.maxLength + '"': "",
                r = i.tip ? i.tip: "",
                l = i.field == "verifycode" ? "none": "";
                var m = '<p id="' + n.$getId(i.field + "Wrapper") + '" class="pass-form-item pass-form-item-' + i.field + '" style="display:' + l + '"><label for="' + n.$getId(i.field) + '" id="' + n.$getId(i.field + "Label") + '" class="pass-label pass-label-' + i.field + '">' + i.label + '</label><input id="' + n.$getId(i.field) + '" type="' + o + '" name="' + i.field + '" class="pass-text-input pass-text-input-' + i.field + '" ' + j + p + " />" + (!i.noError ? '<span id="' + n.$getId(i.field + "Error") + '" class="pass-item-error pass-item-error-' + i.field + '"></span>': "") + (i.hasSucc ? '<span id="' + n.$getId(i.field + "Succ") + '" class="pass-item-succ pass-item-succ-' + i.field + '" style="display:none;"></span>': "") + '<span id="' + n.$getId(i.field + "Tip") + '" class="pass-item-tip pass-item-tip-' + i.field + '" style="display:none"><span id="' + n.$getId(i.field + "TipText") + '" class="pass-item-tiptext pass-item-tiptext-' + i.field + '">' + r + "</span></span></p>";
                return m
            },
            _getHiddenField: function(j, l) {
                var m = this,
                n,
                o = '<p id="' + m.$getId(l || "hiddenFields") + '" style="display:none">';
                for (var k in j) {
                    if (j.hasOwnProperty(k)) {
                        if (typeof(j[k]) == "string") {
                            n = j[k].replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/\x22/g, "&quot;").replace(/\x27/g, "&#39;")
                        } else {
                            n = j[k]
                        }
                        o += '<input type="hidden" id="' + m.$getId((l ? l + "_": "") + k) + '" name="' + k + '" value="' + n + '" />'
                    }
                }
                o += "</p>";
                return o
            },
            _setEvent: function() {
                var k = this,
                i = this.getElement();
                var j = function(l) {
                    k._eventHandler.entrance.apply(k, [l])
                };
                c(k.getElement("form")).on("submit", j);
                c(k.getElement("license")).on("click", j);
                c(k.getElement("verifyCodeChange")).on("click", j);
                c(k.getElement("verifyCodeSend")).on("click", j);
                c(k.getElement("smsVcodeSend")).on("click", j);
                c(i).delegate(".pass-suggest-item label", "click", j);
                c(".pass-text-input", k.getElement()).on({
                    focus: j,
                    blur: j,
                    change: j,
                    keyup: j,
                    mouseover: j,
                    mouseout: j
                })
            },
            _validator: {
                confStorage: {},
                builtInMsg: {
                    required: "璇锋偍濉啓%s",
                    phone: "鎵嬫満鍙风爜鏍煎紡涓嶆纭�",
                    email: "閭鏍煎紡涓嶆纭�"
                },
                builtInRules: {
                    required: /\S+/,
                    phone: /^1[34578]\d{9}$/,
                    email: /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
                },
                init: function(j, i) {
                    this.confStorage[j.$getId()] = i
                },
                validate: function(p, t, o, r) {
                    var A = p.getElement(t),
                    k = {
                        field: t
                    },
                    y = /^\s*(.+?)\s*$/;
                    if (!A || A.offsetHeight == 0) {
                        return false
                    }
                    var w = A.value.replace(y, "$1"),
                    n = this.confStorage[p.$getId()][t],
                    x = n.rules;
                    for (var m = 0, l = x.length; m < l; m++) {
                        var u = x[m],
                        s = this.builtInRules[u],
                        z;
                        if (Object.prototype.toString.call(s).toLowerCase() == "[object function]") {
                            z = s.call(p, A, r)
                        } else {
                            z = new RegExp(s).test(w)
                        }
                        if (!z) {
                            k.error = true;
                            p._validateInfo[t] = 0;
                            k.msg = this.builtInMsg[u].replace(/\%s/, n.desc);
                            p._validateError(k);
                            o.error(k);
                            return
                        }
                    }
                    if (!n.asyncRule) {
                        k.error = false;
                        p._validateInfo[t] = 1;
                        p._validateSuccess(k);
                        o.success(k)
                    } else {
                        n.asyncRule.call(p, {
                            success: function(i) {
                                k.error = false;
                                k.data = i.data;
                                p._validateInfo[t] = 1;
                                p._validateSuccess(k);
                                o.success(k)
                            },
                            error: function(i) {
                                k.error = true;
                                p._validateInfo[t] = 0;
                                k.msg = i.msg;
                                p._validateError(k);
                                o.error(k)
                            }
                        },
                        r)
                    }
                },
                validateAll: function(p, o, k) {
                    var t = this.confStorage[p.$getId()],
                    j = 0,
                    r = false,
                    n = false,
                    u = [],
                    s = this._getActiveValidate(p, true);
                    for (var m in t) {
                        if (n) {
                            break
                        }
                        this.validate(p, m, {
                            success: function(i) {
                                j++;
                                u.push(i);
                                if (j == s) {
                                    l()
                                }
                            },
                            error: function(i) {
                                r = true;
                                j++;
                                u.push(i);
                                if (k) {
                                    l();
                                    return
                                }
                                if (j == s) {
                                    l()
                                }
                            }
                        },
                        "all")
                    }
                    function l() {
                        n = true;
                        if (r) {
                            o && o.error(u)
                        } else {
                            o && o.success(u)
                        }
                    }
                },
                _getActiveValidate: function(n, m) {
                    var l = this.confStorage[n.$getId()],
                    j = [];
                    for (var k in l) {
                        var o = n.getElement(k);
                        if (!o || o.offsetHeight == 0) {
                            continue
                        }
                        j.push(o)
                    }
                    return m ? j.length: j
                },
                addRule: function(i, k) {
                    var j = {};
                    j[i] = k;
                    c.extend(this.builtInRules, j)
                },
                addMsg: function(j, k) {
                    var i = {};
                    i[j] = k;
                    c.extend(this.builtInMsg, i)
                }
            },
            validate: function(l, k) {
                var j = this;
                var i = j.fireEvent("beforeValidate", {
                    validate: {
                        field: l,
                        ele: c(j.getElement(l))
                    }
                });
                if (!i) {
                    return
                }
                j._validator.validate(j, l, {
                    success: function(n) {
                        var m = j.fireEvent("validateSuccess", {
                            validate: n
                        });
                        if (!m) {
                            return
                        }
                        k && k.success && k.success(n)
                    },
                    error: function(n) {
                        var m = j.fireEvent("validateError", {
                            validate: n
                        });
                        if (!m) {
                            return
                        }
                        k && k.error && k.error(n)
                    }
                })
            },
            validateAll: function(l, i) {
                var k = this;
                if (typeof l == "boolean") {
                    i = l
                } else {
                    i = i ? i: false
                }
                var j = k.fireEvent("beforeValidateAll");
                if (!j) {
                    return
                }
                k._validator.validateAll(k, {
                    success: function(n) {
                        var m = k.fireEvent("validateAllSuccess", {
                            validates: n
                        });
                        if (!m) {
                            return
                        }
                        l && l.success && l.success(n)
                    },
                    error: function(n) {
                        var m = k.fireEvent("validateAllError", {
                            validates: n
                        });
                        if (!m) {
                            return
                        }
                        l && l.error && l.error(n)
                    }
                },
                i)
            },
            getValidateStatus: function(i) {
                return this._validateInfo.hasOwnProperty(i) ? this._validateInfo[i] : -1
            },
            setValidateSuccess: function(j) {
                var i = this;
                i._validateInfo[j] = 1;
                i._validateSuccess({
                    field: j
                })
            },
            setValidateError: function(k, l, i) {
                var j = this;
                j._validateInfo[k] = 0;
                j._validateError({
                    field: k,
                    msg: l
                },
                i)
            },
            setGeneralError: function(i) {
                this.getElement("error").innerHTML = i
            },
            clearGeneralError: function() {
                this.getElement("error").innerHTML = ""
            },
            _isSupportPlaceholder: function() {
                return "placeholder" in document.createElement("input")
            },
            _getPlaceholder: function(k) {
                var n = this,
                m = {},
                o = "",
                j = {};
                for (var l = 0; l < k.length; l++) {
                    o = n.lang[k[l].placeholder];
                    if (k[l].clearbtn != 0) {
                        j[l] = c('<span id="' + n.$getId(k[l].label + "_clearbtn") + '" class="pass-clearbtn pass-clearbtn-' + k[l].label + '" style="display:none;"></span>');
                        c(n.getElement(k[l].label)).after(j[l])
                    }
                    if (n._isSupportPlaceholder() && !c.browser.ie) {
                        c(n.getElement(k[l].label)).attr({
                            placeholder: o
                        })
                    } else {
                        m[l] = c('<span id="' + n.$getId(k[l].label + "_placeholder") + '" class="pass-placeholder pass-placeholder-' + k[l].label + '">' + o + "</span>");
                        c(n.getElement(k[l].label)).after(m[l])
                    }
                    n._rendEventPlaceholder(n.getElement(k[l].label), m[l], j[l])
                }
            },
            _rendEventPlaceholder: function(k, m, i) {
                if (!k && !i) {
                    return
                }
                var n = this,
                j,
                l,
                o = function(p) {
                    if (p == 1) {
                        m && m[0] && n.$hide(m[0]);
                        i && i[0] && n.$show(i[0])
                    } else {
                        m && m[0] && n.$show(m[0]);
                        i && i[0] && n.$hide(i[0])
                    }
                };
                setTimeout(function() {
                    if (k.value) {
                        o(1)
                    }
                },
                200);
                c(m).on("mousedown", 
                function() {
                    l = true;
                    clearTimeout(j);
                    j = setTimeout(function() {
                        if (! (n.suggestionDom && n.suggestionDom.style.display != "none")) {
                            k.focus()
                        }
                    },
                    0)
                });
                c(i).on("click", 
                function() {
                    k.value = "";
                    o(0);
                    k.focus();
                    if (n.suggestionDom) {
                        n.suggestionDom.data_delete = true;
                        setTimeout(function() {
                            n.suggestionDom.data_delete = false
                        },
                        200)
                    }
                });
                c(k).on("keyup", 
                function() {
                    if (k.value) {
                        o(1)
                    } else {
                        o(0)
                    }
                });
                c(k).on("focus", 
                function() {
                    inputCheckTimer = setInterval(function() {
                        if (k.value.length) {
                            o(1);
                            clearInterval(inputCheckTimer)
                        } else {
                            o(0)
                        }
                    },
                    30)
                })
            },
            hide: function() {
                this.getElement().style.display = "none"
            },
            show: function() {
                this.getElement().style.display = "block"
            },
            _analysis: function(j, i) {
                if (h.analysis && h.analysis[this.module] && h.analysis[this.module][j]) {
                    h.analysis[this.module][j](this, i);
                    return {
                        preventDefault: false,
                        preventEvent: false
                    }
                }
            },
            _hook: function(j, i) {
                if (h.hook && h.hook[this.module] && h.hook[this.module][j]) {
                    return h.hook[this.module][j](this, i)
                } else {
                    return {
                        preventDefault: false,
                        preventEvent: false
                    }
                }
            },
            fireEvent: function(i, k) {
                var j = this._hook(i, k),
                m = this._analysis(i, k),
                l = true;
                if (!j.preventEvent) {
                    l = this.fire(i, k)
                }
                return ! j.preventDefault && l
            }
        });
        g.passport.login = c.lang.createClass(function(i) {
            var j = this;
            j._domain = {
                https: "https://passport.baidu.com",
                http: "http://passport.baidu.com",
                auto: (window.location ? ((window.location.protocol.toLowerCase() == "https:") ? "https://passport.baidu.com": "http://passport.baidu.com") : ((document.location.protocol.toLowerCase() == "https:") ? "https://passport.baidu.com": "http://passport.baidu.com"))
            };
            j.config = {
                isPhone: false,
                memberPass: true,
                isQuickUser: 0,
                safeFlag: 0,
                product: "",
                charset: "",
                loginMerge: false,
                staticPage: "",
                hasRegUrl: false,
                u: "",
                lang: "zh-CN",
                autosuggest: false,
                hasPlaceholder: false,
                registerLink: "",
                authsiteLogin: "",
                authsiteCfgLogin: "",
                qrcode: false,
                sms: 0,
                uniqueId: false,
                autoFocus: true,
                subpro: ""
            };
            c.extend(j.config, i);
            j.config.product = j.config.product || "isnull";
            this.module = "login";
            j.constant = {
                CHECKVERIFYCODE: true,
                CONTAINER_CLASS: "tang-pass-login",
                LABEL_FOCUS_CLASS: "pass-text-label-focus",
                FOCUS_CLASS: "pass-text-input-focus",
                HOVER_CLASS: "pass-text-input-hover",
                ERROR_CLASS: "pass-text-input-error",
                VERIFYCODE_URL_PREFIX: j._domain.https + "/cgi-bin/genimage?",
                BLANK_IMG_URL: h.apiDomain.staticDomain + "/passApi/img/small_blank_9dbbfbb1.gif",
                MODIFY_PWD_URL_PREFIX: j._domain.https + "/forcsdnpasschange",
                GET_PASSWORD_URL: j._domain.https + "/?getpassindex&tpl=" + encodeURIComponent(j.config.product) + "&u=" + encodeURIComponent(j.config.u),
                REG_URL: j.config.registerLink || j._domain.https + "/v2/?reg&tpl=" + encodeURIComponent(j.config.product) + "&u=" + encodeURIComponent(j.config.u),
                SUBMITFLAG: false
            };
            j.lang = h.err.getCurrent().labelText.login;
            h.data.setContext(c.extend({},
            j.config));
            this.initialized = false;
            this.validatorInited = false;
            this.bdPsWtoken = "";
            this.innerData = {
                normal: {},
                phone: {}
            };
            this.dataFiels = ["userName", "password"];
            this.initTime = new Date().getTime();
            if (j.config.defaultCss) {
                j._loadCssFileW("/passApi/css/uni_login_merge_da3a9459.css", 
                function() {})
            }
            if (j.config.memberPass) {
                j._initApi()
            }
        },
        {
            type: "magic.passport.login",
            superClass: g.passport
        }).extend({
            _getIrregularField: function(k) {
                var j = this,
                i = {
                    verifyCode: '<p id="' + j.$getId("verifyCodeImgWrapper") + '" class="pass-form-item pass-form-item-verifyCode" style="display:none"><label for="' + j.$getId("verifyCode") + '" id="' + j.$getId("verifyCodeLabel") + '" class="pass-label pass-label-verifyCode">' + j.lang.captcha + '</label><input id="' + j.$getId("verifyCode") + '" type="text" name="verifyCode" class="pass-text-input pass-text-input-verifyCode" maxlength="6" /><span  id="' + j.$getId("verifyCodeImgParent") + '" class="pass-verifyCodeImgParent" ><img id="' + j.$getId("verifyCodeImg") + '" class="pass-verifyCode" src="' + j.constant.BLANK_IMG_URL + '" /></span><a id="' + j.$getId("verifyCodeChange") + '" href="#" class="pass-change-verifyCode">' + j.lang.captchaChange + '</a><span id="' + j.$getId("verifyCodeError") + '" class="pass-error pass-error-verifyCode"></span><span id="' + j.$getId("verifyCodeTip") + '" class="pass-tip pass-tip-verifyCode"></span><span id="' + j.$getId("verifyCodeSuccess") + '" class="pass-success pass-success-verifyCode"></span></p>',
                    generalError: '<p id="' + j.$getId("errorWrapper") + '" class="pass-generalErrorWrapper"><span id="' + j.$getId("error") + '" class="pass-generalError pass-generalError-error"></span></p>',
                    rem: '<p id="' + j.$getId("memberPassWrapper") + '" class="pass-form-item pass-form-item-memberPass"><input id="' + j.$getId("memberPass") + '" type="checkbox" name="memberPass" class="pass-checkbox-input pass-checkbox-memberPass"' + (j.config.memberPass ? ' checked="checked"': "") + ' /><label for="' + j.$getId("memberPass") + '" id="' + j.$getId("memberPassLabel") + '" class="">' + j.lang.memberPassLabel + "</label></p>",
                    submit: '<p id="' + j.$getId("submitWrapper") + '" class="pass-form-item pass-form-item-submit"><input id="' + j.$getId("submit") + '" type="submit" value="' + j.lang.login + '" class="pass-button pass-button-submit" /><a class="pass-reglink" href="' + j.constant.REG_URL + '" target="_blank" ' + (j.config.hasRegUrl ? "": 'style="display:none"') + ">" + j.lang.register + '</a><a class="pass-fgtpwd" href="' + j.constant.GET_PASSWORD_URL + '" target="_blank">' + j.lang.fgtPwd + "</a></p>",
                    choiceuser: '<div id="' + j.$getId("choiceuser_article") + '" class="tang-pass-login-choice choiceuser-article"><div class="choiceuser-msg"><p class="choiceuser-msg-title">浜茬埍鐨勭敤鎴凤紝</p><p  class="choiceuser-msg-text">涓轰簡纭繚鎮ㄧ殑甯愬彿瀹夊叏锛岃鍏堢‘璁ゆ偍杈撳叆鐨勫笎鍙锋槸鐢ㄦ埛鍚嶈繕鏄墜鏈哄彿锛�</p></div><div class="choiceuser-buttons"><div class="choiceuser-btn"><input class="pass-button pass-button-choiceuser-username" type="button" value="鐢ㄦ埛鍚�" id="' + j.$getId("choiceuser_btn_username") + '"/><input class="pass-button pass-button-choiceuser-phone" type="button" value="鎵嬫満鍙�" id="' + j.$getId("choiceuser_btn_mobile") + '"/></div><div class="choiceuser-back"><a href="#" id="' + j.$getId("choiceuser_btn_back") + '">' + j.lang.backToLogin + "</a></div></div></div>"
                };
                return i[k]
            },
            _getTemplate: function(j) {
                var o = this,
                l = '<form id="' + o.$getId("form") + '" class="pass-form" method="POST" autocomplete="off">',
                n = {
                    codeString: "",
                    safeFlag: o.config.safeFlag,
                    u: o.config.u,
                    isPhone: o.config.isPhone,
                    staticPage: o.config.staticPage,
                    quick_user: o.config.isQuickUser,
                    logintype: (o.config.diaPassLogin ? "dialogLogin": "basicLogin"),
                    logLoginType: (o.config.diaPassLogin ? "pc_loginDialog": "pc_loginBasic"),
                    subpro: o.config.subpro
                },
                k = [{
                    field: "userName",
                    label: o.config.loginMerge ? o.lang.userName: (o.config.isPhone ? o.lang.phoneNum: o.lang.account),
                    noError: true
                },
                {
                    field: "password",
                    pwd: true,
                    label: o.lang.password,
                    noError: true
                }];
                if (o.config.loginMerge) {
                    n.loginMerge = true;
                    n.isPhone = false
                }
                l += o._getIrregularField("generalError");
                l += o._getHiddenField(n);
                for (var m = 0; m < k.length; m++) {
                    l += o._getRegularField(k[m])
                }
                l += o._getIrregularField("verifyCode");
                l += o._getIrregularField("rem");
                l += o._getIrregularField("submit");
                l += "</form>";
                return l
            },
            _collectData: function() {
                var o = this,
                m = o.innerData[o.config.isPhone ? "phone": "normal"],
                j = o.dataFiels;
                for (var n = 0, k = j.length; n < k; n++) {
                    m[j[n]] = o.getElement(j[n]).value
                }
                return m
            },
            _restoreData: function(o) {
                var p = this,
                m = p.innerData[o ? o: (p.config.isPhone ? "phone": "normal")],
                j = p.dataFiels;
                for (var n = 0, k = j.length; n < k; n++) {
                    p.getElement(j[n]).value = m[j[n]] || ""
                }
                return m
            },
            _loadCssFileW: function(k, i) {
                window._loadedFilesW = window._loadedFilesW || {};
                var m = (window.location ? window.location.protocol: document.location.protocol).toLowerCase();
                if (!window._loadedFilesW[k]) {
                    window._loadedFilesW[k] = true;
                    var j = document.createElement("link");
                    j.rel = "stylesheet";
                    j.type = "text/css";
                    j.href = (m == "https:") ? ("https://passport.baidu.com" + k) : ("http://passport.baidu.com" + k);
                    document.getElementsByTagName("head")[0].appendChild(j);
                    if (j.readyState) {
                        j.onreadystatechange = function() {
                            if (j.readyState == "loaded" || j.readyState == "complete") {
                                j.onreadystatechange = null;
                                i && i()
                            }
                        }
                    } else {
                        j.onload = function() {
                            i && i()
                        }
                    }
                }
            },
            _insertScriptW: function(j, i) {
                window._loadedFilesW = window._loadedFilesW || {};
                if (!window._loadedFilesW[j]) {
                    window._loadedFilesW[j] = true;
                    var l = document,
                    k = l.createElement("SCRIPT");
                    k.type = "text/javascript";
                    k.charset = "UTF-8";
                    if (k.readyState) {
                        k.onreadystatechange = function() {
                            if (k.readyState == "loaded" || k.readyState == "complete") {
                                k.onreadystatechange = null;
                                i && i()
                            }
                        }
                    } else {
                        k.onload = function() {
                            i && i()
                        }
                    }
                    k.src = j;
                    l.getElementsByTagName("head")[0].appendChild(k)
                }
            },
            _authSiteW: function() {
                var j = this,
                i = j.config,
                k = j.getPhoenixId("pass_phoenix_btn");
                if (i.authsitecssLoad) {
                    j._loadCssFileW("/passApi/css/authsite_889e654a.css")
                }
                j._insertScriptW(j._domain.auto + "/phoenix/account/jsapi", 
                function() {
                    if (!window.baidu.phoenix) {
                        return
                    }
                    window.baidu.phoenix.require(i.authsiteLogin, c.extend(i.authsiteCfgLogin || {},
                    {
                        tpl: i.product ? i.product: "",
                        u: i.u,
                        target: k,
                        html: {
                            qzone: '<a class="phoenix-btn-item" href="#">QQ甯愬彿</a>',
                            tsina: '<a class="phoenix-btn-item" href="#">鏂版氮寰崥</a>',
                            renren: '<a class="phoenix-btn-item" href="#">浜轰汉缃�</a>',
                            fetion: '<a class="phoenix-btn-item" href="#">椋炰俊</a>',
                            tqq: '<a class="phoenix-btn-item" href="#">鑵捐寰崥</a>',
                            qunar: '<a class="phoenix-btn-item" href="#">鍘诲摢鍎�</a>'
                        },
                        onAfterRender: function() {
                            var o = c("#" + k + " li");
                            var n = function(p) {
                                p.on("click", 
                                function(r) {
                                    r.preventDefault()
                                })
                            };
                            for (var m = 0; m < o.length; m++) {
                                var l = c(".phoenix-btn-item", o[m]);
                                l.attr({
                                    title: l[0].innerHTML
                                });
                                n(l)
                            }
                        }
                    }))
                })
            },
            getVerifyCode: function(i) {
                var k = this,
                l = {
                    fr: "login",
                    vcodetype: k.vcodetype || ""
                };
                k.getElement("verifyCode").value = "";
                k.$hide("verifyCodeSuccess");
                if (k.getElement("verifyCode_clearbtn")) {
                    k.$hide("verifyCode_clearbtn")
                }
                k.getElement("verifyCodeImg").src = "";
                if (i && i.length) {
                    k.$show("verifyCodeImgWrapper");
                    k.getElement("verifyCodeImg").src = k.constant.VERIFYCODE_URL_PREFIX + i;
                    k.getElement("codeString").value = i;
                    var j = k.fireEvent("renderVerifycode", {
                        verifyStr: i,
                        verifyCodeImg: k.constant.VERIFYCODE_URL_PREFIX + i
                    });
                    if (!j) {
                        return
                    }
                } else {
                    h.data.getVerifyCodeStr(l).success(function(m) {
                        if (m.errInfo.no == 0) {
                            k.$show("verifyCodeImgWrapper");
                            k.getElement("verifyCodeImg").src = k.constant.VERIFYCODE_URL_PREFIX + m.data.verifyStr;
                            k.getElement("codeString").value = m.data.verifyStr;
                            var n = k.fireEvent("renderVerifycode", {
                                verifyStr: m.data.verifyStr,
                                verifyCodeImg: k.constant.VERIFYCODE_URL_PREFIX + m.data.verifyStr
                            });
                            if (!n) {
                                return
                            }
                        }
                    })
                }
                if (k.getElement("verifyCode_placeholder")) {
                    setTimeout(function() {
                        k.$show("verifyCode_placeholder")
                    },
                    200)
                }
            },
            _getWDom: {
                parent: function(i) {
                    return i.parentNode || i.parentElement
                },
                next: function(i) {
                    do {
                        i = i.nextSibling
                    }
                    while (i && i.nodeType != 1);
                    return i
                },
                prev: function(i) {
                    do {
                        i = i.previousSibling
                    }
                    while (i && i.nodeType != 1);
                    return i
                }
            },
            clearVerifyCode: function() {
                var i = this;
                i.$hide("verifyCodeImgWrapper");
                i.getElement("codeString").value = ""
            },
            getPhoenixId: function(j) {
                if (this.config.uniqueId) {
                    return this.$getId(j)
                } else {
                    var i = {
                        pass_phoenix_login: "pass-phoenix-login",
                        pass_phoenix_list_login: "pass-phoenix-list-login",
                        pass_phoenix_btn: "pass_phoenix_btn"
                    };
                    return i[j]
                }
            },
            getPhoenixElement: function(i) {
                if (this.getElement(i)) {
                    return this.getElement(i)
                } else {
                    return document.getElementById(this.getPhoenixId(i))
                }
            },
            _getTemplateOther: function() {
                var i = [],
                k = this;
                var j = 0;
                if (k.config.authsiteLogin) {
                    j = k.config.authsiteLogin.length
                }
                if (k.config.qrcode) {
                    j++
                }
                if (k.config.sms) {
                    j++
                }
                i.push('<div id="' + k.getPhoenixId("pass_phoenix_login") + '" class="tang-pass-login-phoenix"><div class="pass-phoenix-title">鍙互浣跨敤浠ヤ笅鏂瑰紡鐧诲綍<span class="pass-phoenix-note"></span></div><div id="' + k.getPhoenixId("pass_phoenix_list_login") + '" class="pass-phoenix-list clearfix">' + (k.config.phoenixLimit && j > k.config.phoenixLimit ? '<em class="pass-phoenix-show"></em>': "") + ((k.config.sms || (!k.config.diaPassLogin && k.config.qrcode)) ? '<a class="pass-normal-btn" title="鏅€氱櫥褰�" style="display:none" data-type="normal">鏅€氱櫥褰�</a>': "") + (k.config.authsiteLogin ? '<div class="pass-phoenix-btn clearfix" id="' + k.getPhoenixId("pass_phoenix_btn") + '"></div>': "") + (k.config.sms ? '<a class="pass-sms-btn" title="鐭俊鐧诲綍" data-type="sms">鐭俊鐧诲綍</a>': "") + (!k.config.diaPassLogin && k.config.qrcode ? '<a class="pass-qrcode-btn" title="浜岀淮鐮佺櫥褰�" data-type="qrcode">浜岀淮鐮佺櫥褰�</a>': "") + '</div><div class="clear"></div></div>');
                return i.join("")
            },
            _getTemplateQrcode: function() {
                var i = this,
                j = [];
                j.push('<div id="' + i.$getId("qrcode") + '" class="clearfix tang-pass-qrcode" style="display:none;">');
                j.push('<p class="tang-pass-qrcode-title">' + i.lang.qrcodeTitle + "</p>");
                j.push('<p class="tang-pass-qrcode-info">' + i.lang.qrcodeMsg + "</p>");
                j.push('<div class="tang-pass-qrcode-content"><img class="tang-pass-qrcode-img" src="' + i._domain.auto + '/passApi/img/loading_ed5e4a40.gif"/></div>');
                j.push('<a id="' + i.$getId("qrcode_btn_back") + '" class="pass-qrcode-link pass-qrcode-link-back">' + i.lang.backToLogin + "</a>");
                j.push("</div>");
                return j.join("")
            },
            _setEventQrcode: function() {
                var m = this,
                i = this.getElement(),
                l = m._getWDom.parent(i),
                j = m._getWDom.parent(l),
                k = c(".pass-qrcode-btn", j).get(0);
                c(".pass-qrcode-btn", j).on("click", 
                function() {
                    h.spareWData = h.spareWData || {};
                    if (!h.spareWData.channelimg) {
                        m._setChannel();
                        h.spareWData.timer = setInterval(function() {
                            m._setChannel()
                        },
                        5 * 60 * 1000)
                    }
                });
                c(m.getElement("qrcode_btn_back")).on("click", 
                function() {
                    m._changeLoginType("normal")
                })
            },
            _setChannel: function() {
                var i = this;
                h.spareWData = h.spareWData || {};
                h.data.jsonp("/v2/api/getqrcode?lp=pc").success(function(l) {
                    h.spareWData.channelimg = (window.location ? window.location.protocol: document.location.protocol) + "//" + l.data.imgurl;
                    h.spareWData.sign = l.data.sign;
                    i._createChannel();
                    var k = c(".tang-pass-qrcode-img");
                    for (var m = 0, j = k.length; m < j; m++) {
                        k.get(m).src = h.spareWData.channelimg
                    }
                })
            },
            _createChannel: function() {
                var i = this;
                h.spareWData = h.spareWData || {};
                h.data.jsonp("/channel/unicast?channel_id=" + h.spareWData.sign).success(function(j) {
                    if (j.data.errno + "" === "0") {
                        clearInterval(h.spareWData.timer);
                        h.data.jsonp("/v2/api/bdusslogin?bduss=" + j.data.channel_v + "&u=" + i.config.u).success(function(k) {
                            var l = i.fireEvent("loginSuccess", {
                                rsp: k
                            })
                        })
                    } else {
                        if (j.data.errno + "" === "1") {
                            i._createChannel()
                        }
                    }
                })
            },
            _showQrcode: function() {
                var m = this,
                i = this.getElement(),
                l = m._getWDom.parent(i),
                j = m._getWDom.parent(l),
                k = c(".pass-qrcode-btn", j).get(0);
                if (m._getWDom.prev(i)) {
                    m.$hide(m._getWDom.prev(i))
                }
                m.$hide("choiceuser_article").$hide(i).$show(c(".tang-pass-qrcode", l).get(0));
                if (m._getWDom.next(k)) {
                    m.$hide(k)
                } else {
                    m.$hide(m._getWDom.parent(m._getWDom.parent(k)))
                }
            },
            _getTemplateSms: function() {
                var l = this,
                j = (l.config.sms == 2 || l.config.sms == 3) ? "": "none",
                i = '<div id="' + l.$getId("sms") + '" class="tang-pass-login tang-pass-sms" style="display:' + j + '">',
                k = {
                    u: l.config.u,
                    staticPage: l.config.staticPage,
                    tpl: l.config.product ? l.config.product: "",
                    isdpass: "1",
                    switchuname: "",
                    subpro: l.config.subpro
                };
                i += '<p class="tang-pass-sms-title">\u77ed\u4fe1\u767b\u5f55</p>';
                i += '<p class="tang-pass-sms-tip">' + (l.config.smsText || "\u9A8C\u8BC1\u5373\u767B\u5F55\uFF0C\u6CA1\u6709\u767E\u5EA6\u5E10\u53F7\u4E5F\u53EF\u4EE5\u4F7F\u7528") + "</p>";
                i += '<form id="' + l.$getId("smsForm") + '" method="POST">';
                i += l._getHiddenField(k, "smsHiddenFields");
                i += '<p id="' + l.$getId("smsErrorWrapper") + '" class="pass-generalErrorWrapper"><span id="' + l.$getId("smsError") + '" class="pass-generalError"></span></p>';
                i += '<p id="' + l.$getId("smsPhoneWrapper") + '" class="pass-form-item pass-form-item-smsPhone"><label for="' + l.$getId("smsPhone") + '" id="' + l.$getId("smsPhoneLabel") + '" class="pass-label pass-label-smsPhone">' + l.lang.smsPhone + '</label><input id="' + l.$getId("smsPhone") + '" type="text" name="username" class="pass-text-input pass-text-input-smsPhone" /><span id="' + l.$getId("smsPhoneTip") + '" class="pass-item-tip pass-item-tip-smsPhone" style="display:none"><span id="' + l.$getId("smsPhoneTipText") + '"></span></span></p>';
                i += '<p id="' + l.$getId("smsVerifyCodeWrapper") + '" class="pass-form-item pass-form-item-smsVerifyCode"><label for="' + l.$getId("smsVerifyCode") + '" id="' + l.$getId("smsVerifyCodeLabel") + '" class="pass-label pass-label-smsVerifyCode">' + l.lang.smsVerifyCode + '</label><input id="' + l.$getId("smsVerifyCode") + '" type="text" name="password" class="pass-text-input pass-text-input-smsVerifyCode" /><button id="' + l.$getId("smsTimer") + '" class="pass-item-timer">\u53D1\u9001' + l.lang.smsVerifyCode + '</button><span id="' + l.$getId("smsVerifyCodeTip") + '" class="pass-item-tip pass-item-tip-smsVerifyCode" style="display:none"><span id="' + l.$getId("smsVerifyCodeTipText") + '"></span></span></p>';
                i += '<p id="' + l.$getId("smsSubmitWrapper") + '" class="pass-form-item pass-form-item-submit"><input id="' + l.$getId("smsSubmit") + '" type="submit" value="\u767B\u5F55" class="pass-button pass-button-submit" />' + (l.config.sms == 3 ? "": ('<a id="' + l.$getId("sms_btn_back") + '" class="pass-sms-link pass-sms-link-back">' + l.lang.backToLogin + "</a>")) + "</p>";
                i += "</form>";
                i += "</div>";
                return i
            },
            _setEventSms: function() {
                var n = this,
                i = this.getElement(),
                m = n._getWDom.parent(i),
                k = n._getWDom.parent(m),
                l = c("#" + n.$getId("sms"), k).get(0),
                j = c(".pass-text-input", l);
                c(".pass-text-input", l).on("mouseover", 
                function(p) {
                    var o = n.fireEvent("fieldMouseover", {
                        ele: c(this)
                    });
                    if (!o) {
                        return
                    }
                    c(this).addClass(n.constant.HOVER_CLASS)
                });
                c(".pass-text-input", l).on("mouseout", 
                function(p) {
                    var o = n.fireEvent("fieldMouseout", {
                        ele: c(this)
                    });
                    if (!o) {
                        return
                    }
                    c(this).removeClass(n.constant.HOVER_CLASS)
                });
                c(".pass-text-input", l).on("keydown", 
                function(o) {
                    if (o.keyCode == 13) {
                        if (o && o.preventDefault) {
                            o.preventDefault()
                        }
                        n._submitSmsForm(o)
                    }
                });
                c(n.getElement("smsPhone")).on("focus", 
                function(p) {
                    if (!n.initialized) {
                        n._initApi()
                    }
                    var o = n.fireEvent("fieldFocus", {
                        ele: c(this)
                    });
                    if (!o) {
                        return
                    }
                    c(this).addClass(n.constant.FOCUS_CLASS);
                    c(n.getElement("smsPhoneLabel")).addClass(n.constant.LABEL_FOCUS_CLASS);
                    c(this).removeClass(n.constant.ERROR_CLASS)
                });
                c(n.getElement("smsVerifyCode")).on("focus", 
                function(p) {
                    if (!n.initialized) {
                        n._initApi()
                    }
                    var o = n.fireEvent("fieldFocus", {
                        ele: c(this)
                    });
                    if (!o) {
                        return
                    }
                    c(this).addClass(n.constant.FOCUS_CLASS);
                    c(n.getElement("smsVerifyCodeLabel")).addClass(n.constant.LABEL_FOCUS_CLASS);
                    c(this).removeClass(n.constant.ERROR_CLASS)
                });
                c(".pass-text-input", l).on("blur", 
                function(p) {
                    if (this.value) {
                        var o = n.fireEvent("fieldBlur", {
                            ele: c(this)
                        });
                        if (!o) {
                            return
                        }
                        this.name == "username" ? n._validatorPhoneFn(this) : n._validatorSmsFn(this)
                    }
                    c(this).removeClass(n.constant.FOCUS_CLASS);
                    c(n.getElement("smsPhoneLabel")).removeClass(n.constant.LABEL_FOCUS_CLASS)
                });
                c("#" + n.$getId("smsTimer"), l).on("click", 
                function(o) {
                    o.preventDefault();
                    if (n.bdPsWtoken) {
                        n._sendVcode(n)
                    } else {
                        n._getToken(n._sendVcode)
                    }
                });
                c("#" + n.$getId("smsSubmit"), l).on("click", 
                function(o) {
                    if (o && o.preventDefault) {
                        o.preventDefault()
                    }
                    n._submitSmsForm(o)
                });
                if (n.config.sms == 2 || n.config.sms == 3) {
                    if (/msie 6/i.test(navigator.userAgent) || /msie 7/i.test(navigator.userAgent)) {
                        setTimeout(function() {
                            n._changeLoginType("sms")
                        },
                        0)
                    } else {
                        n._changeLoginType("sms")
                    }
                }
                c(n.getElement("sms_btn_back")).on("click", 
                function() {
                    n._changeLoginType("normal")
                })
            },
            _setSmsGeneralError: function(i) {
                this.getElement("smsError").innerHTML = i
            },
            _sendVcode: function() {
                var l = this;
                var i = document.getElementById(l.$getId("smsPhone")),
                k = 60,
                m,
                j = c("#" + l.$getId("sms")).get(0);
                if (!l._validatorPhoneFn(i)) {
                    return
                }
                c("#" + l.$getId("smsTimer"), j).off("click");
                c("#" + l.$getId("smsTimer"), j).on("click", 
                function(n) {
                    n.preventDefault()
                });
                c("#" + l.$getId("smsTimer"), j).removeClass("pass-item-timer");
                c("#" + l.$getId("smsTimer"), j).addClass("pass-item-time-timing");
                h.data.jsonp(l._domain.auto + "/v2/api/senddpass?username=" + i.value + "&bdstoken=" + l.bdPsWtoken + "&tpl=" + (l.config.product ? l.config.product: "")).success(function(n) {
                    if (n.data.errno != 0) {
                        l._setSmsGeneralError(n.data.msg);
                        c("#" + l.$getId("smsTimer"), j).addClass("pass-item-timer");
                        c("#" + l.$getId("smsTimer"), j).removeClass("pass-item-time-timing");
                        document.getElementById(l.$getId("smsTimer")).innerHTML = "\u91CD\u65B0\u53D1\u9001";
                        c("#" + l.$getId("smsTimer"), j).on("click", 
                        function(o) {
                            o.preventDefault();
                            l._sendVcode(l)
                        })
                    } else {
                        m = setInterval(function() {
                            if ((--k) == 0) {
                                clearInterval(m);
                                c("#" + l.$getId("smsTimer"), j).removeClass("pass-item-time-timing");
                                c("#" + l.$getId("smsTimer"), j).addClass("pass-item-timer");
                                document.getElementById(l.$getId("smsTimer")).innerHTML = "\u91CD\u65B0\u53D1\u9001";
                                c("#" + l.$getId("smsTimer"), j).on("click", 
                                function(o) {
                                    o.preventDefault();
                                    l._sendVcode(l)
                                });
                                k = 60
                            } else {
                                document.getElementById(l.$getId("smsTimer")).innerHTML = "\u91CD\u65B0\u53D1\u9001(" + k + ")"
                            }
                        },
                        1000)
                    }
                })
            },
            _validatorPhoneFn: function(j) {
                var i = this;
                if (j.value == "") {
                    i._setSmsGeneralError("\u8BF7\u586B\u5199\u624B\u673A\u53F7");
                    c(j).addClass(i.constant.ERROR_CLASS);
                    return false
                } else {
                    if (!new RegExp(/^1[34578]\d{9}$/).test(j.value)) {
                        i._setSmsGeneralError("\u624B\u673A\u53F7\u7801\u683C\u5F0F\u4E0D\u6B63\u786E");
                        c(j).addClass(i.constant.ERROR_CLASS);
                        return false
                    }
                }
                i._setSmsGeneralError("");
                return true
            },
            _validatorSmsFn: function(j) {
                var i = this;
                if (j.value == "") {
                    i._setSmsGeneralError("\u8BF7\u586B\u5199\u9A8C\u8BC1\u7801");
                    c(j).addClass(i.constant.ERROR_CLASS);
                    return false
                }
                i._setSmsGeneralError("");
                return true
            },
            _submitSmsForm: function() {
                var k = this,
                i = document.getElementById(k.$getId("smsPhone")),
                m = document.getElementById(k.$getId("smsVerifyCode"));
                if (!k._validatorPhoneFn(i)) {
                    return
                }
                if (!k._validatorSmsFn(m)) {
                    return
                }
                var j = k.fireEvent("beforeSubmit");
                if (!j) {
                    return
                }
                var l = c.form.json(k.getElement("smsForm"));
                h.data.login(l).success(function(n) {
                    if (n.errInfo.no == 0) {
                        var o = k.fireEvent("loginSuccess", {
                            rsp: n
                        });
                        if (!o) {
                            return
                        }
                        window.location.href = n.data.u
                    } else {
                        var o = k.fireEvent("loginError", {
                            rsp: n
                        });
                        if (!o) {
                            return
                        }
                        if (n.errInfo.no == 4) {
                            k._setSmsGeneralError(k.lang.captchaErr)
                        } else {
                            k._setSmsGeneralError(n.errInfo.msg)
                        }
                        if (n.errInfo.no == 3 || n.errInfo.no == 4) {
                            k._clearInput("smsVerifyCode")
                        }
                    }
                })
            },
            _setEventChoiceUser: function() {
                var k = this;
                var i = function() {
                    c(k.getElement()).removeClass("tang-pass-login-hide");
                    k.$show(k.getElement()).$hide("choiceuser_article")
                };
                var j = function(l) {
                    c(k.getElement()).removeClass("tang-pass-login-hide");
                    k.$show(k.getElement()).$hide("choiceuser_article");
                    k.submit()
                };
                c(k.getElement("choiceuser_btn_username")).on("click", 
                function(l) {
                    k.getElement("loginMerge").value = "false";
                    j(l)
                });
                c(k.getElement("choiceuser_btn_mobile")).on("click", 
                function(l) {
                    k.getElement("isPhone").value = "true";
                    k.getElement("loginMerge").value = "false";
                    j(l)
                });
                c(k.getElement("choiceuser_btn_back")).on("click", 
                function(l) {
                    l.preventDefault();
                    i()
                })
            },
            _getToken: function(i) {
                var j = this;
                h.spareWData = h.spareWData || {};
                h.data.getApiInfo({
                    apiType: "login"
                }).success(function(k) {
                    j.bdPsWtoken = k.data.token;
                    i && i(j)
                })
            },
            _getRSA: function(j) {
                var i = this;
                h.data.getRsaKey().success(function(l) {
                    if (!l.errInfo.no && l.errInfo.no != 0) {
                        l = l.data
                    }
                    var k = new h.lib.RSA();
                    k.setKey(l.pubkey);
                    j && j({
                        RSA: k,
                        rsakey: l.key
                    })
                })
            },
            _changeLoginType: function(j) {
                var m = this;
                var i = {
                    normal: {
                        $btn: c(".pass-normal-btn", m.getPhoenixElement("pass_phoenix_list_login")),
                        $ele: c(m.getElement("form")).parent()
                    },
                    sms: {
                        $btn: c(".pass-sms-btn", m.getPhoenixElement("pass_phoenix_list_login")),
                        $ele: c(m.getElement("sms"))
                    },
                    qrcode: {
                        $btn: c(".pass-qrcode-btn", m.getPhoenixElement("pass_phoenix_list_login")),
                        $ele: c(m.getElement("qrcode"))
                    }
                },
                l = m.getElement("choiceuser_article");
                j = j || "normal";
                if (l) {
                    m.$hide("choiceuser_article")
                }
                for (var k in i) {
                    if (i[k].$ele && i[k].$ele.length > 0) {
                        if (k == j) {
                            m.$hide(i[k].$btn[0]).$show(i[k].$ele[0])
                        } else {
                            m.$show(i[k].$btn[0]).$hide(i[k].$ele[0])
                        }
                    }
                }
                m.currentLoginType = j
            },
            _doFocus: function(j) {
                var i = this;
                if (!i.config.autoFocus) {
                    return
                }
                if ((typeof j).toLowerCase() == "string" && i.getElement(j)) {
                    i.getElement(j).focus()
                } else {
                    j.focus()
                }
            },
            _clearInput: function(j) {
                var k = this,
                l = k.getElement(j),
                i = k.getElement(j + "_placeholder"),
                m = k.getElement(j + "_clearbtn");
                if (l) {
                    if (i) {
                        k.$show(i)
                    }
                    if (m) {
                        k.$hide(i)
                    }
                    l.value = "";
                    k._doFocus(l)
                }
            },
            _insertAfterW: function(i, l) {
                var k = this;
                var j = k._getWDom.parent(l);
                if (j.lastChild == l) {
                    j.appendChild(i)
                } else {
                    j.insertBefore(i, k._getWDom.next(l))
                }
            },
            _checkCapsLock: function() {
                var j = this,
                i = c(j.getElement("password"));
                i.on("keypress", 
                function(n) {
                    var p = n || window.event;
                    var o = p.keyCode || p.which;
                    var k = p.shiftKey || (o == 16) || false;
                    var m = document.getElementById(j.$getId("caps"));
                    if (((o >= 65 && o <= 90) && !k) || ((o >= 97 && o <= 122) && k)) {
                        if (!m) {
                            var l = document.createElement("span");
                            l.id = j.$getId("caps");
                            l.innerHTML = "澶у皬鍐欓攣瀹氬凡鎵撳紑";
                            var s = document.getElementById(j.$getId("passwordWrapper"));
                            if (s.style.position == "static") {
                                s.style.position = "relative"
                            }
                            if (s.style.zIndex) {
                                s.style.zIndex = s.style.zIndex + 1
                            } else {
                                s.style.zIndex = 20
                            }
                            if (h && h.apiDomain && h.apiDomain.staticDomain) {
                                var r = h.apiDomain.staticDomain
                            } else {
                                var r = j._domain.auto
                            }
                            l.style.cssText = "position:absolute;left:60px;clear:both;top:25px;width:103px;height:37px;font-size:12px;line-height:45px;z-index:20;text-align:center;background:url('" + r + "/passApi/img/caps_1b139230.gif') no-repeat 0 0;";
                            s.appendChild(l)
                        } else {
                            j.$show(m)
                        }
                    } else {
                        if (m) {
                            j.$hide(m)
                        }
                    }
                });
                i.on("blur", 
                function(l) {
                    var k = document.getElementById(j.$getId("caps"));
                    if (k) {
                        j.$hide(k)
                    }
                })
            },
            changeSuggestView: function(i) {
                var j = this;
                if (j.suggestionDom && i.list) {
                    if (i.list == "hide") {
                        j.$hide(j.suggestionDom)
                    } else {
                        if (i.list == "show") {
                            j.$show(j.suggestionDom)
                        }
                    }
                }
                if (j.selectBtn && i.btn) {
                    if (i.btn == "close") {
                        c(j.selectBtn).removeClass("open");
                        c(j.getElement("userName")).addClass("open");
                        j.$show(j.selectBtn)
                    } else {
                        if (i.btn == "open") {
                            c(j.selectBtn).addClass("open");
                            c(j.getElement("userName")).addClass("open");
                            j.$show(j.selectBtn)
                        } else {
                            if (i.btn == "hide") {
                                j.$hide(j.selectBtn);
                                c(j.getElement("userName")).removeClass("open")
                            } else {
                                if (i.btn == "show") {
                                    j.$show(j.selectBtn);
                                    c(j.getElement("userName")).addClass("open")
                                }
                            }
                        }
                    }
                    j.$hide(j.selectBtn)
                }
            },
            _suggestion: function(i) {
                var o = this,
                m = [],
                r = c("#" + o.$getId("userName"), o.getElement()),
                k = ["qq.com", "163.com", "126.com", "sohu.com", "sina.com", "gmail.com", "21cn.com", "hotmail.com", "vip.qq.com", "yeah.net"],
                j = /^([a-zA-Z0-9_.\-+]+)([@]?[a-zA-Z0-9_\-*]*[.]?[a-zA-Z*]*[.]?[a-zA-Z*]*)$/;
                var l = function(u, s) {
                    var t = u;
                    if (u.substr(0, u.indexOf("@") - 1).length > s.maxlength) {
                        t = u.substr(0, s.maxlength - 4) + "鈥�" + u.substr(u.indexOf("@"))
                    }
                    return c('<li class="pass-item-suggsetion" data-select="' + u + '" data-type="' + (s.ifdelete ? "history": "normal") + '">' + t + (s.ifdelete ? '<a data-delete="' + u + '" title="鍒犻櫎璇ヨ褰�"></a>': "") + "</li>").get(0)
                };
                var n = function(t, s) {
                    if (!o.suggestionDom) {
                        o.suggestionDom = document.createElement("ul");
                        o.suggestionDom.id = o.$getId("suggestionWrapper");
                        c(o.getElement("userNameWrapper")).append(o.suggestionDom);
                        c(o.suggestionDom).addClass("pass-suggestion-list");
                        c(o.suggestionDom).on("click", 
                        function(x) {
                            var u = c(x.target),
                            y = u.attr("data-delete"),
                            w = u.attr("data-select");
                            if (y) {
                                x.preventDefault();
                                h.data.getLoginHistory({
                                    token: o.bdPsWtoken,
                                    item: y
                                });
                                o.suggestionDom.data_delete = true;
                                c(u.parent()).hide();
                                c.array(m).remove(y);
                                if (m.length < 1) {
                                    o.changeSuggestView({
                                        list: "hide",
                                        btn: "hide"
                                    })
                                }
                                o._doFocus("userName");
                                setTimeout(function() {
                                    o.suggestionDom.data_delete = false
                                },
                                200)
                            } else {
                                if (u.attr("data-type") == "history") {
                                    s.value = w
                                } else {
                                    o.suggestionDom.data_delete = false;
                                    s.value = w || s.value
                                }
                                if (o.getElement("userName_placeholder")) {
                                    o.$hide("userName_placeholder")
                                }
                                o.changeSuggestView({
                                    list: "hide",
                                    btn: "close"
                                });
                                o._doFocus(s);
                                setTimeout(function() {
                                    o.setGeneralError("");
                                    c(s).removeClass("pass-text-input-error");
                                    o._doFocus("password")
                                },
                                100)
                            }
                        })
                    }
                    o.suggestionDom.innerHTML = "";
                    o.$show(o.suggestionDom);
                    o.suggestionDom.appendChild(t);
                    c(".pass-item-suggsetion", o.suggestionDom).on("mouseover", 
                    function() {
                        c(".pass-item-suggsetion_hover", o.suggestionDom).removeClass("pass-item-suggsetion_hover");
                        c(this).addClass("pass-item-suggsetion_hover")
                    });
                    c(".pass-item-suggsetion", o.suggestionDom).on("mouseout", 
                    function() {
                        c(this).removeClass("pass-item-suggsetion_hover")
                    })
                };
                var p = (function() {
                    if (o.config.loginMerge) {
                        m = o.loginrecord.displayname || []
                    } else {
                        m = o.config.isPhone ? o.loginrecord.phone: o.loginrecord.email
                    }
                    if (m.length > 0) {
                        var u = document.createDocumentFragment();
                        for (var t = 0, s = m.length; t < s; t++) {
                            u.appendChild(l(m[t], {
                                maxlength: i,
                                ifdelete: true
                            }))
                        }
                        n(u, o.getElement("userName"));
                        o.selectBtn = c('<span class="pass-item-selectbtn pass-item-selectbtn-userName" ></span>').get(0);
                        o.getElement("userNameWrapper").appendChild(o.selectBtn);
                        c(o.selectBtn).on("click", 
                        function(w) {
                            setTimeout(function() {
                                if (o.suggestionDom.style.display != "none") {
                                    o.changeSuggestView({
                                        list: "hide",
                                        btn: "close"
                                    })
                                } else {
                                    o.changeSuggestView({
                                        list: "show",
                                        btn: "open"
                                    })
                                }
                            },
                            200)
                        });
                        o.changeSuggestView({
                            list: "hide",
                            btn: "show"
                        })
                    }
                })();
                r.on("keyup", 
                function(D) {
                    if (o.disUnameLogin == 1) {} else {
                        var E = document.createDocumentFragment(),
                        G,
                        F = this,
                        y = 0;
                        if (m.length > 0) {
                            for (var A = 0, B = m.length; A < B; A++) {
                                if (m[A].indexOf(this.value) == 0) {
                                    E.appendChild(l(m[A], {
                                        maxlength: i,
                                        ifdelete: true
                                    })); ++y
                                }
                            }
                        }
                        if (m.length < 1 || y < 1) {
                            G = j.exec(this.value);
                            if (G && G[2]) {
                                for (var A = 0, B = k.length; A < B; A++) {
                                    if (("@" + k[A]).indexOf(G[2]) == 0) {
                                        var C = G[1];
                                        E.appendChild(l(C + "@" + k[A], {
                                            maxlength: i
                                        })); ++y
                                    }
                                }
                            }
                        }
                        if (o.suggestionDom && (D.keyCode !== 38 && D.keyCode !== 40)) {
                            o.$hide(o.suggestionDom)
                        }
                        if (m.length > 1 || (m.length < 2 && o.config.isPhone == false)) {
                            if (y > 0) {
                                if ((D.keyCode !== 38 && D.keyCode !== 40)) {
                                    n(E, F)
                                }
                                if ((D.keyCode == 38 || D.keyCode == 40) && (o.suggestionDom.style.display != "none")) {
                                    var t = o.suggestionDom.childNodes,
                                    w = t.length,
                                    s = -1;
                                    for (var z = 0; z < w; z++) {
                                        if (t[z].className.indexOf("pass-item-suggsetion_hover") > -1) {
                                            s = z
                                        }
                                    }
                                    if (D.keyCode == 38) {
                                        q = s === -1 ? (w - 1) : (s === 0 ? (w - 1) : s - 1)
                                    }
                                    if (D.keyCode == 40) {
                                        q = s === -1 ? 0: (s === w - 1 ? 0: s + 1)
                                    }
                                    c(".pass-item-suggsetion_hover", o.suggestionDom).removeClass("pass-item-suggsetion_hover");
                                    c(t[q], o.suggestionDom).addClass("pass-item-suggsetion_hover");
                                    var x = c(t[q]).attr("data-select");
                                    if (c(t[q]).attr("data-type") == "history") {
                                        F.value = x
                                    } else {
                                        F.value = F.value.substr(0, F.value.indexOf("@")) + x.substr(x.indexOf("@"))
                                    }
                                    if (o.getElement("userName_placeholder")) {
                                        o.$hide("userName_placeholder")
                                    }
                                }
                            }
                        }
                    }
                });
                r.on("keydown", 
                function(s) {
                    if ((s.keyCode == 13 || s.keyCode == 9) && (o.suggestionDom && o.suggestionDom.style.display != "none")) {
                        o.changeSuggestView({
                            list: "hide",
                            btn: "close"
                        });
                        o._doFocus("password");
                        s.preventDefault();
                        s.stopPropagation()
                    }
                });
                r.on("blur", 
                function() {
                    if (o.config.isPhone == false) {
                        setTimeout(function() {
                            if (o.suggestionDom && !o.suggestionDom.data_delete) {
                                o.changeSuggestView({
                                    list: "hide",
                                    btn: "close"
                                })
                            }
                        },
                        150)
                    }
                });
                r.on("focus", 
                function(s) {
                    if (o.config.isPhone == false) {
                        o.changeSuggestView({
                            list: "show",
                            btn: "open"
                        })
                    }
                })
            },
            _operateTips: function() {
                var k = this,
                i = c("#" + k.$getId("userName"), k.getElement());
                if (k.config.isPhone == false) {
                    if (!k.operateTipsDom) {
                        k.operateTipsDom = document.createElement("div");
                        k.operateTipsDom.id = k.$getId("operateTipsWrapper");
                        i.parent().parent().append(k.operateTipsDom);
                        c(k.operateTipsDom).addClass("pass-operate-tips");
                        var j = '<span class="pass-operate-content">\u90ae\u7bb1\u767b\u5f55\u66f4\u5b89\u5168\u54e6</span><span class="pass-operate-down"><em class="pass-operate-down-a">鈼�</em><em class="pass-operate-down-b">鈼�</em></span>';
                        k.operateTipsDom.innerHTML = j
                    } else {
                        k.$show(k.operateTipsDom)
                    }
                }
                i.on("focus", 
                function() {
                    if ((k.config.isPhone == false) && k.operateTipsDom) {
                        k.$show(k.operateTipsDom)
                    }
                });
                i.on("blur", 
                function() {
                    if (k.operateTipsDom) {
                        k.$hide(k.operateTipsDom)
                    }
                })
            },
            _rendPhoenixbtn: function() {
                var i = this,
                k = c(i.getPhoenixElement("pass_phoenix_list_login")),
                j = c(".pass-phoenix-show", i.getPhoenixElement("pass_phoenix_list_login"));
                if (j) {
                    j.on("click", 
                    function() {
                        if (k.hasClass("pass-phoenix-list-hover")) {
                            k.removeClass("pass-phoenix-list-hover")
                        } else {
                            k.addClass("pass-phoenix-list-hover")
                        }
                    })
                }
                k.on("click", 
                function(m) {
                    var l = c(m.target),
                    n = l.attr("data-type");
                    if (n) {
                        i._changeLoginType(n)
                    }
                })
            },
            render: function(p) {
                var m = this;
                if (!m.getElement()) {
                    m.$mappingDom("", p || document.body)
                }
                var n = c(m.getElement());
                n.addClass(m.constant.CONTAINER_CLASS);
                var l = m._getTemplate();
                n.get(0).appendChild(c(l).get(0));
                if (m.config.authsiteLogin || m.config.qrcode || m.config.sms) {
                    if (m.config.sms != 3) {
                        var o = m._getTemplateOther();
                        m._insertAfterW(c(o).get(0), n.get(0));
                        m._rendPhoenixbtn()
                    }
                }
                if (m.config.authsiteLogin) {
                    m._authSiteW()
                }
                if (m.config.qrcode) {
                    var j = m._getTemplateQrcode();
                    m._insertAfterW(c(j).get(0), n.get(0));
                    m._setEventQrcode()
                }
                if (m.config.sms) {
                    var j = m._getTemplateSms();
                    m._insertAfterW(c(j).get(0), n.get(0));
                    m._setEventSms()
                }
                if (m.config.loginMerge) {
                    setTimeout(function() {
                        m.getElement("loginMerge").value = "true"
                    },
                    200)
                }
                if (m.config.hasPlaceholder) {
                    var i = [{
                        label: "userName",
                        placeholder: "userName"
                    },
                    {
                        label: "password",
                        placeholder: "password"
                    },
                    {
                        label: "verifyCode",
                        placeholder: "verifyCode",
                        clearbtn: 0
                    }];
                    if (m.config.sms) {
                        i.push({
                            label: "smsPhone",
                            placeholder: (m.config && m.config.diaPassLogin) ? "smsPhoneMsg": "smsPhone"
                        });
                        i.push({
                            label: "smsVerifyCode",
                            placeholder: "smsVerifyCode"
                        })
                    }
                    m._getPlaceholder(i)
                }
                var k = m.fireEvent("render");
                if (!k) {
                    return
                }
                m._setValidator();
                m._setEvent();
                m._checkCapsLock()
            },
            _initApi: function(j) {
                var i = this;
                i.initialized = true;
                i.initTime = new Date().getTime();
                h.data.getApiInfo({
                    apiType: "login",
                    loginType: (i.config && i.config.diaPassLogin) ? "dialogLogin": "basicLogin"
                }).success(function(k) {
                    var m = i.fireEvent("getApiInfo", {
                        rsp: k
                    });
                    if (!m) {
                        return
                    }
                    if (k.data.disable == 1) {
                        i.setGeneralError(i.lang.sysUpdate)
                    }
                    if (k.errInfo.no == 0) {
                        var l = k.data.token;
                        i.bdPsWtoken = k.data.token;
                        i.loginrecord = {};
                        if (i.config.loginMerge && i.config.autosuggest) {
                            h.data.getLoginHistory({
                                token: i.bdPsWtoken
                            }).success(function(n) {
                                i.loginrecord = n.data;
                                i._suggestion(i.config.diaPassLogin ? 20: 12);
                                if (i.config.memberPass && i.loginrecord.displayname.length > 0) {
                                    i._doFocus("password");
                                    if (i.getElement("userName_placeholder")) {
                                        i.$hide("userName_placeholder")
                                    }
                                    if (!i.getElement("userName").value || i.getElement("userName").value == "") {
                                        i.getElement("userName").value = i.loginrecord.displayname[0]
                                    }
                                    i.$show("userName_clearbtn").$hide("userName_placeholder")
                                }
                            })
                        } else {
                            if (!i.config.isPhone && i.config.memberPass && !i.constant.SUBMITFLAG) {
                                i.getElement("userName").value = k.data.rememberedUserName
                            }
                        }
                        i.disUnameLogin = 0;
                        i.ifShowWarning = k.data.ifShowWarning;
                        if (k.data.spLogin && i.config.diaPassLogin) {
                            i.spLogin = k.data.spLogin
                        }
                        h.data.setContext({
                            token: l
                        });
                        if (!navigator.cookieEnabled) {
                            i.setGeneralError(i.lang.cookieDisable)
                        }
                        if (!i.config.loginMerge && i.config.memberPass && k.data.rememberedUserName && !i.config.isPhone) {
                            if (i.config.diaPassLogin && k.data.usernametype == 3) {
                                i.switchTo("phone")
                            }
                        }
                        if (i.config.diaPassLogin && !i.config.loginMerge) {
                            i._operateTips()
                        }
                        if (i.constant.SUBMITFLAG) {
                            i.getElement("submit").click()
                        } else {
                            j && j.success(k)
                        }
                    }
                })
            },
            submitForm: function() {
                var i = this;
                i.constant.SUBMITFLAG = true
            },
            switchTo: function(j) {
                var k = this,
                i = "";
                if (k.config.loginMerge) {
                    return
                }
                k._collectData();
                setTimeout(function() {
                    k.setGeneralError("");
                    if (k.selectBtn && j == "phone") {
                        k.$hide(k.selectBtn)
                    } else {
                        if (k.selectBtn) {
                            k.$hide(k.selectBtn)
                        }
                    }
                },
                100);
                k.getElement("userNameLabel").innerHTML = j == "phone" ? k.lang.phoneNum: k.lang.account;
                k.config.isPhone = j == "phone" ? true: false;
                k.getElement("isPhone").value = k.config.isPhone;
                k._restoreData();
                k._setValidator();
                if (k.suggestionDom) {
                    k.$hide(k.suggestionDom)
                }
            },
            getCurrentType: function() {
                return this.config.isPhone ? "phone": "normal"
            },
            _setValidator: function() {
                var i = this;
                if (!i.validatorInited) {
                    i._validator.addRule("unameMailLength", 
                    function(k) {
                        var j = String(k.value);
                        if (/^[0-9a-zA-Z\.\_-]+@([0-9a-zA-Z-]+\.)+[a-z]{2,4}$/.test(j)) {
                            return j.length <= 60
                        }
                        return true
                    });
                    i._validator.addMsg("unameMailLength", i.lang.unameMailLengthError);
                    i._validator.addRule("unameInputLogin", 
                    function(k) {
                        var j = String(k.value);
                        if (i.disUnameLogin === 0 && i.config.diaPassLogin && !i.config.isPhone) {
                            if (!/^[0-9a-zA-Z\.\_-]+@([0-9a-zA-Z-]+\.)+[a-z]{2,4}$/.test(j)) {
                                return false
                            }
                        }
                        return true
                    });
                    i._validator.addMsg("unameInputLogin", i.lang.unameInputError);
                    i._validator.addRule("checkVcodeLength", 
                    function(l, j) {
                        var k = l.value;
                        if (k.length != 4 || !i.constant.CHECKVERIFYCODE) {
                            i.$hide("verifyCodeSuccess");
                            return false
                        }
                        return true
                    });
                    i._validator.addMsg("checkVcodeLength", i.lang.verifyCodeLenErr);
                    i._validator.addRule("checkVcodeStatus", 
                    function(k, j) {
                        if (j == "all") {
                            if (!i.constant.CHECKVERIFYCODE) {
                                return false
                            }
                        }
                        return true
                    });
                    i._validator.addMsg("checkVcodeStatus", i.lang.verifyCodeStaErr)
                }
                i.validatorInited = true;
                i.validateRules = {
                    userName: {
                        rules: i.config.loginMerge ? ["required"] : (i.config.isPhone ? ["required", "phone"] : ["required", "unameMailLength", "unameInputLogin"]),
                        desc: i.config.loginMerge ? i.lang.userName: (i.config.isPhone ? i.lang.phoneNum: i.lang.account)
                    },
                    password: {
                        rules: ["required"],
                        desc: i.lang.password
                    },
                    verifyCode: {
                        rules: ["required", "checkVcodeLength", "checkVcodeStatus"],
                        desc: i.lang.captcha
                    }
                };
                i._validator.init(i, i.validateRules)
            },
            _validateError: function(m, j) {
                var k = this,
                l = c(k.getElement(m.field));
                if (k.getElement("operateTipsWrapper") && k.getElement("operateTipsWrapper").style.display != "none") {
                    k.$hide("operateTipsWrapper")
                }
                l.addClass(k.constant.ERROR_CLASS);
                k.setGeneralError(m.msg);
                if (k.disUnameLogin === 0 && m.field == "userName" && m.msg == k.lang.unameInputError) {
                    var i = new Image();
                    i.onload = i.onerror = function() {
                        i.onload = i.onerror = null;
                        i = null
                    };
                    i.src = k._domain.https + "/img/v.gif?cdnversion=00558676.gif?type=login&loginType=userName"
                }
                j && j.callback && callback()
            },
            _enableUnameLoginCallback: function(t, r) {
                var o = this;
                var u = c('<input type="hidden" name="userNameLogin" value="1">'),
                l = o.getElement("pass-pop-login-placeholder-normal"),
                i = o.getElement().parentNode,
                k = c(".tab li", i),
                s = c(".tab a", i).get(0),
                n = c(t);
                if (!t && !r) {
                    r = o.isLoginWeak === 1 ? "normal": "other";
                    n = o.eleLoginWeak
                }
                if (o.disUnameLogin == 0) {
                    o.disUnameLogin = 1;
                    o._validator.confStorage[o.$getId()].userName.desc = "\u7528\u6237\u540D";
                    u.get(0).value = "1";
                    if (l) {
                        l.innerHTML = "\u7528\u6237\u540D"
                    }
                    if (s) {
                        s.innerHTML = "\u7528\u6237\u540D\u767B\u5F55"
                    }
                    if (o.normalLogin) {
                        o.normalLogin.innerHTML = "\u7528\u6237\u540D\u767B\u5F55";
                        c(o.normalLogin).addClass("pass-normal-btn-s2");
                        if (o.normalLogin.style.display != "none") {
                            o._changeLoginType("normal")
                        }
                    }
                    if (o.config.isPhone == true) {
                        o.switchTo("normal");
                        k.removeClass("tab-selected");
                        c(k.get(0)).addClass("tab-selected")
                    }
                    if (o.config.isPhone == false) {
                        o.getElement("userNameLabel").innerHTML = "\u7528\u6237\u540D";
                        var j = o.getElement("error");
                        c(o.getElement("userName")).removeClass("pass-text-input-error");
                        if (o.operateTipsDom) {
                            o.$show(o.operateTipsDom)
                        }
                    } else {
                        if (o.operateTipsDom) {
                            o.$hide(o.operateTipsDom)
                        }
                    }
                    c(".tang-pass-pop-login-placeholder").hide();
                    if (!o.getElement("userName").value) {
                        o.$show("pass-pop-login-placeholder-" + (o.config.isPhone ? "phone": "normal"))
                    }
                    o.changeSuggestView({
                        list: "hide",
                        btn: "hide"
                    });
                    if (r == "normal") {
                        n.removeClass("pass-unamelogin-btn");
                        n.addClass("pass-emaillogin-btn");
                        n.get(0).innerHTML = "\u90AE\u7BB1\u767B\u5F55"
                    } else {
                        if (r == "other") {
                            n.get(0).innerHTML = '\u5fd8\u8bb0\u7528\u6237\u540d?\u4f7f\u7528<a href="###" id="pass-user-login" tabIndex="-1" data-click="other">\u90AE\u7BB1\u767B\u5F55</a>'
                        }
                    }
                    var m = new Image();
                    m.onload = m.onerror = function() {
                        m.onload = m.onerror = null;
                        m = null
                    };
                    m.src = o._domain.https + "/img/v.gif?cdnversion=00558676.gif?type=login&loginType=normalName"
                } else {
                    o.disUnameLogin = 0;
                    o._validator.confStorage[o.$getId()].userName.desc = "\u90AE\u7BB1";
                    u.get(0).value = "0";
                    if (l) {
                        l.innerHTML = "\u90AE\u7BB1"
                    }
                    if (s) {
                        s.innerHTML = "\u90AE\u7BB1\u767B\u5F55"
                    }
                    if (o.normalLogin) {
                        o.normalLogin.innerHTML = "\u90AE\u7BB1\u767B\u5F55";
                        c(o.normalLogin).removeClass("pass-normal-btn-s2");
                        if (o.normalLogin.style.display != "none") {
                            o._changeLoginType("normal")
                        }
                    }
                    if (o.config.isPhone == true) {
                        o.switchTo("normal");
                        k.removeClass("tab-selected");
                        c(k.get(0)).addClass("tab-selected")
                    }
                    if (o.config.isPhone == false) {
                        o.getElement("userNameLabel").innerHTML = "\u90AE\u7BB1";
                        var j = o.getElement("error");
                        c(o.getElement("userName")).removeClass("pass-text-input-error");
                        if (o.operateTipsDom) {
                            o.$show(o.operateTipsDom)
                        }
                    } else {
                        if (o.operateTipsDom) {
                            o.$hide(o.operateTipsDom)
                        }
                    }
                    c(".tang-pass-pop-login-placeholder").hide();
                    if (!o.getElement("userName").value) {
                        c(o.getElement("pass-pop-login-placeholder-" + (o.config.isPhone ? "phone": "normal"))).show()
                    }
                    o.changeSuggestView({
                        list: "hide"
                    });
                    if (o.selectBtn && o.loginrecord && o.loginrecord.email && o.loginrecord.email.length > 1) {
                        o.changeSuggestView({
                            btn: "show"
                        })
                    } else {
                        if (o.selectBtn) {
                            o.changeSuggestView({
                                btn: "hide"
                            })
                        }
                    }
                    if (r == "normal") {
                        n.addClass("pass-unamelogin-btn");
                        n.removeClass("pass-emaillogin-btn");
                        n.get(0).innerHTML = "\u7528\u6237\u540D\u767B\u5F55"
                    } else {
                        if (r == "other") {
                            n.get(0).innerHTML = '\u5fd8\u8bb0\u90ae\u7bb1?\u4f7f\u7528<a href="###" id="pass-user-login" tabIndex="-1" data-click="other">\u7528\u6237\u540D\u767B\u5F55</a>'
                        }
                    }
                }
            },
            _validateSuccess: function(l, i) {
                var j = this,
                k = c(j.getElement(l.field));
                j.clearGeneralError();
                k.removeClass(j.constant.ERROR_CLASS);
                i && i.callback && callback()
            },
            _defaultLoginErr: function(j) {
                var l = this;
                l.vcodetype = j.data.vcodetype;
                if (j.data.codeString) {
                    l.getVerifyCode(j.data.codeString);
                    l._clearInput("verifyCode")
                } else {
                    l.clearVerifyCode()
                }
                if (j.errInfo.no == 400401) {
                    if (l.getElement("choiceuser_article")) {
                        l.$show("choiceuser_article")
                    } else {
                        var k = l._getIrregularField("choiceuser");
                        l._insertAfterW(c(k).get(0), l.getElement());
                        l._setEventChoiceUser()
                    }
                    c(l.getElement()).hide()
                }
                if (j.errInfo.no == 257) {
                    c(l.getElement()).removeClass("tang-pass-login-hide");
                    l.$show(l.getElement()).$hide("choiceuser_article")
                }
                if (j.errInfo.no == 6 || j.errInfo.no == 257) {
                    l._clearInput("verifyCode")
                }
                if (j.errInfo.no == 4) {
                    l._clearInput("password");
                    if (j.data.resetpwd) {
                        var n = "";
                        switch (j.data.resetpwd) {
                        case "1":
                            n = "1" + l.lang.passwordResetIn;
                            break;
                        case "2":
                            n = "2" + l.lang.passwordResetIn;
                            break;
                        case "3":
                            n = "3" + l.lang.passwordResetIn;
                            break;
                        case "4":
                            n = "3" + l.lang.passwordResetOut;
                            break
                        }
                        if (n.length > 0) {
                            j.errInfo.msg = l._format(l.lang.passwordResetWarn, {
                                resetpwd: n
                            })
                        }
                    }
                }
                if (j.errInfo.msg && (j.errInfo.msg.indexOf("#{") >= 0)) {
                    if (j.errInfo.no == 110024) {
                        var i = l._domain.https + "/v2/?regnotify&action=resend&tpl=" + l.config.product + "&email=" + encodeURIComponent(j.data.mail) + "&u=" + encodeURIComponent(j.data.u);
                        j.errInfo.msg = l._format(j.errInfo.msg, {
                            gotourl: i
                        })
                    } else {
                        var m = l.getElement("userName").value;
                        j.errInfo.msg = l._format(j.errInfo.msg, {
                            urldata: "&account=" + m + "&tpl=" + l.config.product + "&u=" + l.config.u
                        })
                    }
                }
                if (j.errInfo.field) {
                    l.setValidateError(j.errInfo.field, j.errInfo.msg, j)
                } else {
                    l.setGeneralError(j.errInfo.msg, j)
                }
            },
            _asyncValidate: {
                checkVerifycode: function(l, j) {
                    var k = this,
                    m = k.getElement("verifyCode"),
                    i = k.getElement("codeString");
                    h.data.checkVerifycode({
                        verifycode: m.value,
                        codestring: i.value
                    }).success(function(n) {
                        var o = k.fireEvent("checkVerifycode", {
                            rsp: n
                        });
                        if (!o) {
                            return
                        }
                        if (n.errInfo.no == 0) {
                            l && l.success(n);
                            k.$show("verifyCodeSuccess");
                            k.constant.CHECKVERIFYCODE = true
                        } else {
                            if (n.errInfo.no == 500002 || n.errInfo.no == 500018) {
                                n.msg = n.errInfo.msg;
                                l && l.error(n);
                                k.$hide("verifyCodeSuccess");
                                k.constant.CHECKVERIFYCODE = false
                            } else {
                                l && l.success(n);
                                k.$hide("verifyCodeSuccess");
                                k.constant.CHECKVERIFYCODE = true
                            }
                        }
                    })
                }
            },
            _format: function(k, i) {
                k = String(k);
                var j = Array.prototype.slice.call(arguments, 1),
                l = Object.prototype.toString;
                if (j.length) {
                    j = j.length == 1 ? (i !== null && (/\[object Array\]|\[object Object\]/.test(l.call(i))) ? i: j) : j;
                    return k.replace(/#\{(.+?)\}/g, 
                    function(m, o) {
                        var n = j[o];
                        if ("[object Function]" == l.call(n)) {
                            n = n(o)
                        }
                        return ("undefined" == typeof n ? "": n)
                    })
                }
                return k
            },
            submit: function() {
                var i = this;
                i.validateAll({
                    success: function() {
                        i._doFocus("submit");
                        i.submitStatus = 1;
                        var n = i.fireEvent("beforeSubmit");
                        if (!n) {
                            return
                        }
                        if (i.spLogin) {
                            var m = c('<input type="hidden" name="splogin" value="' + i.spLogin + '">');
                            i.getElement("hiddenFields").appendChild(m.get(0));
                            i.spLogin = null
                        }
                        var o = c.form.json(i.getElement("form"));
                        if (i.RSA && i.rsakey) {
                            var j = i.getElement("password").value;
                            if (j.length < 128 && !i.config.safeFlag) {
                                o.password = c.url.escapeSymbol(i.RSA.encrypt(j));
                                o.rsakey = i.rsakey;
                                o.crypttype = 12
                            }
                        }
                        var l = i.getElement("submit"),
                        p = 15000,
                        k;
                        l.value = i.lang.logining;
                        k = setTimeout(function() {
                            if (i.submitStatus === 1) {
                                i.setGeneralError(i.lang.submitTimeup)
                            }
                            l.value = i.lang.login
                        },
                        p);
                        o.timeSpan = new Date().getTime() - i.initTime;
                        h.data.login(o).success(function(r) {
                            i.submitStatus = 2;
                            if (r.errInfo.no == 0) {
                                var s = i.fireEvent("loginSuccess", {
                                    rsp: r
                                });
                                if (!s) {
                                    return
                                }
                                if (window.location) {
                                    window.location.href = r.data.u
                                } else {
                                    document.location.href = r.data.u
                                }
                            } else {
                                l.value = i.lang.login;
                                var s = i.fireEvent("loginError", {
                                    rsp: r
                                });
                                if (!s) {
                                    return
                                }
                                i._defaultLoginErr(r)
                            }
                        })
                    }
                },
                true)
            },
            _eventHandler: (function() {
                var j,
                i = {
                    focus: function(n, m) {
                        var l = j.fireEvent("fieldFocus", {
                            ele: this
                        });
                        if (!l) {
                            return
                        }
                        this.addClass(j.constant.FOCUS_CLASS);
                        this.removeClass(j.constant.ERROR_CLASS);
                        c(j.getElement(n + "Label")).addClass(j.constant.LABEL_FOCUS_CLASS)
                    },
                    blur: function(n, m) {
                        var l = j.fireEvent("fieldBlur", {
                            ele: this
                        });
                        if (!l) {
                            return
                        }
                        this.removeClass(j.constant.FOCUS_CLASS);
                        c(j.getElement(n + "Label")).removeClass(j.constant.LABEL_FOCUS_CLASS)
                    },
                    mouseover: function(n, m) {
                        var l = j.fireEvent("fieldMouseover", {
                            ele: this
                        });
                        if (!l) {
                            return
                        }
                        this.addClass(j.constant.HOVER_CLASS)
                    },
                    mouseout: function(n, m) {
                        var l = j.fireEvent("fieldMouseout", {
                            ele: this
                        });
                        if (!l) {
                            return
                        }
                        this.removeClass(j.constant.HOVER_CLASS)
                    },
                    keyup: function(n, m) {
                        var l = j.fireEvent("fieldKeyup", {
                            ele: this
                        });
                        if (!l) {
                            return
                        }
                    }
                },
                k = {
                    focus: {
                        userName: function() {
                            if (j.config.loginMerge && j.getElement("loginMerge")) {
                                j.getElement("loginMerge").value = "true";
                                j.getElement("isPhone").value = ""
                            }
                        },
                        password: function(m, l) {
                            j._getRSA(function(n) {
                                j.RSA = n.RSA;
                                j.rsakey = n.rsakey
                            })
                        },
                        verifyCode: function() {}
                    },
                    blur: {
                        userName: function(m, l) {},
                        password: function(n, m) {
                            var l = this.get(0).value;
                            if (l.length) {
                                j.validate(n)
                            }
                        },
                        verifyCode: function(n, m) {
                            var l = this.get(0).value;
                            if (l.length) {
                                j.validate(n)
                            }
                        }
                    },
                    change: {
                        userName: function(n, m) {
                            var l = this.get(0).value;
                            if (l.length) {
                                j.validate(n, {
                                    success: function() {
                                        h.data.loginCheck({
                                            userName: l,
                                            isPhone: j.config.isPhone
                                        }).success(function(o) {
                                            if (o.data.codeString.length) {
                                                j.getVerifyCode(o.data.codeString);
                                                j.vcodetype = o.data.vcodetype
                                            } else {
                                                j.clearVerifyCode()
                                            }
                                        })
                                    }
                                })
                            }
                        },
                        verifyCode: function(m, l) {}
                    },
                    click: {
                        verifyCodeChange: function(m, l) {
                            j.getElement("verifyCode").value = "";
                            j._doFocus("verifyCode");
                            j.getVerifyCode();
                            l.preventDefault()
                        }
                    },
                    keyup: {
                        verifyCode: function(n, m) {
                            var l = j.getElement("verifyCode"),
                            o = c(l);
                            if (l.value && l.value.length == 4) {
                                j._asyncValidate.checkVerifycode.call(j, {
                                    error: function(p) {
                                        o.addClass(j.constant.ERROR_CLASS);
                                        j.setGeneralError(p.msg)
                                    },
                                    success: function() {
                                        o.removeClass(j.constant.ERROR_CLASS);
                                        j.clearGeneralError()
                                    }
                                })
                            } else {
                                j.$hide("verifyCodeSuccess")
                            }
                        }
                    },
                    submit: function(l) {
                        j.submit();
                        l.preventDefault()
                    }
                };
                return {
                    entrance: function(o) {
                        j = this;
                        var n = c(o.target),
                        m = o.target.name;
                        if (!m && o.target.id) {
                            var l = o.target.id.match(/\d+__(.*)$/);
                            if (l) {
                                m = l[1]
                            }
                        }
                        if (!m) {
                            return
                        }
                        if (i.hasOwnProperty(o.type)) {
                            i[o.type].apply(c(o.target), [m, o])
                        }
                        if (k.hasOwnProperty(o.type)) {
                            if (typeof k[o.type] == "function") {
                                k[o.type].apply(c(o.target), [o])
                            }
                            if (k[o.type].hasOwnProperty(m)) {
                                k[o.type][m].apply(c(o.target), [m, o])
                            }
                        }
                        if (!j.initialized && o.type == "focus") {
                            j._initApi()
                        }
                    }
                }
            })(),
            $dispose: function() {
                var i = this;
                if (i.disposed) {
                    return
                }
                c.dom(i.getElement()).removeClass(i.constant.CONTAINER_CLASS);
                i.getElement().removeChild(i.getElement("form"));
                g.Base.prototype.$dispose.call(i)
            }
        });
        return g
    });